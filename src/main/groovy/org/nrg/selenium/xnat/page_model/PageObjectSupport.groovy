package org.nrg.selenium.xnat.page_model

import org.nrg.selenium.extension.WebDriverExtension
import org.nrg.selenium.xnat.XnatDriver
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators

interface PageObjectSupport {

    XnatDriver getXnatDriver()

    WebDriverExtension getDriverExtension()

    XnatLocators getLocators()

    def <X extends PageObjectSupport> X loadPage(Class<X> pageClass, boolean takeAllScreenshots, boolean suppressLoadablePageNavigation)

    def <X extends PageObjectSupport> X getInstance(Class<X> pageClass)

}