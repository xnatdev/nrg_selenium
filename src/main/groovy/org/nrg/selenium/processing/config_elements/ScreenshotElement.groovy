package org.nrg.selenium.processing.config_elements

import org.nrg.selenium.xnat.XnatDriver
import org.openqa.selenium.By

class ScreenshotElement implements PipelineConfigElement {

    private By by

    ScreenshotElement(By by) {
        this.by = by
    }

    @Override
    void performElementAction(XnatDriver xnatDriver) {
        if (by != null) {
            xnatDriver.waitForElement(by)
        }
        xnatDriver.uploadTargetedScreenshot()
    }

}
