package org.nrg.selenium.xnat.page_model.report

import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.resource_settings.ProjectResourceSettingsUpload
import org.nrg.xnat.pogo.Extensible
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertTrue

class CustomResourceUploadDialog<T extends Extensible<T>, X extends XnatReportPage<T, X>> implements OverlayingPage<CustomResourceUploadDialog, XnatReportPage>, XnatPageComponent, ScreenshotableComponent<CustomResourceUploadDialog> {

    protected By resourceTitleLocator = By.id('whatToDoSelect')
    protected By extractFilesLocator = By.id('extractRequestBox')
    protected By fileUploadLocator = By.id('file-upload-input')
    protected By fileUploadCompleteLocator = XnatLocators.tagContainingText('span', 'Upload complete')
    protected By doneButtonLocator = By.id('xmodal-abu-done-button')

    @Override
    List<By> getComponentLocators() {
        [By.id('xmodal-abu')]
    }

    @Override
    void performNavigationToUnderlyingPage() {
        exceptionSafeClick(doneButtonLocator)
    }

    CustomResourceUploadDialog<T, X> selectResourceTitle(String title) {
        exceptionSafeSelect(resourceTitleLocator, title)
        this
    }

    CustomResourceUploadDialog<T, X> markExtractFiles(boolean state) {
        performCheckBox(extractFilesLocator, state)
        this
    }

    CustomResourceUploadDialog<T, X> uploadFile(File file) {
        specifyFile(fileUploadLocator, file)
        this
    }

    CustomResourceUploadDialog<T, X> handleOverwriteCheck(boolean checkPresent) {
        if (checkPresent) {
            assertTrue(closeAlertAndGetItsText().contains('Do you want to overwrite existing files?'))
        }
        this
    }

    XnatReportPage<T, X> uploadToProjectResourceSettings(ProjectResourceSettingsUpload upload) {
        selectResourceTitle(upload.title)
        upload.extractCompressedFiles.ifPresent({ value -> markExtractFiles(value) })
        uploadFile(upload.file)
        upload.overwriteAllowed.ifPresent({ value -> handleOverwriteCheck(value) })
        waitForElement(fileUploadCompleteLocator)
        capture()
        returnToUnderlyingPage()
    }

}
