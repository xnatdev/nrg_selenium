package org.nrg.selenium.xnat.page_model.admin

import org.nrg.selenium.xnat.page_model.LoadablePage
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.SimpleTabbedPage

class PluginSettingsPage implements NavigablePageObject<PluginSettingsPage>, LoadablePage, SimpleTabbedPage<PluginSettingsPage> {

    @Override
    void navigateTo() {
        topNav('Administer', 'Plugin Settings')
    }

}
