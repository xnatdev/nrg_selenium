package org.nrg.selenium.xnat.page_model.users

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.xnat.pogo.users.User
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertEquals

class UserSelfServicePage implements NavigablePageObject<UserSelfServicePage>, ScreenshotableComponent<UserSelfServicePage> {

    User currentUser
    protected XpathLocator baseButtonLocator = new XpathLocator("//input[@name='eventSubmit_doSetup']")
    protected By changeEmailLocator = By.id('new_email')
    protected By changeEmailConfirmationLocator = By.id('confirm_email')
    protected By currentPasswordLocator = By.id('current_password')
    protected By changePasswordLocator = By.id('new_password')
    protected By changePasswordConfirmationLocator = By.id('confirm_password')
    protected XpathLocator changeEmailButtonLocator = baseButtonLocator.nthInstance(1)
    protected XpathLocator changePasswordButtonLocator = baseButtonLocator.nthInstance(2)

    @Override
    List<By> getComponentLocators() {
        [By.id('layout_content')]
    }

    UserSelfServicePage fillEmailField(String email) {
        fill(changeEmailLocator, email)
        this
    }

    UserSelfServicePage fillEmailConfirmationField(String email) {
        fill(changeEmailConfirmationLocator, email)
        this
    }

    UserSelfServicePage clickChangeEmailButton() {
        exceptionSafeClick(changeEmailButtonLocator)
        this
    }

    UserSelfServicePage fillCurrentPasswordField(String currentPassword) {
        fill(currentPasswordLocator, currentPassword)
        this
    }

    UserSelfServicePage fillNewPasswordField(String newPassword) {
        fill(changePasswordLocator, newPassword)
        this
    }

    UserSelfServicePage fillNewPasswordConfirmationField(String newPassword) {
        fill(changePasswordConfirmationLocator, newPassword)
        this
    }

    UserSelfServicePage clickChangePasswordButton() {
        exceptionSafeClick(changePasswordButtonLocator)
        this
    }

    // TODO: make this work for non-admins on recent XNATs
    UserSelfServicePage changeEmail(String newEmail) {
        fillEmailField(newEmail)
        fillEmailConfirmationField(newEmail)
        uploadScreenshot()
        clickChangeEmailButton()
        readSuccess().assertTextEquals('Email address changed.').uploadScreenshot()
        currentUser.email(newEmail)
        this
    }

    UserSelfServicePage changePassword(String newPassword) {
        fillCurrentPasswordField(currentUser.password)
        fillNewPasswordField(newPassword)
        fillNewPasswordConfirmationField(newPassword)
        uploadScreenshot()
        clickChangePasswordButton()
        readSuccess().assertTextEquals('Password changed.').capture()
        currentUser.password(newPassword)
        this
    }

}
