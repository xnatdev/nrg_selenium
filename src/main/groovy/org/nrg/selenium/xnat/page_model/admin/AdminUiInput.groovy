package org.nrg.selenium.xnat.page_model.admin

import org.openqa.selenium.By

class AdminUiInput implements AdminUiElement {

    String value

    AdminUiInput(By by, String value, By screenshot = null, boolean takeScreenshot = true) {
        setLocator(by)
        setValue(value)
        setScreenshotLocator(screenshot)
        setTakeScreenshot(takeScreenshot)
    }

    @Override
    void performElementAction() {
        fill(locator, value)
    }

}
