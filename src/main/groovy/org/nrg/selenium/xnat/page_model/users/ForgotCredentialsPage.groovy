package org.nrg.selenium.xnat.page_model.users

import org.nrg.selenium.exceptions.CredentialEmailLimitException
import org.nrg.selenium.exceptions.UnknownInterpretableValueException
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatLoginPage
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By

class ForgotCredentialsPage implements XnatPageComponent, ScreenshotableComponent<ForgotCredentialsPage> {

    protected String requestLimitExceededMessage = 'You have exceeded the allowed number of email requests. Please try again later.'
    protected String requestUsernameSuccessMessage = 'The corresponding username for this email address has been emailed to your account.'
    protected String requestPasswordSuccessMessage = 'You have been sent an email with a link to reset your password. Please check your email.'
    protected XpathLocator forgotUsernameFormLocator = XnatLocators.byId('register_form')
    protected XpathLocator forgotPasswordFormLocator = XnatLocators.byId('login_form')
    protected XpathLocator buttonSublocator = new XpathLocator('.//input[@type="submit"]')
    protected By forgotUsernameFieldLocator = By.name('email')
    protected By submitForgotUsernameButtonLocator = forgotUsernameFormLocator.joinSublocator(buttonSublocator)
    protected By forgotPasswordFieldLocator = By.name('username')
    protected By submitForgotPasswordButtonLocoator = forgotPasswordFormLocator.joinSublocator(buttonSublocator)

    @Override
    List<By> getComponentLocators() {
        [forgotUsernameFormLocator, forgotPasswordFormLocator]
    }

    XnatLoginPage requestUsername(String email) throws CredentialEmailLimitException {
        fill(forgotUsernameFieldLocator, email)
        uploadScreenshot()
        exceptionSafeClick(submitForgotUsernameButtonLocator)
        processRequest(requestUsernameSuccessMessage)
    }

    XnatLoginPage requestPassword(String username) throws CredentialEmailLimitException {
        fill(forgotPasswordFieldLocator, username)
        uploadScreenshot()
        exceptionSafeClick(submitForgotPasswordButtonLocoator)
        processRequest(requestPasswordSuccessMessage)
    }

    protected XnatLoginPage processRequest(String expectedMessage) {
        final XnatLoginPage loginPage = loadPage(XnatLoginPage, false, true)
        final String listedMessage = loginPage.loginMessage.text
        switch (listedMessage) {
            case expectedMessage :
                return loginPage.capture()
            case requestLimitExceededMessage :
                throw new CredentialEmailLimitException()
            default :
                throw new UnknownInterpretableValueException("Unknown message on login page: ${listedMessage}")
        }
    }

}
