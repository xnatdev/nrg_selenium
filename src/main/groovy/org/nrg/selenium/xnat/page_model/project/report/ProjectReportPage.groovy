package org.nrg.selenium.xnat.page_model.project.report

import org.nrg.jira.components.zephyr.TestStatus
import org.nrg.selenium.xnat.page_model.IndexPage
import org.nrg.selenium.xnat.page_model.XnatDeleteComponent
import org.nrg.selenium.xnat.page_model.download.DataDownloadPage
import org.nrg.selenium.xnat.page_model.project.delete.ProjectDeletePage
import org.nrg.selenium.xnat.page_model.report.XnatReportPage
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.prearchive.XnatPrearchive
import org.nrg.selenium.xnat.page_model.project.edit.ProjectEditPage
import org.nrg.selenium.xnat.page_model.subject.report.SubjectReportPage
import org.nrg.selenium.xnat.page_model.ui_element.YuiTable
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.users.UserGroup
import org.nrg.xnat.pogo.users.UserGroups
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertEquals

class ProjectReportPage implements XnatReportPage<Project, ProjectReportPage>, ProjectReportBase {

    protected By titleLocator = By.xpath("//*[@class='edit_header1']")
    protected By summaryModule = By.id('project_summary_module')
    protected By uploadLocator = By.linkText('Upload Images')
    protected By securityWarningLocator = By.xpath('//div[@class="error"]//strong')
    protected By securityWarningTextLocator = By.xpath("//div[@class='error']/p[contains(text(),'account has not been granted')]")
    protected By securityWarningPageLocator = By.id('project-page')
    protected By requestAccessButtonLocator = By.partialLinkText('Request Access')
    protected By tabWrangerDropdownLocator = By.id('search_selector')
    protected By projectPrearchiveLocator = By.linkText('View Prearchive')

    @Override
    ProjectEditPage loadEditPage() {
        loadDetailsTab().loadEditPage()
    }

    @Override
    void performInitialValidation() {
        loadDetailsTab().performInitialValidation()
        waitForProjectLoad()
    }

    @Override
    void validateObject(Project project) {
        assertEquals(project.title, projectTitle)
        loadDetailsTab().assertPageRepresents(project)
        // TODO: other fields
        captureStep(stepCounter.intValue(), TestStatus.PASS, "Project Title = ${project.title}, Running Title = ${project.runningTitle}, ID = ${project.id}", true, componentLocators) // TODO: clean up
    }

    @Override
    List<By> getComponentLocators() {
        [titleLocator, summaryModule] + readActionsBox().componentLocators
    }

    @Override
    ProjectDeletePage loadDeleteComponent() {
        loadDetailsTab().loadDeletePage()
    }

    ProjectReportPage waitForProjectLoad() {
        waitForAsyncCalls()
        if (!isElementPresentQuick(securityWarningLocator)) {
            loadProjectDataTable()
        }
        this
    }

    ProjectReportPage selectProjectDataTab(String tabName) {
        if (isElementPresentQuick(locators.tab(tabName))) {
            exceptionSafeClick(locators.tab(tabName))
        } else {
            exceptionSafeSelect(tabWrangerDropdownLocator, tabName)
        }
        this
    }

    SubjectReportPage clickSubjectLink(String subjectIdentifier) {
        loadProjectDataTable().clickSubjectLink(subjectIdentifier)
    }

    String getProjectTitle() {
        exceptionSafeGetText(titleLocator)
    }

    String getProjectIdString() {
        loadDetailsTab().projectIdString
    }

    String getProjectDescription() {
        loadDetailsTab().description
    }

    String getProjectKeywords() {
        loadDetailsTab().keywords
    }

    ProjectReportPage assertPageRepresentsOwnerGroup() {
        assertTabsPresent([ProjectDetailsTab, ProjectAccessTab, ProjectManageTab])
        capture()
    }

    ProjectReportPage assertPageRepresentsMemberGroup() {
        assertTabsPresent([ProjectDetailsTab])
        assertTabsNotPresent([ProjectAccessTab, ProjectManageTab])
        assertElementPresent(uploadLocator)
        capture()
    }

    ProjectReportPage assertPageRepresentsCollaboratorGroup() {
        assertTabsPresent([ProjectDetailsTab])
        assertTabsNotPresent([ProjectAccessTab, ProjectManageTab])
        assertElementNotPresent(uploadLocator)
        capture()
    }

    ProjectReportPage assertTabsPresent(List<Class<? extends ProjectReportTab>> tabs) {
        tabs.each { tab ->
            assertElementPresent(getInstance(tab).tabLocator())
        }
        this
    }

    ProjectReportPage assertTabsNotPresent(List<Class<? extends ProjectReportTab>> tabs) {
        tabs.each { tab ->
            assertElementNotPresent(getInstance(tab).tabLocator())
        }
        this
    }

    ProjectReportPage assertPageRepresentsGroup(UserGroup group) {
        switch (group) {
            case UserGroups.OWNER :
                assertPageRepresentsOwnerGroup()
                break
            case UserGroups.MEMBER :
                assertPageRepresentsMemberGroup()
                break
            case UserGroups.COLLABORATOR :
                assertPageRepresentsCollaboratorGroup()
                break
            default :
                throw new IllegalArgumentException('Custom user groups do not have a standard permission check.')
        }
        this
    }

    ProjectReportPage assertProtectedProjectWarning(Project project) {
        assertElementText('Security Warning', securityWarningLocator)
        assertElementText("Your account has not been granted access to this project's data. If you would like to view this data, you will need to request access. Request Access", securityWarningTextLocator)
        loadPage(ProjectDetailsTab).assertPageRepresents(project)
        captureStep(securityWarningPageLocator)
        this
    }

    RequestProjectAccessPage clickRequestAccessButton() {
        exceptionSafeClick(requestAccessButtonLocator)
        loadPage(RequestProjectAccessPage)
    }

    XnatPrearchive loadProjectPrearchive() {
        exceptionSafeClick(projectPrearchiveLocator)
        loadPage(XnatPrearchive)
    }

    DataDownloadPage clickDownloadImages() {
        actionsBoxNavigation(['Download Images'])
        loadPage(DataDownloadPage)
    }

    YuiTable loadProjectDataTable() {
        loadPage(YuiTable)
    }

}
