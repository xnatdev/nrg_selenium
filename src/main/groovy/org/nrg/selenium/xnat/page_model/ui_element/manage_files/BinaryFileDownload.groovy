package org.nrg.selenium.xnat.page_model.ui_element.manage_files

import org.nrg.selenium.util.BrowserDownloadManager

class BinaryFileDownload implements ResourceFileRender {

    File downloadedFile

    protected BinaryFileDownload locateFile(BrowserDownloadManager downloadManager) {
        downloadedFile = downloadManager.findDownload()
        this
    }

}
