package org.nrg.selenium.xnat.page_model

import org.nrg.xnat.pogo.users.User
import org.openqa.selenium.By

/**
 * When logging out of XNAT, you will end up on either the login page or index page depending on if guest is enabled.
 * This class provides an object for that.
 */
class XnatLogoutPage implements XnatPageComponent {

    protected By logoutSuccessfulLocator = By.xpath("//span[@id='user_info']//span[text()='Guest'] | //button[@id='loginButton']")

    XnatLogoutPage waitForLogout() {
        waitForElement(logoutSuccessfulLocator) // redirected to Login.vm if Guest is off, redirected to Index.vm if it isn't
        this
    }

    IndexPage asIndexPage() {
        loadPage(IndexPage)
    }

    XnatLoginPage asLoginPage() {
        loadPage(XnatLoginPage)
    }

    IndexPage seleniumLogin() {
        loadLoginPage().seleniumLogin()
    }

    IndexPage seleniumAdminLogin() {
        loadLoginPage().seleniumAdminLogin()
    }

    IndexPage login(User user) {
        loadLoginPage().login(user)
    }

}
