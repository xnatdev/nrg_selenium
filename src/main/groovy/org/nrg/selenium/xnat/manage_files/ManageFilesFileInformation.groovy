package org.nrg.selenium.xnat.manage_files

class ManageFilesFileInformation {

    String rename
    String format
    String content
    String tags
    boolean specifyOverwrite = false
    boolean extract = false

    ManageFilesFileInformation rename(String rename) {
        setRename(rename)
        this
    }

    ManageFilesFileInformation format(String format) {
        setFormat(format)
        this
    }

    ManageFilesFileInformation content(String content) {
        setContent(content)
        this
    }

    ManageFilesFileInformation tags(String tags) {
        setTags(tags)
        this
    }

    ManageFilesFileInformation overwrite(boolean overwrite) {
        setSpecifyOverwrite(overwrite)
        this
    }

    ManageFilesFileInformation extract(boolean extract) {
        setExtract(extract)
        this
    }

}
