package org.nrg.selenium.xnat.page_model.ui_element

import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By

class YuiSendEmailDialog implements XnatPageComponent, ScreenshotableComponent<YuiSendEmailDialog>, OverlayingPage<YuiSendEmailDialog, YuiTable> {

    protected By mainDialogLocator = By.id('all_users_popup') // don't ask me
    protected By recipientLocator = By.id('email_toAddresses')
    protected By subjectLocator = By.id('email_subject')
    protected By bodyLocator = By.id('email_message')
    protected By cancelButtonLocator = XnatLocators.buttonWithText('Cancel')
    protected By submitButtonLocator = XnatLocators.buttonWithText('Submit')

    @Override
    void performNavigationToUnderlyingPage() {
        exceptionSafeClick(cancelButtonLocator)
    }

    @Override
    List<By> getComponentLocators() {
        [mainDialogLocator]
    }

    YuiTable clickCancelButton() {
        returnToUnderlyingPage()
    }

    YuiSendEmailDialog fillRecipient(String recipient) {
        fill(recipientLocator, recipient)
        this
    }

    YuiSendEmailDialog fillSubject(String subject) {
        fill(subjectLocator, subject)
        this
    }

    YuiSendEmailDialog fillBody(String body) {
        fill(bodyLocator, body)
        this
    }

    YuiTable submitEmail() {
        exceptionSafeClick(submitButtonLocator)
        underlyingPage
    }

}
