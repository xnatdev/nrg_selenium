package org.nrg.selenium.xnat.page_model.session.report

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.util.ResourceValidation
import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.xnat.pogo.experiments.Scan
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertEquals

class ReviewAndArchiveScanTable implements ScanListRecord<ReviewAndArchiveScanTable>, OverlayingPage<ReviewAndArchiveScanTable, ReviewAndArchivePage>, XnatPageComponent {

    @Override
    By scanAssertionReadyLocator() {
        // wait for the table to not have any asterisks for unloaded quality fields and not have any unloaded sizes
        By.xpath('//tbody[@id="scan_tbody" and not(.//i[@class="fa fa-asterisk"]) and not(.//div[text()="Loading..."])]')
    }

    @Override
    List<String> readScanIds() {
        throw new UnsupportedOperationException('Method not yet implemented.')
    }

    @Override
    XpathLocator scanTableLocator() {
        XnatLocators.byId('scan_tbody').parent()
    }

    @Override
    List<Scan> readScans() {
        throw new UnsupportedOperationException('Method not yet implemented.')
    }

    @Override
    void performNavigationToUnderlyingPage() {}

    String readScanFileString(String scanId) {
        exceptionSafeGetText(scanFileStringLocator(scanId))
    }

    ReviewAndArchiveScanTable assertScanSize(String scanId, ResourceValidation resourceValidation) {
        final List<String> words = readScanFileString(scanId).split(' ')
        assertEquals(4, words.size())
        assertEquals(String.valueOf(resourceValidation.numFiles), words[0])
        assertEquals('files,', words[1])
        assertEquals(String.valueOf(resourceValidation.fileSize) - '.0', words[2])
        assertEquals(resourceValidation.dataUnit.sessionPageRepresentation, words[3])
        this
    }

    protected By scanFileStringLocator(String scanId) {
        By.id("scan${scanId}Files")
    }

}
