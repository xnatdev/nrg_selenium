package org.nrg.selenium.xnat.page_model.custom_variable

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.ui_element.HtmlTable
import org.nrg.testing.TimeUtils

import java.time.LocalDate

trait CustomVariableBase<X extends CustomVariableBase<X>> implements HtmlTable<X>, XnatPageComponent, ScreenshotableComponent<X> { // TODO: why is declaring ScreenshotableComponent again necessary when it seems like it should be redundant

    XpathLocator getCustomVariableTable() {
        new XpathLocator("//table[starts-with(@id, 'customvar-')]")
    }

    @Override
    XpathLocator getTableLocator() {
        customVariableTable
    }

    String variableValueDisplay(Object value) {
        (value instanceof LocalDate) ? TimeUtils.MM_DD_YYYY.format(value) : String.valueOf(value)
    }

    @Override
    X uploadScreenshot() {
        ScreenshotableComponent.super.uploadScreenshot()
    }

    @Override
    X capture() {
        ScreenshotableComponent.super.capture()
    }

}