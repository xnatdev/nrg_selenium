package org.nrg.selenium.xnat.page_model.session.report

import org.nrg.xnat.versions.XnatVersion
import org.nrg.xnat.versions.XnatVersionList
import org.openqa.selenium.TimeoutException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.ExpectedCondition
import org.openqa.selenium.support.ui.WebDriverWait

import java.time.Duration

import static org.testng.AssertJUnit.fail

class ImagingSessionReportPage_1_7_7 extends ImagingSessionReportPage {

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        XnatVersionList.VERSIONS_BEFORE_1_8
    }

    @Override
    ImagingSessionReportPage assertAutoRunCompletion(boolean captureXNAT2900Step = false, int maximumAutorunTime = 120) {
        try {
            new WebDriverWait(driver, Duration.ofSeconds(maximumAutorunTime)).until(new ExpectedCondition<Boolean>() {
                @Override
                Boolean apply(WebDriver unused) {
                    navigate().refresh()
                    exceptionSafeClick(historyExpanderLocator)
                    final String status = findElement(autoRunHistoryStatusLocator).text
                    if (status == 'Failed') {
                        fail('AutoRun failed')
                    }
                    (status == 'Complete' && findElement(autoRunHistoryPercentageLocator).text == '100.0')
                }
            })
        } catch (TimeoutException ignored) {
            fail('AutoRun did not complete in time. It is also possible this failed because of XNAT-2900.')
        }

        captureStep([historyExpanderLocator, processingHistoryTableLocator])
        navigate().refresh()

        exceptionSafeClick(historyExpanderLocator)

        if (driver.findElements(autoRunHistoryNameLocator).size() > 1) {
            fail('Failure: Duplicate AutoRun pipelines. Check XNAT-2900')
        }

        if (captureXNAT2900Step) {
            captureStep([historyExpanderLocator, processingHistoryTableLocator])
        }
        this
    }

}
