package org.nrg.selenium.xnat.versions

import org.nrg.selenium.xnat.XnatDriver
import org.nrg.xnat.versions.XnatVersion
import org.reflections.Reflections

class XnatDriverList {

    public static final Map<Class<? extends XnatVersion>, Class<? extends XnatDriver>> KNOWN_VERSION_CLASS_DRIVER_MAP = [:]

    static void readXnatVersions() {
        if (KNOWN_VERSION_CLASS_DRIVER_MAP.isEmpty()) {
            new Reflections('org.nrg.selenium.xnat.versions').getSubTypesOf(XnatDriver).each { driverClass ->
                driverClass.newInstance().handledVersions.each { versionClass ->
                    KNOWN_VERSION_CLASS_DRIVER_MAP.put(versionClass, driverClass)
                }
            }
        }
    }

}
