package org.nrg.selenium.extension

import org.apache.commons.lang3.StringUtils
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By
import org.openqa.selenium.support.pagefactory.ByChained

class XpathLocator extends By.ByXPath {

    String expression

    XpathLocator(String expression) {
        super(expression)
        this.expression = expression
    }

    boolean isJoinable() {
        true
    }

    XpathLocator joinSublocator(String sublocator) {
        sublocator ? new XpathLocator("${expression}${StringUtils.stripStart(sublocator, '.')}") : this
    }

    XpathLocator joinSublocator(XpathLocator sublocator) {
        if (!sublocator.isJoinable()) {
            throw new UnsupportedOperationException('This type of XpathLocator cannot be joined.')
        }
        joinSublocator(sublocator.expression)
    }

    XpathLocator nthInstance(String index) {
        new XpathLocator("(${expression})[${index}]")
    }

    /**
     * Returns a new object for the n-th instance of this (starting at 1, for consistency with XPath)
     * @param  index [starting at 1]
     * @return new {@link XpathLocator} for the n-th instance
     */
    XpathLocator nthInstance(int index) {
        nthInstance(String.valueOf(index))
    }

    XpathLocator lastInstance() {
        nthInstance('last()')
    }

    XpathUnion unionSublocators(List<XpathLocator> subLocators) {
        new XpathUnion(subLocators.collect { subLocator ->
            this.joinSublocator(subLocator)
        })
    }

    XpathLocator parent() {
        joinSublocator(XnatLocators.PARENT)
    }

    XpathLocator grandparent() {
        joinSublocator(XnatLocators.GRANDPARENT)
    }

    By chain(By sublocator) {
        new ByChained(this, sublocator)
    }

}
