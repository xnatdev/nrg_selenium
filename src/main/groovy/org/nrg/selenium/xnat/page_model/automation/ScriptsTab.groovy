package org.nrg.selenium.xnat.page_model.automation

import org.apache.commons.lang3.StringUtils
import org.nrg.selenium.CustomConditions
import org.nrg.selenium.enums.ScriptingLanguageType
import org.nrg.selenium.exceptions.DuplicateScriptException
import org.nrg.selenium.exceptions.MissingScriptContentException
import org.nrg.selenium.exceptions.MissingScriptIdException
import org.nrg.selenium.exceptions.ScriptingLanguageNotSpecifiedException
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.ui_element.GenericHtmlTable
import org.nrg.selenium.xnat.page_model.ui_element.HtmlTable
import org.nrg.selenium.xnat.page_model.ui_element.popup.XModal
import org.nrg.selenium.xnat.scripting.AutomationScript
import org.openqa.selenium.By
import org.openqa.selenium.support.pagefactory.ByChained

import static org.testng.AssertJUnit.assertEquals

class ScriptsTab implements AutomationTab, HtmlTable<ScriptsTab> {

    protected By addScriptButtonLocator = By.id('add-script-button')
    protected By scriptLanguageLocator = By.id('add-script-language')
    protected By secondaryScriptLanguageLocator = By.cssSelector('div.inner > select.language')
    protected XpathLocator scriptIdSublocator = new XpathLocator('.//td[1]')
    protected XpathLocator scriptLabelSublocator = new XpathLocator('.//td[2]')
    protected XpathLocator scriptDescriptionSublocator = new XpathLocator('.//td[3]')
    protected XpathLocator scriptTableLocator = XnatLocators.byId('scripts-table')

    @Override
    By tabLocator() {
        locators.tab('Scripts')
    }

    @Override
    XpathLocator getTableLocator() {
        scriptTableLocator
    }

    @Override
    void performInitialValidation() {
        clickUntilOtherElementVisible(tabLocator(), addScriptButtonLocator, 0)
        captureStep([XnatLocators.H3_TAG, tabLocator()])
    }

    ScriptsTab addScript(AutomationScript script) throws ScriptingLanguageNotSpecifiedException, MissingScriptIdException, MissingScriptContentException, DuplicateScriptException {
        final String scriptID = script.scriptID
        commentStep("Script ID: ${scriptID}")
        launchScriptCreation(script).
                fillScriptId(scriptID).
                fillScriptLabel(script.scriptLabel).
                fillScriptDescription(script.scriptDescription).
                fillScriptContents(script.scriptContents).
                uploadScreenshot().
                submit()
        script.setScriptLabel(script.effectiveLabel) // Once saved, the label is set to the ID if it was not provided
        validateScriptTableEntry(script)
        captureStep(scriptRow(script))
        this
    }

    ScriptsTab prespecifyScriptLanguage(ScriptingLanguageType languageType) {
        exceptionSafeSelect(scriptLanguageLocator, languageType.languageName)
        uploadTargetedScreenshot([scriptLanguageLocator, addScriptButtonLocator])
        findElement(addScriptButtonLocator).click()
        this
    }

    ScriptEditModal launchScriptCreation(AutomationScript script) throws ScriptingLanguageNotSpecifiedException {
        if (script.prespecifyLanguage) {
            prespecifyScriptLanguage(script.language)
        } else {
            prespecifyScriptLanguage(ScriptingLanguageType.NONE)
            exceptionSafeSelect(secondaryScriptLanguageLocator, script.language.languageName)
            final XModal languageModal = readXModal(secondaryScriptLanguageLocator).uploadScreenshot().clickOK()
            if (script.language == ScriptingLanguageType.NONE) {
                readXModal(loadPage(ScriptEditModal).postSaveDialogLocator).
                        assertTextEquals("Please select a language before proceeding.").
                        uploadScreenshot().
                        clickOK()
                languageModal.clickCancel()
                throw new ScriptingLanguageNotSpecifiedException()
            }
        }
        loadPage(ScriptEditModal)
    }

    String readScriptId(AutomationScript script) {
        findElement(scriptRow(script).joinSublocator(scriptIdSublocator)).text
    }

    String readScriptLabel(AutomationScript script) {
        findElement(scriptRow(script).joinSublocator(scriptLabelSublocator)).text
    }

    String readScriptDescription(AutomationScript script) {
        findElement(scriptRow(script).joinSublocator(scriptDescriptionSublocator)).text
    }

    ScriptsTab validateScriptTableEntry(AutomationScript script) {
        if (StringUtils.isEmpty(script.scriptDescription)) {
            script.setScriptDescription("Default description: script ID ${script.scriptID} configured to run with null")
        }
        assertEquals(script.scriptID, readScriptId(script))
        assertEquals(script.scriptLabel, readScriptLabel(script))
        assertEquals(script.scriptDescription, readScriptDescription(script))
        this
    }

    ScriptsTab captureScripts() {
        loadPage(GenericHtmlTable).withLocator(scriptTableLocator).captureTable(16)
        this
    }

    ScriptsTab editScript(AutomationScript originalScript, String newLabel, String newDescription, String newContents) {
        performScriptEditSubmission(originalScript, newLabel, newDescription, newContents)
    }

    ScriptsTab duplicateScript(AutomationScript originalScript, AutomationScript newScript) {
        performScriptDuplicationSubmission(originalScript, newScript)
    }

    ScriptsTab deleteScript(AutomationScript script) {
        final String scriptID = script.scriptID
        exceptionSafeClick(scriptDeleteLink(script))
        readXModal('Are you sure you want to delete').
                assertTextEquals("Are you sure you want to delete the script \"${scriptID}\"?\n\nAny events or triggers associated with this script will also be deleted.").
                uploadScreenshot().
                clickOK() // Delete button
        readXModal('The script').
                assertTextEquals('The script was successfully deleted.').
                uploadScreenshot().
                clickOK()
        wait.until(CustomConditions.elementNotPresent(scriptRow(script)))
        assertElementNotPresent(scriptRow(script))
        captureScripts()
    }

    ScriptsTab validateScript(AutomationScript script) {
        validateScriptTableEntry(script)
        exceptionSafeClick(scriptModalLink(script))
        loadPage(ScriptEditModal).assertLinesRepresentScript(script).uploadScreenshot().cancel()
        captureScreenshotlessStep()
        this
    }

    ScriptsTab assertScriptNotPresent(String id) {
        assertColumnDoesNotContain(0, id)
    }

    protected ScriptsTab performScriptDuplicationSubmission(AutomationScript originalScript, AutomationScript newScript) {
        exceptionSafeClick(scriptDuplicateLink(originalScript))
        loadPage(ScriptEditModal).duplicate(originalScript, newScript)
    }

    protected ScriptsTab performScriptEditSubmission(AutomationScript originalScript, String newLabel, String newDescription, String newContents) {
        exceptionSafeClick(scriptEditLink(originalScript))
        final ScriptEditModal editModal = loadPage(ScriptEditModal)
        if (newLabel != null) {
            editModal.fillScriptLabel(newLabel)
            originalScript.setScriptLabel(newLabel)
        }
        if (newDescription != null) {
            editModal.fillScriptDescription(newDescription)
            originalScript.setScriptDescription(newDescription)
        }
        if (newContents != null) {
            editModal.forceClearScriptContents().fillScriptContents(newContents)
            originalScript.setScriptContents(newContents)
        }
        editModal.uploadScreenshot().submit()
    }

    protected XpathLocator scriptRow(String scriptId) {
        new XpathLocator("//tr[@data-script-id='${scriptId}']")
    }

    protected XpathLocator scriptRow(AutomationScript script) {
        scriptRow(script.scriptID)
    }

    protected By scriptEditLink(AutomationScript script) {
        new ByChained(scriptRow(script), By.linkText('edit'))
    }

    protected By scriptDuplicateLink(AutomationScript script) {
        new ByChained(scriptRow(script), By.linkText('duplicate'))
    }

    protected By scriptDeleteLink(AutomationScript script) {
        new ByChained(scriptRow(script), By.linkText('delete'))
    }

    protected By scriptModalLink(AutomationScript script) {
        scriptRow(script).joinSublocator(scriptIdSublocator)
    }

}
