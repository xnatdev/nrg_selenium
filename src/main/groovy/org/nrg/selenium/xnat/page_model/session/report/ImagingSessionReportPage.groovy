package org.nrg.selenium.xnat.page_model.session.report

import org.nrg.selenium.util.ResourceValidation
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.admin.processing.PipelineLaunchSelectionWindow
import org.nrg.selenium.xnat.page_model.download.DataDownloadPage
import org.nrg.selenium.xnat.page_model.report.DataReportPage
import org.nrg.selenium.xnat.page_model.report.XnatReportPage
import org.nrg.selenium.xnat.page_model.session.delete.CommonDeleteDialog
import org.nrg.selenium.xnat.page_model.session.edit.ImagingSessionEditPage
import org.nrg.testing.TimeUtils
import org.nrg.xnat.enums.Gender
import org.nrg.xnat.enums.Handedness
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.Scan
import org.nrg.xnat.pogo.users.User
import org.openqa.selenium.By
import org.openqa.selenium.TimeoutException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.ExpectedCondition
import org.openqa.selenium.support.ui.WebDriverWait

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

import static org.testng.AssertJUnit.*

class ImagingSessionReportPage implements XnatReportPage<ImagingSession, ImagingSessionReportPage>, DataReportPage<ImagingSession, ImagingSessionReportPage>, NavigablePageObject<ImagingSessionReportPage> {

    protected By projectTabLocator = By.xpath("//em[contains(text(), 'Projects')]")
    protected By sessionTitleLocator = By.id('report_title')
    protected By sessionDetailsLocator = By.id('expt_summary_module')
    protected By historyExpanderLocator = By.id('IMG7899')
    protected By autoRunHistoryStatusLocator = By.xpath("//a[text()='AutoRun']/../following-sibling::td[2]")
    protected By autoRunHistoryPercentageLocator = By.xpath("//a[text()='AutoRun']/../following-sibling::td[3]")
    protected By autoRunHistoryNameLocator = By.linkText('AutoRun')
    protected By processingHistoryTableLocator = By.xpath("//div[@id='span7899']//table")
    protected By emailLinkLocator = By.linkText('Email')
    protected String accessionNumberIdentifier = 'Accession #'
    protected String dateAddedIdentifier = 'Date Added'
    protected String dateIdentifier = 'Date'
    protected String timeIdentifier = 'Time'
    protected String visitIdentifier = 'Visit'
    protected String scannerNameIdentifier = 'Scanner Name'
    protected String scannerTypeIdentifier = 'Scanner Type'
    protected String acquisitionSiteIdentifier = 'AcquisitionSiteIdentifier'
    protected String subjectIdentifier = 'Subject'
    protected String genderIdentifier = 'Gender'
    protected String handednessIdentifier = 'Handedness'
    protected String ageIdentifier = 'Age'
    SessionReportPageScanTable scanTable

    @Override
    ImagingSessionEditPage loadEditPage() {
        findElement(editLinkLocator).click()
        loadPage(ImagingSessionEditPage, false, true)
    }

    @Override
    void validateObject(ImagingSession session) {
        assertEquals("${session.dataType.singularName}: ${session.label}", sessionTitle)
        capture()
    }

    @Override
    List<By> getComponentLocators() {
        [sessionTitleLocator, sessionDetailsLocator] + readActionsBox().componentLocators + readBreadcrumbs().componentLocators
    }

    @Override
    List<String> downloadXmlActionsPath() {
        ['Download', 'Download XML']
    }

    @Override
    void performInitialValidation() {
        scanTable = loadPage(SessionReportPageScanTable).withUnderlyingPage(this)
        waitForElement(sessionTitleLocator)
        waitForElementVisible(emailLinkLocator)
        waitForAsyncCalls()
    }

    @Override
    CommonDeleteDialog loadDeleteComponent() {
        actionsBoxNavigation(['Delete'])
        loadPage(CommonDeleteDialog)
    }

    String getSessionTitle() {
        findElement(sessionTitleLocator).text
    }

    String readAccessionNumber() {
        readStandardDetailField(accessionNumberIdentifier)
    }

    String readDateAddedField() {
        readStandardDetailField(dateAddedIdentifier)
    }

    LocalDate readDate() {
        LocalDate.parse(readStandardDetailField(dateIdentifier), TimeUtils.MM_DD_YYYY)
    }

    LocalTime readTime() {
        LocalTime.parse(readStandardDetailField(timeIdentifier))
    }

    String readVisit() {
        readStandardDetailField(visitIdentifier)
    }

    String readScannerName() {
        readStandardDetailField(scannerNameIdentifier)
    }

    String readScannerType() {
        readStandardDetailField(scannerTypeIdentifier)
    }

    String readAcquisitionSite() {
        readStandardDetailField(acquisitionSiteIdentifier)
    }

    String readSubjectLabel() {
        readStandardDetailField(subjectIdentifier)
    }

    Gender readGender() {
        Gender.get(readStandardDetailField(genderIdentifier))
    }

    Handedness readHandedness() {
        Handedness.get(readStandardDetailField(handednessIdentifier))
    }

    int readAge() {
        Integer.parseInt(readStandardDetailField(ageIdentifier))
    }

    ImagingSessionReportPage assertAccessionNumber(String expectedAccessionNumber) {
        assertEquals(expectedAccessionNumber, readAccessionNumber())
        this
    }

    ImagingSessionReportPage assertDateAdded(LocalDateTime expectedDateTime, User user) {
        assertEquals("${expectedDateTime.format(TimeUtils.AMERICAN_DATE_TIME)} (${user.username})", readDateAddedField())
        this
    }

    ImagingSessionReportPage assertDate(LocalDate expectedDate) {
        assertEquals(expectedDate, readDate())
        this
    }

    ImagingSessionReportPage assertTime(LocalTime expectedTime) {
        assertEquals(expectedTime, readTime())
        this
    }

    ImagingSessionReportPage assertVisit(String expectedVisit) {
        assertEquals(expectedVisit, readVisit())
        this
    }

    ImagingSessionReportPage assertScannerName(String expectedName) {
        assertEquals(expectedName, readScannerName())
        this
    }

    ImagingSessionReportPage assertScannerType(String expectedType) {
        assertEquals(expectedType, readScannerType())
        this
    }

    ImagingSessionReportPage assertAcquisitionSite(String expectedAcquisitionSite) {
        assertEquals(expectedAcquisitionSite, readAcquisitionSite())
        this
    }

    ImagingSessionReportPage assertSubjectLabel(String expectedLabel) {
        assertEquals(expectedLabel, readSubjectLabel())
        this
    }

    ImagingSessionReportPage assertGender(Gender expectedGender) {
        assertEquals(expectedGender, readGender())
        this
    }

    ImagingSessionReportPage assertHandedness(Handedness expectedHandedness) {
        assertEquals(expectedHandedness, readHandedness())
        this
    }

    ImagingSessionReportPage assertAge(int expectedAge) {
        assertEquals(expectedAge, readAge())
        this
    }

    ImagingSessionReportPage assertAutoRunCompletion(boolean captureXNAT2900Step = false, int maximumAutorunTime = 120) {
        this
    }

    DataDownloadPage clickDownloadImages() {
        actionsBoxNavigation(['Download', 'Download Images'])
        loadPage(DataDownloadPage)
    }

    PipelineLaunchSelectionWindow clickBuild() {
        noteWindows()
        actionsBoxNavigation(['Build'])
        loadPage(PipelineLaunchSelectionWindow)
    }

    ImagingSessionReportPage assertScansEqualTo(Collection<Scan> scans) {
        scanTable.assertScansEqualTo(scans)
        this
    }

    ScanPreview viewFirstScan() {
        scanTable.viewFirstScan()
    }

    ScanPreview viewScan(Scan scan) {
        scanTable.viewScan(scan)
    }

    ImagingSessionReportPage assertSnapshotGeneration() {
        viewFirstScan().assertSnapshotPresent().closePreview()
    }

    ScanPreview loadScanPreview() {
        scanTable.loadScanPreview()
    }

    String readScanFileString(Object scanId) {
        scanTable.readScanFileString(scanId)
    }

    String readSessionFileString() {
        scanTable.readSessionFileString()
    }

    ImagingSessionReportPage assertScanResourcesRepresent(Object scanId, ResourceValidation resourceValidation) {
        scanTable.assertScanResourcesRepresent(scanId, resourceValidation)
        this
    }

    ImagingSessionReportPage assertSummedScanResourcesRepresent(ResourceValidation resourceValidation) {
        scanTable.assertSummedScanResourcesRepresent(resourceValidation)
        this
    }

    ImagingSessionReportPage assertFilesRepresentSample1(double tolerance = 0.3) {
        scanTable.assertFilesRepresentSample1(tolerance)
        this
    }

    ImagingSessionReportPage assertProjectTabNotPresent() {
        assertElementNotPresent(projectTabLocator)
        this
    }

}
