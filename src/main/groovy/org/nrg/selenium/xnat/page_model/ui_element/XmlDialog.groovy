package org.nrg.selenium.xnat.page_model.ui_element

import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.ui_element.popup.XModal
import org.openqa.selenium.By

class XmlDialog implements OverlayingPage<XmlDialog, YuiTable>, XnatPageComponent {

    XModal xModal
    protected modalLocator = By.id('showXmlContent')

    @Override
    void performInitialValidation() {
        xModal = readXModal(modalLocator)
    }

    @Override
    void performNavigationToUnderlyingPage() {
        xModal.clickOK()
    }

}
