package org.nrg.selenium.xnat.page_model.ui_element.notification

import org.openqa.selenium.By

class XnatAlert implements XnatNotification<XnatAlert> {

    @Override
    By getLocator() {
        By.cssSelector('div.alert')
    }

}
