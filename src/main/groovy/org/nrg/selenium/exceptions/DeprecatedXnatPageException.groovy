package org.nrg.selenium.exceptions

import org.nrg.xnat.versions.*

class DeprecatedXnatPageException extends UnsupportedOperationException {

    DeprecatedXnatPageException(String firstNoncompatibleVersion, String readablePageDescriptor) {
        super("The '${readablePageDescriptor}' page was removed in XNAT ${firstNoncompatibleVersion}, and can no longer be loaded.")
    }

    DeprecatedXnatPageException(Class<? extends XnatVersion> firstNoncompatibleVersion, String readablePageDescriptor) {
        this(firstNoncompatibleVersion.newInstance().versionKeys[0], readablePageDescriptor)
    }

}
