package org.nrg.selenium.xnat.page_model

trait ConfigPage<X extends ConfigPage<X>> implements PageObjectSupport {

    String commonSuccessMessage = 'Your settings have been successfully updated.'

    X assertCommonConfigSaveSuccess() {
        (this as XnatPageComponent).readXModal(commonSuccessMessage).assertTextEquals(commonSuccessMessage).capture().clickOK()
        this as X
    }

}