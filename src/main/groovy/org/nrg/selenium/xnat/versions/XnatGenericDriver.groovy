package org.nrg.selenium.xnat.versions

import org.nrg.selenium.xnat.XnatDriver
import org.nrg.xnat.versions.*

class XnatGenericDriver extends XnatDriver {

    @Override
    void customize() {}

    @Override
    boolean is16() {
        false
    }

    @Override
    boolean is17() {
        false
    }

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        XnatVersionList.KEY_TO_VERSION_MAP.values() as List<Class<? extends XnatVersion>>
    }

}
