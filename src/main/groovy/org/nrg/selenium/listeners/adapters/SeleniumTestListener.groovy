package org.nrg.selenium.listeners.adapters

import groovy.util.logging.Log4j
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.nrg.selenium.BaseSeleniumTest
import org.nrg.selenium.util.WebDriverUtils
import org.nrg.selenium.xnat.XnatDriver
import org.nrg.testing.TestController
import org.nrg.testing.listeners.adapters.BaseTestListener
import org.nrg.testing.listeners.adapters.jira.JIRATest
import org.nrg.testing.xnat.conf.Settings
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.testng.ITestResult

import java.nio.file.Paths

@Log4j
class SeleniumTestListener extends BaseTestListener {

    private TestController testController
    private WebDriver driver
    private XnatDriver xnatDriver

    @Override
    void onConfigurationFailure(ITestResult itr) {
        super.onConfigurationFailure(itr)
        setFields(itr)
        stopDriver()
    }

    @Override
    void onTestComplete(ITestResult testResult) {
        setFields(testResult)
    }

    @Override
    void onStart(ITestResult testResult) {
        setFields(testResult)
    }

    @Override
    void onFailure(ITestResult testResult) {
        final JIRATest currentTest = testController.currentTest
        final String failedFiles = Settings.getFailedScreenshotPath(testClassName)

        final File screenshot = WebDriverUtils.takeScreenshot(failedFiles, Settings.getFailureScreenshotName(testName), driver)
        if (screenshot != null) {
            currentTest.postStepAttachment(screenshot, xnatDriver.step)
            currentTest.postExecutionAttachment(screenshot)
        }
        if (Settings.DOM_SETTING) {
            try {
                final Document document = Jsoup.parse(driver.findElement(By.tagName('body')).getAttribute('innerHTML'))
                final File domFile = Paths.get(failedFiles, "page_content_${testName}.dom").toFile()
                domFile.createNewFile()
                domFile << document.html()
                currentTest.postStepAttachment(domFile, xnatDriver.step)
                currentTest.postExecutionAttachment(domFile)
            } catch (Exception e) {
                log.info('Failed to capture DOM for failing test', e)
            }
        }
        stopDriver()
    }

    @Override
    void onSuccess(ITestResult testResult) {
        stopDriver()
    }

    @Override
    void onSkipped(ITestResult testResult) {
        stopDriver()
    }

    private void setFields(ITestResult testResult) {
        try {
            driver = (testResult.instance as BaseSeleniumTest).driver
            xnatDriver = (testResult.instance as BaseSeleniumTest).xnatDriver
            testController = xnatDriver.testController
        } catch (NullPointerException ignored) {
            log.warn("Could not set driver in SeleniumTestListener due to NPE (in ${(testResult == null) ? 'testResult' : 'testResult.instance'})")
        }
    }

    private void stopDriver() {
        if (driver != null) {
            driver.quit()
        }
    }

}