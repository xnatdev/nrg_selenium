package org.nrg.selenium.processing.config_elements

import org.apache.commons.lang3.time.StopWatch
import org.nrg.selenium.xnat.XnatDriver
import org.nrg.testing.TimeUtils
import org.nrg.testing.xnat.conf.Settings
import org.openqa.selenium.Alert
import org.openqa.selenium.NoAlertPresentException

import static org.testng.AssertJUnit.*

class AlertElement implements PipelineConfigElement {

    private String text

    AlertElement(String text) {
        this.text = text
    }

    @Override
    void performElementAction(XnatDriver xnatDriver) {
        final String alertText = getAlertText(xnatDriver)
        if (text != null) {
            assertEquals(text, alertText)
        }
    }

    private String getAlertText(XnatDriver xnatDriver) {
        final StopWatch stopWatch = TimeUtils.launchStopWatch()
        while (true) {
            TimeUtils.checkStopWatch(stopWatch, Settings.DEFAULT_TIMEOUT, 'Could not find alert in time.')
            try {
                final Alert alert = xnatDriver.switchTo().alert()
                final String alertText = alert.text
                alert.accept()
                return alertText
            } catch (NoAlertPresentException ignored) {}
        }
    }

}
