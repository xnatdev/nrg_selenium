package org.nrg.selenium.processing.jackson.deserializer

import com.fasterxml.jackson.databind.ObjectMapper
import org.nrg.selenium.enums.LocatorType
import org.nrg.selenium.processing.element_comparators.Element
import org.nrg.selenium.processing.element_comparators.ElementSpecifications
import org.nrg.selenium.processing.element_comparators.Locator
import org.nrg.selenium.processing.element_comparators.cache.TextIndexArray
import org.nrg.selenium.processing.element_comparators.comparator.EqualityElementComparator
import org.nrg.selenium.processing.element_comparators.comparator.MaxPercentToleranceElementComparator
import org.nrg.selenium.processing.jackson.module.SeleniumProcessingJacksonModule
import org.nrg.testing.FileIOUtils
import org.nrg.xnat.jackson.mappers.YamlObjectMapper

import org.testng.annotations.Test

import static org.testng.AssertJUnit.assertEquals

class TestElementSpecificationsMarshalling {

    @Delegate private final ObjectMapper YAML_MAPPER = new YamlObjectMapper().registerModule(new SeleniumProcessingJacksonModule())
    private final ElementSpecifications elementSpecifications = composeSpecifications()

    @Test
    void testDeserialize() {
        assertEquals(elementSpecifications, readValue(FileIOUtils.loadResource('sample_element_specifications.yaml'), ElementSpecifications))
    }

    @Test
    void testSerialize() {
        assertEquals(FileIOUtils.loadResource('sample_element_specifications_serialized.yaml').text, writeValueAsString(elementSpecifications))
    }

    @Test
    void testRoundTrip() {
        assertEquals(elementSpecifications, readValue(writeValueAsString(elementSpecifications), ElementSpecifications))
    }

    private ElementSpecifications composeSpecifications() {
        new ElementSpecifications(comparators : [
                new EqualityElementComparator(elements : [
                        new Element(locator : new Locator(type : LocatorType.XPATH, value : '//my/div[3]'), value : 'happy'),
                        new Element(locator : new Locator(type : LocatorType.XPATH, value : '//my/div[4]'), value : '3.00'),
                        new Element(locator : new Locator(type : LocatorType.XPATH, value : '//my/div[5]'), value : 'cat'),
                        new Element(locator : new Locator(type : LocatorType.ID, value : 'summary'), value : 'passed')
                ]),
                new MaxPercentToleranceElementComparator(tolerance : 1.0, elements : [
                        new Element(locator : new Locator(type : LocatorType.XPATH, value : "\$summary_tbody/tr[\$indexCache(Left-Pallidum)]/td[2]"), value : 100000.0)
                ])
        ], locatorCaches: [
                new TextIndexArray(id : 'indexCache', locator : "//span[@id='full_summary']//tbody/tr/td[1]", offset : 2)
        ], locatorReplacements: ['$summary_tbody' : "//span[@id='full_summary']//tbody"])
    }

}