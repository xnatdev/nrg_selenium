package org.nrg.selenium.xnat.page_model.ui_element.manage_files

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent

import static org.testng.AssertJUnit.assertEquals

trait ManageFilesDirectory<X extends ManageFilesDirectory<X>> implements ManageFilesItem, ScreenshotableComponent<X> {

    String folderName

    @Override
    XpathLocator getSelfSublocator() {
        fileManagerItem(folderName)
    }

    @Override
    X uploadScreenshot() {
        ScreenshotableComponent.super.uploadScreenshot()
    }

    @Override
    X capture() {
        ScreenshotableComponent.super.capture()
    }

    X clickExpander() {
        checkFileManager()
        fileManager.clickExpander(this) as X
    }

    X expand() {
        checkFileManager()
        fileManager.expand(this) as X
    }

    X assertChildFileNotPresent(String fileName) {
        expand()
        assertElementNotPresent(getFile(fileName).fileLinkLocator)
        this as X
    }

    X assertNumberOfChildren(int expectedNumber) {
        assertEquals(expectedNumber, readChildren().size())
        this as X
    }

    ManageFilesSubfolder getSubfolder(String name) {
        expand()
        final ManageFilesSubfolder subfolder = loadPage(ManageFilesSubfolder)
        subfolder.setParent(this)
        subfolder.setFileManager(fileManager)
        subfolder.setFolderName(name)
        subfolder
    }

    ManageFilesFile getFile(String fileName) {
        expand()
        final ManageFilesFile file = loadPage(ManageFilesFile)
        file.setParent(this)
        file.setFileManager(fileManager)
        file.setFileName(fileName)
        file
    }

    List<ManageFilesItem> readChildren() {
        fileManager.readChildren(this)
    }

    void checkFileManager() {
        if (fileManager == null) {
            throw new UnsupportedOperationException('ManageFilesDirectory object is missing ManageFiles object. ManageFilesDirectory must be initialized with ManageFiles#getResource or ManageFiles object must be set manually.')
        }
    }

}