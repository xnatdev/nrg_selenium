package org.nrg.selenium.xnat.page_model.admin

import org.nrg.xnat.versions.*
import org.openqa.selenium.By

class JavaMelodyMonitoringServlet_1_7_3 extends JavaMelodyMonitoringServlet {

    JavaMelodyMonitoringServlet_1_7_3() {
        numObjectsInNrgCacheLocator = By.xpath('(//td[text()="nrg"]/following-sibling::td)[2]')
    }

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev, Xnat_1_7_2, Xnat_1_7_3]
    }

}
