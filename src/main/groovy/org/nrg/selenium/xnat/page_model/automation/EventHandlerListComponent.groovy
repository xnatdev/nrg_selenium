package org.nrg.selenium.xnat.page_model.automation

import org.nrg.selenium.exceptions.InvalidEventHandlerException
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.ui_element.popup.XModal
import org.nrg.selenium.xnat.scripting.EventHandler
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertEquals
import static org.testng.AssertJUnit.assertTrue

class EventHandlerListComponent implements NavigablePageObject<EventHandlerListComponent>, ScreenshotableComponent<EventHandlerListComponent> {

    XpathLocator eventNameSublocator = new XpathLocator('.//dd[1]')
    XpathLocator scriptSublocator = new XpathLocator('.//dd[2]')
    XpathLocator descriptionSublocator = new XpathLocator('.//dd[3]')
    XpathLocator eventTypeSublocator = new XpathLocator('.//dd[7]')
    XpathLocator eventFiltersSublocator = new XpathLocator('.//dd[8]')
    By addEventHandlerButtonLocator = XnatLocators.byPartialText('Add Event Handler')
    By noEventHandlersLocator = By.id('no_event_handlers')

    @Override
    List<By> getComponentLocators() {
        [By.id('events_list')]
    }

    EventHandlerListComponent addEventHandler(EventHandler eventHandler) throws InvalidEventHandlerException {
        final EventHandlerEditModal eventHandlerEditModal = launchEdit().performEventHandlerSubmission(eventHandler)
        if (eventHandler.script == null || eventHandler.eventID == null) {
            readXModal('select').
                    assertTextEquals(getBadEventHandlerMessage()).
                    uploadScreenshot().
                    clickOK()
            eventHandlerEditModal.cancel()
            throw new InvalidEventHandlerException()
        }
        readXModal('Your event').assertTextEquals('Your event handler was successfully added.').capture().clickOK()
        validateEventHandler(eventHandler)
        this
    }

    EventHandlerListComponent assertNoEventHandlers() {
        waitForElementVisible(noEventHandlersLocator)
        this
    }

    EventHandlerListComponent deleteEventHandler(EventHandler eventHandler) {
        uploadTargetedScreenshot(getEventHandlerElementLocator(eventHandler))
        exceptionSafeClick(getEventHandlerElementLocator(eventHandler).joinSublocator(XnatLocators.byText('delete')))
        final XModal confirmationModal = readXModal('Are you sure').assertTextContains('Are you sure you want to delete the handler')
        assertTrue(confirmationModal.bodyText.contains(eventHandler.eventID.replaceAll(' ', '_')) || confirmationModal.bodyText.contains(eventHandler.eventID))
        confirmationModal.uploadScreenshot().clickOK()
        readXModal('The event').
                assertTextEquals('The event handler was successfully deleted.').
                capture().
                clickOK()
        loadPage(EventHandlerListComponent)
    }

    String readEventId(EventHandler eventHandler) {
        findElement(getEventHandlerElementLocator(eventHandler).joinSublocator(eventNameSublocator)).text
    }

    String readScriptId(EventHandler eventHandler) {
        findElement(getEventHandlerElementLocator(eventHandler).joinSublocator(scriptSublocator)).text
    }

    String readDescription(EventHandler eventHandler) {
        findElement(getEventHandlerElementLocator(eventHandler).joinSublocator(descriptionSublocator)).text
    }

    String readEventType(EventHandler eventHandler) {
        findElement(getEventHandlerElementLocator(eventHandler).joinSublocator(eventTypeSublocator)).text
    }

    String readEventFilters(EventHandler eventHandler) {
        findElement(getEventHandlerElementLocator(eventHandler).joinSublocator(eventFiltersSublocator)).text
    }

    EventHandlerListComponent validateEventHandler(EventHandler eventHandler) {
        assertEquals(eventHandler.eventID, readEventId(eventHandler))
        assertEquals(eventHandler.script.scriptID, readScriptId(eventHandler))
        assertEquals(eventHandler.description ?: '', readDescription(eventHandler))
        assertEquals("Event Class: ${eventHandler.eventType.displayName}", readEventType(eventHandler))
        assertEquals("Event Filters: {status=[${eventHandler.status}]}", readEventFilters(eventHandler))
        capture()
    }

    String getBadEventHandlerMessage() {
        'Please select an Event and Script to create an\nEvent Handler.'
    }

    protected EventHandlerEditModal launchEdit() {
        exceptionSafeClick(addEventHandlerButtonLocator)
        loadPage(EventHandlerEditModal)
    }

    protected XpathLocator getEventHandlerElementLocator(EventHandler eventHandler) {
        XnatLocators.tagWithText('dd', eventHandler.script.scriptID).parent()
    }

}