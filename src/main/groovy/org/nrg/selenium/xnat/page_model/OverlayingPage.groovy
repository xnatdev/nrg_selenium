package org.nrg.selenium.xnat.page_model

trait OverlayingPage<X extends PageObjectSupport, Y extends XnatPageComponent> implements PageObjectSupport {

    def underlyingPage // typing as Y generates invalid class file

    Y getUnderlyingPage() {
        underlyingPage
    }

    X withUnderlyingPage(Y page) {
        setUnderlyingPage(page)
        this as X
    }

    Y returnToUnderlyingPage() {
        performNavigationToUnderlyingPage()
        underlyingPage.performInitialValidation()
        underlyingPage
    }

    abstract void performNavigationToUnderlyingPage()

}