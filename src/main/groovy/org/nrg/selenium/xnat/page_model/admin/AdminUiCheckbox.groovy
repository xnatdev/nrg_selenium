package org.nrg.selenium.xnat.page_model.admin

import org.openqa.selenium.By

class AdminUiCheckbox implements AdminUiElement {

    boolean checked

    AdminUiCheckbox(By by, boolean checked, By screenshot = null, boolean takeScreenshot = true) {
        setLocator(by)
        setScreenshotLocator(screenshot)
        setTakeScreenshot(takeScreenshot)
        setChecked(checked)
    }

    @Override
    void performElementAction() {
        performCheckBox(locator, checked)
    }

}
