package org.nrg.selenium.xnat.page_model.session.edit

import org.nrg.jira.components.zephyr.TestStatus
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.XnatEditPage
import org.nrg.selenium.xnat.page_model.custom_variable.CustomVariableEditPage
import org.nrg.selenium.xnat.page_model.report.XnatReportPage
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.session.report.ImagingSessionReportPage
import org.nrg.selenium.xnat.page_model.ui_element.popup.XModal
import org.nrg.testing.util.RandomHelper
import org.nrg.xnat.pogo.DataType
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.sessions.PETSession
import org.openqa.selenium.By
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.Select

import java.time.LocalDate
import java.time.format.TextStyle

class ImagingSessionEditPage implements XnatEditPage<ImagingSession, ImagingSessionEditPage, ImagingSessionReportPage>, CustomVariableEditPage, NavigablePageObject<ImagingSessionEditPage> {

    protected By tracerLocator = By.id('xnat:petSessionData.tracer.name')
    protected By tracerInput = By.id('new_value')
    protected By tracerButton = XnatLocators.SELECT_BUTTON
    protected By scanCountLocator = By.xpath("//tbody[@id='scan_tbody']/tr")
    protected By shortSummaryLocator = By.xpath("//div[@class='withColor withThinBorder']")
    protected By editContextProjectLocator = By.id('project_placeholder')
    protected By editContextSubjectLocator = By.id('subject_placeholder')
    protected By editContextSessionLabelLocator = By.id('label_placeholder')
    protected By modifyButtonLocator = XnatLocators.MODIFY_BUTTON
    protected By moveProjectIconLocator = By.xpath("//a[@onclick='modifyProject();']/i")
    protected By moveProjectDialogLocator = By.id('projectDialog_c')
    protected By moveToProjectLocator = By.id('new_project')
    protected By reassignToSubjectIconLocator = By.xpath("//a[@onclick='modifySubject();']/i")
    protected By editLabelIconLocator = By.xpath("//a[@onclick='modifyLabel()']/i")
    protected By editLabelDialogLocator = By.id('labelDialog')
    protected By labelChangeLocator = By.id('new_label')
    protected By backButtonLocator = By.xpath('//*[@name="form1"]/input[@type="button"]')

    @Override
    void populateFields(ImagingSession session, boolean takeAllScreenshots) {
        if (![DataType.MR_SESSION, DataType.CT_SESSION, DataType.PET_SESSION].contains(session.dataType)) {
            throw new UnsupportedOperationException("DataType ${session.dataType} for session is not supported.")
        }
        if (session.primaryProject == null || session.subject == null) {
            throw new UnsupportedOperationException('Must have project and subject specified in session object for this method')
        }
        final DataType sessionDataType = session.dataType
        final String xsiType = sessionDataType.xsiType
        final String code = sessionDataType.code.toLowerCase()

        pseudoSelect(projectLocator, session.primaryProject.runningTitle)
        pseudoSelect(subjectLocator, session.subject.label)
        findElement(experimentTypeLocator(xsiType)).click()
        fill(labelLocator(xsiType), session.label)

        if (sessionDataType == DataType.PET_SESSION) {
            final String tracer = (session instanceof PETSession && session.tracer) ? session.tracer : RandomHelper.randomID()
            if (findElement(tracerLocator).tagName == 'select') {
                final Select select = new Select(findElement(tracerLocator))
                if (selectContainsOption(tracerLocator, tracer)) {
                    select.selectByVisibleText(tracer)
                } else {
                    select.selectByVisibleText('Add custom entry...')
                    fill(tracerInput, tracer)
                    findElement(tracerButton).click()
                }
            } else {
                fill(tracerLocator, tracer)
            }
            session.setDate(session.date ?: LocalDate.parse('2010-03-14')) // PET form requires date
        }

        if (session.date != null) {
            final LocalDate date = session.date
            new Select(driver.findElement(monthLocator(xsiType))).selectByVisibleText(date.month.getDisplayName(TextStyle.FULL, Locale.US))
            new Select(driver.findElement(dayLocator(xsiType))).selectByVisibleText(Integer.toString(date.dayOfMonth))
            new Select(driver.findElement(yearLocator(xsiType))).selectByVisibleText(Integer.toString(date.year))
        }
        findElements(scanCountLocator).size().times { index ->
            fill(scanLocator(xsiType, code, index), "testScan${index}")
        }
    }

    @Override
    Class<XnatReportPage> getAssociatedReportClass() {
        ImagingSessionReportPage
    }

    @Override
    void navigateTo() {
        home()
        topNav('New', 'Experiment')
    }

    ImagingSessionReportPage create(Project project, Subject subject, ImagingSession session) {
        create(session.project(project).subject(subject), false) as ImagingSessionReportPage
    }

    ImagingSessionReportPage clickBackButton() {
        exceptionSafeClick(backButtonLocator)
        loadPage(ImagingSessionReportPage)
    }

    ImagingSessionEditPage moveSessionToProject(String newProject) {
        exceptionSafeClick(moveProjectIconLocator)
        exceptionSafeSelect(moveToProjectLocator, newProject)
        uploadTargetedScreenshot(moveProjectDialogLocator)
        exceptionSafeClick(modifyButtonLocator)
        confirmMoveSessionToProject()
        wait.until(ExpectedConditions.textToBe(editContextProjectLocator, newProject))
        this
    }

    ReassignSessionToSubjectDialog loadAssignToSubjectDialog() {
        waitForElement(reassignToSubjectIconLocator)
        exceptionSafeClick(reassignToSubjectIconLocator)
        loadPage(ReassignSessionToSubjectDialog).withUnderlyingPage(this)
    }

    ImagingSessionEditPage reassignSessionToSubject(String newSubject) {
        loadAssignToSubjectDialog().assignToSubject(newSubject)
        wait.until(ExpectedConditions.textToBe(editContextSubjectLocator, newSubject))
        captureSimpleSummary()
    }

    ImagingSessionEditPage modifySessionLabel(String newLabel, boolean takeAllScreenshots = false) {
        waitForElement(editLabelIconLocator)
        exceptionSafeClick(editLabelIconLocator)
        if (takeAllScreenshots) {
            captureStep(editLabelDialogLocator)
        }
        fill(labelChangeLocator, newLabel)
        if (takeAllScreenshots) {
            captureStep(stepCounter, TestStatus.PASS, "label: ${newLabel}", true, editLabelDialogLocator)
        }
        wait.until(ExpectedConditions.textToBePresentInElementValue(labelChangeLocator, newLabel))
        findElement(modifyButtonLocator).click()
        confirmSessionRelabel()
        if (takeAllScreenshots) {
            captureSimpleSummary()
        }
        wait.until(ExpectedConditions.textToBe(editContextSessionLabelLocator, newLabel))
        captureSimpleSummary()
    }
    
    ImagingSessionEditPage captureSimpleSummary() {
        captureStep(shortSummaryLocator)
        this
    }

    protected By getProjectLocator() {
        locators.pseudoselect('project')
    }

    protected By getSubjectLocator() {
        locators.pseudoselect('part_id')
    }

    protected By experimentTypeLocator(String xsiType) {
        By.id("dt_${xsiType}")
    }

    protected By labelLocator(String xsiType) {
        By.id("${xsiType}/label")
    }

    protected By monthLocator(String xsiType) {
        By.id("${xsiType}.date.month")
    }

    protected By dayLocator(String xsiType) {
        By.id("${xsiType}.date.date")
    }

    protected By yearLocator(String xsiType) {
        By.id("${xsiType}.date.year")
    }

    protected By scanLocator(String xsiType, String code, int index) {
        By.id("${xsiType}/scans/scan[${index}][@xsi:type=xnat:${code}ScanData]/ID")
    }

    protected void confirmSessionRelabel() {
        readXModal('Modifying the')
                .waitUntilTextNonempty()
                .assertTextEquals(relabelSessionMessage())
                .uploadScreenshot()
                .clickOK()
    }

    protected void confirmMoveSessionToProject() {
        final XModal initialModal = readXModal('?') // either a sharing confirmation and move confirmation, or just move confirmation. Find out which case we're in
        if (initialModal.getBodyText().contains('share this')) {
            initialModal.uploadScreenshot().clickOK()
        }
        readXModal('Modifying the').
                assertTextEquals(moveSessionMessage()).
                uploadScreenshot().
                clickOK()
    }

    String moveSessionMessage() {
        postArchiveAnonEnabled() ?
            "Modifying the primary project of an imaging session will result in the moving of files on the file server into the new project's storage space. If a project level anonymization script is configured, it will result in the re-execution of project level anonymization (though reject statements will be ignored). Are you sure you want to make this change?" :
            "Modifying the primary project of an imaging session will result in the moving of files on the file server into the new project's storage space. Are you sure you want to make this change?"
    }

    String relabelSessionMessage() {
        postArchiveAnonEnabled() ?
                "Modifying the Session of an imaging session will result in the moving of files on the file server within the project's storage space. If a project level anonymization script is configured, it will result in the re-execution of project level anonymization (though reject statements will be ignored). Are you sure you want to make this change?" :
                "Modifying the Session of an imaging session will result in the moving of files on the file server within the project's storage space. Are you sure you want to make this change?"
    }

}
