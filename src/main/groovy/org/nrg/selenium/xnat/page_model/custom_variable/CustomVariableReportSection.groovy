package org.nrg.selenium.xnat.page_model.custom_variable

import org.nrg.xnat.pogo.custom_variable.CustomVariable
import org.nrg.xnat.pogo.custom_variable.CustomVariableSet
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertEquals

class CustomVariableReportSection implements CustomVariableBase<CustomVariableReportSection> {

    protected By variableSetTitleLocator = customVariableTable.joinSublocator('./thead//th')

    String readCustomVariableValue(CustomVariable variable) {
        exceptionSafeGetText(variableValueLocator(variable))
    }

    String readSetTitle() {
        exceptionSafeGetText(variableSetTitleLocator).trim()
    }

    CustomVariableReportSection assertSetTitle(CustomVariableSet variableSet) {
        assertEquals(getExpectedReportTitle(variableSet), readSetTitle())
        this
    }

    CustomVariableReportSection assertCustomVariableValues(Map<CustomVariable<?>, Object> expectedValues) {
        expectedValues.each { variable, value ->
            assertEquals(variableValueDisplay(value), readCustomVariableValue(variable))
        }
        this
    }

    String getExpectedReportTitle(CustomVariableSet variableSet) {
        variableSet.description ?: "${variableSet.name} Fields"
    }

    CustomVariableReportSection captureCustomVariableReportTable() {
        uploadTargetedScreenshot(variableSetTitleLocator)
        captureTable(10)
        this
    }

    CustomVariableReportSection captureCustomVariableSetTitle() {
        captureStep(variableSetTitleLocator)
        this
    }

    protected By variableValueLocator(CustomVariable variable) {
        By.xpath("//th[text()='${variable.name}']/following-sibling::td")
    }

}
