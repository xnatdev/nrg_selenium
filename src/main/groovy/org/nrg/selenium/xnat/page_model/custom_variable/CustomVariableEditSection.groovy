package org.nrg.selenium.xnat.page_model.custom_variable

import org.nrg.xnat.enums.VariableType
import org.nrg.xnat.pogo.custom_variable.CustomVariable
import org.openqa.selenium.By

class CustomVariableEditSection implements CustomVariableBase<CustomVariableEditSection> {

    protected By editHeaderLocator = By.xpath("//h3[text()='Custom Variable Sets']")
    protected By editTitleLocator = By.xpath('//p/strong')

    CustomVariableEditSection populateCustomVariable(CustomVariable customVariable, Object variableValue) {
        if (!customVariable.possibleValues.isEmpty()) {
            exceptionSafeSelect(enumeratedVariableLocator(customVariable), String.valueOf(variableValue))
        } else if (customVariable.type == VariableType.BOOLEAN) {
            exceptionSafeClick(booleanVariableLocator(customVariable, variableValue as boolean))
        } else {
            fill(inputVariableLocator(customVariable), variableValueDisplay(variableValue))
        }
        this
    }

    CustomVariableEditSection populateCustomVariables(Map<CustomVariable<?>, Object> variableValueMap) {
        variableValueMap.each { customVariable, value ->
            populateCustomVariable(customVariable, value)
        }
        captureCustomVariableEditTable()
    }

    CustomVariableEditSection captureCustomVariableEditTable() {
        uploadTargetedScreenshot(editHeaderLocator, editTitleLocator)
        captureTable(10)
        this
    }

    CustomVariableMissingRequiredDialog readRequiredVariableDialog() {
        final CustomVariableMissingRequiredDialog dialog = loadPage(CustomVariableMissingRequiredDialog)
        dialog.setEditSection(this)
        dialog
    }

    protected By enumeratedVariableLocator(CustomVariable variable) {
        By.xpath("//select[contains(@id, '${variable.name.toLowerCase()}')]")
    }

    protected By inputVariableLocator(CustomVariable variable) {
        By.xpath("//input[contains(@id, '${variable.name.toLowerCase()}')]")
    }

    protected By booleanVariableLocator(CustomVariable variable, boolean value) {
        By.xpath("//input[contains(@id, '${variable.name.toLowerCase()}') and (@value='${value}')]")
    }

}
