package org.nrg.selenium.xnat.page_model

import org.nrg.selenium.extension.WebDriverExtension
import org.nrg.selenium.xnat.XnatDriver
import org.nrg.selenium.xnat.page_model.defaults.XnatDefaults
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.ui_element.notification.XnatAlert
import org.nrg.selenium.xnat.page_model.ui_element.notification.XnatError
import org.nrg.selenium.xnat.page_model.ui_element.notification.XnatMessage
import org.nrg.selenium.xnat.page_model.ui_element.notification.XnatNote
import org.nrg.selenium.xnat.page_model.ui_element.notification.XnatSuccess
import org.nrg.selenium.xnat.page_model.ui_element.notification.XnatWarning
import org.nrg.selenium.xnat.page_model.ui_element.popup.FormValidationModal
import org.nrg.selenium.xnat.page_model.ui_element.popup.XModal
import org.nrg.selenium.xnat.page_model.ui_element.popup.XnatDialog
import org.nrg.selenium.xnat.page_model.ui_element.popup.PopupLocator
import org.nrg.testing.xnat.rest.XnatRestDriver
import org.nrg.xnat.versions.XnatVersion
import org.openqa.selenium.By

trait XnatPageComponent implements PageObjectSupport {

    @Delegate WebDriverExtension driverExtension
    @Delegate XnatDriver xnatDriver
    XnatRestDriver restDriver
    XnatLocators locators = constructLocators()
    XnatDefaults defaults = constructDefaults()

    void performInitialValidation() {}

    List<Class<? extends XnatVersion>> getHandledVersions() {[]}

    void setXnatDriver(XnatDriver driver) {
        xnatDriver = driver
        setDriverExtension(driver.driverExtension)
        restDriver = driver.restDriver
    }

    @Override
    <X extends PageObjectSupport> X loadPage(Class<X> pageClass, boolean takeAllScreenshots = false, boolean suppressLoadablePageNavigation = false) {
        final X page = getInstance(pageClass)
        (page as XnatPageComponent).setXnatDriver(xnatDriver)
        if (page instanceof LoadablePage && !suppressLoadablePageNavigation) {
            page.navigateTo(takeAllScreenshots)
        }
        waitForDocumentReadyState()
        (page as XnatPageComponent).performInitialValidation()
        page
    }

    @Override
    <X extends PageObjectSupport> X getInstance(Class<X> pageClass) {
        final Class<? extends XnatVersion> version = xnatDriver?.xnatConfig?.xnatVersion
        (version ? PageObjectRegistry.getPageObject(pageClass as Class<XnatPageComponent>, version).newInstance() : pageClass.newInstance()) as X
    }

    boolean isSubComponent() {
        this instanceof XnatLocators || this instanceof XnatDefaults
    }

    XnatLocators constructLocators() {
        (isSubComponent()) ? null : getInstance(XnatLocators)
    }

    XnatDefaults constructDefaults() {
        (isSubComponent()) ? null : getInstance(XnatDefaults)
    }

    XnatAlert readAlert() {
        loadPage(XnatAlert)
    }

    XnatMessage readMessage() {
        loadPage(XnatMessage)
    }

    XnatError readError() {
        loadPage(XnatError)
    }

    XnatNote readNote() {
        loadPage(XnatNote)
    }

    XnatSuccess readSuccess() {
        loadPage(XnatSuccess)
    }

    XnatWarning readWarning() {
        loadPage(XnatWarning)
    }

    XnatLoginPage loadLoginPage() {
        loadPage(XnatLoginPage)
    }

    XModal readXModal(By locator) {
        loadPage(XModal).locatedBy(locator)
    }

    XModal readXModal(PopupLocator locator) {
        loadPage(XModal).locatedBy(locator)
    }

    XModal readXModal(String partialText) {
        loadPage(XModal).locatedBy(partialText)
    }

    XnatDialog readXnatDialog(By locator) {
        loadPage(XnatDialog).locatedBy(locator)
    }

    XnatDialog readXnatDialog(PopupLocator locator) {
        loadPage(XnatDialog).locatedBy(locator)
    }

    XnatDialog readXnatDialog(String partialText) {
        loadPage(XnatDialog).locatedBy(partialText)
    }

    FormValidationModal readFormValidationModal() {
        loadPage(FormValidationModal).locate()
    }

}
