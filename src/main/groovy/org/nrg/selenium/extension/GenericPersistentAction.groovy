package org.nrg.selenium.extension

class GenericPersistentAction<X> {

    String failureString
    boolean allowFalseyReturn = false
    boolean skipTimeoutSetting = false
    Map<String, Integer> exceptions = [:]
    boolean voidResult = false
    Closure<X> actionClosure

    GenericPersistentAction<X> failureString(String failureString) {
        setFailureString(failureString)
        this
    }

    GenericPersistentAction<X> allowFalseyReturn(boolean value) {
        setAllowFalseyReturn(value)
        this
    }

    GenericPersistentAction<X> skipTimeoutSetting(boolean value) {
        setSkipTimeoutSetting(value)
        this
    }

    GenericPersistentAction<X> exceptions(Map<String, Integer> exceptions) {
        setExceptions(exceptions)
        this
    }

    GenericPersistentAction<X> action(Closure<X> action) {
        setActionClosure(action)
        this
    }

    GenericPersistentAction<X> voidAction(Closure<X> action) {
        voidResult = true
        setActionClosure(action)
        this
    }

}
