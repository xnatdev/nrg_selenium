package org.nrg.selenium.xnat.page_model.upload

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.ui_element.HtmlTable

import static org.testng.AssertJUnit.assertEquals

class SpreadsheetUploadPreview implements XnatPageComponent, HtmlTable<SpreadsheetUploadPreview>, OverlayingPage<SpreadsheetUploadPreview, SpreadsheetUploadStep3Page> {

    final XpathLocator previewTableLocator = new XpathLocator('//*[@id="CONDITIONS"]/table')
    final XpathLocator removeButtonSublocator = new XpathLocator(".//input[@value='REMOVE']")

    @Override
    XpathLocator getTableLocator() {
        previewTableLocator
    }

    @Override
    void performNavigationToUnderlyingPage() {}

    @Override
    SpreadsheetUploadPreview assertNumberOfDataRows(int expectedRows) {
        assertEquals(expectedRows, findElements(previewTableLocator.joinSublocator(removeButtonSublocator)).size())
        HtmlTable.super.assertNumberOfDataRows(expectedRows)
    }

    @Override
    SpreadsheetUploadPreview assertSpecificDataCell(int columnIndex, int dataRowIndex, String expectedValue) {
        assertEquals('"' + expectedValue + '"', readDataCellEntry(columnIndex, dataRowIndex))
        this
    }

}
