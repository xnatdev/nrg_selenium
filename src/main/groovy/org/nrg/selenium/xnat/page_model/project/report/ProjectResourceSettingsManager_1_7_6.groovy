package org.nrg.selenium.xnat.page_model.project.report

import org.nrg.selenium.extension.XpathLocator
import org.nrg.xnat.versions.*

class ProjectResourceSettingsManager_1_7_6 extends ProjectResourceSettingsManager {

    ProjectResourceSettingsManager_1_7_6() {
        resultOptionsSublocator = new XpathLocator('.//dd[7]')
    }

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev, Xnat_1_7_2, Xnat_1_7_3, Xnat_1_7_4, Xnat_1_7_5, Xnat_1_7_5_2, Xnat_1_7_6]
    }

}
