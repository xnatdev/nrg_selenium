package org.nrg.selenium.xnat.page_model.subject.edit

import org.nrg.jira.components.zephyr.TestStatus
import org.nrg.selenium.CustomConditions
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatEditPage
import org.nrg.selenium.xnat.page_model.custom_variable.CustomVariableEditPage
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.report.XnatReportPage
import org.nrg.selenium.xnat.page_model.subject.report.SubjectReportPage
import org.nrg.testing.TimeUtils
import org.nrg.xnat.enums.Gender
import org.nrg.xnat.enums.Handedness
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertEquals
import static org.testng.AssertJUnit.assertTrue

class SubjectEditPage implements XnatEditPage<Subject, SubjectEditPage, SubjectReportPage>, ScreenshotableComponent<SubjectEditPage>, CustomVariableEditPage, NavigablePageObject<SubjectEditPage> {

    protected By subjectFormLocator = By.id('editSubjectForm')
    protected By projectLocator = By.id('xnat:subjectData/project')
    protected By editContextLabelLocator = By.id('subject_label_placeholder')
    protected By groupLocator = By.id('xnat:subjectData/group')
    protected By labelLocator = By.id('xnat:subjectData/label')
    protected By sourceLocator = By.id('xnat:subjectData/src')
    protected By yobRadioLocator = By.id('yob')
    protected By yobLocator = demographic('yob')
    protected By ageRadioLocator = By.id('age')
    protected By ageLocator = demographic('age')
    protected By dobRadioLocator = By.id('dob')
    protected By dobLocator = demographic('dob')
    protected By educationLocator = demographic('education')
    protected By raceLocator = demographic('race')
    protected By ethnicityLocator = demographic('ethnicity')
    protected By heightLocator = demographic('height')
    protected By weightLocator = demographic('weight')
    protected By genderLocator = By.xpath("//th[text()='Gender']/../td/div")
    protected By handednessLocator = By.xpath("//th[text()='Handedness']/../td/div")
    protected By modifyButtonLocator = XnatLocators.MODIFY_BUTTON
    protected By editLabelIconLocator = By.xpath("//i[@title='Edit Label']")
    protected By editLabelFieldLocator = By.id('new_label')
    protected By editLabelDialogLocator = By.id('subjectLabelDialog')

    @Override
    void populateFields(Subject subject, boolean takeAllScreenshots) {
        final Project project = subject.project
        if (project == null) {
            throw new UnsupportedOperationException('Must specify project in subject object to use this method')
        }
        if (takeAllScreenshots) {
            capture()
        }
        waitForElement(projectLocator)
        if (findElement(projectLocator).tagName == 'select') { // otherwise it's already hard-coded onto the page and does not to be set
            pseudoSelect(projectPseudoselect(), project.runningTitle)
        }
        if (takeAllScreenshots) {
            capture()
        }
        fillSubjectLabel(subject.label)
        fillMetaFields(subject)
        if (takeAllScreenshots) {
            final String representation = [
                    'Project' : project.id,
                    'Subject' : subject.label,
                    'group' : subject.group,
                    'YOB' : subject.yob,
                    'age' : subject.age,
                    'DOB' : subject.dob,
                    'gender' : subject.gender?.toString(),
                    'handedness' : subject.handedness?.toString(),
                    'education' : subject.education,
                    'race' : subject.race,
                    'ethnicity' : subject.ethnicity,
                    'height' : subject.height,
                    'weight' : subject.weight,
                    'source' : subject.src
            ].findResults { key, value ->
                value ? "${key} = ${value}" : null
            }.join(', ')
            captureStep(stepCounter, TestStatus.PASS, representation, true, subjectFormLocator)
        }
    }

    @Override
    void navigateTo() {
        home()
        topNav('New', 'Subject')
    }

    @Override
    List<By> getComponentLocators() {
        [subjectFormLocator]
    }

    @Override
    Class<XnatReportPage> getAssociatedReportClass() {
        SubjectReportPage
    }

    SubjectReportPage create(Project project, Subject subject, boolean takeAllScreenshots = false) {
        create(subject.project(project), takeAllScreenshots) as SubjectReportPage
    }

    // Methods for filling subject information

    SubjectEditPage fillMetaFields(Subject subject) {
        fillGroup(subject.group)
        fillBirthData(subject)
        selectGender(subject.gender)
        selectHandedness(subject.handedness)
        fillEducation(subject.education)
        fillRace(subject.race)
        fillEthnicity(subject.ethnicity)
        fillHeight(subject.height)
        fillWeight(subject.weight)
        fillSource(subject.src)
    }

    SubjectEditPage fillSubjectLabel(String label) {
        fill(labelLocator, label)
        this
    }

    SubjectEditPage fillGroup(String group) {
        fill(groupLocator, group)
        this
    }

    SubjectEditPage fillYOB(int yob) {
        findElement(yobRadioLocator).click()
        fill(yobLocator, yob)
        this
    }

    SubjectEditPage fillAge(int age) {
        findElement(ageRadioLocator).click()
        fill(ageLocator, age)
        this
    }

    SubjectEditPage fillDOB(String dob) {
        findElement(dobRadioLocator).click()
        fill(dobLocator, dob)
        this
    }

    SubjectEditPage fillBirthData(Subject subject) {
        if (subject.yob) {
            fillYOB(subject.yob)
        } else if (subject.age) {
            fillAge(subject.age)
        } else if (subject.dob) {
            fillDOB(subject.dob.format(TimeUtils.MM_DD_YYYY))
        }
        this
    }

    SubjectEditPage selectGender(Gender gender) {
        pseudoSelect(genderLocator, gender?.capitalize())
        this
    }

    SubjectEditPage selectHandedness(Handedness handedness) {
        pseudoSelect(handednessLocator, handedness?.capitalize())
        this
    }

    SubjectEditPage fillEducation(int education) {
        fillNonzero(educationLocator, education)
        this
    }

    SubjectEditPage fillRace(String race) {
        fill(raceLocator, race)
        this
    }

    SubjectEditPage fillEthnicity(String ethnicity) {
        fill(ethnicityLocator, ethnicity)
        this
    }

    SubjectEditPage fillHeight(int height) {
        fillNonzero(heightLocator, height)
        this
    }

    SubjectEditPage fillWeight(int weight) {
        fillNonzero(weightLocator, weight)
        this
    }

    SubjectEditPage fillWeight(String weight) {
        fill(weightLocator, weight)
        this
    }

    SubjectEditPage fillSource(String source) {
        fill(sourceLocator, source)
        this
    }

    protected By demographic(String dataType) {
        By.id("xnat:subjectData/demographics[@xsi:type=xnat:demographicData]/${dataType}")
    }

    protected By projectPseudoselect() {
        locators.pseudoselect('xnat_subjectData_project')
    }

    SubjectEditPage modifySubjectLabel(String newLabel) {
        exceptionSafeClick(editLabelIconLocator)
        waitForElement(editLabelFieldLocator)
        captureStep(editLabelDialogLocator)
        wait.until(CustomConditions.webElementClearSuccessful(editLabelFieldLocator))
        fill(editLabelFieldLocator, newLabel)
        captureStep(stepCounter, TestStatus.PASS, "label = ${newLabel}", true, editLabelDialogLocator)
        findElement(modifyButtonLocator).click()
        assertEquals(changeLabelMessage(), closeAlertAndGetItsText())
        wait.until(CustomConditions.elementTextMatchesText(editContextLabelLocator, newLabel))
        capture()
    }

    String changeLabelMessage() {
        postArchiveAnonEnabled() ?
            "Modifying the label of a Subject will result in the moving of files on the file server within the project's storage space. If a project level anonymization script is configured, it will result in the re-execution of project level anonymization (though reject statements will be ignored). Are you sure you want to make this change?" :
            "Modifying the label of a Subject will result in the moving of files on the file server within the project's storage space. Are you sure you want to make this change?"
    }

}
