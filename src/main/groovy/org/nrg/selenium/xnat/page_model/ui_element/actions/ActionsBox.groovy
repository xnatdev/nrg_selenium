package org.nrg.selenium.xnat.page_model.ui_element.actions

import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.openqa.selenium.By

class ActionsBox implements XnatPageComponent, ScreenshotableComponent {

    protected By componentLocator = By.xpath("//div[@id='actionsMenu']//a[normalize-space()='Actions']")

    @Override
    List<By> getComponentLocators() {
        [componentLocator]
    }

    void actionsBoxNavigation(List<String> structure) {
        mouseoverCombo(locatorsFor(structure))
    }

    List<By> locatorsFor(List<String> structure) {
        componentLocators + structure.collect { locators.byText(it) }
    }

}
