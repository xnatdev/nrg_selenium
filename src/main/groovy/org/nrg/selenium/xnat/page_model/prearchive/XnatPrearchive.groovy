package org.nrg.selenium.xnat.page_model.prearchive

import org.nrg.selenium.enums.PrearchiveStatus
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.session.report.ReviewAndArchivePage
import org.nrg.testing.TimeUtils
import org.nrg.xnat.pogo.Project
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebDriverException
import org.openqa.selenium.support.ui.ExpectedCondition
import org.openqa.selenium.support.ui.WebDriverWait

import java.time.Duration

class XnatPrearchive implements ScreenshotableComponent<XnatPrearchive>, NavigablePageObject<XnatPrearchive> {

    protected XpathLocator prearchiveUiLocator = XnatLocators.byId('prearchive_table')
    protected By prearchiveDataLoadIndicatorLocator = By.xpath("//div[contains(text(), 'row')]")
    protected By findMyStudyLocator = By.xpath("//h3[text()='Find my study']")
    protected By refreshButtonLocator = XnatLocators.tagWithText('div', 'Refresh')
    protected By archiveButtonLocator = XnatLocators.tagWithText('div', 'Archive')
    protected By reviewAndArchiveButtonLocator = XnatLocators.tagWithText('div', 'Review and Archive')
    protected By rebuildButtonLocator = XnatLocators.tagWithText('div', 'Rebuild')
    protected By detailsButtonLocator = By.xpath("//div[@id='prearchive_table']//div[text()='Details']")
    protected By requestingDataIndicatorLocator = XnatLocators.byPartialText('Requesting data from server')
    protected By receivingDataIndicatorLocator = XnatLocators.byPartialText('Requesting data from server')
    protected XpathLocator checkboxSublocator = new XpathLocator("./div[contains(@class, 'icon')]")
    protected XpathLocator prearchiveRowLocator = prearchiveUiLocator.joinSublocator('./div/div[1]/div[1]/div[2]/div[1]/div/div[2]/div[1]/div') // this is very brittle, yes. However, the prearchive UI doesn't really change ever, and if we rewrite it, any locator I use will need to be replaced anyway
    protected String projectSublocator = './div[2]'

    @Override
    void performInitialValidation() {
        waitForElementVisible(refreshButtonLocator)
        waitForElementVisible(archiveButtonLocator)
        2.times {
            waitForElementNotVisible(requestingDataIndicatorLocator)
            waitForElementNotVisible(receivingDataIndicatorLocator)
        }
    }

    @Override
    List<By> getComponentLocators() {
        [prearchiveUiLocator, findMyStudyLocator]
    }

    XnatPrearchive refresh() {
        findElement(refreshButtonLocator).click()
        this
    }

    PrearchiveSessionSubset searchFor(PrearchiveSearchCriteria searchCriteria) {
        final XpathLocator subsetCriteria = buildSearchLocator(searchCriteria)
        waitForExpectedNumberOfPrearchiveElements(subsetCriteria, searchCriteria.expectedNumber, searchCriteria.maxWaitTime, searchCriteria.samplingTime)
        final PrearchiveSessionSubset sessions = loadPage(PrearchiveSessionSubset)
        sessions.setPrearchive(this)
        sessions.setSubsetLocator(subsetCriteria)
        sessions.setSize(searchCriteria.expectedNumber)
        sessions.setOriginalSearchCriteria(searchCriteria)
        sessions
    }

    PrearchiveSessionSubset findSessionsInProjects(int expectedNumSessions, List<Project> possibleProjects, PrearchiveStatus expectedStatus = null) {
        searchFor(new PrearchiveSearchCriteria(expectedNumber: expectedNumSessions, projects: possibleProjects, status: expectedStatus))
    }

    PrearchiveSessionSubset findSessionsInProject(int expectedNumSessions, Project project, PrearchiveStatus expectedStatus = null) {
        findSessionsInProjects(expectedNumSessions, [project], expectedStatus)
    }

    PrearchiveSessionSubset findOnlySessionInProject(Project expectedProject, PrearchiveStatus expectedStatus = null) {
        findSessionsInProject(1, expectedProject, expectedStatus)
    }

    XnatPrearchive assertNoMatches(PrearchiveSearchCriteria searchCriteria) {
        waitForElement(prearchiveDataLoadIndicatorLocator)
        assertElementNotPresent(buildSearchLocator(searchCriteria))
        this
    }

    XnatPrearchive assertPrearchiveEmpty() {
        assertNoMatches(new PrearchiveSearchCriteria())
    }

    protected XpathLocator buildSearchLocator(PrearchiveSearchCriteria searchCriteria) {
        final List<String> criteria = []
        if (searchCriteria.projects) {
            final String projectCriterionString = searchCriteria.projects.collect { project ->
                "text()='${project}'"
            }.join(' or ')
            criteria << "${projectSublocator}[${projectCriterionString}]".toString()
        }
        if (searchCriteria.status) {
            criteria << "./div[7][text()='${searchCriteria.status}']".toString()
        }
        if (criteria.isEmpty()) {
            criteria << '*'
        }
        prearchiveRowLocator.joinSublocator("./div[${criteria.join(' and ')}]")
    }

    protected List<By> checkboxesFor(PrearchiveSessionSubset sessions) {
        (1 .. sessions.size).collect {
            sessions.subsetLocator.joinSublocator(checkboxSublocator).nthInstance(it)
        }
    }

    protected By actionConfirmation(String verb, int numSessions) {
        By.xpath("//div[contains(text(), 'Are you sure you want to ${verb} ${numSessions}/${numSessions} sessions?')]")
    }

    protected XnatPrearchive rebuild(PrearchiveSessionSubset sessions) {
        waitForAsyncCalls()
        exceptionSafeClick(rebuildButtonLocator)
        uploadTargetedScreenshot(prearchiveUiLocator, actionConfirmation('rebuild', sessions.size))
        clickOKButton()
        this
    }

    protected XnatPrearchive archive(PrearchiveSessionSubset sessions) {
        waitForAsyncCalls()
        exceptionSafeClick(archiveButtonLocator)
        uploadTargetedScreenshot(prearchiveUiLocator, actionConfirmation('archive', sessions.size))
        clickOKButton()
        waitForNotElement(sessions.subsetLocator)
        this
    }

    protected ReviewAndArchivePage reviewAndArchive() {
        waitForAsyncCalls()
        exceptionSafeClick(reviewAndArchiveButtonLocator)
        loadPage(ReviewAndArchivePage)
    }

    protected PrearchiveDetails loadDetails() {
        noteWindows()
        exceptionSafeClick(detailsButtonLocator)
        final PrearchiveDetails details = loadPage(PrearchiveDetails)
        details.setPrearchive(this)
        details
    }

    protected void waitForExpectedNumberOfPrearchiveElements(By by, int numElements, int timeout, int samplingTime) {
        performInitialValidation()
        new WebDriverWait(driver, Duration.ofSeconds(timeout)).until(new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver webDriver) {
                setTimeout(samplingTime)
                try {
                    refresh()
                    if (findElements(by).size() == numElements) {
                        restoreTimeout()
                        return true
                    }
                } catch (WebDriverException ignored) {}
                false
            }
        })
    }

}
