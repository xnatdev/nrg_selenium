package org.nrg.selenium.xnat.page_model

import org.nrg.selenium.xnat.page_model.ui_element.notification.LoggedInSiteAlert
import org.nrg.selenium.xnat.page_model.ui_element.notification.XnatSiteAlert

trait SiteAlertContainer implements PageObjectSupport {

    Class<? extends XnatSiteAlert> alertClass() {
        LoggedInSiteAlert
    }

    XnatSiteAlert readSiteAlert() {
        loadPage(alertClass(), false)
    }

    boolean siteAlertIsPresent() {
        driverExtension.isElementPresentQuick(getInstance(alertClass()).locator)
    }

}