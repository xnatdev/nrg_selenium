package org.nrg.selenium.enums

enum ActivityTabStatus {

    IN_PROGRESS ('item'),
    SUCCESS ('item text-success'),
    ERROR ('item text-error')

    ActivityTabStatus (final String domClassName) {
        this.domClassName = domClassName
    }

    final String domClassName

    static ActivityTabStatus get(String className) {
        values().find { value ->
            value.domClassName == className
        }
    }

}