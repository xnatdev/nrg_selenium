package org.nrg.selenium.xnat.page_model

import org.nrg.xnat.versions.*

class XnatSetupPage_1_7_6 extends XnatSetupPage {

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev, Xnat_1_7_2, Xnat_1_7_3, Xnat_1_7_4, Xnat_1_7_5, Xnat_1_7_5_2, Xnat_1_7_6]
    }

    @Override
    protected void processSuccessDialog() {
        readXModal('Your XNAT site is ready to use.').clickOK()
    }

}
