package org.nrg.selenium.processing.jackson.serializer

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.jsontype.TypeSerializer
import org.nrg.selenium.processing.element_comparators.comparator.ElementComparator
import org.nrg.xnat.jackson.serializers.CustomSerializer

abstract class ElementComparatorSerializer<T extends ElementComparator> extends CustomSerializer<T> {

    private static final JsonSubTypes comparatorSubTypes = ElementComparator.getAnnotation(JsonSubTypes)
    private static final String typeKey = ElementComparator.getAnnotation(JsonTypeInfo).property()

    @Override
    void serializeWithType(T value, JsonGenerator gen, SerializerProvider serializers, TypeSerializer typeSer) {
        serialize(value, gen, serializers)
    }

    @Override
    void serialize(T value, JsonGenerator gen, SerializerProvider provider) {
        gen.writeStartObject()

        writeStringFieldIfNonnull(gen, typeKey, comparatorSubTypes.value().find { it.value() == value.getClass() }.name())
        writeSpecificFields(value, gen, provider)
        writeListFieldIfNonempty(gen, 'elements', value.elements)

        gen.writeEndObject()
    }

    abstract void writeSpecificFields(T value, JsonGenerator gen, SerializerProvider provider)

}
