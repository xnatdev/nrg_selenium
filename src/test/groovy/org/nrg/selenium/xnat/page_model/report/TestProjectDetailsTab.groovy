package org.nrg.selenium.xnat.page_model.report

import org.nrg.selenium.xnat.page_model.PageObjectRegistry
import org.nrg.selenium.xnat.page_model.project.report.ProjectDetailsTab
import org.nrg.xnat.versions.*
import org.testng.annotations.Test

import static org.testng.AssertJUnit.assertEquals

class TestProjectDetailsTab {

    @Test
    void testStringEscapingDisplay() {
        final String UNESCAPED = 'DESCRIPTION GOOD & CORRECT'
        final String ESCAPED = 'DESCRIPTION GOOD &amp CORRECT'
        assertEquals(UNESCAPED, makePage(Xnat_1_7_5_2).escapeOnProjectPage(UNESCAPED))
        assertEquals(ESCAPED, makePage(Xnat_1_7_5).escapeOnProjectPage(UNESCAPED))
    }

    private ProjectDetailsTab makePage(Class<? extends XnatVersion> versionClass) {
        PageObjectRegistry.getPageObject(ProjectDetailsTab, versionClass).newInstance() as ProjectDetailsTab
    }


}
