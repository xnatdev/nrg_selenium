package org.nrg.selenium.xnat.page_model.project.report

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.xnat.pogo.users.UserGroup
import org.openqa.selenium.By

class ProjectManageFeaturesPage implements NavigablePageObject<ProjectManageFeaturesPage>, ScreenshotableComponent<ProjectManageFeaturesPage> {

    protected XpathLocator tableLocator = XnatLocators.byId('project-features')
    protected static final String GROUP = 'Group'

    @Override
    List<By> getComponentLocators() {
        [tableLocator]
    }

    protected String xpathIndexForColumn(String column) {
        "count(${tableLocator.expression}/thead/tr/th[text()='${column}']/preceding-sibling::th)+1"
    }

    protected XpathLocator locatorForFeatureCheckbox(UserGroup group, String feature) {
        tableLocator.joinSublocator(".//tr[./td[${xpathIndexForColumn(GROUP)}][text()='${group.pluralName()}']]/td[${xpathIndexForColumn(feature)}]//input")
    }

    ProjectManageFeaturesPage setFeatureStatus(UserGroup group, String feature, boolean status) {
        final By checkboxLocator = locatorForFeatureCheckbox(group, feature)
        waitForElement(checkboxLocator)
        if (!isElementVisible(checkboxLocator)) {
            if (status) {
                throw new UnsupportedOperationException('Feature checkbox is not visible, indicating it is banned at the site level. It cannot be enabled.')
            } else {
                return this
            }
        }
        final boolean currentState = exceptionSafeGetAttribute(checkboxLocator, 'class').contains('half-check')
        if (currentState != status) {
            exceptionSafeClick(checkboxLocator)
        }
        this
    }

}
