package org.nrg.selenium.xnat.page_model

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.ui_element.HtmlTable

class DefaultTablePage implements XnatPageComponent, HtmlTable<DefaultTablePage> {

    @Override
    XpathLocator getTableLocator() {
        new XpathLocator('//table[@class="x_rs_t"]')
    }

}
