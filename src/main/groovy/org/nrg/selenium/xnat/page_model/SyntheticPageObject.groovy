package org.nrg.selenium.xnat.page_model

import org.nrg.selenium.xnat.XnatDriver

class SyntheticPageObject implements NavigablePageObject<SyntheticPageObject> {

    SyntheticPageObject(XnatDriver xnatDriverInstance) {
        setXnatDriver(xnatDriverInstance)
    }

}
