package org.nrg.selenium.xnat.page_model.admin

import org.nrg.selenium.xnat.page_model.IndexPage
import org.nrg.selenium.xnat.page_model.LoadablePage
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.site_search.AdvancedSearchResult
import org.nrg.selenium.xnat.page_model.site_search.DataTypeListing
import org.openqa.selenium.By

class LegacyAdminOptionsPage implements NavigablePageObject<LegacyAdminOptionsPage>, LoadablePage, ScreenshotableComponent<LegacyAdminOptionsPage> {

    protected By clearDbCacheLocator = By.linkText('Clear DB Cache')
    protected By viewWorkflowsLocator = By.linkText('View All Workflows')

    @Override
    void navigateTo() {
        topNav('Administer', 'More...')
    }

    @Override
    List<By> getComponentLocators() {
        [By.id('admin-more')]
    }

    IndexPage clearDBCache() {
        clickOption(clearDbCacheLocator)
        captureScreenshotlessStep()
        loadPage(IndexPage)
    }

    AdvancedSearchResult viewAllWorkflows() {
        clickOption(viewWorkflowsLocator)
        loadPage(AdvancedSearchResult)
    }

    protected void clickOption(By option) {
        waitForElementVisible(option)
        uploadTargetedScreenshot(option)
        exceptionSafeClick(option)
        waitForNotElement(option)
    }

}
