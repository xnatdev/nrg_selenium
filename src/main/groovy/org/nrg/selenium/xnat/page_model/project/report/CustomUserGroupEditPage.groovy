package org.nrg.selenium.xnat.page_model.project.report

import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.xnat.enums.DataAccessLevel
import org.nrg.xnat.pogo.DataType
import org.nrg.xnat.pogo.users.CustomUserGroup
import org.openqa.selenium.By

class CustomUserGroupEditPage implements NavigablePageObject<CustomUserGroupEditPage> {

    protected By displayNameLocator = By.id('xdat:userGroup/displayName')
    protected By permissionsTableLocator = By.id('groupPerm')

    ProjectAccessTab createCustomUserGroup(CustomUserGroup group) {
        fill(displayNameLocator, group.singularName())
        group.accessLevelMap.each { dataType, accessLevel ->
            Permission.values().each { permission ->
                if (accessLevel in permission.applicableAccessLevels()) {
                    exceptionSafeClick(getPermissionCheckbox(dataType, permission))
                }
            }
        }
        uploadTargetedScreenshot(locators.parentOf(displayNameLocator))
        captureTable(permissionsTableLocator, 14)
        submitForm()
        loadPage(ProjectReportPage).loadAccessTab()
    }

    protected By getPermissionCheckbox(DataType xnatDataType, Permission permission) {
        By.xpath("//td[@title='${permission.title}']//input[contains(@name,'${xnatDataType.xsiType}')]")
    }

    protected enum Permission {
        READ ('Read') {
            @Override
            List<DataAccessLevel> applicableAccessLevels() {
                [DataAccessLevel.READ_ONLY]
            }
        },
        CREATE_AND_EDIT ('Edit') {
            @Override
            List<DataAccessLevel> applicableAccessLevels() {
                [DataAccessLevel.CREATE_AND_EDIT, DataAccessLevel.ALL]
            }
        },
        DELETE ('Delete') {
            @Override
            List<DataAccessLevel> applicableAccessLevels() {
                [DataAccessLevel.DELETE, DataAccessLevel.ALL]
            }
        }

        String title

        Permission(String title) {
            setTitle(title)
        }

        abstract List<DataAccessLevel> applicableAccessLevels()
    }

}
