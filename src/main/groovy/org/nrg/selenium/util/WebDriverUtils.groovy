package org.nrg.selenium.util

import groovy.util.logging.Log4j
import ij.ImagePlus
import ij.process.ColorProcessor
import ij.process.ImageProcessor
import org.apache.commons.io.FileUtils
import org.apache.commons.lang3.SystemUtils
import org.nrg.selenium.browser.BrowserSupport
import org.nrg.selenium.browser.ChromeSupport
import org.nrg.selenium.browser.FirefoxSupport

import org.nrg.selenium.xnat.XnatDriver
import org.nrg.testing.FileIOUtils
import org.openqa.selenium.*

import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import java.awt.image.RasterFormatException
import java.nio.file.Paths

@Log4j
class WebDriverUtils {

    public static final String PASTE_KEYS = (SystemUtils.IS_OS_MAC_OSX ? Keys.COMMAND : Keys.CONTROL) + 'v'
    private static final int[] WHITE_PIXEL = [255, 255, 255, 0]
    private static final int MIN_WHITESPACE_FOR_REMOVAL = 50
    private static final int NUM_WHITESPACE_ROWS_RETAIN = 20
    private static final String IMAGE_FORMAT = 'png'
    private static double devicePixelRatio = -1
    
    static File takeScreenshot(String dirPath, String fileName, WebDriver driver) {
        FileIOUtils.mkdirs(dirPath)
        final File dest = Paths.get(dirPath, fileName).toFile()
        try {
            ImageIO.write(
                    postprocessImage(ImageIO.read(screenshot(driver))),
                    IMAGE_FORMAT,
                    dest
            )
            return dest
        } catch (IOException ignored) {
            log.warn("Could not take screenshot in directory ${dirPath} with file name ${fileName}.")
        } catch (NullPointerException ignored) {
            log.info('Could not take a screenshot because the WebDriver object has not yet been initialized')
        } catch (NoSuchWindowException ignored) {
            log.info('Could not take a screenshot because window not found')
        } catch (WebDriverException ignored) {
            log.info('Could not take a screenshot', ignored)
        }
        null
    }

    static void takeTargetedScreenshot(String dirPath, String fileName, XnatDriver xnatDriver, By... targets) {
        performTargetedScreenshot(dirPath, fileName, xnatDriver, Arrays.asList(targets), true)
    }

    private static performTargetedScreenshot(String dirPath, String fileName, XnatDriver xnatDriver, List<By> targets = [], boolean continueRecursion = true) {
        final WebDriver driver = xnatDriver.driver
        if (devicePixelRatio < 0) {
            devicePixelRatio = (driver as JavascriptExecutor).executeScript('return window.devicePixelRatio') as double
        }
        if (targets.size() == 0) {
            takeScreenshot(dirPath, fileName, driver)
            return
        }
        FileIOUtils.mkdirs(dirPath)
        try {
            final File screenshot = screenshot(driver)
            final BufferedImage fullScreenshot = ImageIO.read(screenshot)
            final List<Integer> xComponents = []
            final List<Integer> yComponents = []

            final Closure<List<Integer>> rescale = { List<Integer> source ->
                source.collect { coordinate ->
                    Math.round(devicePixelRatio * coordinate) as int
                }
            }
            final Closure mapElementCoordinates = { WebElement element ->
                final Point point = element.location
                final Dimension size = element.size
                xComponents.addAll(rescale([point.x, point.x + size.width]))
                yComponents.addAll(rescale([point.y, point.y + size.height]))
            }
            targets.each { target ->
                xnatDriver.waitForElementVisible(target)
                try {
                    mapElementCoordinates(driver.findElement(target))
                } catch (StaleElementReferenceException ignored) {
                    mapElementCoordinates(xnatDriver.exceptionSafeFind(target))
                }
            }

            final int x = Math.max(0, xComponents.min() - 3) // left border is 3 pixels to the left of the leftmost element (unless that's less than 0)
            final int y = Math.max(0, yComponents.min() - 3)

            final BufferedImage croppedScreenshot
            try {
                croppedScreenshot = fullScreenshot.getSubimage(
                        x + xnatDriver.horizIframeOffset,
                        y + xnatDriver.vertIframeOffset,
                        Math.min(fullScreenshot.width, xComponents.max() + 3) - x,
                        Math.min(fullScreenshot.height, yComponents.max() + 3) - y
                )
            } catch (RasterFormatException rasterFormatException) {
                if (targets.size() == 1) {
                    if (continueRecursion) {
                        xnatDriver.scrollToElement(targets[0])
                        performTargetedScreenshot(dirPath, fileName, xnatDriver, [targets[0]], false)
                    } else {
                        log.warn('Failed to take a screenshot of even a single element due to RasterFormatException. This suggests the element is so large it cannot fit within a single screen. Instead, we will take a screenshot of the entire window since it is better than nothing.', rasterFormatException)
                        performTargetedScreenshot(dirPath, fileName, xnatDriver, [], false)
                    }
                } else {
                    performTargetedScreenshot(dirPath, fileName, xnatDriver, [targets[0]], true) // if we get this exception when trying to capture multiple elements, we'll just screenshot the first one
                }
                return
            }

            ImageIO.write(postprocessImage(croppedScreenshot), IMAGE_FORMAT, screenshot)
            FileUtils.copyFile(screenshot, Paths.get(dirPath, fileName).toFile())
        } catch (IOException ignored) {
            log.warn("Could not take screenshot in directory ${dirPath} with file name ${fileName}.")
        } catch (NoSuchWindowException ignored) {
            log.info('Could not take a screenshot because window was not found.')
        }
    }

    static boolean isFirefox() {
        BrowserSupport.cachedBrowserSupport instanceof FirefoxSupport
    }

    static boolean isChrome() {
        BrowserSupport.cachedBrowserSupport instanceof ChromeSupport
    }

    static String getXMLUrl(String url) {
        isFirefox() ? "view-source:${url}" : url
    }

    private static File screenshot(WebDriver driver) {
        (driver as TakesScreenshot).getScreenshotAs(OutputType.FILE)
    }

    private static BufferedImage postprocessImage(BufferedImage screenshot) {
        final ImagePlus imagePlus = new ImagePlus('Screenshot', screenshot)
        final int height = imagePlus.height
        final int width = imagePlus.width
        final Map<List<Integer>, List<List<Integer>>> colorToLinesMap = [:]
        final List<List<Integer>> allBands = []
        (0 ..< height).each { rowIndex ->
            final List<Integer> leftmostPixel = imagePlus.getPixel(0, rowIndex) as List<Integer>
            if ((1 ..< width).every { columnIndex ->
                leftmostPixel == imagePlus.getPixel(columnIndex, rowIndex) as List<Integer>
            }) {
                final List<List<Integer>> connectedRows = colorToLinesMap.get(leftmostPixel) ?: []
                if (connectedRows.isEmpty() || connectedRows.last().last() != rowIndex - 1) {
                    final List<Integer> band = [rowIndex]
                    connectedRows << band
                    allBands << band
                } else {
                    connectedRows.last() << rowIndex
                }
                colorToLinesMap.put(leftmostPixel, connectedRows)
            }
        }

        if (allBands.isEmpty()) {
            return screenshot
        } else {
            final List<Integer> largestBand = allBands.max { it.size() }
            //final List<Integer> largestBand = connectedRows.max { it.size() }
            if (largestBand.size() < MIN_WHITESPACE_FOR_REMOVAL) {
                return screenshot
            } else {
                final int upperWhitespaceIndex = largestBand[0] + NUM_WHITESPACE_ROWS_RETAIN
                final int lowerWhitespaceIndex = largestBand.last() - NUM_WHITESPACE_ROWS_RETAIN
                final int stitchedHeight = imagePlus.height - (lowerWhitespaceIndex - upperWhitespaceIndex)
                final ImageProcessor stitchedProcessor = new ColorProcessor(width, stitchedHeight)
                (0 ..< width).each { x ->
                    (0 ..< stitchedHeight).each { y ->
                        stitchedProcessor.putPixel(
                                x,
                                y,
                                imagePlus.getPixel(x, (y <= upperWhitespaceIndex) ? y : y + lowerWhitespaceIndex - upperWhitespaceIndex)
                        )
                    }
                }
                return stitchedProcessor.getBufferedImage()
            }
        }
    }

}
