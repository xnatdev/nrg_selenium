package org.nrg.selenium.processing.element_comparators.comparator

import groovy.transform.EqualsAndHashCode
import org.nrg.selenium.processing.exceptions.IncorrectValueException
import org.nrg.testing.xnat.processing.exceptions.ProcessingValidationException
import org.nrg.selenium.processing.exceptions.WebElementNotFound
import org.nrg.selenium.xnat.XnatDriver
import org.openqa.selenium.By
import org.openqa.selenium.WebDriverException

@EqualsAndHashCode
class EqualityElementComparator extends ElementComparator {

    @Override
    void checkPointsMatch(XnatDriver xnatDriver, By locator, String expectedValue) throws ProcessingValidationException {
        try {
            final String actualValue = xnatDriver.findElement(locator).text
            if (expectedValue != actualValue) {
                throw new IncorrectValueException(locator, 'elements were expected to have exactly equal text', expectedValue, actualValue)
            }
        } catch (WebDriverException ignored) {
            throw new WebElementNotFound(locator)
        }
    }

}
