package org.nrg.selenium.xnat.page_model

import org.nrg.selenium.CustomConditions
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.extension.XpathUnion
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage
import org.nrg.selenium.xnat.page_model.session.report.ImagingSessionReportPage
import org.nrg.selenium.xnat.page_model.ui_element.popup.XnatDialog
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.users.CustomUserGroup
import org.nrg.xnat.pogo.users.UserGroup
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.pagefactory.ByChained
import org.openqa.selenium.support.ui.ExpectedCondition

import static org.nrg.xnat.pogo.users.UserGroups.OWNER
import static org.testng.AssertJUnit.fail

class IndexPage implements NavigablePageObject<IndexPage> {

    protected XpathLocator homeLocator = XnatLocators.byId('nav-home')
    protected By xnatDataSummaryLocator = XnatLocators.byPartialText('XNAT currently contains')
    protected By recentDataScreenshotLocator = By.id('data-container')
    protected XpathLocator recentDataParentLocator = XnatLocators.byId('min_expt_list')
    protected XpathLocator projectsListingLocator = XnatLocators.byId('min_projects_list')
    protected By projectAccessRequestDialogLocator = new XpathLocator("//div[text()='Project Access Request Form']").grandparent()
    protected By projectAccessRequestLevelLocator = By.xpath("//select[@name='access_level']")
    protected By projectAccessRequestSubmitButtonLocator = XnatLocators.buttonWithText('Submit Request')
    protected By projectAccessRequestReviewButtonLocator = XnatLocators.buttonWithText('Review')
    protected By projectAccessRequestProjectSublocator = By.xpath('.//tbody/tr/td[1]')
    protected By projectAccessRequestAcceptButtonLocator = XnatLocators.buttonWithText('Accept')
    protected By projectAccessRequestNoneRemainingLocator = XnatLocators.byText('You have no outstanding Project Access Requests.')

    @Override
    void performInitialValidation() {
        waitForElement(homeLocator)
        wait.until(CustomConditions.elementIsDisplayed(homeLocator))
        waitForElement(xnatDataSummaryLocator)
    }

    IndexPage requestProjectAccess(Project project, UserGroup accessLevel, String comment) {
        exceptionSafeClick(accessRequestLink(project))
        captureStep(projectAccessRequestDialogLocator)
        exceptionSafeSelect(projectAccessRequestLevelLocator, accessLevel.pluralName())
        fill(new ByChained(projectAccessRequestDialogLocator, locators.TEXTAREA_TAG), comment)
        uploadTargetedScreenshot(projectAccessRequestDialogLocator)
        exceptionSafeClick(projectAccessRequestSubmitButtonLocator)
        readNote().assertTextEquals('Access request sent.').capture()
        this
    }

    IndexPage acceptSingleNondirectPAR(Project project) {
        readMessage().assertTextContains('You have been invited to join 1 project(s).').uploadScreenshot()
        findElement(projectAccessRequestReviewButtonLocator).click()
        final XnatDialog invitationDialog = readXnatDialog('Project Invitations')
        assertNormalizedText(project.runningTitle, invitationDialog.getSubElementLocator(projectAccessRequestProjectSublocator))
        invitationDialog.capture()
        exceptionSafeClick(projectAccessRequestAcceptButtonLocator)
        waitForElementVisible(projectAccessRequestNoneRemainingLocator)
        invitationDialog.capture()
        clickOKButton()
        this
    }

    IndexPage assertNoMatchingItems() {
        readNote().assertTextEquals('No matching items found.').capture()
        this
    }

    IndexPage assertProjectAccessRequestLinkPresent(Project project) {
        assertElementPresent(accessRequestLink(project))
        this
    }

    IndexPage assertDisplayedProjectGroup(Project project, UserGroup group) {
        if (group instanceof CustomUserGroup) {
            throw new UnsupportedOperationException('Cannot verify inclusion in a custom user group on the splash page (XNAT does not list the group).')
        }
        final String article = group == OWNER ? 'an' : 'a'
        assertElementText("You are ${article} ${group.singularName().toLowerCase()} for this project.", projectGroupExplanationLocator(project))
        this
    }

    IndexPage captureRecentSessions(List<ImagingSession> sessions, int timeout = 300) {
        if (sessions.any { it.primaryProject == null }) {
            fail('Each session object must have its project object set to use this method.')
        }
        captureRecentSessions(sessions.size(), timeout, new XpathUnion(sessions.collect { session ->
            sessionByProject(session.primaryProject, session)
        }))
    }

    IndexPage captureRecentSession(ImagingSession session, int timeout = 300) {
        captureRecentSessions([session], timeout)
    }

    IndexPage captureRecentSessions(int expectedNumberOfSessions, List<Project> possibleProjects, int timeout = 300) {
        captureRecentSessions(expectedNumberOfSessions, timeout, new XpathUnion(possibleProjects.collect { project ->
            sessionByProject(project)
        }))
    }

    IndexPage captureRecentSession(Project expectedProject, int timeout = 300) {
        captureRecentSessions(1, timeout, sessionByProject(expectedProject))
    }

     IndexPage captureProjectListing() {
         captureStep(projectsListingLocator)
         this
     }

    ProjectReportPage clickProjectListing(Project project) {
        final By projectLink = projectLinkLocator(project)

        final ExpectedCondition<Boolean> byGoneAfterClick = new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver webDriver) {
                try {
                    webDriver.findElement(projectLink).click()
                } catch (Exception ignored) {
                    return true // assume the page transitioned since the last element present check
                }
                !isElementPresent(projectLink)
            }
        }

        waitForElement(projectLink)
        setTimeout(0)
        wait.until(byGoneAfterClick) // super-nuclear option to click this link until it finally goes away (that is, the project page starts loading), to handle intermittent failures
        restoreTimeout()
        loadPage(ProjectReportPage)
    }

    ImagingSessionReportPage clickRecentSession(Project expectedProject, ImagingSession session = null) {
        exceptionSafeClick(sessionByProject(expectedProject, session))
        loadPage(ImagingSessionReportPage)
    }

    ImagingSessionReportPage clickRecentSession(ImagingSession session) {
        if (session.primaryProject == null) {
            fail('Session object must have its project object set to use this method')
        }
        clickRecentSession(session.primaryProject, session)
    }

    IndexPageSearch startIndexPageSearch() {
        loadPage(IndexPageSearch)
    }

    XpathLocator getHomeLocator() {
        homeLocator
    }

    protected IndexPage captureRecentSessions(int expectedNumberOfSessions, int timeout, By genericLocator) {
        waitForElements(genericLocator, expectedNumberOfSessions, timeout)
        captureStep(recentDataScreenshotLocator)
        this
    }

    protected XpathLocator projectLocator(Project project) {
        new XpathLocator("//b[text()='Project ID: ${project.id}']").joinSublocator(XnatLocators.nearestAncestorByTag('div[@class]'))
    }

    protected XpathLocator projectGroupExplanationLocator(Project project) {
        projectLocator(project).joinSublocator('./div[4]')
    }

    protected XpathLocator projectLinkLocator(Project project) {
        projectLocator(project).joinSublocator('.//h3/a')
    }

    protected By accessRequestLink(Project project) {
        new ByChained(projectLocator(project), By.xpath(".//a[contains(text(), 'Request')]"))
    }

    protected XpathLocator sessionByProject(Project project, ImagingSession session = null) {
        final String sessionConditional = session ? " and text()='${session}'" : ''
        recentDataParentLocator.joinSublocator(".//a[contains(@href, '/app/action/DisplayItemAction') and ./../..//a[@title='${project}']${sessionConditional}]")
    }

}
