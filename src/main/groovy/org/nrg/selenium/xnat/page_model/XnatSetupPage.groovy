package org.nrg.selenium.xnat.page_model

import org.nrg.selenium.CustomConditions
import org.openqa.selenium.By

class XnatSetupPage implements XnatPageComponent {

    protected By pageLoadLocator = By.xpath("//h2[text()='XNAT Site Setup']")
    protected By adminEmailLocator = By.id('site-admin-email')
    protected By archivePathLocator = By.id('archive-path')
    protected By smtpHostLocator = By.id('smtp-hostname')
    protected By smtpPortLocator = By.id('smtp-port')
    protected By smtpProtocolLocator = By.id('smtp-protocol')

    @Override
    void performInitialValidation() {
        waitForElement(pageLoadLocator)
        wait.until(CustomConditions.attributeNonempty(archivePathLocator, 'value'))
    }

    XnatSetupPage fillAdminEmail(String email) {
        fill(adminEmailLocator, email)
        this
    }

    XnatSetupPage fillSmtpHost(String hostname) {
        fill(smtpHostLocator, hostname)
        this
    }

    XnatSetupPage fillSmtpPort(String port) {
        fill(smtpPortLocator, port)
        this
    }

    XnatSetupPage fillSmtpProtocol(String protocol) {
        fill(smtpProtocolLocator, protocol)
        this
    }

    IndexPage submit() {
        submitForm()
        processSuccessDialog()
        loadPage(IndexPage)
    }

    protected void processSuccessDialog() {
        readXnatDialog('Initial site configuration has been completed!').clickOK()
    }

}
