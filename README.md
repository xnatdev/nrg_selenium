# Selenium Tests for XNAT Systems #

## Usage ##
This project contains a base skeleton with extensive support for running Selenium tests. Instructions for configuration of tests can be found in the parent project [NRG_test](https://bitbucket.org/xnatdev/nrg_test).

## Screenshots ##
All existing screenshots will be deleted when running the test suite again, so make sure to back them up somewhere if you do not want to lose them.

### Failing Tests ###
Although screenshots taken at certain points of tests may be unhelpful, a screenshot will be taken on each test failure. These screenshots are placed in target/failed_test_screenshots/{class name}.

### Important Steps ###
Screenshots of important steps of tests are placed in target/test_step_screenshots/{class name}.

## Browser Support ##
Spotty Chrome support and extremely spotty IE support is available, but Firefox is the recommended/supported browser. Chrome seems to be significantly faster than Firefox, but has major issues on CentOS, so the default browser remains Firefox. For IE, the configuration available at https://code.google.com/p/selenium/wiki/InternetExplorerDriver#Required_Configuration must be set up.

## Jenkins ##
If you wish to use this test suite with jenkins, install the Xvfb plugin. Under build environment, Xvfb screen should be set to at least 1280x1024x24.

## JIRA Integration ##
The test suite will create a cycle with executions for tests executed in JIRA as long as xnat.jira is set to true. Failure screenshots will be attached to failing tests along with a stack trace.