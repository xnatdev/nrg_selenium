package org.nrg.selenium.xnat.page_model

import static org.testng.AssertJUnit.*

trait TextAssertable<X extends TextAssertable<X>> implements PageObjectSupport {

    abstract String getText()

    X assertTextContains(String expectedText) {
        assertTrue(text.contains(expectedText))
        this as X
    }

    X assertTextEquals(String expectedText) {
        assertEquals(expectedText.trim(), text.trim())
        this as X
    }

}