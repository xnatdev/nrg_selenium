package org.nrg.selenium.processing.config_elements

import org.nrg.selenium.xnat.XnatDriver
import org.openqa.selenium.By

class FillElement implements PipelineConfigElement {

    private By by
    private String text

    FillElement(By by, String text) {
        this.by = by
        this.text = text
    }

    @Override
    void performElementAction(XnatDriver xnatDriver) {
        xnatDriver.fill(by, text)
    }

}
