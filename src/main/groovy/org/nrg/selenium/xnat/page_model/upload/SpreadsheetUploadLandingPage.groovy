package org.nrg.selenium.xnat.page_model.upload

import org.nrg.selenium.xnat.page_model.LoadablePage
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.xnat.pogo.DataType
import org.openqa.selenium.By

class SpreadsheetUploadLandingPage implements NavigablePageObject<SpreadsheetUploadLandingPage>, LoadablePage, ScreenshotableComponent<SpreadsheetUploadLandingPage> {

    protected By stepExplanationLocator = By.cssSelector('div.edit_header1')
    protected By titleLocator = By.xpath("//input[@name='title']")
    protected By dataTypeLocator = By.id('root_data_type')
    protected By submitButtonLocator = By.name('eventSubmit_doPrep')
    DataType currentDataType

    @Override
    void navigateTo() {
        topNav('Upload', 'Spreadsheet')
    }

    @Override
    void performInitialValidation() {
        assertElementText('Step 1: Select the root data type to be inserted.', stepExplanationLocator)
    }

    @Override
    List<By> getComponentLocators() {
        [By.name('form1')]
    }

    SpreadsheetUploadLandingPage fillTitle(String title) {
        fill(titleLocator, title)
        this
    }

    SpreadsheetUploadLandingPage selectDataType(DataType dataType) {
        exceptionSafeSelect(dataTypeLocator, dataType.xsiType)
        setCurrentDataType(dataType)
        this
    }

    SpreadsheetUploadStep2Page submit() {
        exceptionSafeClick(submitButtonLocator)
        loadPage(SpreadsheetUploadStep2Page).withDataType(currentDataType)
    }

}
