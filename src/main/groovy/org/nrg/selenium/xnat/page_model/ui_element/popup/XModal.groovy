package org.nrg.selenium.xnat.page_model.ui_element.popup

import org.openqa.selenium.By

class XModal implements XnatPopup<XModal> {

    @Override
    By getPopupLocatorHook() {
        By.xpath("./ancestor-or-self::div[contains(@class, 'xmodal v1')]")
    }

    @Override
    By getTitleLocator() {
        By.xpath("./div[@class='title']/span")
    }

    @Override
    By getBodyLocator() {
        By.xpath("./div[@class='body content scroll' or @class='body content']")
    }
    
}
