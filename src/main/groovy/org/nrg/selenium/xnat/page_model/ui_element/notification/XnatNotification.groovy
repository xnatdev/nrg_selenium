package org.nrg.selenium.xnat.page_model.ui_element.notification

import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.TextAssertable
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.openqa.selenium.By
import org.testng.AssertJUnit

import static org.testng.AssertJUnit.assertEquals

trait XnatNotification<X extends XnatNotification<X>> implements XnatPageComponent, ScreenshotableComponent, TextAssertable<X> {

    @Override
    void performInitialValidation() {
        waitForElement(locator)
        assertElementPresent(locator)
    }

    @Override
    List<By> getComponentLocators() {
        [locator]
    }

    abstract By getLocator()

    @Override
    String getText() {
        exceptionSafeGetText(locator)
    }

}