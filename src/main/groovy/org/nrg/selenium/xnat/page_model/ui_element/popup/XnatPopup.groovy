package org.nrg.selenium.xnat.page_model.ui_element.popup

import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.TextAssertable
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.openqa.selenium.By
import org.openqa.selenium.support.pagefactory.ByChained

trait XnatPopup<X extends XnatPopup<X>> implements XnatPageComponent, ScreenshotableComponent<X>, TextAssertable<X> {

    By popupObjectLocator

    @Override
    List<By> getComponentLocators() {
        [popupObjectLocator]
    }

    X locatedBy(By popupLocator) {
        popupObjectLocator = new ByChained(popupLocator, popupLocatorHook)
        waitForElement(popupObjectLocator)
        this as X
    }

    X locatedBy(PopupLocator popupLocator) {
        locatedBy(popupLocator.by())
    }

    X locatedBy(String partialText) {
        locatedBy(new PartialTextPopupLocator(partialText))
    }

    By getSubElementLocator(By subElement) {
        new ByChained(popupObjectLocator, subElement)
    }

    String getPopupTitle() {
        exceptionSafeGetText(getSubElementLocator(titleLocator))
    }

    String getBodyText() {
        exceptionSafeGetText(getSubElementLocator(bodyLocator))
    }

    X clickSubelement(By subElement) {
        exceptionSafeClick(getSubElementLocator(subElement))
        this as X
    }

    X clickOK() {
        clickSubelement(locators.okSubbuttons)
    }

    X clickCancel() {
        clickSubelement(locators.cancelCloseSubbutton)
    }

    X waitUntilTextNonempty() {
        exceptionSafeGetText(getSubElementLocator(bodyLocator), false)
        this as X
    }

    @Override
    String getText() {
        bodyText
    }

    abstract By getPopupLocatorHook()

    abstract By getTitleLocator()

    abstract By getBodyLocator()

}
