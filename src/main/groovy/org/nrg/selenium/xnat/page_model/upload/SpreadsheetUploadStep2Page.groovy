package org.nrg.selenium.xnat.page_model.upload

import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.xnat.pogo.DataType
import org.openqa.selenium.By

class SpreadsheetUploadStep2Page implements NavigablePageObject<SpreadsheetUploadStep2Page>, ScreenshotableComponent<SpreadsheetUploadStep2Page> {

    DataType rootDataType

    @Override
    void performInitialValidation() {
        waitForElement(componentLocators[0])
    }

    @Override
    List<By> getComponentLocators() {
        [By.name('form1')]
    }

    SpreadsheetUploadStep2Page withDataType(DataType dataType) {
        setRootDataType(dataType)
        this
    }

    SpreadsheetUploadStep2Page selectRelativeFieldExtension(String relativeField, String extensionDataType) {
        exceptionSafeSelect(fieldDropdownLocator(relativeField), extensionDataType)
        this
    }

    SpreadsheetUploadStep2Page checkRelativeFieldBox(String relativeField) {
        performCheckBox(fieldCheckboxLocator(relativeField), true)
        this
    }

    SpreadsheetUploadStep2Page checkRelativeFieldBoxes(List<String> relativeFields) {
        relativeFields.each { checkRelativeFieldBox(it) }
        this
    }

    SpreadsheetUploadStep2Page checkRelativeFieldExtensionBox(String extension, String relativeField) {
        performCheckBox(fieldCheckboxLocator("${extension}/${relativeField}"), true)
        this
    }

    SpreadsheetUploadStep2Page checkRelativeFieldExtensionBoxes(String extension, List<String> relativeFields) {
        relativeFields.each { checkRelativeFieldExtensionBox(extension, it) }
        this
    }

    SpreadsheetUploadStep3Page submit() {
        submitForm()
        loadPage(SpreadsheetUploadStep3Page)
    }

    protected By fieldDropdownLocator(String relativeField) {
        By.id("${rootDataType.xsiType}/${relativeField}SEL")
    }

    protected By fieldCheckboxLocator(String relativeField) {
        By.xpath("//label[text()='${rootDataType.xsiType}/${relativeField}']/preceding-sibling::input")
    }

}
