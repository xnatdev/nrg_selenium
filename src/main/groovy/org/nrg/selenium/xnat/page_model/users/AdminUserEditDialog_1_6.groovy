package org.nrg.selenium.xnat.page_model.users

import groovy.util.logging.Log4j
import org.nrg.selenium.CustomConditions
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.xnat.versions.*
import org.nrg.xnat.pogo.users.User
import org.openqa.selenium.By

@Log4j
class AdminUserEditDialog_1_6 extends AdminUserEditDialog_1_7_4 {

    protected By dialogScreenshotLocator = By.id('layout_content') // this is OK since we are within the iframe
    protected By doneButtonLocator = XnatLocators.DONE_BUTTON

    AdminUserEditDialog_1_6() {
        userRoleEditSaveButtonLocator = By.xpath("//input[@onclick='XNAT.app.userRoles.update();']")
    }

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev]
    }

    @Override
    List<By> getComponentLocators() {
        [dialogScreenshotLocator]
    }

    @Override
    void performInitialValidation() {
        switchToNewIframe()
        waitForElementVisible(usernameLocator())
    }

    @Override
    AdminUserEditDialog_1_6 save() {
        submitForm()
        this
    }

    @Override
    protected By userScreenshotElement(User user) {
        By.id('subjectSummary')
    }

    @Override
    AdminUserEditDialog viewAdvancedSettings() {
        this
    }

    @Override
    AdminUserEditDialog closeAdvancedSettings() {
        this
    }

    @Override
    void performNavigationToUnderlyingPage() {
        exceptionSafeClick(doneButtonLocator)
        leaveIframe()
    }

    @Override
    AdminUsersPage createUser(User user) {
        capture()
        fillCommonFields(user)
        save()
        logUser(user)
        if (user.isAdmin()) {
            wait.until(CustomConditions.elementIsDisplayed(siteAdminLocator))
            exceptionSafeClick(siteAdminLocator)
            exceptionSafeClick(allDataAdminLocator)
        }
        setEnabled(true)
        saveUserRoles()
        waitForElement(disableUserLinkLocator)
        // wait until the page has reloaded. e.g. Enabled text has been updated
        capture()
        user.enabled(true).verified(true)
        performNavigationToUnderlyingPage()
    }

}
