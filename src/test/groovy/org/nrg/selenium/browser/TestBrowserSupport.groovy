package org.nrg.selenium.browser

import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

import static org.testng.AssertJUnit.*

class TestBrowserSupport {

    @BeforeMethod
    void clearCache() {
        BrowserSupport.cachedBrowserSupport = null
    }

    @Test
    void testBrowserCaching() {
        setBrowserAndCheck('Chrome', ChromeSupport)
        setBrowserAndCheck('FF', FirefoxSupport)
        setBrowserAndCheck('Google Chrome', ChromeSupport)
    }

    @Test
    void testBrowserCachingUnknownBrowser() {
        try {
            BrowserSupport.cacheBrowser('MicrosoftEdge')
            fail('Attempting to cache unknown browser should have thrown exception')
        } catch (RuntimeException rte) {
            assertTrue(rte.message.contains('Unknown browser: MicrosoftEdge'))
            assertTrue(rte.message.contains('Chrome'))
            assertTrue(rte.message.contains('MozillaFirefox'))
        }
    }

    private void setBrowserAndCheck(String browserKey, Class<? extends BrowserSupport> browserSupportClass) {
        BrowserSupport.cacheBrowser(browserKey)
        assertTrue(BrowserSupport.cachedBrowserSupport.class == browserSupportClass)
        clearCache()
    }

}
