package org.nrg.selenium.xnat.page_model

import org.apache.commons.lang3.StringUtils
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import static org.testng.AssertJUnit.assertEquals
import static org.testng.AssertJUnit.assertTrue

class AuditTrailPage implements NavigablePageObject<AuditTrailPage>, ScreenshotableComponent<AuditTrailPage> {

    protected XpathLocator dataTable = XnatLocators.byId('auditTrail')
    protected String headersSublocator = './dl[@class="header"]//dd'
    protected int indexForEvent
    protected int indexForStatus
    protected int indexForAgent
    protected int indexForTimestamp
    protected final List<String> activeFilters = []
    protected static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern('uuuuMMddHHmmss')

    @Override
    List<By> getComponentLocators() {
        [dataTable]
    }

    @Override
    void performInitialValidation() {
        waitForElement(dataTable)
        final List<String> columns = exceptionSafeGetTextList(dataTable.joinSublocator(headersSublocator))
        indexForEvent = columns.indexOf('Event')
        indexForStatus = columns.indexOf('Status')
        indexForAgent = columns.indexOf('Agent')
        indexForTimestamp = columns.indexOf('Timestamp')
        discardFilters()
    }

    AuditTrailPage discardFilters() {
        activeFilters.clear()
        activeFilters << 'contains(@class, "auditTrailItem")'
        this
    }

    AuditTrailPage filterEntriesSince(LocalDateTime filterTime) {
        activeFilters << columnWithIndexSatisfiesCondition(indexForTimestamp, "translate(text(), ' -:', '') >= ${DATE_TIME_FORMATTER.format(filterTime)}")
        this
    }

    AuditTrailPage assertNumberOfEvents(int numEventsExpected) {
        assertEquals(numEventsExpected, findElements(rowsMatchingActiveFiltersLocator()).size())
        this
    }

    List<String> readStatuses() {
        readColumnByIndex(indexForStatus)
    }

    AuditTrailPage assertAllStatuses(String expectedStatus) {
        readStatuses().each { status ->
            assertEquals(expectedStatus, status)
        }
        this
    }

    List<String> readAgents() {
        readColumnByIndex(indexForAgent)
    }

    AuditTrailPage assertAllAgents(String expectedAgent) {
        readAgents().each { agent ->
            assertEquals(expectedAgent, agent)
        }
        this
    }

    List<String> readEvents() {
        readColumnByIndex(indexForEvent).collect { event ->
            StringUtils.removeEnd(event, ' (details)')
        }
    }

    AuditTrailPage assertEventContains(String expectedEvents) {
        assertTrue(readEvents().contains(expectedEvents))
        this
    }

    protected List<String> readColumnByIndex(int javaColumnIndex) {
        exceptionSafeGetTextList(rowsMatchingActiveFiltersLocator().joinSublocator(columnWithIndex(javaColumnIndex)))
    }

    protected XpathLocator rowsMatchingActiveFiltersLocator() {
        dataTable.joinSublocator("./dl[${activeFilters.join(' and ')}]")
    }

    protected String columnWithIndex(int javaColumnIndex) {
        ".//dd[${javaColumnIndex + 1}]"
    }

    protected String columnWithIndexSatisfiesCondition(int javaColumnIndex, String condition) {
        "${columnWithIndex(javaColumnIndex)}[${condition}]"
    }

}
