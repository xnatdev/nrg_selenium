package org.nrg.selenium.xnat.page_model.project.delete

import org.nrg.selenium.xnat.page_model.IndexPage
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatDeleteComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage
import org.nrg.selenium.xnat.page_model.ui_element.HasBreadcrumbs
import org.openqa.selenium.By

class ProjectDeletePage implements NavigablePageObject<ProjectDeletePage>, ScreenshotableComponent<ProjectDeletePage>, XnatDeleteComponent, HasBreadcrumbs {

    protected By deleteEntireProjectLocator = By.xpath("//span[text()='Delete Entire Project']/../..//div[@class='ygtvspacer']")
    protected By deleteArbitrarySubjectLocator = By.xpath("//span[contains(text(), 'Subject: ')]/../preceding-sibling::td/div[@class='ygtvspacer']")
    protected By deleteButtonLocator = XnatLocators.DELETE_BUTTON
    protected By okButtonLocator = XnatLocators.buttonWithText('OK')

    @Override
    List<By> getComponentLocators() {
        readBreadcrumbs().componentLocators + [By.name('form1')]
    }

    IndexPage delete(int waitTolerance, boolean takeAllScreenshots) {
        if (takeAllScreenshots) {
            capture()
        }
        exceptionSafeClick(deleteEntireProjectLocator)
        if (takeAllScreenshots) {
            capture()
        }
        submitForm()
        if (takeAllScreenshots) {
            readXModal('Submission will cause').capture()
        }
        exceptionSafeClick(okButtonLocator)
        if (takeAllScreenshots) {
            captureStep(deletionDialogLocator)
        }
        performDeletionRequest(waitTolerance)
        loadPage(IndexPage)
    }

    ProjectDeletePage clearProjectArchive() {
        waitForElement(deleteArbitrarySubjectLocator)
        clickAllResults(deleteArbitrarySubjectLocator)
        submitForm()
        clickOKButton()
        performDeletionRequest(120)
        loadPage(ProjectDeletePage)
    }

    protected void performDeletionRequest(int waitTolerance) {
        waitForElement(deleteButtonLocator)
        exceptionSafeClick(deleteButtonLocator)
        setTimeout(waitTolerance)
        handleSuccessDialog()
        restoreTimeout()
    }

    protected void handleSuccessDialog() {
        readXnatDialog('successfully deleted').assertTextEquals('All items were successfully deleted.').capture().clickOK()
    }

}
