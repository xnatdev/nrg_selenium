package org.nrg.selenium.xnat.page_model

import org.nrg.xnat.versions.*
import org.testng.annotations.Test

import static org.testng.AssertJUnit.assertEquals

class TestPageObjects {

    @Test
    void testPageObjectLookup() {
        assertEquals(DummyPageComponent, lookup(Xnat_1_7_7))
        assertEquals(DummyPageComponent, lookup(Xnat_1_7_5_2))
        assertEquals(DummyPageComponent1_7_5, lookup(Xnat_1_7_5))
        assertEquals(DummyPageComponent1_7_5, lookup(Xnat_1_7_4))
        assertEquals(DummyPageComponent1_7_3, lookup(Xnat_1_7_3))
        assertEquals(DummyPageComponent1_7_3, lookup(Xnat_1_7_2))
        assertEquals(DummyPageComponent1_6, lookup(Xnat_1_6dev))
    }

    private Class<DummyPageComponent> lookup(Class<? extends XnatVersion> versionClass) {
        PageObjectRegistry.getPageObject(DummyPageComponent, versionClass) as Class<DummyPageComponent>
    }

    private class DummyPageComponent implements XnatPageComponent {}

    private class DummyPageComponent1_7_5 extends DummyPageComponent {
        @Override
        List<Class<? extends XnatVersion>> getHandledVersions() {
            [Xnat_1_7_4, Xnat_1_7_5]
        }
    }

    private class DummyPageComponent1_7_3 extends DummyPageComponent1_7_5 {
        @Override
        List<Class<? extends XnatVersion>> getHandledVersions() {
            [Xnat_1_7_2, Xnat_1_7_3]
        }
    }

    private class DummyPageComponent1_6 extends DummyPageComponent1_7_3 {
        @Override
        List<Class<? extends XnatVersion>> getHandledVersions() {
            [Xnat_1_6dev]
        }
    }

}
