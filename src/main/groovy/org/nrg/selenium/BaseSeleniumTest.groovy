package org.nrg.selenium

import org.nrg.selenium.annotations.DisableWebDriver
import org.nrg.selenium.extension.WebDriverExtension
import org.nrg.selenium.listeners.adapters.SeleniumTestListener

import org.nrg.selenium.xnat.XnatDriver
import org.nrg.selenium.xnat.page_model.DefaultTablePage
import org.nrg.selenium.xnat.page_model.XnatLogoutPage
import org.nrg.selenium.xnat.page_model.defaults.XnatDefaults
import org.nrg.selenium.xnat.page_model.error.GenericRestletError
import org.nrg.selenium.xnat.page_model.error.GenericTomcatError
import org.nrg.selenium.xnat.page_model.SyntheticPageObject
import org.nrg.selenium.xnat.page_model.XnatLoginPage
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.GeneralXnatErrorPage
import org.nrg.selenium.xnat.page_model.users.AdminUsersPage
import org.nrg.selenium.xnat.versions.XnatDriverList
import org.nrg.testing.xnat.BaseXnatRestTest
import org.nrg.testing.xnat.Users
import org.nrg.testing.xnat.conf.Settings
import org.nrg.testing.xnat.conf.XnatConfig
import org.nrg.xnat.pogo.users.User
import org.openqa.selenium.WebDriver
import org.openqa.selenium.net.HostIdentifier
import org.openqa.selenium.support.ui.WebDriverWait
import org.testng.ITestContext
import org.testng.annotations.BeforeMethod
import org.testng.annotations.BeforeSuite
import org.testng.annotations.Listeners

import java.lang.reflect.Method

@Listeners(SeleniumTestListener)
class BaseSeleniumTest extends BaseXnatRestTest {

    @Delegate WebDriver driver
    @Delegate(excludes = ['setTestController']) XnatDriver xnatDriver
    @Delegate WebDriverExtension extension // TODO: why isn't this being pulled in to subclasses through @Delegate recursion?
    protected WebDriverWait wait
    protected XnatLoginPage loginPage
    protected long testStartTime

    @BeforeSuite
    void setupSeleniumTests(ITestContext testContext) {
        HostIdentifier.getHostName() // cache earlier to avoid causing NoClassDefFoundError mayhem later (why??)
        XnatDriverList.readXnatVersions()
        XnatDriver.getInstance(true, null, null).customize()
        constructXnatDriver(true, Settings.DEFAULT_XNAT_CONFIG)
    }

    @BeforeMethod(alwaysRun = true)
    void setUpSeleniumTest(Method m, ITestContext testContext) {
        testStartTime = System.currentTimeMillis()
        final boolean suppressWebDriver = methodContainsAnnotation(m, DisableWebDriver)
        constructXnatDriver(suppressWebDriver, null)
        setXnatDriverProperties(m)
        if (!suppressWebDriver) {
            navigateToLoginPage()
        }
    }

    void setXnatDriverProperties(Method m) {
        xnatDriver.setTestClass(m.declaringClass.simpleName)
        xnatDriver.setTestName(m.name)
    }

    def <X extends XnatPageComponent> X loadPage(Class<X> pageClass) {
        new SyntheticPageObject(xnatDriver).loadPage(pageClass)
    }

    XnatLoginPage navigateToLoginPage() {
        loginPage = new SyntheticPageObject(xnatDriver).loadLoginPage()
    }

    GenericRestletError readRestletErrorPage() {
        loadPage(GenericRestletError)
    }

    GenericTomcatError readTomcatErrorPage() {
        loadPage(GenericTomcatError)
    }

    GeneralXnatErrorPage readXnatErrorPage() {
        loadPage(GeneralXnatErrorPage)
    }

    DefaultTablePage readDefaultTablePage() {
        loadPage(DefaultTablePage)
    }

    XnatDefaults getDefaults() {
        loginPage.defaults
    }

    XnatLogoutPage logLogout() {
        new SyntheticPageObject(xnatDriver).logLogout()
    }

    void waitForSessionRebuilder(int minutes = 5) {
        new SyntheticPageObject(xnatDriver).waitForSessionRebuilder(minutes)
    }

    protected void constructXnatDriver(boolean suppressWebDriver, XnatConfig xnatConfig) {
        if (driver != null) {
            driver.quit()
        } // only 1 WebDriver allowed at a time!

        xnatDriver = XnatDriver.getInstance(suppressWebDriver, xnatConfig, restDriver)

        if (!suppressWebDriver) {
            driver = xnatDriver.driver
            wait = xnatDriver.wait
        }

        xnatDriver.setTestController(testController)
        restDriver.setTestController(testController)
        if (testController.isTestRunning()) {
            xnatDriver.setStepCounter(stepCounter)
            xnatDriver.setTestClass(testController.testClass)
            xnatDriver.setTestName(testController.testName)
        }
        extension = xnatDriver.driverExtension
    }

    protected void constructXnatDriver(XnatConfig xnatConfig) {
        constructXnatDriver(false, xnatConfig)
    }

    protected void constructXnatDriver() {
        constructXnatDriver(false, null)
    }

    @Override
    protected void setupXnat() {
        constructXnatDriver()
        xnatDriver.setupXnat()
        if (driver != null) {
            driver.quit()
        }
    }

    @Override
    protected List<User> createGenericUsers(int numUsers) {
        constructXnatDriver()
        final AdminUsersPage usersPage = navigateToLoginPage().seleniumAdminLogin().loadAdminUsersPage()
        final List<User> users = (1 .. numUsers).collect {
            final User user = Users.genericAccount()
            usersPage.createUser(user)
            user
        }
        usersPage.logout()
        driver.quit()
        users
    }

}
