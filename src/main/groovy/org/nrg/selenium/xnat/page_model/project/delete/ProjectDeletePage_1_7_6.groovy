package org.nrg.selenium.xnat.page_model.project.delete

import org.nrg.xnat.versions.*

class ProjectDeletePage_1_7_6 extends ProjectDeletePage {

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev, Xnat_1_7_2, Xnat_1_7_3, Xnat_1_7_4, Xnat_1_7_5, Xnat_1_7_5_2, Xnat_1_7_6]
    }

    @Override
    protected void handleSuccessDialog() {
        readXModal('successfully deleted').assertTextEquals('All items were successfully deleted.').capture().clickOK()
    }

}
