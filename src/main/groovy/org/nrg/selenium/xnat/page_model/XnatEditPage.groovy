package org.nrg.selenium.xnat.page_model

import org.nrg.selenium.xnat.page_model.report.XnatReportPage
import org.nrg.testing.xnat.versions.XnatTestingVersionManager
import org.nrg.xnat.pogo.Extensible
import org.nrg.xnat.versions.Xnat_1_8_10

trait XnatEditPage<
            T extends Extensible<T>,
            X extends NavigablePageObject<X>,
            Y extends XnatReportPage<T, Y>
        > extends NavigablePageObject<X> implements LoadablePage {

    abstract void populateFields(T xnatObject, boolean takeAllScreenshots)

    abstract Class<XnatReportPage> getAssociatedReportClass()

    Y create(T xnatObject, boolean takeAllScreenshots = false) {
        populateFields(xnatObject, takeAllScreenshots)
        submitAndValidate(xnatObject)
    }

    X submit() {
        submitForm()
        this as X
    }

    Y submitAndValidate(T xnatObject) {
        submit()
        final XnatReportPage<T, Y> reportPage = loadPage(associatedReportClass)
        reportPage.assertPageRepresents(xnatObject)
    }

    boolean postArchiveAnonEnabled() {
        XnatTestingVersionManager.testedVersionPrecedes(Xnat_1_8_10.class) || restDriver.interfaceFor(restDriver.mainAdminUser).readPostArchiveAnonStatus()
    }

}
