package org.nrg.selenium.xnat.page_model.upload

import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.xnat.pogo.Project
import org.openqa.selenium.By

class SpreadsheetUploadStep3Page implements NavigablePageObject<SpreadsheetUploadStep3Page>, ScreenshotableComponent<SpreadsheetUploadStep3Page> {

    protected By csvFileLocator = By.name('csv_to_store')
    protected By uploadButtonLocator = By.name('eventSubmit_doUpload')
    protected By projectLocator = By.name('project')
    protected By projectScreenshotLocator = By.xpath('//table[./tbody/tr/td[text()="Project"]]') // more complex locator than previously, but supports refactor in XNAT 1.8.8
    protected By submitButtonLocator = By.name('eventSubmit_doProcess')

    @Override
    List<By> getComponentLocators() {
        [By.xpath('//table[@bgcolor]')]
    }

    @Override
    void performInitialValidation() {
        waitForElement(componentLocators[0])
    }

    SpreadsheetUploadStep3Page uploadCsvFile(File file) {
        exceptionSafeFind(csvFileLocator).sendKeys(file.absolutePath)
        capture()
        exceptionSafeClick(uploadButtonLocator)
        this
    }

    SpreadsheetUploadPreview readPreviewTable() {
        loadPage(SpreadsheetUploadPreview).withUnderlyingPage(this)
    }

    SpreadsheetUploadStep3Page selectProject(Project project) {
        exceptionSafeSelect(projectLocator, project.runningTitle)
        this
    }

    SpreadsheetUploadStep3Page captureProject() {
        captureStep(projectScreenshotLocator)
        this
    }

    SpreadsheetUploadReviewPage submit() {
        exceptionSafeClick(submitButtonLocator)
        loadPage(SpreadsheetUploadReviewPage)
    }

}
