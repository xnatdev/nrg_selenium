package org.nrg.selenium.processing.element_comparators.comparator

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import org.nrg.selenium.processing.element_comparators.Element
import org.nrg.testing.xnat.processing.exceptions.ProcessingValidationException
import org.nrg.selenium.xnat.XnatDriver
import org.openqa.selenium.By

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = 'type'
)
@JsonSubTypes([
        @JsonSubTypes.Type(value = EqualityElementComparator, name = 'Equals'),
        @JsonSubTypes.Type(value = MaxPercentToleranceElementComparator, name = 'maxPercentTolerance')
])
abstract class ElementComparator {

    List<Element> elements = []

    abstract void checkPointsMatch(XnatDriver seleniumUtils, By locator, String expectedValue) throws ProcessingValidationException

}
