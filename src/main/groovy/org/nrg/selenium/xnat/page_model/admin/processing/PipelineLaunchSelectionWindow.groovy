package org.nrg.selenium.xnat.page_model.admin.processing

import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.openqa.selenium.By

class PipelineLaunchSelectionWindow implements XnatPageComponent, ScreenshotableComponent<PipelineLaunchSelectionWindow> {

    protected By submitButtonLocator = By.name('eventSubmit_doRedirect')

    @Override
    List<By> getComponentLocators() {
        [By.id('form1')]
    }

    @Override
    void performInitialValidation() {
        switchToNewWindow()
    }

    PipelineLaunchSelectionWindow selectPipeline(String pipelineName) {
        exceptionSafeClick(pipelineElementLocator(pipelineName))
        this
    }

    GenericPipelineLaunchWizard submit() {
        exceptionSafeClick(submitButtonLocator)
        loadPage(GenericPipelineLaunchWizard)
    }

    protected By pipelineElementLocator(String pipelineName) {
        By.xpath("//td[normalize-space(text())='${pipelineName}']/input")
    }

}
