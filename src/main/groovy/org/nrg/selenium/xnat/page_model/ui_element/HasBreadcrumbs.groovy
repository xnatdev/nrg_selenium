package org.nrg.selenium.xnat.page_model.ui_element

import org.nrg.selenium.xnat.page_model.PageObjectSupport

trait HasBreadcrumbs implements PageObjectSupport {

    BreadcrumbSection readBreadcrumbs() {
        loadPage(BreadcrumbSection, false)
    }

}
