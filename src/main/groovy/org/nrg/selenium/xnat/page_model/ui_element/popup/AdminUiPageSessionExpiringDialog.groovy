package org.nrg.selenium.xnat.page_model.ui_element.popup

import org.nrg.selenium.xnat.page_model.IndexPage
import org.nrg.selenium.xnat.page_model.admin.AdminUiPage

class AdminUiPageSessionExpiringDialog implements SessionExpiringDialog<AdminUiPage> {}
