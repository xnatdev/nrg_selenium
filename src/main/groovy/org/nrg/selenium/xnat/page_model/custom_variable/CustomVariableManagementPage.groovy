package org.nrg.selenium.xnat.page_model.custom_variable

import org.nrg.selenium.CustomConditions
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage
import org.nrg.selenium.xnat.page_model.ui_element.HtmlTable
import org.nrg.xnat.enums.VariableType
import org.nrg.xnat.pogo.custom_variable.CustomVariable
import org.nrg.xnat.pogo.custom_variable.CustomVariableSet
import org.openqa.selenium.By
import org.openqa.selenium.support.ui.ExpectedConditions

class CustomVariableManagementPage implements NavigablePageObject<CustomVariableManagementPage>, HtmlTable<CustomVariableManagementPage> {

    protected By dataTypeLocator = By.id('protocols_sel')
    protected By createSetLinkLocator = By.xpath("//div[text()='Add a custom variable set']")
    protected By setNameLocator = By.name('_ID')
    protected By projectSpecificLocator = By.cssSelector('label > input[type=checkbox]')
    protected By descriptionLocator = By.name('description')
    protected XpathLocator lastVariableNameLocator = new XpathLocator("//td/input[@type='text']").lastInstance()
    protected XpathLocator lastVariableRowLocator = lastVariableNameLocator.grandparent()
    protected By lastVariableTypeLocator = lastVariableRowLocator.chain(XnatLocators.SELECT_TAG)
    protected XpathLocator lastVariableRequiredLocator = lastVariableRowLocator.joinSublocator(".//input[@type='checkbox']")
    protected XpathLocator lastVariablePossibleValuesButtonLocator = lastVariableRowLocator.joinSublocator(".//input[@type='button']")
    protected By lastVariableLastPossibleValueLocator = lastVariableRowLocator.chain(new XpathLocator(".//div/input[@type='text']").lastInstance())
    protected XpathLocator variableTableLocator = new XpathLocator("//th[text()='Required']").joinSublocator(XnatLocators.nearestAncestorByTag('table'))
    protected XpathLocator variableRowCountLocator = variableTableLocator.joinSublocator('./tbody/tr')
    protected By addVariableButtonLocator = By.xpath("//input[@value='Add Variable']")
    protected List<By> variableSetMetadataScreenshotElements = [By.xpath("//th[text()='Custom Variable Set']"), By.xpath("//td[contains(text(), 'Please list')]")]
    protected By saveButtonLocator = By.xpath("//input[@value='Save']")
    protected By doneButtonLocator = XnatLocators.byText('Done') // element is an <input> in XNAT version < 1.7.5, but a proper <button> in version >= 1.7.5

    @Override
    List<By> getComponentLocators() {
        [new XpathLocator("//div[text()='Project Protocol Management']").joinSublocator(XnatLocators.nearestAncestorByTag('table')), dataTypeLocator]
    }

    @Override
    XpathLocator getTableLocator() {
        variableTableLocator
    }

    @Override
    void performInitialValidation() {
        waitForElement(dataTypeLocator)
    }

    CustomVariableManagementPage fillVariableSetName(String setName) {
        fill(setNameLocator, setName)
        this
    }

    CustomVariableManagementPage setProjectSpecific(boolean isProjectSpecific) {
        performCheckBox(projectSpecificLocator, isProjectSpecific)
        this
    }

    CustomVariableManagementPage fillDescription(String description) {
        fill(descriptionLocator, description)
        this
    }

    CustomVariableManagementPage fillLastVariableName(String name) {
        fill(lastVariableNameLocator, name)
        this
    }

    CustomVariableManagementPage selectLastVariableType(VariableType variableType) {
        exceptionSafeSelect(lastVariableTypeLocator, variableType.toString())
        this
    }

    CustomVariableManagementPage setLastVariableRequired(boolean isRequired) {
        performCheckBox(lastVariableRequiredLocator, isRequired)
        this
    }

    CustomVariableManagementPage setLastVariableLastPossibleValue(Object value) {
        exceptionSafeClick(lastVariablePossibleValuesButtonLocator)
        fill(lastVariableLastPossibleValueLocator, String.valueOf(value))
        this
    }

    CustomVariableManagementPage createCustomVariableSet(CustomVariableSet variableSet) {
        exceptionSafeSelect(dataTypeLocator, variableSet.dataType.pluralName)
        waitForElement(createSetLinkLocator)
        uploadScreenshot()
        exceptionSafeClick(createSetLinkLocator)
        capture()
        fillVariableSetName(variableSet.name)
        setProjectSpecific(variableSet.isProjectSpecific)
        fillDescription(variableSet.description)
        variableSet.customVariables.each { variable ->
            addCustomVariable(variable)
        }
        uploadTargetedScreenshot(variableSetMetadataScreenshotElements)
        captureTable(8)
        exceptionSafeClick(saveButtonLocator)
        assertCustomVariableSetExists(variableSet)
        capture()
    }

    CustomVariableManagementPage addCustomVariable(CustomVariable customVariable) {
        fillLastVariableName(customVariable.name).selectLastVariableType(customVariable.type).setLastVariableRequired(customVariable.required)
        customVariable.possibleValues.each { value ->
            setLastVariableLastPossibleValue(value)
        }
        final int variableRows = findElements(variableRowCountLocator).size()
        exceptionSafeClick(addVariableButtonLocator)
        wait.until(ExpectedConditions.numberOfElementsToBe(variableRowCountLocator, variableRows + 1))
        this
    }

    CustomVariableManagementPage assertCustomVariableSetExists(CustomVariableSet variableSet) {
        final By locator = savedVariableSetLocator(variableSet)
        waitForElement(locator)
        wait.until(CustomConditions.elementClickable(locator))
        this
    }

    ProjectReportPage returnToProjectPage() {
        exceptionSafeClick(doneButtonLocator)
        loadPage(ProjectReportPage)
    }

    protected By savedVariableSetLocator(CustomVariableSet variableSet) {
        By.xpath("//div[text()='${variableSet.name}: ${variableSet.description}']")
    }

}
