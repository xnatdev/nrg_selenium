package org.nrg.selenium.enums

enum PrearchiveStatus {

    ERROR ('Error'),
    READY   ('Ready'),
    RECEIVING  ('Receiving')

    private final String status

    PrearchiveStatus(String status) {
        this.status = status
    }

    @Override
    String toString() {
        status
    }

}
