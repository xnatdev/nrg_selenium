package org.nrg.selenium.xnat.page_model.automation

import org.nrg.selenium.exceptions.DeprecatedXnatPageException
import org.openqa.selenium.By

class SiteEventsTab implements AutomationTab {

    protected By siteEventIdLocator = By.cssSelector('td.site-event-id')

    @Override
    By tabLocator() {
        throw new DeprecatedXnatPageException('1.7', 'dedicated site events tab')
    }

}
