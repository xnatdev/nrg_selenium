package org.nrg.selenium.enums

import org.nrg.xnat.pogo.DataType

import static org.testng.AssertJUnit.*

enum ResourceSettingsCategory {

    PROJECT (DataType.PROJECT, 'Project'),
    SUBJECT (DataType.SUBJECT, 'Subject') {
        @Override
        void assertXsiType(String observedValue) {
            assertTrue(observedValue in [xsiType, buttonText])
        }
    },
    SUBJECT_ASSESSOR (DataType.SUBJECT_ASSESSOR, 'Subject Assessor'),
    IMAGE_SESSION (DataType.IMAGE_SESSION, 'Image Sessions'),
    SCAN (DataType.IMAGE_SCAN, 'Scan'),
    IMAGE_ASSESSOR (DataType.IMAGE_ASSESSOR, 'Image Assessor')

    private final String xsiType
    private final String buttonText

    ResourceSettingsCategory(DataType dataType, String buttonText) {
        xsiType = dataType.xsiType
        this.buttonText = buttonText
    }

    String getXsiType() {
        xsiType
    }

    String getButtonText() {
        buttonText
    }

    void assertXsiType(String observedValue) {
        assertEquals(xsiType, observedValue)
    }

}