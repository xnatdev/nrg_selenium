package org.nrg.selenium.xnat.page_model.ui_element

import org.apache.commons.lang3.StringUtils
import org.nrg.selenium.extension.StandaloneXpathLocator
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.PageObjectSupport
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertEquals
import static org.testng.AssertJUnit.assertFalse
import static org.testng.AssertJUnit.assertTrue
import static org.testng.AssertJUnit.fail

/**
 * Represents an entire HTML <table> entity. All indices are indexed *STARTING FROM 0* to be consistent with Java at the expense of XPATH.
 * Therefore, {@link HtmlTable#readDataCellEntry(0, 0)} would return the first non-header cell.
 * @param <X> Class implementing this trait
 */
trait HtmlTable<X extends HtmlTable<X>> implements PageObjectSupport, ScreenshotableComponent<X> {

    XpathLocator dataCellSublocator = new XpathLocator('./td')

    /**
     * @return Should identify the correct HTML <table> element
     */
    abstract XpathLocator getTableLocator()

    @Override
    List<By> getComponentLocators() {
        [tableLocator]
    }

    StandaloneXpathLocator getArbitraryRowLocator() {
        tableLocator.unionSublocators([new XpathLocator(''), XnatLocators.byPossibleTagName(['tbody', 'thead'], true)]).joinSublocator('./tr')
    }

    StandaloneXpathLocator getArbitraryHeaderLocator() {
        arbitraryRowLocator.joinSublocator('./th')
    }

    StandaloneXpathLocator getArbitraryDataRowLocator() {
        tableLocator.unionSublocators([new XpathLocator(''), XnatLocators.TBODY_TAG]).joinSublocator('./tr[./td]')
    }

    XpathLocator getArbitraryDataCellLocator() {
        arbitraryDataRowLocator.joinSublocator(dataCellSublocator)
    }

    void clickDataCellLink(String linkText) {
        driverExtension.exceptionSafeClick(dataCellWithLink(linkText))
    }

    void clickDataCellLink(int columnIndex, int dataRowIndex) {
        driverExtension.exceptionSafeClick(specificDataCellLocator(columnIndex, dataRowIndex).joinSublocator('.//a'))
    }

    List<String> readHeaders() {
        driverExtension.exceptionSafeGetTextList(arbitraryHeaderLocator)*.trim()
    }

    Map<String, Integer> readHeaderIndices() {
        readHeaders().indexed().collectEntries { index, header ->
            StringUtils.isNotEmpty(header) ? [(header): index] : [:]
        } as Map<String, Integer>
    }

    String readDataCellEntry(int columnIndex, int dataRowIndex) {
        driverExtension.exceptionSafeGetText(specificDataCellLocator(columnIndex, dataRowIndex)).trim()
    }

    List<String> readColumn(int columnIndex, boolean includeHeader = true) {
        driverExtension.findElements(columnCellsLocator(columnIndex, includeHeader))*.text*.trim()
    }

    int countColumns() {
        readHeaders().size()
    }

    List<String> readEntriesForColumn(String columnName) {
        final int columnIndex = readHeaderIndices()[columnName]
        if (columnIndex == -1) {
            fail("Could not find column ${columnName} in HTML table.")
        }
        readColumn(columnIndex, false)
    }

    boolean columnContainsValue(int columnIndex, String expectedElement) {
        readColumn(columnIndex, true).contains(expectedElement)
    }

    List<String> readDataRow(int dataRowIndex) {
        driverExtension.findElements(dataRowCellsLocator(dataRowIndex))*.text*.trim()
    }

    boolean dataRowContainsValue(int dataRowIndex, String expectedElement) {
        readDataRow(dataRowIndex).contains(expectedElement)
    }

    int countDataRows() {
        driverExtension.findElements(arbitraryDataRowLocator).size()
    }

    List<List<String>> readAllDataRows() {
        (0 ..< countDataRows()).collect { index ->
            readDataRow(index)
        }
    }

    int countAllRows() {
        driverExtension.findElements(arbitraryRowLocator).size()
    }

    List<List<String>> readEntireTable() {
        [readHeaders()] + readAllDataRows()
    }

    X assertSpecificDataCell(int columnIndex, int dataRowIndex, String expectedValue) {
        assertEquals(expectedValue, readDataCellEntry(columnIndex, dataRowIndex))
        this as X
    }

    X assertSpecificDataCellByColumnName(String columnName, int dataRowIndex, String expectedValue) {
        assertSpecificDataCell(readHeaderIndices()[columnName], dataRowIndex, expectedValue)
    }

    X assertNumberOfDataRows(int numExpected) {
        assertEquals(numExpected, countDataRows())
        this as X
    }

    X assertNumberOfDataRowsIsAtLeast(int minimumAcceptableNumber) {
        assertTrue(countDataRows() >= minimumAcceptableNumber)
        this as X
    }

    X assertDataRowEquals(int dataRowIndex, List<String> expectedElements) {
        assertEquals(expectedElements, readDataRow(dataRowIndex))
        this as X
    }

    X assertDataRowContains(int dataRowIndex, String expectedElement) {
        assertTrue(dataRowContainsValue(dataRowIndex, expectedElement))
        this as X
    }

    X assertDataRowDoesNotContain(int dataRowIndex, String expectedElement) {
        assertFalse(dataRowContainsValue(dataRowIndex, expectedElement))
        this as X
    }

    X assertColumnContains(int columnIndex, String expectedElement) {
        assertTrue(columnContainsValue(columnIndex, expectedElement))
        this as X
    }

    X assertColumnDoesNotContain(int columnIndex, String expectedElement) {
        assertFalse(columnContainsValue(columnIndex, expectedElement))
        this as X
    }

    X assertAllDataRows(List<List<String>> expectedRows) {
        assertEquals(expectedRows, readAllDataRows())
        this as X
    }

    X assertEntireTable(List<List<String>> expectedTable) {
        assertEquals(expectedTable, readEntireTable())
        this as X
    }

    X uploadTable(int segmentSize) {
        final int numRows = countAllRows()
        final int numDivisions = Math.ceil(numRows / segmentSize).intValue()
        (0 ..< numDivisions).each { divisionIndex ->
            xnatDriver.uploadTargetedScreenshot(
                    (divisionIndex * segmentSize ..< Math.min((divisionIndex + 1) * segmentSize, numRows)).collect { rowIndex ->
                        rowLocator(rowIndex)
                    }
            )
        }
        this as X
    }

    X captureTable(int segmentSize) {
        uploadTable(segmentSize)
        xnatDriver.captureScreenshotlessStep()
        this as X
    }

    X captureLeadingRows(int numberOfDataRows) {
        xnatDriver.uploadTargetedScreenshot([rowLocator(0), rowLocator(numberOfDataRows)])
        this as X
    }

    XpathLocator columnCellsLocator(int columnIndex, boolean includeHeader) {
        final List<String> rowContainerNames = ['tbody']
        final List<String> elementTagNames = ['td']
        if (includeHeader) {
            rowContainerNames << 'thead'
            elementTagNames << 'th'
        }
        tableLocator.unionSublocators([
                new XpathLocator(''),
                XnatLocators.byPossibleTagName(rowContainerNames, true)
        ]).joinSublocator('./tr').joinSublocator(XnatLocators.byPossibleTagName(elementTagNames, true)).joinSublocator("[${columnIndex + 1}]")
    }

    XpathLocator rowLocator(int rowIndex) {
        arbitraryRowLocator.nthInstance(rowIndex + 1)
    }

    XpathLocator dataRowLocator(int dataRowIndex) {
        arbitraryDataRowLocator.nthInstance(dataRowIndex + 1)
    }

    XpathLocator dataRowCellsLocator(int dataRowIndex) {
        dataRowLocator(dataRowIndex).joinSublocator(dataCellSublocator)
    }

    XpathLocator specificDataCellLocator(int columnIndex, int dataRowIndex) {
        dataRowCellsLocator(dataRowIndex).nthInstance(columnIndex + 1)
    }

    XpathLocator dataCellWithLink(String linkText) {
        arbitraryDataCellLocator.joinSublocator(XnatLocators.tagWithText('a', linkText))
    }

}