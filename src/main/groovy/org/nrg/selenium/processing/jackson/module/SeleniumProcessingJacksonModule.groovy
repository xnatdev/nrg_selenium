package org.nrg.selenium.processing.jackson.module

import com.fasterxml.jackson.databind.module.SimpleModule
import org.nrg.selenium.processing.element_comparators.*
import org.nrg.selenium.processing.element_comparators.comparator.EqualityElementComparator

import org.nrg.selenium.processing.element_comparators.comparator.MaxPercentToleranceElementComparator
import org.nrg.selenium.processing.jackson.deserializer.*
import org.nrg.selenium.processing.jackson.serializer.ElementSerializer
import org.nrg.selenium.processing.jackson.serializer.EqualsComparatorSerializer
import org.nrg.selenium.processing.jackson.serializer.PercentComparatorSerializer

class SeleniumProcessingJacksonModule extends SimpleModule {

    SeleniumProcessingJacksonModule() {
        super('Selenium_Processing_Validation_(De)serializers')

        addDeserializer(Element, new ElementDeserializer())
        addDeserializer(EqualityElementComparator, new EqualityElementComparatorDeserializer())
        addDeserializer(MaxPercentToleranceElementComparator, new MaxPercentComparatorDeserializer())

        addSerializer(Element, new ElementSerializer())
        addSerializer(EqualityElementComparator, new EqualsComparatorSerializer())
        addSerializer(MaxPercentToleranceElementComparator, new PercentComparatorSerializer())
    }

}
