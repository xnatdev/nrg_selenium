package org.nrg.selenium.processing.jackson.deserializer

import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.selenium.processing.element_comparators.Element
import org.nrg.selenium.processing.element_comparators.comparator.ElementComparator
import org.nrg.xnat.jackson.deserializers.CustomDeserializer

abstract class ElementComparatorDeserializer<T extends ElementComparator> extends CustomDeserializer<T> {

    @Override
    T deserialize(ObjectCodec objectCodec, JsonNode jsonNode) {
        final T comparator = mainDeserialization(objectCodec, jsonNode)
        comparator.setElements(readObjectList(jsonNode, 'elements', objectCodec, Element))
        comparator
    }

    abstract T mainDeserialization(ObjectCodec objectCodec, JsonNode jsonNode)

}
