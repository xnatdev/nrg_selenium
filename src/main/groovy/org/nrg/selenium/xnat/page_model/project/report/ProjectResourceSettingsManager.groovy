package org.nrg.selenium.xnat.page_model.project.report

import org.nrg.selenium.enums.ResourceSettingsCategory
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.resource_settings.ProjectResourceSettingsConfig
import org.nrg.xnat.pogo.Project
import org.openqa.selenium.By

import static org.testng.AssertJUnit.*

class ProjectResourceSettingsManager implements XnatPageComponent, ScreenshotableComponent<ProjectResourceSettingsManager>, OverlayingPage<ProjectResourceSettingsManager, ProjectManageTab> {

    protected By closeButtonLocator = By.xpath(".//button[text()='Close' and not(@disabled)]")
    protected By titleEditLocator = By.id('pResource.name')
    protected By descriptionEditLocator = By.id('pResource.desc')
    protected By scriptEditLocator = By.id('pScriptSelect')
    protected By dataTypeEditLocator = By.id('pResource.type')
    protected By scanTypesEditLocator = By.id('pResource.filter')
    protected By levelEditLocator = By.id('pResource.level')
    protected By resourceFolderEditLocator = By.id('pResource.label')
    protected By subfolderEditLocator = By.id('pResource.subdir')
    protected By overwriteEditLocator = By.id('pResource.overwrite')
    protected By editSubmitButton = By.id('cruAddBut')
    protected XpathLocator resultTypeSublocator = new XpathLocator('.//dd[2]')
    protected XpathLocator resultNameSublocator = new XpathLocator('.//dd[3]')
    protected XpathLocator resultResourceFolderSublocator = new XpathLocator('.//dd[4]')
    protected XpathLocator resultSubfolderSublocator = new XpathLocator('.//dd[5]')
    protected XpathLocator resultOverwriteSublocator = new XpathLocator('.//dd[6]')
    protected XpathLocator resultOptionsSublocator = new XpathLocator('.//dd[10]')
    protected XpathLocator resultDescriptionSublocator = new XpathLocator(".//dd[./b[text()='Description:']]")
    protected XpathLocator resultScriptSublocator = new XpathLocator(".//dd[./b[contains(text(),'Script')]]")
    protected XpathLocator removeButtonSublocator = new XpathLocator('.//dd/button')
    
    @Override
    List<By> getComponentLocators() {
        [By.id('pResource_settings_dialog_c')]
    }

    @Override
    void performNavigationToUnderlyingPage() {
        exceptionSafeClick(closeButtonLocator)
    }

    String readTypeForSettingConfig(String configTitle) {
        exceptionSafeGetText(settingsReportRowLocator(configTitle).joinSublocator(resultTypeSublocator))
    }

    String readResourceFolderForSettingConfig(String configTitle) {
        exceptionSafeGetText(settingsReportRowLocator(configTitle).joinSublocator(resultResourceFolderSublocator))
    }

    String readSubfolderForSettingConfig(String configTitle) {
        exceptionSafeGetText(settingsReportRowLocator(configTitle).joinSublocator(resultSubfolderSublocator)).trim()
    }

    boolean readOverwriteForSettingConfig(String configTitle) {
        Boolean.parseBoolean(exceptionSafeGetText(settingsReportRowLocator(configTitle).joinSublocator(resultOverwriteSublocator)))
    }

    String readScriptSectionForSettingConfig(String configTitle) {
        exceptionSafeGetText(settingsReportRowLocator(configTitle).joinSublocator(resultScriptSublocator))
    }

    String readDescriptionForSettingConfig(String configTitle) {
        exceptionSafeGetText(settingsReportRowLocator(configTitle).joinSublocator(resultDescriptionSublocator))
    }

    String readOptionsForSettingConfig(String configTitle) {
        exceptionSafeGetText(settingsReportRowLocator(configTitle).joinSublocator(resultOptionsSublocator))
    }

    ProjectResourceSettingsManager assertResourceSettingMatches(ProjectResourceSettingsConfig expectedConfig) {
        final String expectedTitle = expectedConfig.title
        final String type = readTypeForSettingConfig(expectedTitle)
        if (expectedConfig.dataType) {
            assertEquals(expectedConfig.dataType, type)
        } else {
            expectedConfig.category.assertXsiType(type)
        }
        assertElementText(expectedTitle, settingsReportRowLocator(expectedTitle).joinSublocator(resultNameSublocator))
        assertEquals(expectedConfig.resourceFolder, readResourceFolderForSettingConfig(expectedTitle))
        if (expectedConfig.subfolder) {
            assertEquals(expectedConfig.subfolder, readSubfolderForSettingConfig(expectedTitle))
        }
        assertEquals(expectedConfig.overwrite, readOverwriteForSettingConfig(expectedTitle))
        if (expectedConfig.script) {
            assertEquals("Script to run upon upload completion: ${expectedConfig.script}", readScriptSectionForSettingConfig(expectedTitle))
        }
        if (expectedConfig.description) {
            assertEquals("Description: ${expectedConfig.description}", readDescriptionForSettingConfig(expectedTitle))
        }
        final String resultOptions = readOptionsForSettingConfig(expectedTitle)
        if (expectedConfig.scanTypes) {
            assertEquals(expectedConfig.scanTypes, resultOptions)
        } else if (expectedConfig.level) {
            assertEquals(expectedConfig.level.assertionName, resultOptions)
        }
        capture()
    }

    ProjectResourceSettingsManager setupNewResourceSetting(ProjectResourceSettingsConfig config) {
        capture()
        if (config.category == null) {
            fail('Category variable must be set in config object to setup project resource settings')
        }
        exceptionSafeClick(categoryButtonLocator(config.category))
        fill(titleEditLocator, config.title)
        fill(descriptionEditLocator, config.description)
        exceptionSafeSelect(scriptEditLocator, config.script)
        exceptionSafeSelect(dataTypeEditLocator, config.dataType)
        fill(levelEditLocator, config.level?.selectionName)
        fill(scanTypesEditLocator, config.scanTypes)
        fill(resourceFolderEditLocator, config.resourceFolder)
        fill(subfolderEditLocator, config.subfolder)
        performCheckBox(overwriteEditLocator, config.overwrite)
        uploadScreenshot()
        exceptionSafeClick(editSubmitButton)
        capture()
        assertResourceSettingMatches(config)
    }

    ProjectResourceSettingsManager deleteResourceSetting(String title) {
        final XpathLocator settingRow = settingsReportRowLocator(title)
        exceptionSafeClick(settingRow.joinSublocator(removeButtonSublocator))
        waitForNotElement(settingRow)
        capture()
    }

    protected By categoryButtonLocator(ResourceSettingsCategory category) {
        XnatLocators.tagWithText('input', category.buttonText)
        By.xpath("//input[@value='${category.buttonText}']")
    }

    protected XpathLocator settingsReportRowLocator(String configTitle) {
        XnatLocators.tagWithText('dd', configTitle).joinSublocator(XnatLocators.nearestAncestorByTag('dl'))
    }

}
