package org.nrg.selenium.xnat.page_model.custom_variable

import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.ui_element.popup.XModal
import org.nrg.xnat.enums.VariableType
import org.nrg.xnat.pogo.custom_variable.CustomVariable
import org.nrg.xnat.pogo.custom_variable.CustomVariableSet
import org.openqa.selenium.By

class CustomVariableMissingRequiredDialog implements XnatPageComponent {

    protected XModal xModal
    CustomVariableEditSection editSection

    @Override
    void performInitialValidation() {
        xModal = readXModal('Missing required').assertTextContains('Missing required variable values:')
    }

    CustomVariableMissingRequiredDialog assertVariableMissing(CustomVariable customVariable) {
        assertElementPresent(xModal.getSubElementLocator(missingVariableSublocator(customVariable)))
        this
    }

    CustomVariableMissingRequiredDialog assertAllVariablesRequired(CustomVariableSet variableSet) {
        variableSet.customVariables.each { customVariable ->
            if (customVariable.type != VariableType.BOOLEAN) {
                assertVariableMissing(customVariable)
            }
        }
        xModal.captureStep()
        this
    }

    CustomVariableEditSection closeAndReturn() {
        xModal.clickOK()
        editSection
    }

    protected By missingVariableSublocator(CustomVariable customVariable) {
        By.xpath(".//span[text() = '${customVariable.name}']")
    }

}
