package org.nrg.selenium.xnat.page_model.ui_element.activity

import org.nrg.selenium.enums.ActivityTabStatus
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.TextAssertable
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By

class ActivityTabItem implements XnatPageComponent, TextAssertable<ActivityTabItem> {

    protected XpathLocator actionsSublocator = new XpathLocator('./div[@class="actions"]')
    protected XpathLocator dismissSublocator = actionsSublocator.joinSublocator('./a[@class="icn close"]')
    protected XpathLocator detailsSublocator = actionsSublocator.joinSublocator('./a[@class="icn details"]')
    String itemId
    ActivityTab activityTab

    @Override
    String getText() {
        exceptionSafeGetText(getItemLocator())
    }

    ActivityTabItem withId(String id) {
        itemId = id
        this
    }

    ActivityTabItem withActivityTab(ActivityTab tab) {
        activityTab = tab
        this
    }

    ActivityTabStatus readStatus() {
        ActivityTabStatus.get(exceptionSafeGetAttribute(getItemLocator(), 'class'))
    }

    ActivityTab dismiss() {
        exceptionSafeClick(dismissLocator())
        activityTab
    }

    ActivityTabDetailsDialog expandDetails() {
        exceptionSafeClick(detailsLocator())
        loadPage(ActivityTabDetailsDialog).withId(itemId.substring(3))
    }

    protected XpathLocator getItemLocator() {
        XnatLocators.byId(itemId)
    }


    protected By dismissLocator() {
        getItemLocator().joinSublocator(dismissSublocator)
    }

    protected By detailsLocator() {
        getItemLocator().joinSublocator(detailsSublocator)
    }

}
