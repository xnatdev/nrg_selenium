package org.nrg.selenium.xnat.page_model.site_search

import org.nrg.xnat.versions.*

class AdvancedSearchPage2_1_7_6 extends AdvancedSearchPage2 {

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev, Xnat_1_7_2, Xnat_1_7_3, Xnat_1_7_4, Xnat_1_7_5, Xnat_1_7_5_2, Xnat_1_7_6]
    }

    @Override
    AdvancedSearchPage2 specifyTracer(String tracer) {
        selectCriterion(tracerIdentifier, tracer)
    }

}
