package org.nrg.selenium.xnat.page_model.users

import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatLoginPage
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By

class PasswordResetPage implements XnatPageComponent, ScreenshotableComponent<PasswordResetPage> {

    protected By formLocator = XnatLocators.FORM_TAG
    protected By passwordLocator = By.id('new_password')
    protected By passwordConfirmationLocator = By.id('confirm_password')
    protected By submitButtonLocator = By.xpath('//input[@type="submit"]')

    @Override
    List<By> getComponentLocators() {
        [formLocator]
    }

    @Override
    void performInitialValidation() {
        waitForElement(formLocator)
    }

    PasswordResetPage fillPassword(String password) {
        fill(passwordLocator, password)
        this
    }

    PasswordResetPage fillPasswordConfirmation(String password) {
        fill(passwordConfirmationLocator, password)
        this
    }

    PasswordResetPage clickSubmitButton() {
        exceptionSafeClick(submitButtonLocator)
        this
    }

    XnatLoginPage resetPassword(String newPassword) {
        fillPassword(newPassword)
        fillPasswordConfirmation(newPassword)
        uploadScreenshot()
        clickSubmitButton()
        loadPage(XnatLoginPage, false, true).
        assertLoginMessage('Password changed.').
        capture()
    }

}
