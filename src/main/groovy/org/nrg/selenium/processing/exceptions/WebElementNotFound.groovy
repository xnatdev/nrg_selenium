package org.nrg.selenium.processing.exceptions

import org.nrg.testing.xnat.processing.exceptions.ProcessingValidationException
import org.openqa.selenium.By

class WebElementNotFound extends ProcessingValidationException {

    private By locator

    WebElementNotFound(By locator) {
        this.locator = locator
    }

    @Override
    String getMessage() {
        "Could not locate element with locator: ${locator}"
    }

}
