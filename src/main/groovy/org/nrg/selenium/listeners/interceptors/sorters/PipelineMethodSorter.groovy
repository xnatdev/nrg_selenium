package org.nrg.selenium.listeners.interceptors.sorters

import groovy.util.logging.Log4j
import org.nrg.scheduling.*
import org.nrg.selenium.processing.BasePipelineTest
import org.nrg.testing.TestNgUtils
import org.nrg.testing.annotations.PipelineLaunchParams
import org.nrg.testing.listeners.interceptors.sorters.DefaultMethodSorter
import org.nrg.testing.xnat.conf.Settings
import org.testng.IMethodInstance
import org.testng.ITestContext
import org.testng.ITestNGMethod

@Log4j
class PipelineMethodSorter extends DefaultMethodSorter {

    @SuppressWarnings('UnnecessaryQualifiedReference')
    PipelineMethodSorter() {
        DefaultMethodSorter.disable()
    }

    @Override
    List<IMethodInstance> intercept(List<IMethodInstance> methods, ITestContext context) {
        if (Settings.DYNAMIC_ORDERING) {
            log.info('Dynamic ordering is enabled. System will attempt to order pipeline executions to minimize makespan.')
            if (!methods.any { it.instance instanceof BasePipelineTest }) {
                throw new RuntimeException("Dynamic ordering can only be used when running pipeline tests [test classes must all extend ${BasePipelineTest.simpleName}]")
            }
            orderByJobShopSolution(methods, Settings.QUEUE_SLOTS)
        } else {
            orderMethods(methods)
        }
    }

    protected List<IMethodInstance> orderByJobShopSolution(final List<IMethodInstance> methods, int queueSlots) {
        final JobList<IMethodInstance> jobList = new JobList<>()

        methods.each { method ->
            final ITestNGMethod test = method.method
            final PipelineLaunchParams launchParams = TestNgUtils.getAnnotation(test, PipelineLaunchParams)
            final String testName = TestNgUtils.getTestName(test)
            if (testName.startsWith('testLaunch')) {
                if (launchParams == null) {
                    throw new RuntimeException("${testName} does not have required @${PipelineLaunchParams.simpleName} annotation.")
                }
                jobList << new Job<IMethodInstance>(method, launchParams.estimatedRuntime())
            }
        }

        final ProcessorSchedulingProblem<IMethodInstance> schedulingProblem = new ProcessorSchedulingProblem<>(jobList, queueSlots)
        log.info('Attempting to solve pipeline ordering problem via Mixed-Integer Linear Programming formulation [and delegation to ojalgo].')
        final ProcessorSchedulingSolution<IMethodInstance> schedulingSolution = schedulingProblem.solve(Algorithms.MIP)

        final List<IMethodInstance> orderedMethods = []
        schedulingSolution.solutionByQueueOrder.each { testJob ->
            orderedMethods << testJob.jobId
        }

        schedulingSolution.solutionByCompletionOrder.each { testJob ->
            final IMethodInstance instance = testJob.jobId
            final String testName = TestNgUtils.getTestName(instance)
            final String pipelineName = testName.substring(10)
            final IMethodInstance correpondingCheckTest = methods.find { checkTest ->
                TestNgUtils.getTestName(checkTest) == "testCheck${pipelineName}"
            }
            if (correpondingCheckTest == null) {
                throw new RuntimeException("Could not find corresponding pipeline check test for launch test ${testName}.")
            } else {
                orderedMethods << correpondingCheckTest
            }
        }

        log.info("Estimated total pipeline runtime in minutes: ${schedulingSolution.makespan}")
        orderedMethods
    }

}
