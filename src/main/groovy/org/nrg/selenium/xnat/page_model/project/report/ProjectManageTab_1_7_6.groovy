package org.nrg.selenium.xnat.page_model.project.report

import org.nrg.xnat.versions.*

class ProjectManageTab_1_7_6 extends ProjectManageTab {

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        XnatVersionList.VERSIONS_BEFORE_1_8
    }

    @Override
    protected String getAutoRunTabIdentifier() {
        'Notifications Configuration'
    }

}
