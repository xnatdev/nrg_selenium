package org.nrg.selenium.xnat.page_model.automation

import org.nrg.selenium.enums.EventType
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.scripting.AutomationScript
import org.nrg.selenium.xnat.scripting.EventHandler
import org.openqa.selenium.By

class EventHandlerEditModal implements XnatPageComponent {

    protected By eventTypeLocator = By.id('select_eventClass')
    protected By eventIdLocator = By.id('select_event')
    protected By eventStatusLocator = By.id('filter_sel_status')
    protected By eventScriptLocator = By.id('select_scriptId')
    protected By eventDescriptionLocator = By.id('description')
    protected By modalLocator = By.xpath("//div[@title='Add Event Handler']")
    protected By cancelButtonLocator = XnatLocators.buttonWithText('Cancel')
    protected By saveButtonLocator = XnatLocators.buttonWithText('Save')

    EventHandlerEditModal selectEventType(EventType eventType) {
        exceptionSafeSelect(eventTypeLocator, eventType?.displayName)
        this
    }

    EventHandlerEditModal selectEvent(String event) {
        exceptionSafeSelect(eventIdLocator, event)
        this
    }

    EventHandlerEditModal selectStatus(String status) {
        exceptionSafeSelect(eventStatusLocator, status)
        this
    }

    EventHandlerEditModal selectScript(String scriptString) {
        exceptionSafeSelect(eventScriptLocator, scriptString)
        this
    }

    EventHandlerEditModal fillEventHandlerDescription(String description) {
        fill(eventDescriptionLocator, description)
        this
    }

    String formatScriptSelectionText(AutomationScript script) {
        "${script.scriptID}:${script.effectiveLabel}"
    }

    EventHandlerListComponent cancel() {
        exceptionSafeClick(cancelButtonLocator)
        loadPage(EventHandlerListComponent)
    }

    EventHandlerEditModal submit() {
        exceptionSafeClick(saveButtonLocator)
        this
    }

    EventHandlerEditModal performEventHandlerSubmission(EventHandler eventHandler) {
        selectEventType(eventHandler.eventType)
        selectEvent(eventHandler.eventID)
        selectStatus(eventHandler.status)
        if (eventHandler.script) {
            selectScript(formatScriptSelectionText(eventHandler.script))
        }
        fillEventHandlerDescription(eventHandler.description)
        readXModal(modalLocator).uploadScreenshot()
        submit()
    }


}
