package org.nrg.selenium.listeners.interceptors.sorters.dummy

import org.nrg.selenium.listeners.interceptors.sorters.TestPipelineMethodSorter
import org.nrg.testing.annotations.PipelineCheckParams
import org.nrg.testing.annotations.PipelineLaunchParams
import org.nrg.testing.enums.Pipeline
import org.nrg.testing.unit.UnitTestId
import org.testng.annotations.Test

@Test(groups = TestPipelineMethodSorter.DUMMY)
class TestPipelineDummyClass {

    @UnitTestId([1, 2])
    @PipelineLaunchParams(estimatedRuntime = 100, session = '', pipeline = Pipeline.CUSTOM)
    void testLaunch100() {}

    @UnitTestId([1, 2])
    @PipelineLaunchParams(estimatedRuntime = 55, session = '', pipeline = Pipeline.CUSTOM)
    void testLaunch55() {}

    @UnitTestId([1, 2])
    @PipelineLaunchParams(estimatedRuntime = 50, session = '', pipeline = Pipeline.CUSTOM)
    void testLaunch50() {}

    @UnitTestId([1, 2])
    @PipelineLaunchParams(estimatedRuntime = 40, session = '', pipeline = Pipeline.CUSTOM)
    void testLaunch40() {}

    @UnitTestId([1, 2])
    @PipelineLaunchParams(estimatedRuntime = 30, session = '', pipeline = Pipeline.CUSTOM)
    void testLaunch30() {}

    @UnitTestId([1, 2])
    @PipelineLaunchParams(estimatedRuntime = 20, session = '', pipeline = Pipeline.CUSTOM)
    void testLaunch20() {}

    @UnitTestId([1, 2])
    @PipelineLaunchParams(estimatedRuntime = 5, session = '', pipeline = Pipeline.CUSTOM)
    void testLaunch5() {}

    @UnitTestId([1, 2])
    @PipelineCheckParams(pipeline = Pipeline.CUSTOM, maxRuntime = 0, session = '')
    void testCheck100() {}

    @UnitTestId([1, 2])
    @PipelineCheckParams(pipeline = Pipeline.CUSTOM, maxRuntime = 0, session = '')
    void testCheck55() {}

    @UnitTestId([1, 2])
    @PipelineCheckParams(pipeline = Pipeline.CUSTOM, maxRuntime = 0, session = '')
    void testCheck50() {}

    @UnitTestId([1, 2])
    @PipelineCheckParams(pipeline = Pipeline.CUSTOM, maxRuntime = 0, session = '')
    void testCheck40() {}

    @UnitTestId([1, 2])
    @PipelineCheckParams(pipeline = Pipeline.CUSTOM, maxRuntime = 0, session = '')
    void testCheck30() {}

    @UnitTestId([1, 2])
    @PipelineCheckParams(pipeline = Pipeline.CUSTOM, maxRuntime = 0, session = '')
    void testCheck20() {}

    @UnitTestId([1])
    @PipelineCheckParams(pipeline = Pipeline.CUSTOM, maxRuntime = 0, session = '')
    void testCheck5() {}

}
