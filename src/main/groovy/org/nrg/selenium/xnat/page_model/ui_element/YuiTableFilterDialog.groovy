package org.nrg.selenium.xnat.page_model.ui_element

import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By
import org.openqa.selenium.support.ui.Select

import static org.testng.AssertJUnit.assertTrue

class YuiTableFilterDialog implements XnatPageComponent, ScreenshotableComponent<YuiTableFilterDialog>, OverlayingPage<YuiTableFilterDialog, YuiTable> {

    protected String equalsIdentifier = '='
    protected String notEqualIdentifier = '!='
    protected String likeIdentifier = 'LIKE'
    protected String greaterThanIdentifier = '>'
    protected String greaterThanOrEqualsIdentifier = '>='
    protected String lesserThanIdentifier = '<'
    protected String lesserThanOrEqualsIdentifier = '<='
    protected String listInclusionIdentifier = 'IN'
    protected String betweenIdentifier = 'BETWEEN'
    protected String isNullIdentifier = 'IS NULL'
    protected String isNotNullIdentifier = 'IS NOT NULL'
    protected By filterTypeLocator = By.xpath('//select[./option[text()="SELECT"]][1]')
    protected By valueLocator = By.xpath('//*[(self::input or self::select) and @style="display: inline;"]')
    protected By moreButtonLocator = By.xpath('//input[@value="More..."]')
    protected By cancelButtonLocator = XnatLocators.CANCEL_BUTTON
    protected By submitButtonLocator = XnatLocators.buttonWithText('Submit')

    @Override
    void performNavigationToUnderlyingPage() {
        exceptionSafeClick(cancelButtonLocator)
    }

    @Override
    List<By> getComponentLocators() {
        [By.id('search-column-filter')]
    }

    YuiTableFilterDialog addEqualsFilter(String comparisonText) {
        filter(equalsIdentifier, comparisonText)
    }

    YuiTableFilterDialog addNotEqualFilter(String comparisonText) {
        filter(notEqualIdentifier, comparisonText)
    }

    YuiTableFilterDialog addLikeFilter(String comparisonText) {
        filter(likeIdentifier, comparisonText)
    }

    YuiTableFilterDialog addGreaterThanFilter(String comparisonText) {
        filter(greaterThanIdentifier, comparisonText)
    }

    YuiTableFilterDialog addGreaterThanOrEqualsFilter(String comparisonText) {
        filter(greaterThanOrEqualsIdentifier, comparisonText)
    }

    YuiTableFilterDialog addLesserThanFilter(String comparisonText) {
        filter(lesserThanIdentifier, comparisonText)
    }

    YuiTableFilterDialog addLesserThanOrEqualsFilter(String comparisonText) {
        filter(lesserThanOrEqualsIdentifier, comparisonText)
    }

    YuiTableFilterDialog addListInclusionFilter(List<String> list) {
        addListInclusionFilter(list.join(','))
    }

    YuiTableFilterDialog addListInclusionFilter(String comparisonText) {
        filter(listInclusionIdentifier, comparisonText)
    }

    YuiTableFilterDialog addBetweenFilter(String lesserValue, String greaterValue) {
        addBetweenFilter("${lesserValue} AND ${greaterValue}")
    }

    YuiTableFilterDialog addBetweenFilter(String comparisonText) {
        filter(betweenIdentifier, comparisonText)
    }

    YuiTableFilterDialog addNullFilter() {
        filter(isNullIdentifier, null)
    }

    YuiTableFilterDialog addNotNullFilter() {
        filter(isNotNullIdentifier, null)
    }

    YuiTable submitFilters() {
        exceptionSafeClick(submitButtonLocator)
        underlyingPage.performInitialValidation()
        underlyingPage
    }

    protected void selectFilterType(String filterName) {
        exceptionSafeSelect(filterTypeLocator, filterName)
    }

    protected YuiTableFilterDialog filter(String filterName, String filterText) {
        exceptionSafeSelect(filterTypeLocator, filterName)
        if (filterName == equalsIdentifier) {
            final Select dropdown = new Select(exceptionSafeFind(valueLocator))
            dropdown.selectByValue(filterText)
            assertTrue(dropdown.firstSelectedOption.text.matches("${filterText} \\([1-9]\\d*\\)"))
        } else {
            fill(valueLocator, filterText)
        }
        exceptionSafeClick(moreButtonLocator)
        this
    }

}
