package org.nrg.selenium.extension

/**
 * Cannot be joined to another parent XpathLocator object
 */
class StandaloneXpathLocator extends XpathLocator {

    StandaloneXpathLocator(String locator) {
        super(locator)
    }

    @Override
    StandaloneXpathLocator joinSublocator(String sublocator) {
        new StandaloneXpathLocator(super.joinSublocator(sublocator).expression)
    }

    @Override
    boolean isJoinable() {
        false
    }

}
