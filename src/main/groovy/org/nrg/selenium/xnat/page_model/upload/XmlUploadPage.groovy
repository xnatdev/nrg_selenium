package org.nrg.selenium.xnat.page_model.upload

import org.nrg.selenium.xnat.page_model.LoadablePage
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage
import org.nrg.selenium.xnat.page_model.session.report.ImagingSessionReportPage
import org.nrg.selenium.xnat.page_model.subject.report.SubjectReportPage
import org.openqa.selenium.By

class XmlUploadPage implements NavigablePageObject<XmlUploadPage>, LoadablePage, ScreenshotableComponent<XmlUploadPage> {

    protected By allowDataDeletionRadioLocator = By.xpath('//input[@value="true"]')
    protected By disallowDataDeletionRadioLocator = By.xpath('//input[@value="false"]')
    protected By xmlFileLocator = By.name('xml_to_store')
    protected By uploadButtonLocator = By.xpath('//input[@value="Upload"]')

    @Override
    void navigateTo() {
        topNav("Upload", "XML")
    }

    @Override
    List<By> getComponentLocators() {
        [By.id('uploadForm')]
    }

    XmlUploadPage markAllowDataDeletionAs(boolean allow) {
        exceptionSafeClick(allow ? allowDataDeletionRadioLocator : disallowDataDeletionRadioLocator)
        this
    }

    XmlUploadPage attachFile(File file) {
        exceptionSafeFind(xmlFileLocator).sendKeys(file.absolutePath)
        this
    }

    ProjectReportPage uploadAsProject() {
        upload(ProjectReportPage)
    }

    SubjectReportPage uploadAsSubject() {
        upload(SubjectReportPage)
    }

    ImagingSessionReportPage uploadAsSession() {
        upload(ImagingSessionReportPage)
    }

    def <X extends NavigablePageObject<X>> X upload(Class<X> expectedResultClass) {
        exceptionSafeClick(uploadButtonLocator)
        loadPage(expectedResultClass)
    }

}
