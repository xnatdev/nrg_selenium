package org.nrg.selenium.processing

import com.fasterxml.jackson.databind.ObjectMapper
import groovy.util.logging.Log4j
import org.apache.commons.io.FileUtils
import org.apache.commons.lang3.time.StopWatch
import org.nrg.jira.components.zephyr.TestStatus
import org.nrg.selenium.listeners.interceptors.sorters.PipelineMethodSorter
import org.nrg.selenium.BaseSeleniumTest
import org.nrg.selenium.processing.element_comparators.*
import org.nrg.selenium.xnat.page_model.session.report.ImagingSessionReportPage
import org.nrg.testing.CollectionUtils
import org.nrg.testing.CommonStringUtils
import org.nrg.testing.TimeUtils
import org.nrg.testing.util.IgnoreNullList
import org.nrg.testing.xnat.processing.exceptions.ProcessingValidationException
import org.nrg.selenium.processing.jackson.module.SeleniumProcessingJacksonModule
import org.nrg.testing.xnat.processing.CreatedAssessor
import org.nrg.testing.xnat.processing.GeneratedEmail
import org.nrg.testing.xnat.processing.ProcessingCheckable
import org.nrg.selenium.processing.config_elements.PipelineConfigElement
import org.nrg.testing.annotations.PipelineCheckParams
import org.nrg.testing.annotations.PipelineLaunchParams
import org.nrg.testing.enums.Pipeline
import org.nrg.testing.xnat.conf.Settings
import org.nrg.testing.xnat.processing.files.ProcessingFileSetRequest
import org.nrg.xnat.jackson.mappers.YamlObjectMapper
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Workflow
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.subinterfaces.WorkflowSubinterface
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.TimeoutException
import org.openqa.selenium.interactions.Actions
import org.testng.ITestContext
import org.testng.annotations.AfterSuite
import org.testng.annotations.BeforeMethod
import org.testng.annotations.BeforeSuite
import org.testng.annotations.Listeners

import java.lang.reflect.Method

import static org.testng.AssertJUnit.fail

@Log4j
@Listeners(PipelineMethodSorter)
@Deprecated
/**
 * This class will not work at all in its current state. I rewrote the nrg_selenium library to follow the common Page Object Model more closely
 * (although with some intentional deviations). I didn't finish rewriting this class, along with likely other pipeline test classes, as it seems
 * we are going to stop running the pipeline tests as the processing moves to containers. If we do decide to again support the pipeline tests
 * in the future, we will either need to finish the rewrite of this class (and possibly others), or use a very old version of this library
 * (would not recommend).
 * - Charlie, 2021-02-02
 */
class BasePipelineTest extends BaseSeleniumTest {

    public static final ObjectMapper YAML_MAPPER = new YamlObjectMapper().registerModule(new SeleniumProcessingJacksonModule())
    protected final List<PipelineConfigElement> config = []
    protected final IgnoreNullList<String> verificationErrors = new IgnoreNullList<>()
    protected final List<ProcessingCheckable> processingCheckables = []
    protected Project pipelineProject
    protected String testName
    protected ImagingSession currentSession
    protected Pipeline pipeline
    protected String pipelineLaunchName
    protected String pipelineHistoryName
    protected boolean customLaunch
    protected String pipelineName
    protected int queueTolerance
    protected int completionTolerance
    protected String assessorContainingPipeline

    @BeforeSuite(alwaysRun = true)
    void setStartTime() {
        GeneratedEmail.setSuiteStartTime()
        constructXnatDriver(true, Settings.DEFAULT_XNAT_CONFIG) // generate locators object for next step
    }

    @BeforeMethod(alwaysRun = true)
    void initializeConfig(Method method) {
        [config, verificationErrors, processingCheckables].each { list ->
            list.clear()
        }
        testName = method.name
    }

    @BeforeMethod(alwaysRun = true)
    void readPipelineAnnotations(Method m, ITestContext testContext) {
        final PipelineLaunchParams launchParams = m.getAnnotation(PipelineLaunchParams)
        final PipelineCheckParams checkParams = m.getAnnotation(PipelineCheckParams)
        final String currentAccessionNumber = (launchParams != null) ? launchParams.session() : checkParams.session()
        if (pipelineProject == null) {
            pipelineProject = new Project(restDriver.mainInterface().jsonQuery().get(formatRestUrl("/experiments/${currentAccessionNumber}")).then().assertThat().statusCode(200).and().extract().jsonPath().getString('items[0].data_fields.project'))
        }
        currentSession = restDriver.readExperiment(mainUser, currentAccessionNumber, ImagingSession)

        if (launchParams != null) {
            pipeline = launchParams.pipeline()
            customLaunch = launchParams.customLaunch()
            if (pipeline == Pipeline.CUSTOM) {
                pipelineLaunchName = launchParams.pipelineLaunchName()
                pipelineHistoryName = launchParams.pipelineHistoryName()
            } else {
                pipelineLaunchName = pipeline.getLaunchName()
                pipelineHistoryName = pipeline.getHistoryName()
            }
        } else { // Pipeline method should have either launch params or validation params. NPE will be thrown here otherwise. That's fine (should fail)
            pipeline = checkParams.pipeline()
            pipelineName = pipeline == Pipeline.CUSTOM ? checkParams.pipelineName() : pipeline.historyName
            queueTolerance = checkParams.maxQueueTime()
            completionTolerance = checkParams.maxRuntime()
            assessorContainingPipeline = checkParams.assessorContainingPipelineHistory()
        }
    }

    @AfterSuite
    void deleteData() {
        FileUtils.deleteDirectory(new File(Settings.TEMP_SUBDIR))
    }

    /**
     * Perform a basic test for an arbitrary pipeline.
     */
    protected void performPipelineTest() {
        final ImagingSessionReportPage sessionReportPage = loginPage.seleniumLogin().
                searchForSession(currentSession.accessionNumber).
                capture()

        if (customLaunch) {
            performLaunchConfig()
        } else {
            sessionReportPage.clickBuild().
                    capture().
                    selectPipeline(pipelineLaunchName).
                    capture().
                    submit()
            performLaunchConfig()
            returnToMainWindow()
        }

        sessionReportPage.capture()

        final By workflowBar = By.xpath("//td[@valign='middle']/b[text()='${pipelineHistoryName}:'] | //div[@id='workflow_messages']//*[contains(text(), 'Active Workflow: ${pipelineHistoryName}')]")

        xnatDriver.waitForElementWithRefresh(workflowBar)
        captureStep(locators.PROJECT_BREADCRUMB, workflowBar, locators.BUILD)

        logLogout()
    }

    private void performLaunchConfig() {
        config.each { configEntry ->
            configEntry.performElementAction(xnatDriver)
        }
    }

    /**
     * Performs general tests to assert pipeline ran successfully
     */
    protected void checkPipelineTest() {
        final List<CreatedAssessor> assessors = CollectionUtils.ofType(processingCheckables, CreatedAssessor)
        final List<ProcessingFileSetRequest> fileSets = CollectionUtils.ofType(processingCheckables, ProcessingFileSetRequest)
        final List<GeneratedEmail> emailChecks = CollectionUtils.ofType(processingCheckables, GeneratedEmail)

        seleniumLogin()
        xnatDriver.findBySearch(currentSession.accessionNumber)

        if (assessorContainingPipeline != '') {
            driver.findElement(By.linkText(assessorContainingPipeline))
        }
        checkPipelineCompletion(queueTolerance, completionTolerance, pipelineName)

        xnatDriver.findBySearch(currentSession.accessionNumber)
        xnatDriver.exceptionSafeClick(By.partialLinkText('SUBJECT: '))
        captureStep(locators.SUBJECT_LABEL, By.id('all_expts'))

        if (pipeline == Pipeline.FS_5_3) {
            driver.findElement(By.linkText('Freesurfer')).click()
            checkPipelineCompletion(60*15, 60*30, 'Freesurfer_Snapshots')
            xnatDriver.findBySearch(currentSession.accessionNumber)
            xnatDriver.exceptionSafeClick(By.partialLinkText('SUBJECT: '))
            captureStep(locators.SUBJECT_LABEL, By.id('all_expts'))
        }

        if (!assessors.isEmpty()) {
            assessors.each { assessor ->
                try {
                    xnatDriver.waitForElement(By.linkText(assessor.name))
                } catch (TimeoutException | NoSuchElementException ignored) {
                    testController.failBecause("Assessor not found: ${assessor.name}")
                }
            }
            captureStep(stepCounter, locators.SUBJECT_LABEL, By.id('all_expts'))
        }

        if (!fileSets.isEmpty()) {
            fileSets.each { fileSet ->
                verificationErrors.addAll(fileSet.checkFilesMatch(currentSession, restDriver, xnatDriver))
            }
        }

        if (!emailChecks.isEmpty()) {
            emailChecks.each { email ->
                final String emailVerification = email.checkEmail()
                if (emailVerification != null) {
                    verificationErrors << emailVerification
                    restDriver.captureStep(TestStatus.FAIL, emailVerification)
                } else {
                    testController.commentStep('Email verified.')
                    captureScreenshotlessStep()
                }
            }
        }
    }

    protected void checkPipelineCompletion(int queueTolerance, int completionTolerance, String pipelineName) {
        final By pipelineHistory = By.linkText(pipelineName)

        xnatDriver.waitForElement(locators.SESSION_HISTORY)
        captureStep(locators.PROJECT_BREADCRUMB, locators.ACTIONS_BOX)
        xnatDriver.setTimeout(0)
        if (!xnatDriver.isElementPresent(pipelineHistory)) {
            driver.findElement(locators.SESSION_HISTORY).click()
        }
        xnatDriver.setTimeout(Settings.DEFAULT_TIMEOUT)

        new Actions(driver).keyDown(Keys.ALT).click(driver.findElement(pipelineHistory)).keyUp(Keys.ALT).build().perform() // send an alt+click

        try {
            xnatDriver.waitForElementWithRefresh(locators.WORKFLOW_ID_TEXT, queueTolerance)
        } catch (Exception ignored) {
            testController.failBecause('Pipeline has almost certainly failed (does not even have workflowid).')
        }

        captureStep(locators.H3_TAG, By.xpath("//font[text()='Related Items']"))

        final String workflowId = driver.findElement(locators.WORKFLOW_ID_TEXT).text

        captureStep(stepCounter, TestStatus.PASS, "Workflow: ${workflowId}", true, By.xpath("//font[text()='Execution parameters']"), locators.WORKFLOW_ID_TEXT)
        final StopWatch queueChecker = TimeUtils.launchStopWatch()

        while (true) {
            if (TimeUtils.maxTimeReached(queueChecker, queueTolerance)) {
                testController.failBecause("Pipeline (${pipelineName}) did not escape the queue in allotted time: ${queueTolerance} seconds.")
            }

            if (mainInterface().readWorkflowStatus(workflowId) != Workflow.WORKFLOW_QUEUED) {
                captureScreenshotlessStep()
                break
            }
            TimeUtils.sleep(60000)
            driver.navigate().refresh()
        }

        final StopWatch completionChecker = TimeUtils.launchStopWatch()

        while (true) {
            if (TimeUtils.maxTimeReached(completionChecker, completionTolerance)) {
                testController.failBecause("Pipeline (${pipelineName}) escaped from Queued status but did not complete in allotted time: ${completionTolerance} seconds.")
            }
            final String status = mainInterface().readWorkflowStatus(workflowId)
            switch (status) {
                case Workflow.WORKFLOW_COMPLETE:
                    log.info("REST call for pipeline ${pipelineName} (${workflowId}) reports that it is complete.")
                    captureScreenshotlessStep()
                    return
                case Workflow.WORKFLOW_FAILED:
                    testController.failBecause("Pipeline (${pipelineName}) execution failed in XNAT.")
            }

            TimeUtils.sleep(60000)
            driver.navigate().refresh()
        }
    }

    protected void checkVerificationErrors() {
        if (!verificationErrors.isEmpty()) {
            log.warn("Verification errors were encountered:\n${verificationErrors.join('\n')}")
            testController.failBecause('Verification errors occurred in validating results.')
        }
    }

    protected void compareTable(ElementSpecifications entries, String tableName, By... locators) {
        final IgnoreNullList<String> newErrors = new IgnoreNullList<>()

        final Map<String, String> replacements = entries.locatorReplacements
        entries.locatorCaches.each { locatorCache ->
            locatorCache.addToCache(driver, replacements)
        }
        entries.comparators.each { comparator ->
            comparator.elements.each { element ->
                final Locator locator = element.locator
                locator.setValue(CommonStringUtils.replaceEach(locator.value, replacements))
            }
        }

        xnatDriver.startTimer()
        entries.comparators.each { comparator ->
            comparator.elements.each { element ->
                try {
                    comparator.checkPointsMatch(xnatDriver, element.locator.by(), element.value)
                    log.debug("Element satisfied: ${element.locator.by()}")
                } catch (ProcessingValidationException pve) {
                    log.warn("Element verification failed: ${element.locator.by()}")
                    newErrors << pve.message
                    verificationErrors << pve.message
                }
                xnatDriver.checkAndRenewTimer()
            }
        }
        if (newErrors.isEmpty()) {
            log.info("Values were correctly checked for: ${tableName}")
            if (locators.length > 0) {
                captureStep(stepCounter, TestStatus.PASS, null, true, locators)
            } else {
                captureScreenshotlessStep()
            }
        } else {
            captureStep(stepCounter, TestStatus.FAIL, newErrors.join(), true, locators)
        }
    }

    @SuppressWarnings('GroovyMissingReturnStatement')
    protected ElementSpecifications readElementsFile(String dataFile) {
        try {
            final ElementSpecifications elements = YAML_MAPPER.readValue(mainCredentials().get(formatRestUrl("/experiments/${currentSession.accessionNumber}/resources/validation/files/${dataFile}")).asInputStream(), ElementSpecifications)
            captureScreenshotlessStep()
            elements
        } catch (IOException ignored) {
            fail("Couldn't read elements file: ${dataFile}")
        }
    }

    protected String getSessionLabel(String accessionNumber) {
        restDriver.mainInterface().jsonQuery().get(formatRestUrl("/experiments/${accessionNumber}")).then().assertThat().statusCode(200).and().extract().jsonPath().getString('items[0].data_fields.label')
    }

}
