package org.nrg.selenium.xnat.page_model.admin

import org.nrg.selenium.CustomConditions
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By
import org.openqa.selenium.support.ui.WebDriverWait

import java.time.Duration

import static org.testng.AssertJUnit.assertTrue

class JavaMelodyMonitoringServlet implements XnatPageComponent {

    protected By statsSummaryLocator = XnatLocators.H3_TAG
    protected By graphContainerLocator = By.xpath('//div[@id="detailsGraphs"]/..')
    protected By cacheExpanderLocator = By.id('jcaches_0Img')
    protected By numObjectsInNrgCacheLocator = By.xpath('(//a[text()="nrg"]/../following-sibling::td)[2]')
    protected By cacheScreenshotLocator = By.id('jcaches_0')

    @Override
    void performInitialValidation() {
        assertElementTextContains("Statistics of JavaMelody monitoring", statsSummaryLocator)
    }

    JavaMelodyMonitoringServlet captureGraphs() {
        captureStep([statsSummaryLocator, graphContainerLocator])
        this
    }

    JavaMelodyMonitoringServlet verifyBasicCaching() {
        exceptionSafeClick(cacheExpanderLocator)
        new WebDriverWait(driver, Duration.ofSeconds(3)).until(CustomConditions.textIsNonempty(numObjectsInNrgCacheLocator))
        assertTrue(Integer.parseInt(exceptionSafeGetText(numObjectsInNrgCacheLocator).replace(',', '')) > 0)
        // Make sure that the number in the cell is greater than 0
        captureStep(cacheScreenshotLocator)
        this
    }

}
