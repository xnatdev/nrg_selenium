package org.nrg.selenium.xnat.page_model.users

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.extension.XpathUnion
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By

class PostregistrationPage implements XnatPageComponent, ScreenshotableComponent<PostregistrationPage> {

    protected XpathLocator verificationRequiredHeader = new XpathLocator('//div[@id="email-verification"]/h1')
    protected XpathLocator verificationUnnecessaryHeader = new XpathLocator('//div[@id="login_area"]//h1')
    protected By screenshotLocator = By.xpath('//*[@id="login_area" or @id="email-verification"]')
    protected By resendEmailButtonLocator = By.xpath('//button[@onclick="resend()"]')
    protected String verificationSentString = 'Email Verification Sent'
    protected String registrationReceivedString = 'Registration Received'

    @Override
    void performInitialValidation() {
        waitForElement(new XpathUnion([verificationRequiredHeader, verificationUnnecessaryHeader]))
    }

    @Override
    List<By> getComponentLocators() {
        [screenshotLocator]
    }

    PostregistrationPage assertVerificationRequired() {
        assertElementText(verificationSentString, verificationRequiredHeader)
        this
    }

    PostregistrationPage assertVerificationNotRequired() {
        assertElementText(registrationReceivedString, verificationUnnecessaryHeader)
        this
    }

    PostregistrationPage clickResendEmailButtion() {
        exceptionSafeClick(resendEmailButtonLocator)
        this
    }

}
