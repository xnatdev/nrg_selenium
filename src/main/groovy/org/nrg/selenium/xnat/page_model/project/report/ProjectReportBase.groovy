package org.nrg.selenium.xnat.page_model.project.report

import org.nrg.selenium.xnat.page_model.PageObjectSupport

trait ProjectReportBase implements PageObjectSupport {

    ProjectReportPage projectPage

    def <X extends ProjectReportTab> X loadTab(Class<X> tab) {
        driverExtension.exceptionSafeClick(getInstance(tab).tabLocator())
        final X tabObject = loadPage(tab, false)
        if (projectPage != null) {
            tabObject.setProjectPage(projectPage)
        } else {
            tabObject.setProjectPage(this as ProjectReportPage)
        }
        tabObject
    }

    ProjectAccessTab loadAccessTab() {
        loadTab(ProjectAccessTab)
    }

    ProjectManageTab loadManageTab() {
        loadTab(ProjectManageTab)
    }

    ProjectDetailsTab loadDetailsTab() {
        loadTab(ProjectDetailsTab)
    }

}