package org.nrg.selenium.xnat.page_model

import org.openqa.selenium.By

trait ScreenshotableComponent<X extends ScreenshotableComponent<X>> implements PageObjectSupport {

    abstract List<By> getComponentLocators()

    X uploadScreenshot() {
        xnatDriver.uploadTargetedScreenshot(componentLocators)
        this as X
    }

    X capture() {
        xnatDriver.captureStep(componentLocators)
        this as X
    }

}