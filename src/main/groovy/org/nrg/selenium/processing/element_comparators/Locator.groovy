package org.nrg.selenium.processing.element_comparators

import groovy.transform.EqualsAndHashCode
import org.nrg.selenium.enums.LocatorType
import org.openqa.selenium.By

@EqualsAndHashCode
class Locator {

    LocatorType type
    String value

    Locator type(LocatorType type) {
        setType(type)
        this
    }

    Locator value(String value) {
        setValue(value)
        this
    }

    By by() {
        type.byFor(value)
    }

}
