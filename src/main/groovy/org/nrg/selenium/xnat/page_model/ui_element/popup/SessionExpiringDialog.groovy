package org.nrg.selenium.xnat.page_model.ui_element.popup

import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.testing.TimeUtils
import org.openqa.selenium.By

import static org.testng.AssertJUnit.*

trait SessionExpiringDialog<X extends NavigablePageObject<X>> implements XnatPageComponent, ScreenshotableComponent<SessionExpiringDialog>, OverlayingPage<SessionExpiringDialog, X> {

    XnatDialog expirationDialog
    By hoursLocator = By.xpath(".//b[@class='mono timeout-hours']")
    By minutesLocator = By.xpath(".//b[@class='mono timeout-minutes']")
    By secondsLocator = By.xpath(".//b[@class='mono timeout-seconds']")
    By renewButtonLocator = XnatLocators.buttonWithText('Renew')
    By closeButtonLocator = XnatLocators.buttonWithText('Close')

    @Override
    void performInitialValidation() {
        expirationDialog = readXnatDialog(secondsLocator)
        assertElementText('0', hoursLocator)
        assertElementText('00', minutesLocator)
    }

    @Override
    void performNavigationToUnderlyingPage() {
        exceptionSafeClick(closeButtonLocator)
    }

    @Override
    List<By> getComponentLocators() {
        expirationDialog.componentLocators
    }

    int readRemainingSeconds() {
        Integer.parseInt(exceptionSafeGetText(secondsLocator))
    }

    SessionExpiringDialog<X> assertRemainingSeconds(int expectedSeconds, int tolerance) {
        assertTrue(Math.abs(readRemainingSeconds() - expectedSeconds) < tolerance)
        expirationDialog.uploadScreenshot()
        this
    }

    X refreshExpiringSession(int expectedSeconds, int maxSeconds, int tolerance) {
        assertRemainingSeconds(expectedSeconds, tolerance)
        exceptionSafeClick(renewButtonLocator)
        TimeUtils.sleep(1000) // Give it a second to update
        underlyingPage.assertSessionTimer(maxSeconds, tolerance)
    }

}
