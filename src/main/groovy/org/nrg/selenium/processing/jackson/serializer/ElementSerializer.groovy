package org.nrg.selenium.processing.jackson.serializer

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.nrg.selenium.enums.LocatorType
import org.nrg.selenium.processing.element_comparators.Element
import org.nrg.selenium.processing.element_comparators.Locator
import org.nrg.xnat.jackson.serializers.CustomSerializer

class ElementSerializer extends CustomSerializer<Element> {

    @Override
    void serialize(Element value, JsonGenerator gen, SerializerProvider provider) {
        gen.writeStartObject()

        final Locator locator = value.locator
        if (locator.type == LocatorType.XPATH) {
            writeStringFieldIfNonnull(gen, 'locator', locator.value)
        } else {
            writeObjectFieldIfNonnull(gen, 'locator', locator)
        }
        writeStringFieldIfNonnull(gen, 'value', value.value)

        gen.writeEndObject()
    }

}
