package org.nrg.selenium.xnat.page_model.error

import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.openqa.selenium.By

trait GenericError<X extends GenericError<X>> implements XnatPageComponent, ScreenshotableComponent<X> {

    abstract By getMainErrorLocator()

    @Override
    List<By> getComponentLocators() {
        [mainErrorLocator]
    }

    String readDisplayedError() {
        exceptionSafeGetText(mainErrorLocator)
    }

    X assertDisplayedError(String expectedError) {
        assertElementText(expectedError, mainErrorLocator)
        this as X
    }

}
