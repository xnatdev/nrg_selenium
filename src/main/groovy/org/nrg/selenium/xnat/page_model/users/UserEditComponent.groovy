package org.nrg.selenium.xnat.page_model.users

import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.xnat.pogo.users.User
import org.openqa.selenium.By

trait UserEditComponent<X extends UserEditComponent<X>> implements XnatPageComponent {

    abstract By usernameLocator()
    abstract By passwordLocator()
    abstract By passwordCheckLocator()
    abstract By firstNameLocator()
    abstract By lastNameLocator()
    abstract By emailLocator()

    X fillCommonFields(User user) {
        fillUsername(user.username)
        fillPassword(user.password)
        fillPasswordCheck(user.password)
        fillFirstName(user.firstName)
        fillLastName(user.lastName)
        fillEmail(user.email)
    }

    X fillUsername(String username) {
        fill(usernameLocator(), username)
        this as X
    }

    X fillPassword(String password) {
        fill(passwordLocator(), password)
        this as X
    }

    X fillPasswordCheck(String password) {
        fill(passwordCheckLocator(), password)
        this as X
    }

    X fillFirstName(String firstName) {
        fill(firstNameLocator(), firstName)
        this as X
    }

    X fillLastName(String lastName) {
        fill(lastNameLocator(), lastName)
        this as X
    }

    X fillEmail(String email) {
        fill(emailLocator(), email)
        this as X
    }

    X assertPasswordComplexityWarnings(List<String> warnings) {
        readFormValidationModal().assertErrorsEqual(warnings).capture().clickOK()
        this as X
    }

}
