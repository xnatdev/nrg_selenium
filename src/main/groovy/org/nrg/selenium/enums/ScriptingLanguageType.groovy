package org.nrg.selenium.enums

enum ScriptingLanguageType {
    GROOVY ('groovy', 'groovy'),
    PYTHON ('python', 'py'),
    JAVASCRIPT ('javascript', 'js'),
    NONE ('Select a Language', null)

    private final String languageName
    private final String fileExtension

    ScriptingLanguageType(String languageName, String fileExtension) {
        this.languageName = languageName
        this.fileExtension = fileExtension
    }

    String getLanguageName() {
        languageName
    }

    String getFileExtension() {
        fileExtension
    }
}
