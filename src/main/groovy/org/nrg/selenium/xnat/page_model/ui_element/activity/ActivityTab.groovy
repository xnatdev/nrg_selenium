package org.nrg.selenium.xnat.page_model.ui_element.activity

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By

class ActivityTab implements XnatPageComponent, ScreenshotableComponent<ActivityTab> {

    protected XpathLocator mainLocator = XnatLocators.byId('activity-tab')
    protected XpathLocator arbitraryItemLocator = mainLocator.joinSublocator('./div[@class="panel-body"]/div')
    List<ActivityTabItem> activityItems

    @Override
    void performInitialValidation() {
        readItems()
    }

    @Override
    List<By> getComponentLocators() {
        [mainLocator]
    }

    List<ActivityTabItem> readItems() {
        activityItems = findElements(arbitraryItemLocator)*.getAttribute('id').collect { id ->
            loadPage(ActivityTabItem).withId(id).withActivityTab(this)
        }
        activityItems
    }

    ActivityTabItem waitForNewItem() {
        waitForElement(arbitraryItemLocator.nthInstance(activityItems.size() + 1))
        readItems().last()
    }

}
