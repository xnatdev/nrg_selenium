package org.nrg.selenium.processing.element_comparators.cache

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import groovy.transform.EqualsAndHashCode
import org.openqa.selenium.WebDriver

@EqualsAndHashCode()
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = 'type')
@JsonSubTypes(
        @JsonSubTypes.Type(value = TextIndexArray.class, name = 'textIndexArray')
)
abstract class LocatorCache {

    String id
    String locator

    abstract void addToCache(WebDriver driver, Map<String, String> currentCache)

}
