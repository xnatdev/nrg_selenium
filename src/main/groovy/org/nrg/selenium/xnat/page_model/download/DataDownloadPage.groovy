package org.nrg.selenium.xnat.page_model.download

import org.nrg.selenium.util.BrowserDownloadManager
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.ui_element.popup.XnatDialog
import org.nrg.xnat.pogo.DataType
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.openqa.selenium.By

class DataDownloadPage implements NavigablePageObject<DataDownloadPage>, ScreenshotableComponent<DataDownloadPage> {

    protected List<By> screenshotElements = [By.id('layout_content'), By.xpath('//h2[text()="Imaging Data Download"]')]
    protected By allSessionCheckboxLocator = By.id('select-all-sessions')
    protected By allFormatsCheckboxLocator = By.id('select-all-scan-formats')
    protected By allScanTypesCheckboxLocator = By.id('select-all-scan-types')
    protected By allResourcesCheckboxLocator = By.id('select-all-resources')
    protected By allAssessorsCheckboxLocator = By.id('select-all-assessors')
    protected By includeProjectCheckboxLocator = By.id('project-in-path')
    protected By includeSubjectCheckboxLocator = By.id('subject-in-path')
    protected By simplifyArchiveCheckboxLocator = By.id('simplify-archive')
    protected By zipDownloadOptionLocator = By.id('direct-download')
    protected By downloadIdSublocator = XnatLocators.B_TAG
    protected By confirmDownloadSublocator = By.xpath('.//button[contains(text(), "Download")]')
    protected By submitButtonLocator = XnatLocators.buttonWithText('Submit')

    @Override
    List<By> getComponentLocators() {
        screenshotElements
    }

    @Override
    void performInitialValidation() {
        ([submitButtonLocator] + screenshotElements).each { locator ->
            waitForElement(locator as By)
        }
    }

    DataDownloadPage checkAllSessions() {
        performTriStateCheckBox(allSessionCheckboxLocator, true)
        this
    }

    DataDownloadPage uncheckAllSessions() {
        performTriStateCheckBox(allSessionCheckboxLocator, false)
        this
    }

    DataDownloadPage checkSession(ImagingSession session) {
        performCheckBox(sessionCheckboxLocator(session), true)
        this
    }

    DataDownloadPage checkSessions(List<ImagingSession> sessions) {
        sessions.each { session ->
            checkSession(session)
        }
        this
    }

    DataDownloadPage uncheckSession(ImagingSession session) {
        performCheckBox(sessionCheckboxLocator(session), false)
        this
    }

    DataDownloadPage uncheckSessions(List<ImagingSession> sessions) {
        sessions.each { session ->
            uncheckSession(session)
        }
        this
    }

    DataDownloadPage checkAllFormats() {
        performTriStateCheckBox(allFormatsCheckboxLocator, true)
        this
    }

    DataDownloadPage uncheckAllFormats() {
        performTriStateCheckBox(allFormatsCheckboxLocator, false)
        this
    }

    DataDownloadPage checkFormat(String format) {
        performCheckBox(scanFormatCheckboxLocator(format), true)
        this
    }

    DataDownloadPage uncheckFormat(String format) {
        performCheckBox(scanFormatCheckboxLocator(format), false)
        this
    }

    DataDownloadPage checkFormats(List<String> formats) {
        formats.each { format ->
            checkFormat(format)
        }
        this
    }

    DataDownloadPage uncheckFormats(List<String> formats) {
        formats.each { format ->
            uncheckFormat(format)
        }
        this
    }

    DataDownloadPage checkAllScanTypes() {
        performTriStateCheckBox(allScanTypesCheckboxLocator, true)
        this
    }

    DataDownloadPage uncheckAllScanTypes() {
        performTriStateCheckBox(allScanTypesCheckboxLocator, false)
        this
    }

    DataDownloadPage checkScanType(String scanType) {
        performCheckBox(scanTypeCheckboxLocator(scanType), true)
        this
    }

    DataDownloadPage uncheckScanType(String scanType) {
        performCheckBox(scanTypeCheckboxLocator(scanType), false)
        this
    }

    DataDownloadPage checkScanTypes(List<String> scanTypes) {
        scanTypes.each { scanType ->
            checkScanType(scanType)
        }
        this
    }

    DataDownloadPage uncheckScanTypes(List<String> scanTypes) {
        scanTypes.each { scanType ->
            uncheckScanType(scanType)
        }
        this
    }

    DataDownloadPage checkAllResources() {
        performTriStateCheckBox(allResourcesCheckboxLocator, true)
        this
    }

    DataDownloadPage uncheckAllResources() {
        performTriStateCheckBox(allResourcesCheckboxLocator, false)
        this
    }

    DataDownloadPage checkResource(String resource) {
        performCheckBox(resourceCheckboxLocator(resource), true)
        this
    }

    DataDownloadPage uncheckResource(String resource) {
        performCheckBox(resourceCheckboxLocator(resource), false)
        this
    }

    DataDownloadPage checkResources(List<String> resources) {
        resources.each { resource ->
            checkResource(resource)
        }
        this
    }

    DataDownloadPage uncheckResources(List<String> resources) {
        resources.each { resource ->
            uncheckResource(resource)
        }
        this
    }

    DataDownloadPage checkAllAssessors() {
        performTriStateCheckBox(allAssessorsCheckboxLocator, true)
        this
    }

    DataDownloadPage uncheckAllAssessors() {
        performTriStateCheckBox(allAssessorsCheckboxLocator, false)
        this
    }

    DataDownloadPage checkAssessor(DataType dataType) {
        performCheckBox(assessorCheckboxLocator(dataType), true)
        this
    }

    DataDownloadPage uncheckAssessor(DataType dataType) {
        performCheckBox(assessorCheckboxLocator(dataType), false)
        this
    }

    DataDownloadPage checkAssessors(List<DataType> dataTypes) {
        dataTypes.each { dataType ->
            checkAssessor(dataType)
        }
        this
    }

    DataDownloadPage uncheckAssessors(List<DataType> dataTypes) {
        dataTypes.each { dataType ->
            uncheckAssessor(dataType)
        }
        this
    }

    DataDownloadPage markProjectInclusionSetting(boolean state) {
        performCheckBox(includeProjectCheckboxLocator, state)
        this
    }

    DataDownloadPage markSubjectInclusionSetting(boolean state) {
        performCheckBox(includeSubjectCheckboxLocator, state)
        this
    }

    DataDownloadPage markSimplifiedDownloadStructureSetting(boolean state) {
        performCheckBox(simplifyArchiveCheckboxLocator, state)
        this
    }

    DataDownloadPage selectZipDownloadOption() {
        exceptionSafeClick(zipDownloadOptionLocator)
        this
    }

    File downloadAsFile() {
        exceptionSafeClick(submitButtonLocator)
        final BrowserDownloadManager downloadManager = new BrowserDownloadManager()
        final XnatDialog confirmationDialog = readXnatDialog('After the download begins')
        final String downloadId = exceptionSafeGetText(confirmationDialog.getSubElementLocator(downloadIdSublocator), false)
        confirmationDialog.capture().clickSubelement(confirmDownloadSublocator)
        downloadManager.findDownload(downloadId + ".zip")
    }

    protected By sessionCheckboxLocator(ImagingSession session) {
        By.xpath("//input[@class='select-item select-sessions' and contains(@value, ':${session.label}:')]")
    }

    protected By scanFormatCheckboxLocator(String format) {
        By.xpath("//input[@class='select-item select-scan-formats' and @value='${format}']")
    }

    protected By scanTypeCheckboxLocator(String type) {
        By.xpath("//input[@class='select-item select-scan-types' and @value='${type}']")
    }

    protected By resourceCheckboxLocator(String resourceName) {
        By.xpath("//input[@class='select-item select-resources' and @value='${resourceName}']")
    }

    protected By assessorCheckboxLocator(DataType dataType) {
        By.xpath("//input[@class='select-item select-assessors' and @value='${dataType.xsiType}']")
    }

}
