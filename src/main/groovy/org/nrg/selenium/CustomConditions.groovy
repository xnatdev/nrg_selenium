package org.nrg.selenium

import groovy.util.logging.Log4j
import org.apache.commons.lang3.StringUtils
import org.nrg.selenium.xnat.XnatDriver
import org.nrg.testing.xnat.conf.Settings
import org.openqa.selenium.*
import org.openqa.selenium.support.ui.ExpectedCondition
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.Select

import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

@Log4j
class CustomConditions {

    private CustomConditions() {}

    static ExpectedCondition<Boolean> textIsNonempty(final By locator) {
        ExpectedConditions.textMatches(locator, Pattern.compile('.+'))
    }

    static ExpectedCondition<Boolean> textEqualToSelectedOption(final By locator, final String text) {
        new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver driver) {
                try {
                    new Select(driver.findElement(locator)).firstSelectedOption.text == text
                } catch (StaleElementReferenceException ignored) {
                    null
                }
            }
        }
    }

    static ExpectedCondition<Boolean> elementValueTrueFalse(final By locator) {
        // check for the specified element to have a value of 'true' or 'false'
        new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver driver) {
                try {
                    driver.findElement(locator).getAttribute('value') in ['true', 'false']
                } catch (StaleElementReferenceException ignored) {
                    null
                }
            }
        }
    }

    static ExpectedCondition<Boolean> elementTextMatchesText(final By locator, final String regex) {
        new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver driver) {
                try {
                    driver.findElement(locator).text.matches(regex)
                } catch (StaleElementReferenceException ignored) {
                    null
                }
            }
        }
    }

    static ExpectedCondition<Boolean> elementNotPresent(final By by) {
        new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver driver) {
                try {
                    setTimeout(driver, 0)
                    if (driver.findElements(by).isEmpty()) {
                        setTimeout(driver, Settings.DEFAULT_TIMEOUT)
                        return true
                    }
                    return false
                } catch (StaleElementReferenceException ignored) {
                    return false
                } catch (WebDriverException ignored) {
                    return true
                }
            }
        }
    }

    static ExpectedCondition<Boolean> elementIsDisplayed(final By by) {
        new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver driver) {
                try {
                    setTimeout(driver, 0)
                    if (driver.findElement(by).isDisplayed()) {
                        setTimeout(driver, Settings.DEFAULT_TIMEOUT)
                        return true
                    }
                    return false
                } catch (WebDriverException ignored) {
                    // org.openqa.selenium.WebDriverException: unknown error: Element is not clickable at point ...
                    return null
                }
            }
        }
    }

    static ExpectedCondition<Boolean> elementIsEnabled(final By by) {
        new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver driver) {
                try {
                    driver.findElement(by).isEnabled()
                } catch (WebDriverException ignored) {
                    null
                }
            }
        }
    }

    static ExpectedCondition<Boolean> webElementClearSuccessful(final By by) {
        new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver driver) {
                try {
                    driver.findElement(by).clear()
                    return true
                } catch (StaleElementReferenceException ignored) {
                    return null
                } catch (InvalidElementStateException ignored) {
                    return false
                }
            }
        }
    }

    static ExpectedCondition<Boolean> presenceOfElementsLocated(final By by, final int numElements, final int timeout) {
        new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver driver) {
                setTimeout(driver, 10)
                try {
                    driver.navigate().refresh()
                    if (driver.findElements(by).size() == numElements) {
                        setTimeout(driver, timeout)
                        return true
                    }
                } catch (WebDriverException ignored) {}
                null
            }
        }
    }

    static ExpectedCondition<Boolean> clickUntilOtherElementVisible(final By clicked, final By visibilityCheck, final int checkDuration) {
        new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver driver) {
                setTimeout(driver, checkDuration)
                try {
                    driver.findElement(clicked).click()
                    if (driver.findElement(visibilityCheck).isDisplayed()) {
                        setTimeout(driver, Settings.DEFAULT_TIMEOUT)
                        return true
                    } else {
                        log.info("Clicking on element ${clicked} still did not reveal element ${visibilityCheck}...")
                        return false
                    }
                } catch (WebDriverException ignored) {
                    return null
                }
            }
        }
    }

    static ExpectedCondition<Boolean> clickUntilAttributeEquals(final By locator, final String attribute, final String attributeValue) {
        new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver driver) {
                setTimeout(driver, 0)
                try {
                    driver.findElement(locator).click()
                    if (attributeValue == driver.findElement(locator).getAttribute(attribute)) {
                        setTimeout(driver, Settings.DEFAULT_TIMEOUT)
                        return true
                    }
                } catch (Exception ignored) {}
                false
            }
        }
    }

    static ExpectedCondition<Boolean> attributeEndsWith(final By locator, final String attribute, final String endText) {
        attributeSatisfies(locator, attribute, { x -> x.endsWith(endText) })
    }

    static ExpectedCondition<Boolean> attributeEquals(final By locator, final String attribute, final String desiredText) {
        attributeSatisfies(locator, attribute, { x -> x == desiredText })
    }

    static ExpectedCondition<Boolean> attributeIsOneOf(final By locator, final String attribute, final List<String> possibleValues) {
        attributeSatisfies(locator, attribute, { x -> x in possibleValues })
    }

    static ExpectedCondition<Boolean> attributeNonempty(final By locator, final String attribute) {
        attributeSatisfies(locator, attribute, { x -> !StringUtils.isEmpty(x as String) })
    }

    static ExpectedCondition<Boolean> attributeSatisfies(final By locator, final String attribute, Closure<Boolean> criterionClosure) {
        new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver driver) {
                setTimeout(driver, 0)
                try {
                    final WebElement element = driver.findElement(locator)
                    if (criterionClosure(element.getAttribute(attribute))) {
                        setTimeout(driver, Settings.DEFAULT_TIMEOUT)
                        return true
                    } else {
                        return false
                    }
                } catch (WebDriverException ignored) {
                    return null
                }
            }
        }
    }

    static ExpectedCondition<Boolean> elementClickable(final By locator) {
        new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver driver) {
                setTimeout(driver, 0)
                try {
                    final WebElement element = driver.findElement(locator)
                    if (element.isDisplayed() && element.isEnabled()) {
                        setTimeout(driver, Settings.DEFAULT_TIMEOUT)
                        return true
                    } else {
                        return false
                    }
                } catch (WebDriverException ignored) {
                    return null
                }
            }
        }
    }

    static ExpectedCondition<Boolean> asyncDone() {
        new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver driver) {
                final boolean jqDone = (driver as JavascriptExecutor).executeScript('return jQuery.active == 0')
                final boolean yuiDone = (driver as JavascriptExecutor).executeScript('var inProgress=0; for(var i=0; i < YAHOO.util.Connect._transaction_id; i++) {if(YAHOO.util.Connect.isCallInProgress(i)) inProgress++;} return inProgress;').toString() == '0'
                jqDone && yuiDone
            }
        }
    }

    static ExpectedCondition<Boolean> urlContains(final String fragment) {
        new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver driver) {
                driver.currentUrl.contains(fragment)
            }
        }
    }

    private static void setTimeout(WebDriver driver, int timeout) {
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS)
    }
}
