package org.nrg.selenium.xnat.page_model.site_search

import org.apache.commons.lang3.StringUtils
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.xnat.pogo.Project
import org.openqa.selenium.By

class AdvancedSearchPage2 implements NavigablePageObject<AdvancedSearchPage2> {

    protected By criteriaSectionScreenshotLocator = By.cssSelector('li.step3')
    protected String tracerIdentifier = 'Tracer'

    AdvancedSearchPage2 screenshotCriteriaSection() {
        uploadTargetedScreenshot(criteriaSectionScreenshotLocator)
        this
    }

    AdvancedSearchPage2 captureCriteriaSection() {
        captureStep(criteriaSectionScreenshotLocator)
        this
    }

    AdvancedSearchPage2 selectProject(Project project) {
        pseudoSelect(By.xpath("//div[@class='project-multi-select']"), project.id)
        this
    }

    AdvancedSearchPage2 selectCriterion(String criterionName, String selectionValue) {
        exceptionSafeSelect(selectionLocator(criterionName), selectionValue)
        this
    }

    AdvancedSearchPage2 fillCriterion(String criterionName, String fillValue) {
        fill(inputLocator(criterionName), fillValue)
        this
    }

    AdvancedSearchPage2 specifyTracer(String tracer) {
        fillCriterion(tracerIdentifier, tracer)
    }

    AdvancedSearchResult submitSearch() {
        submitForm()
        loadPage(AdvancedSearchResult)
    }

    protected By selectionLocator(String dropdownLabel) {
        By.xpath("//select[./preceding-sibling::h5[text()='${StringUtils.stripEnd(dropdownLabel, ':')}:']]")
    }

    protected By inputLocator(String criterionName) {
        By.xpath("//input[@type='text'][./preceding-sibling::h5[text()='${StringUtils.stripEnd(criterionName, ':')}:']]")
    }

}
