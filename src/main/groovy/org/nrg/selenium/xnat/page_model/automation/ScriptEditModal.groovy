package org.nrg.selenium.xnat.page_model.automation

import org.nrg.selenium.exceptions.DuplicateScriptException
import org.nrg.selenium.exceptions.MissingScriptContentException
import org.nrg.selenium.exceptions.MissingScriptIdException
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.ui_element.popup.XModal
import org.nrg.selenium.xnat.scripting.AutomationScript
import org.openqa.selenium.By
import org.openqa.selenium.Keys

import static org.testng.AssertJUnit.assertEquals

class ScriptEditModal implements XnatPageComponent, ScreenshotableComponent<ScriptEditModal> {

    protected By scriptIdLocator = By.name('script-id-input')
    protected By scriptLabelLocator = By.name('scriptLabel')
    protected By scriptDescriptionLocator = By.name('script-description')
    protected By scriptContentsLocator = XnatLocators.TEXTAREA_TAG
    protected By modalLocator = By.xpath("//div[contains(@title, 'XNAT Script Editor')]/..")
    protected XpathLocator arbitraryScriptLineLocator = new XpathLocator("//div[@class='ace_layer ace_text-layer']/div[@class='ace_line']")
    protected By postSaveDialogLocator = By.xpath("//div[normalize-space(@class)='xmodal v1 xmodal-message enter esc open top']")
    protected By cancelButtonLocator = XnatLocators.buttonWithText('Cancel')
    protected By saveButtonLocator = XnatLocators.buttonWithText('Save and Close')

    @Override
    List<By> getComponentLocators() {
        [modalLocator]
    }

    ScriptsTab cancel() {
        exceptionSafeClick(cancelButtonLocator)
        loadPage(ScriptsTab)
    }

    ScriptEditModal fillScriptId(String scriptId) {
        fill(scriptIdLocator, scriptId)
        this
    }

    ScriptEditModal fillScriptLabel(String scriptLabel) {
        exceptionSafeClick(scriptLabelLocator)
        fill(scriptLabelLocator, scriptLabel)
        this
    }

    ScriptEditModal fillScriptDescription(String scriptDescription) {
        fill(scriptDescriptionLocator, scriptDescription)
        this
    }

    ScriptEditModal forceClearScriptContents() {
        forceClear(scriptContentsLocator)
        this
    }

    ScriptEditModal fillScriptContents(String scriptContents) {
        // fill(..., true) is needed because the content locator isn't displayed
        if (scriptContents != null) {
            scriptContents.eachLine { line ->
                findElement(scriptContentsLocator).sendKeys(Keys.HOME)
                findElement(scriptContentsLocator).sendKeys(Keys.SHIFT + Keys.END)
                findElement(scriptContentsLocator).sendKeys(Keys.SHIFT + Keys.ARROW_DOWN)
                findElement(scriptContentsLocator).sendKeys(Keys.DELETE)
                findElement(scriptContentsLocator).sendKeys(line + '\n')
            }
            findElement(scriptContentsLocator).sendKeys(Keys.BACK_SPACE)
        }
        this
    }

    ScriptsTab submit() throws MissingScriptIdException, MissingScriptContentException, DuplicateScriptException {
        exceptionSafeClick(saveButtonLocator)
        final XModal responseDialog = readXModal(postSaveDialogLocator).uploadScreenshot()
        final String dialogText = responseDialog.bodyText
        responseDialog.clickOK()

        switch (dialogText) {
            case 'Your script was successfully saved.' :
                captureScreenshotlessStep()
                break
            case 'Please give your script an ID before saving.' :
                cancel()
                throw new MissingScriptIdException()
            case 'Please add script content and try again.' :
                cancel()
                throw new MissingScriptContentException()
            case 'A script with that ID already exists. Please enter a new ID and try again.' :
                cancel()
                throw new DuplicateScriptException()
            default :
                throw new RuntimeException("Unknown script error content: ${dialogText}")
        }
        loadPage(ScriptsTab)
    }

    ScriptsTab duplicate(AutomationScript originalScript, AutomationScript newScript) {
        fillScriptId(newScript.scriptID)
        setDuplicatedScriptLabel(originalScript, newScript)
        if (newScript.scriptDescription != null) {
            if (newScript.appendScriptOnDuplicate) {
                findElement(scriptDescriptionLocator).sendKeys(newScript.scriptDescription)
                newScript.setScriptDescription(originalScript.scriptDescription + newScript.scriptDescription)
            } else {
                fillScriptDescription(newScript.scriptDescription)
            }
        }
        if (newScript.scriptContents != null) {
            if (newScript.appendScriptOnDuplicate) {
                findElement(scriptContentsLocator).sendKeys(newScript.scriptContents + '\n')
                newScript.setScriptContents(newScript.scriptContents + '\n' + originalScript.scriptContents)
            } else {
                forceClearScriptContents()
                fillScriptContents(newScript.scriptContents)
            }
        }
        uploadScreenshot().submit()
    }

    void setDuplicatedScriptLabel(AutomationScript originalScript, AutomationScript newScript) {
        if (newScript.scriptLabel == null) {
            fillScriptLabel('')
            newScript.setScriptLabel(newScript.scriptID)
        } else {
            fillScriptLabel(newScript.scriptLabel)
        }
    }

    List<String> readScriptLines() {
        (1 .. findElements(arbitraryScriptLineLocator).size()).collect { index ->
            exceptionSafeGetText(arbitraryScriptLineLocator.nthInstance(index))
        }
    }

    ScriptEditModal assertLinesRepresentScript(AutomationScript script) {
        assertEquals(script.scriptContents.split('\n') as List<String>, readScriptLines())
        this
    }

}
