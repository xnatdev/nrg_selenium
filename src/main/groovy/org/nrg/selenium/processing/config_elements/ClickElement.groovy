package org.nrg.selenium.processing.config_elements

import org.nrg.selenium.xnat.XnatDriver
import org.openqa.selenium.By

class ClickElement implements PipelineConfigElement {

    private By by

    ClickElement(By by) {
        this.by = by
    }

    @Override
    void performElementAction(XnatDriver xnatDriver) {
        xnatDriver.exceptionSafeClick(by)
    }

}
