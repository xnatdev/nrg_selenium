package org.nrg.selenium.xnat.page_model.ui_element.notification

import org.openqa.selenium.By

class XnatWarning implements XnatNotification<XnatWarning> {

    @Override
    By getLocator() {
        By.cssSelector('div.warning')
    }

}
