package org.nrg.selenium.extension

class XpathUnion extends StandaloneXpathLocator {

    final List<XpathLocator> expressions

    XpathUnion(List<XpathLocator> locators) {
        super("(${locators*.expression.join(' | ')})")
        expressions = locators
    }

    @Override
    StandaloneXpathLocator joinSublocator(String sublocator) {
        new StandaloneXpathLocator(super.joinSublocator(sublocator).expression)
    }

    @Override
    XpathLocator nthInstance(String index) {
        new StandaloneXpathLocator("${expression}[${index}]")
    }
}
