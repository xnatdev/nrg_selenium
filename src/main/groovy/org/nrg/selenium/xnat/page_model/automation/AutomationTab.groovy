package org.nrg.selenium.xnat.page_model.automation

import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.openqa.selenium.By

trait AutomationTab implements XnatPageComponent, AutomationBase {

    abstract By tabLocator()

}
