package org.nrg.selenium.xnat.page_model.ui_element.notification

import org.nrg.selenium.enums.SiteAlertType
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.pagefactory.ByChained

import static org.testng.AssertJUnit.assertEquals

trait XnatSiteAlert implements XnatNotification<XnatSiteAlert> {

    SiteAlertType alertType

    @Override
    void performInitialValidation() {
        XnatNotification.super.performInitialValidation()
        final List<WebElement> elements = findElements(locator)
        assertEquals(1, elements.size()) // can only have 1 site alert
        alertType = readType(elements[0])
    }

    abstract SiteAlertType readType(WebElement element)

    void clickLinkInAlert() {
        exceptionSafeClick(new ByChained(locator, XnatLocators.HYPERLINK_TAG))
    }

}
