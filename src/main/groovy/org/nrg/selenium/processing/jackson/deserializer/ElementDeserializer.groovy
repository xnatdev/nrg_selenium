package org.nrg.selenium.processing.jackson.deserializer

import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.nrg.selenium.enums.LocatorType
import org.nrg.selenium.processing.element_comparators.Element
import org.nrg.selenium.processing.element_comparators.Locator
import org.nrg.xnat.jackson.deserializers.CustomDeserializer

class ElementDeserializer extends CustomDeserializer<Element> {

    @Override
    Element deserialize(ObjectCodec objectCodec, JsonNode jsonNode) throws IOException {
        final Element element = new Element()
        if (fieldNonnull(jsonNode, 'locator')) {
            final JsonNode locatorNode = jsonNode.get('locator')
            if (locatorNode.isTextual()) {
                element.setLocator(new Locator().type(LocatorType.XPATH).value(locatorNode.asText()))
            } else {
                element.setLocator((objectCodec as ObjectMapper).readValue(locatorNode.toString(), Locator))
            }
        }
        if (fieldNonnull(jsonNode, 'value')) {
            element.setValue(jsonNode.get('value').asText())
        }
        element
    }

}
