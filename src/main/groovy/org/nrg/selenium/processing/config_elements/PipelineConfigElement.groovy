package org.nrg.selenium.processing.config_elements

import org.nrg.selenium.xnat.XnatDriver

interface PipelineConfigElement {

    void performElementAction(XnatDriver seleniumUtils)

}
