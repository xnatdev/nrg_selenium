package org.nrg.selenium.xnat.page_model.prearchive

import org.nrg.xnat.versions.*
import org.openqa.selenium.By

class XnatPrearchive_1_6 extends XnatPrearchive {

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev]
    }

    @Override
    List<By> getComponentLocators() {
        [prearchiveUiLocator]
    }

}
