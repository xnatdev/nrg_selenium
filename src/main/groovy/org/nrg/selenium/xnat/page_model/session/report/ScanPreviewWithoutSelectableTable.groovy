package org.nrg.selenium.xnat.page_model.session.report

import org.nrg.xnat.versions.*

class ScanPreviewWithoutSelectableTable extends ScanPreview {

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev, Xnat_1_7_2, Xnat_1_7_3, Xnat_1_7_4]
    }

    @Override
    void performNavigationToUnderlyingPage() {}

}
