package org.nrg.selenium.enums

enum ManageFilesResourceLevel {

    RESOURCES ('resources', 'Resources'),
    SCANS ('scans', 'scans'),
    ASSESSORS ('assessors', 'assessors')

    String creationName
    String validationName

    ManageFilesResourceLevel(String creationName, String validationName) {
        setCreationName(creationName)
        setValidationName(validationName)
    }

    String getCreationName() {
        creationName
    }

    String getValidationName() {
        validationName
    }

}