package org.nrg.selenium.processing.config_elements

import org.nrg.selenium.xnat.XnatDriver
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertEquals

class AssertTextElement implements PipelineConfigElement {

    private By by
    private String expectedText

    AssertTextElement(By by, String expectedText) {
        this.by = by
        this.expectedText = expectedText
    }

    @Override
    void performElementAction(XnatDriver xnatDriver) {
        assertEquals(expectedText, xnatDriver.exceptionSafeGetText(by))
    }

}
