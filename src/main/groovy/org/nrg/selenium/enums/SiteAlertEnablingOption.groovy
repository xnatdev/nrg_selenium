package org.nrg.selenium.enums

enum SiteAlertEnablingOption {
    OFF ('Off'),
    LOGIN_PAGE ('On (Login Page Only)'),
    ON_EVERYWHERE ('On (Login and Site Header)')

    private final String settingName

    SiteAlertEnablingOption(String settingName) {
        this.settingName = settingName
    }

    String getSettingName() {
        settingName
    }
}
