package org.nrg.selenium.xnat.page_model.upload

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.ui_element.HtmlTable
import org.openqa.selenium.By

class SpreadsheetUploadReviewPage implements NavigablePageObject<SpreadsheetUploadReviewPage>, HtmlTable<SpreadsheetUploadReviewPage> {

    protected By headerLocator = XnatLocators.H2_TAG
    protected By submitButtonLocator = By.name('eventSubmit_doStore')

    @Override
    void performInitialValidation() {
        assertElementText('Please review the records before storing.', headerLocator)
    }

    @Override
    List<By> getComponentLocators() {
        [By.id('breadcrumbs'), By.xpath("//input[@value='Cancel']")]
    }

    @Override
    XpathLocator getTableLocator() {
        new XpathLocator("//div[@id='layout_content']/table")
    }

    SpreadsheetUploadResultPage submit() {
        exceptionSafeClick(submitButtonLocator)
        loadPage(SpreadsheetUploadResultPage)
    }

}
