package org.nrg.selenium.xnat.page_model.users

import org.nrg.selenium.extension.XpathUnion
import org.nrg.selenium.xnat.page_model.IndexPage
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.ui_element.popup.FormValidationModal
import org.nrg.testing.xnat.conf.Settings
import org.openqa.selenium.By

class RegistrationPage implements UserEditComponent<RegistrationPage>, ScreenshotableComponent<RegistrationPage> {

    protected By logoLocator = By.xpath('//*[@id="header_logo"]/img')
    protected By commentsLocator = By.name('comments')
    protected By phoneLocator = By.name('phone')
    protected By labLocator = By.name('lab')
    protected By registerButtonLocator = By.id('Register')

    @Override
    List<By> getComponentLocators() {
        [logoLocator, registerButtonLocator]
    }

    @Override
    By usernameLocator() {
        xdatUserField('login')
    }

    @Override
    By passwordLocator() {
        xdatUserField('primary_password')
    }

    @Override
    By passwordCheckLocator() {
        xdatUserField('password_check')
    }

    @Override
    By firstNameLocator() {
        xdatUserField('firstname')
    }

    @Override
    By lastNameLocator() {
        xdatUserField('lastname')
    }

    @Override
    By emailLocator() {
        xdatUserField('email')
    }

    RegistrationPage fillComment(String comment) {
        fill(commentsLocator, comment)
        this
    }

    RegistrationPage fillPhone(String phone) {
        fill(phoneLocator, phone)
        this
    }

    RegistrationPage fillLab(String lab) {
        fill(labLocator, lab)
        this
    }

    RegistrationPage fillSpecificFields(String comment, String phone, String lab) {
        fillComment(comment).fillPhone(phone).fillLab(lab)
    }

    RegistrationPage clickRegisterButton() {
        final PostregistrationPage postregistrationPageInstance = getInstance(PostregistrationPage)
        final IndexPage indexPageInstance = getInstance(IndexPage)
        final FormValidationModal formValidationModalInstance = getInstance(FormValidationModal)
        final By completionLocator = new XpathUnion([
                indexPageInstance.homeLocator,
                XnatLocators.byText(postregistrationPageInstance.verificationSentString),
                XnatLocators.byText(postregistrationPageInstance.registrationReceivedString),
                formValidationModalInstance.modalLocator

        ])
        clickUntilOtherElementVisible(registerButtonLocator, completionLocator, 0)
        this
    }

    PostregistrationPage assertRegistrationRequiresEmailVerification() {
        loadPage(PostregistrationPage).assertVerificationRequired()
    }

    PostregistrationPage assertRegistrationDoesNotRequireEmailVerification() {
        loadPage(PostregistrationPage).assertVerificationNotRequired()
    }

    protected By xdatUserField(String property) {
        By.id("xdat:user.${property}")
    }

}
