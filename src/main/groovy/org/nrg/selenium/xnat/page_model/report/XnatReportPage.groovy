package org.nrg.selenium.xnat.page_model.report

import org.nrg.selenium.util.BrowserDownloadManager
import org.nrg.selenium.xnat.page_model.AuditTrailPage
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatDeleteComponent
import org.nrg.selenium.xnat.page_model.XnatEditPage
import org.nrg.selenium.xnat.page_model.ui_element.HasBreadcrumbs
import org.nrg.selenium.xnat.page_model.ui_element.actions.ActionsBox
import org.nrg.selenium.xnat.page_model.ui_element.actions.HasActionsBox
import org.nrg.selenium.xnat.page_model.ui_element.manage_files.ManageFiles
import org.nrg.xnat.pogo.Extensible
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertTrue

trait XnatReportPage<T extends Extensible<T>, X extends XnatReportPage<T, X>> extends NavigablePageObject<X> implements ScreenshotableComponent<X>, HasActionsBox, HasBreadcrumbs {

    By editLinkLocator = By.linkText('Edit')
    By manageFilesLocator = By.linkText('Manage Files')
    By auditTrailTabLocator = By.xpath("//em[contains(text(), 'Audit Trail')]")
    String customResourceUploaderIdentifier = 'Upload Additional Files'

    abstract <U extends XnatEditPage<T, U, X>> XnatEditPage<T, U, X> loadEditPage()

    abstract <U extends XnatDeleteComponent> U loadDeleteComponent()

    abstract void validateObject(T xnatObject)

    X assertPageRepresents(T xnatObject) {
        validateObject(xnatObject)
        this as X
    }

    List<String> downloadXmlActionsPath() {
        ['Download XML']
    }

    File downloadXml() {
        waitForDocumentReadyState()
        final BrowserDownloadManager downloadManager = new BrowserDownloadManager()
        actionsBoxNavigation(downloadXmlActionsPath())
        final File xml = downloadManager.findDownload()
        assertTrue(xml.name.endsWith('.xml'))
        xml
    }

    ManageFiles loadManageFiles() {
        retryClickIfNotVisible(manageFilesLocator, getInstance(ManageFiles).titleLocator, 1)
        loadPage(ManageFiles).withUnderlyingPage(this)
    }

    AuditTrailPage clickAuditTrailTab() {
        exceptionSafeClick(auditTrailTabLocator)
        loadPage(AuditTrailPage)
    }

    /*
     The click action for this element is wired into the page late, so there's no guarantee it will work. We'll wait
     for timeout as usual on attempting to load the dialog that should have been launched, but give it one last try
     */
    CustomResourceUploadDialog<T, X> loadCustomResourceUploader(boolean retry = true) {
        actionsBoxNavigation([customResourceUploaderIdentifier])
        try {
            loadPage(CustomResourceUploadDialog).withUnderlyingPage(this)
        } catch (Throwable throwable) {
            if (retry) {
                loadCustomResourceUploader(false)
            } else {
                throw throwable
            }
        }
    }

    XnatReportPage<T, X> assertCustomResourceUploadStatus(boolean isPresent) {
        final ActionsBox actionsBox = readActionsBox()
        final By locator = actionsBox.locatorsFor([customResourceUploaderIdentifier]).last()
        if (isPresent) {
            assertElementPresent(locator)
        } else {
            waitForElementNotVisible(locator) // can't just assertNotPresent since it's there, but set to hidden in the DOM
        }
        actionsBox.capture()
        this
    }

}