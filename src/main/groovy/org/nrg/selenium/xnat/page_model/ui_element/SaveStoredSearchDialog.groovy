package org.nrg.selenium.xnat.page_model.ui_element

import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By

class SaveStoredSearchDialog implements OverlayingPage<SaveStoredSearchDialog, YuiTable>, ScreenshotableComponent<SaveStoredSearchDialog>, XnatPageComponent {

    protected By dialogLocator = By.xpath('//div[text()="Save Search"]/..')
    protected By cancelButtonLocator = XnatLocators.buttonWithText('Cancel')
    protected By submitButtonLocator = XnatLocators.buttonWithText('Submit')
    protected By briefDescriptionLocator = By.id('save_brief')
    protected By fullDescriptionLocator = By.id('save_desc')

    @Override
    void performNavigationToUnderlyingPage() {
        exceptionSafeClick(cancelButtonLocator)
    }

    YuiTable clickCancel() {
        returnToUnderlyingPage()
    }

    SaveStoredSearchDialog fillBriefDescription(String description) {
        fill(briefDescriptionLocator, description)
        this
    }

    SaveStoredSearchDialog fillFullDescription(String description) {
        fill(fullDescriptionLocator, description)
        this
    }

    YuiTable saveSearch() {
        exceptionSafeClick(submitButtonLocator)
        underlyingPage
    }

    @Override
    List<By> getComponentLocators() {
        [dialogLocator]
    }

}
