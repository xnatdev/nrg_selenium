package org.nrg.selenium.xnat.page_model.admin

import org.openqa.selenium.By

class AdminUiButton implements AdminUiElement {

    AdminUiButton(By by, By screenshot = null, boolean takeScreenshot = true) {
        setLocator(by)
        setScreenshotLocator(screenshot)
        setTakeScreenshot(takeScreenshot)
    }

    @Override
    void performElementAction() {
        exceptionSafeClick(locator)
    }

}
