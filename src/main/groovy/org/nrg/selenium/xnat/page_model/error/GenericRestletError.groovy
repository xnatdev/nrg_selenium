package org.nrg.selenium.xnat.page_model.error

import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By

class GenericRestletError implements GenericError<GenericRestletError> {

    @Override
    By getMainErrorLocator() {
        XnatLocators.H3_TAG
    }

}
