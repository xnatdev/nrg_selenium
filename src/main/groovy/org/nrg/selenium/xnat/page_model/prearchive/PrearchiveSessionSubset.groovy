package org.nrg.selenium.xnat.page_model.prearchive

import org.nrg.selenium.enums.PrearchiveStatus
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.session.report.ReviewAndArchivePage
import org.nrg.xnat.pogo.Project
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertEquals

class PrearchiveSessionSubset implements XnatPageComponent, ScreenshotableComponent<PrearchiveSessionSubset> {

    int size
    XnatPrearchive prearchive
    XpathLocator subsetLocator
    PrearchiveSearchCriteria originalSearchCriteria
    boolean checked = false

    @Override
    List<By> getComponentLocators() {
        prearchive.componentLocators
    }

    PrearchiveSessionSubset checkAll() {
        if (!checked) {
            clickAll(prearchive.checkboxesFor(this))
            uploadScreenshot()
            checked = true
        }
        this
    }

    XnatPrearchive rebuild() {
        checkAll()
        prearchive.rebuild(this)
    }

    XnatPrearchive archive() {
        checkAll()
        prearchive.archive(this)
    }

    XnatPrearchive rebuildAndArchive() {
        originalSearchCriteria.setStatus(PrearchiveStatus.READY)
        rebuild().searchFor(originalSearchCriteria).archive()
    }

    ReviewAndArchivePage reviewAndArchive() {
        if (size != 1) {
            throw new UnsupportedOperationException('Review and Archive process can only be initiated for a single session at a time.')
        }
        checkAll()
        prearchive.reviewAndArchive()
    }

    List<String> readProjectsForResults() {
        findElements(subsetLocator.joinSublocator(prearchive.projectSublocator))*.text
    }

    PrearchiveSessionSubset assertProjectsInSubset(Collection<Project> projects) {
        assertEquals(projects*.id.toSet(), readProjectsForResults().toSet())
        this
    }

    PrearchiveDetails loadDetails() {
        if (size != 1) {
            throw new UnsupportedOperationException('Details can only be viewed for a single session.')
        }
        prearchive.loadDetails()
    }

}
