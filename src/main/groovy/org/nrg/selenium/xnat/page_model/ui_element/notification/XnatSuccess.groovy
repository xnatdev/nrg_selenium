package org.nrg.selenium.xnat.page_model.ui_element.notification

import org.openqa.selenium.By

class XnatSuccess implements XnatNotification<XnatSuccess> {

    @Override
    By getLocator() {
        By.cssSelector('div.success')
    }

}
