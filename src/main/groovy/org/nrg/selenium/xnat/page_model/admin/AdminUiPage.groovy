package org.nrg.selenium.xnat.page_model.admin

import org.apache.commons.lang3.StringUtils
import org.nrg.selenium.enums.PasswordExpirationType
import org.nrg.selenium.enums.SeriesImportFilterType
import org.nrg.selenium.enums.SiteAlertEnablingOption
import org.nrg.selenium.enums.SiteAlertType
import org.nrg.selenium.xnat.page_model.LoadablePage
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.SimpleTabbedPage
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.xnat.enums.PetMrProcessingSetting
import org.nrg.xnat.pogo.AnonScript
import org.openqa.selenium.By
import org.openqa.selenium.support.pagefactory.ByChained

class AdminUiPage implements NavigablePageObject<AdminUiPage>, LoadablePage, SimpleTabbedPage<AdminUiPage> {

    String siteSetupTab = 'Site Setup'
    String securityTab = 'Security'
    String registrationOptionsTab = 'Registration Options'
    String sessionUploadTab = 'Session Upload, Import & Anonymization'
    String fileSystemTab = 'File System'
    String fileSystemBehaviorTab = 'File System Behavior'
    String siteWideAlertTab = 'Site-wide Alerts'
    protected By saveSuccessLocator = By.xpath("//div[@class='banner top-banner success']")
    protected By loginRequiredLocator = By.id('require-login')
    protected By siteAlertStatusLocator = By.id('site-wide-alert-status')
    protected By siteAlertContentLocator = By.name('siteWideAlertMessage')
    protected By siteAlertTypeLocator = By.name('siteWideAlertType')
    protected By restrictUserListLocator = By.id('restrict-user-list-access-to-admins')
    protected By allowNonadminProjectsLocator = By.id('ui-allow-non-admin-project-creation')
    protected By aliasTokenTimeoutLocator = By.id('alias-token-timeout')
    protected By aliasTokenTimeoutScheduleLocator = By.id('alias-token-timeout-schedule')
    protected By sessionTimeoutLocator = By.id('session-timeout')
    protected By passwordComplexityLocator = By.id('password-complexity')
    protected By passwordComplexityMessageLocator = By.id('password-complexity-message')
    protected By requireSaltedPasswordLocator = By.id('requireSaltedPasswords')
    protected By maxConcurrentSessionsLocator = By.name('concurrentMaxSessions')
    protected By inactivityIntervalLocator = By.name('inactivityBeforeLockout')
    protected By inactivityScheduleLocator = By.name('inactivityBeforeLockoutSchedule')
    protected By loginFailureMessageLocator = By.id('ui-login-failure-message')
    protected By maxFailedLoginsLocator = By.id('max-failed-logins')
    protected By failedLoginLockoutDurationLocator = By.id('max-failed-logins-lockout-duration')
    protected By failedLoginsScheduleLocator = By.id('reset-failed-logins-schedule')
    protected By passwordExpirationDisabledLocator = By.id('passwordExpirationTypeDisabled')
    protected By passwordExpirationIntervalIndicatorLocator = By.id('passwordExpirationTypeInterval')
    protected By passwordExpirationIntervalFieldLocator = By.id('password-expiration-interval')
    protected By passwordExpirationDateIndicatorLocator = By.id('passwordExpirationTypeDate')
    protected By passwordExpirationDateFieldLocator = By.id('password-expiration-date')
    protected By passwordExpirationAreaLocator = By.xpath("//*[text()='Password Expiration']/..")
    protected By autoenableLocator = By.id('user-registration')
    protected By autoenableWithParLocator = By.id('par')
    protected By enableAnonLocator = By.id('enable-sitewide-anonymization-script')
    protected By anonScriptLocator = By.id('sitewide-anonymization-script')
    protected By enableSeriesImportFilterLocator = By.id('enable-sitewide-series-import-filter')
    protected By seriesImportFilterModeLocator = By.id('sitewide-series-import-filter-mode')
    protected By seriesImportFilterTextLocator = By.id('sitewide-series-import-filter')
    protected By petMrLocator = By.id('sitewide-pet-mr')
    protected By zipExtensionsLocator = By.id('zip-extensions')

    @Override
    void navigateTo() {
        topNav('Administer', 'Site Administration')
    }

    // Assumes all config elements are in the same section of the page
    AdminUiPage setConfigSettings(String tab, List<AdminUiElement> settings) {
        switchToTab(tab)
        settings.each { element ->
            element.setXnatDriver(xnatDriver)
            element.setElement()
        }
        exceptionSafeClick(saveButtonForElement(settings[0].locator))
        assertSiteSaveSuccess()
    }

    AdminUiPage setConfigSetting(String tab, AdminUiElement setting) {
        setConfigSettings(tab, [setting])
    }

    AdminUiPage assertSiteSaveSuccess() {
        assertElementText('Data saved successfully.', saveSuccessLocator)
        setTimeout(3) // we don't want to wait the entire duration of the timeout if the banner disappears before we can screenshot it
        try {
            captureStep(saveSuccessLocator)
        } catch (Exception ignored) {
            captureScreenshotlessStep() // If the save success banner disappears before we can screenshot it, just skip the screenshot
        }
        restoreTimeout()
        waitForNotElement(saveSuccessLocator)
        this
    }

    AdminUiPage setAppletLinkDisplayed(boolean state) {
        throw new UnsupportedOperationException('Applet link configuration setting has been removed in 1.7')
    }

    AdminUiPage setSiteAlert(SiteAlertEnablingOption enablingOption, String alertMessage, SiteAlertType alertType) {
        setConfigSettings(siteWideAlertTab, [
                new AdminUiSelect(siteAlertStatusLocator, enablingOption.settingName, null, false),
                new AdminUiInput(siteAlertContentLocator, alertMessage, null, false),
                new AdminUiSelect(siteAlertTypeLocator, StringUtils.capitalize(alertType.name().toLowerCase()))
        ])
    }

    AdminUiPage setLoginRequired(boolean state) {
        setConfigSetting(securityTab, new AdminUiSwitchBox(loginRequiredLocator, state))
    }

    AdminUiPage toggleLoginRequired() {
        setConfigSetting(securityTab, new AdminUiSwitchBoxToggle(loginRequiredLocator))
    }

    AdminUiPage setUserListRestriction(boolean state) {
        setConfigSetting(securityTab, new AdminUiSwitchBox(restrictUserListLocator, state))
    }

    AdminUiPage setNonadminProjectsAllowed(boolean state) {
        setConfigSetting(securityTab, new AdminUiSwitchBox(allowNonadminProjectsLocator, state))
    }

    AdminUiPage setSessionTimeout(String timeout) {
        setConfigSetting(securityTab, new AdminUiInput(sessionTimeoutLocator, timeout))
    }

    AdminUiPage restoreDefaultSessionTimeout() {
        setSessionTimeout(defaults.defaultSessionTimeout)
    }

    AdminUiPage setAliasTokenTimeout(String timeout, String schedule) {
        setConfigSettings(securityTab, [
                new AdminUiInput(aliasTokenTimeoutLocator, timeout),
                new AdminUiInput(aliasTokenTimeoutScheduleLocator, schedule)
        ])
    }

    AdminUiPage restoreDefaultAliasTokenTimeouts() {
        setAliasTokenTimeout(defaults.defaultAliasTokenInterval, defaults.defaultAliasTokenSchedule)
    }

    AdminUiPage setPasswordComplexity(String regex, String complexityMessage) {
        setConfigSettings(securityTab, [
                new AdminUiInput(passwordComplexityLocator, regex),
                new AdminUiInput(passwordComplexityMessageLocator, complexityMessage)
        ])
    }

    AdminUiPage restoreDefaultPasswordComplexity() {
        setPasswordComplexity(defaults.defaultPasswordComplexity, defaults.defaultPasswordComplexityWarning)
    }

    AdminUiPage setPasswordSaltRequirement(boolean saltRequired) {
        setConfigSetting(securityTab, new AdminUiSwitchBox(requireSaltedPasswordLocator, saltRequired))
    }

    AdminUiPage setMaxSessions(int numSessions) {
        setConfigSetting(securityTab, new AdminUiInput(maxConcurrentSessionsLocator, numSessions as String))
    }

    AdminUiPage restoreDefaultMaxSessions() {
        setMaxSessions(defaults.defaultMaxSessions)
    }

    AdminUiPage setInactivityLockout(String lockoutInterval, String lockoutSchedule) {
        setConfigSettings(securityTab, [
                new AdminUiInput(inactivityIntervalLocator, lockoutInterval),
                new AdminUiInput(inactivityScheduleLocator, lockoutSchedule)
        ])
    }

    AdminUiPage restoreDefaultInactivityLockoutSettings() {
        setInactivityLockout(defaults.defaultInactivityLockout, defaults.defaultInactivitySchedule)
    }

    AdminUiPage setLoginFailureOptions(String message, Integer maxFailedLogins, String lockoutDuration, String resetSchedule) {
        setConfigSettings(securityTab, [
                (loginFailureMessageLocator) : message,
                (maxFailedLoginsLocator) : maxFailedLogins,
                (failedLoginLockoutDurationLocator) : lockoutDuration,
                (failedLoginsScheduleLocator) : resetSchedule
        ].findResults { locator, val ->
            val != null ? new AdminUiInput(locator, val.toString()) : null
        } as List<AdminUiElement>)
    }

    AdminUiPage setPasswordExpiration(PasswordExpirationType expirationType, String expirationData) {
        setConfigSettings(securityTab, buildPasswordExpirationElements(expirationType, expirationData))
    }

    AdminUiPage setAutoEnable(boolean state) {
        setConfigSetting(registrationOptionsTab, new AdminUiSwitchBox(autoenableLocator, state))
    }

    AdminUiPage setPARSettings(boolean autoEnable, boolean parAutoEnable) {
        setConfigSettings(registrationOptionsTab, [
                new AdminUiSwitchBox(autoenableLocator, autoEnable),
                new AdminUiSwitchBox(autoenableWithParLocator, parAutoEnable)
        ])
    }

    AdminUiPage setSiteAnonScript(boolean enabled, AnonScript script) {
        setConfigSettings(sessionUploadTab, [
                new AdminUiSwitchBox(enableAnonLocator, enabled, null, false),
                new AdminUiInput(anonScriptLocator, script?.contents, XnatLocators.enclosingFormFor(anonScriptLocator))
        ])
    }

    AdminUiPage disableSiteAnonScript() {
        setSiteAnonScript(false, null)
    }

    AdminUiPage setSiteSeriesImportFilter(boolean enabled, SeriesImportFilterType filterMode, String filter) {
        setConfigSettings(sessionUploadTab, [
                new AdminUiSwitchBox(enableSeriesImportFilterLocator, enabled, null, false),
                new AdminUiSelect(seriesImportFilterModeLocator, filterMode?.uiDisplayText, null, false),
                new AdminUiInput(seriesImportFilterTextLocator, filter, XnatLocators.enclosingFormFor(seriesImportFilterTextLocator), true)
        ])
    }

    AdminUiPage setSitePetMrSetting(PetMrProcessingSetting setting) {
        if (setting == PetMrProcessingSetting.DEFAULT_TO_SITE) {
            throw new UnsupportedOperationException('PET/MR setting to defer to the site-level setting is only available at the project level.')
        }
        setConfigSettings(sessionUploadTab, [new AdminUiSelect(petMrLocator, setting.uiDisplayText, XnatLocators.enclosingFormFor(petMrLocator))])
    }

    AdminUiPage setZipExtensions(String zipExtensions) {
        setConfigSetting(fileSystemBehaviorTab, new AdminUiInput(zipExtensionsLocator, zipExtensions))
    }

    protected By saveButtonForElement(By containedElement) {
        // Given an Admin UI element, navigate the DOM upward to find the corresponding <form> then drill down again to find the submit button
        new ByChained(containedElement, XnatLocators.nearestAncestorByTag('form'), By.xpath(".//button[text()='Save']"))
    }

    protected List<AdminUiElement> buildPasswordExpirationElements(PasswordExpirationType expirationType, String expirationData) {
        final List<AdminUiElement> elements
        switch (expirationType) {
            case PasswordExpirationType.DISABLED :
                elements = [new AdminUiButton(passwordExpirationDisabledLocator, null, false)]
                break
            case PasswordExpirationType.INTERVAL :
                elements = [
                        new AdminUiButton(passwordExpirationIntervalIndicatorLocator, null, false),
                        new AdminUiInput(passwordExpirationIntervalFieldLocator, expirationData, null, false)
                ]
                break
            case PasswordExpirationType.DATE :
                elements = [
                        new AdminUiButton(passwordExpirationDateIndicatorLocator, null, false),
                        new AdminUiInput(passwordExpirationDateFieldLocator, expirationData, null, false)
                ]
        }
        elements << new AdminUiScreenshot(passwordExpirationAreaLocator)
        elements
    }

}
