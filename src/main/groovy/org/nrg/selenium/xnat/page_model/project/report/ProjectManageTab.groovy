package org.nrg.selenium.xnat.page_model.project.report

import org.nrg.selenium.CustomConditions
import org.nrg.selenium.enums.SeriesImportFilterType
import org.nrg.selenium.xnat.page_model.ConfigPage
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.automation.EventHandlerListComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.resource_settings.ProjectResourceSettingsConfig
import org.nrg.xnat.enums.PetMrProcessingSetting
import org.nrg.xnat.pogo.AnonScript
import org.openqa.selenium.By

class ProjectManageTab implements ProjectReportTab, ConfigPage<ProjectManageTab>, ScreenshotableComponent<ProjectManageTab> {

    protected String anonymizationIdentifier = 'Anonymization Script'
    protected String seriesImportFiltersIdentifier = 'Series Import Filters'
    protected String eventHandlersIdentifier = 'Event Handlers'
    protected String projectResourceSettingsIdentifier = 'Project Resource Settings'
    protected String petMrIdentifier = 'DICOM Configuration'
    protected By currentSubtabLocator = By.xpath("//h3[@class='active']")
    protected By anonEnabledLocator = By.id('enable_anon_script')
    protected By anonScriptContentLocator = By.id('script_text_area')
    protected By anonSaveLocator = By.id('anon_script_save')
    protected By seriesImportFilterEnabledLocator = By.id('enable_series_import_filter')
    protected By seriesImportFilterModeLocator = By.id('series_import_filter_mode_list')
    protected By seriesImportFilterContentLocator = By.id('series_import_filter_text_area')
    protected By seriesImportFilterSaveLocator = By.id('series_import_filter_save')
    protected By launchProjectResourceSettingsButton = By.xpath('//input[@value="Start"]')
    protected By notificationsInputLocator = By.id('notif_list')
    protected By notificationsSaveLocator = By.id('notif_script_save')
    protected By petMrLocator = By.id('separatePETMR')

    @Override
    By tabLocator() {
        locators.tab('Manage')
    }

    @Override
    List<By> getComponentLocators() {
        [By.id('othermgmt')]
    }

    @Override
    void performInitialValidation() {
        wait.until(CustomConditions.elementClickable(subtabLocator(petMrIdentifier)))
    }

    ProjectManageTab loadSubtab(String subTab) {
        final By locator = subtabLocator(subTab)
        waitForElement(locator)
        if ('active' != exceptionSafeGetAttribute(locator, 'class')) {
            exceptionSafeClick(locator)
        }
        this
    }

    ProjectManageTab setAnonScript(boolean enabled, AnonScript script) {
        final String scriptContents = script?.contents
        loadSubtab(anonymizationIdentifier)
        boolean isDirty = performCheckBox(anonEnabledLocator, enabled)
        if (scriptContents != null && exceptionSafeGetAttribute(anonScriptContentLocator, 'value') != scriptContents) {
            persistentFill(anonScriptContentLocator, scriptContents)
            isDirty = true
        }
        uploadTargetedScreenshot([currentSubtabLocator, anonSaveLocator])
        if (isDirty) {
            exceptionSafeClick(anonSaveLocator)
            assertCommonConfigSaveSuccess()
        } else {
            captureScreenshotlessStep()
        }
        this
    }

    ProjectManageTab disableAnonScript() {
        setAnonScript(false, null)
    }

    Boolean readAnonStatus() {
        loadSubtab(anonymizationIdentifier)
        exceptionSafeFind(anonEnabledLocator).isSelected()
    }

    String readAnonContents() {
        loadSubtab(anonymizationIdentifier)
        getValue(anonScriptContentLocator)
    }

    ProjectManageTab setProjectSeriesImportFilter(boolean enabled, SeriesImportFilterType filterMode, String filter) {
        loadSubtab(seriesImportFiltersIdentifier)
        boolean isDirty = performCheckBox(seriesImportFilterEnabledLocator, enabled) // mirror dirty check that determines if save button is enabled in UI
        if (filterMode != null) {
            final String displayText = filterMode.uiDisplayText
            if (exceptionSafeGetSelectedOption(seriesImportFilterModeLocator) != displayText) {
                exceptionSafeSelect(seriesImportFilterModeLocator, displayText)
                isDirty = true
            }
        }
        if (filter != null && exceptionSafeGetAttribute(seriesImportFilterContentLocator, 'value') != filter) {
            fill(seriesImportFilterContentLocator, filter)
            isDirty = true
        }
        uploadTargetedScreenshot([currentSubtabLocator, seriesImportFilterSaveLocator])
        if (isDirty) {
            exceptionSafeClick(seriesImportFilterSaveLocator)
            readXModal('Your changes').assertTextContains('Your changes to the series import filters').capture().clickOK()
        } else {
            captureScreenshotlessStep()
        }
        this
    }

    ProjectManageTab setupProjectResourceSettings(ProjectResourceSettingsConfig config) {
        loadProjectResourceSettingsManager().setupNewResourceSetting(config).returnToUnderlyingPage()
    }

    ProjectManageTab deleteProjectResourceConfiguration(ProjectResourceSettingsConfig config) {
        loadProjectResourceSettingsManager().deleteResourceSetting(config.title).returnToUnderlyingPage()
    }

    EventHandlerListComponent loadEventManagement() {
        loadSubtab(eventHandlersIdentifier)
        loadPage(EventHandlerListComponent)
    }

    ProjectResourceSettingsManager loadProjectResourceSettingsManager() {
        loadSubtab(projectResourceSettingsIdentifier)
        exceptionSafeClick(launchProjectResourceSettingsButton)
        loadPage(ProjectResourceSettingsManager).withUnderlyingPage(this)
    }

    ProjectManageTab setNotificationEmails(List<String> emails) {
        loadSubtab(getAutoRunTabIdentifier())
        wait.until(CustomConditions.webElementClearSuccessful(notificationsInputLocator))
        fill(notificationsInputLocator, emails.join(','))
        uploadTargetedScreenshot([currentSubtabLocator, notificationsSaveLocator])
        exceptionSafeClick(notificationsSaveLocator)
        readXModal('Emails updated').capture().clickOK()
        this
    }

    ProjectManageTab setPetMrSetting(PetMrProcessingSetting setting) {
        loadSubtab(petMrIdentifier)
        exceptionSafeSelect(petMrLocator, setting.uiDisplayText)
        this
    }

    protected By subtabLocator(String subtabText) {
        XnatLocators.tagWithText('h3', subtabText)
    }

    protected String getAutoRunTabIdentifier() {
        'AutoRun Configuration'
    }

}
