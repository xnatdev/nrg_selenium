package org.nrg.selenium.xnat.page_model.session.edit

import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By
import org.openqa.selenium.support.ui.Select

import static org.testng.AssertJUnit.assertTrue

class ReassignSessionToSubjectDialog implements XnatPageComponent, ScreenshotableComponent<ReassignSessionToSubjectDialog>, OverlayingPage<ReassignSessionToSubjectDialog, ImagingSessionEditPage> {

    protected By reassignToSubjectLocator = By.id('new_subject')
    protected By cancelButtonLocator = XnatLocators.CANCEL_BUTTON
    protected By modifyButtonLocator = XnatLocators.MODIFY_BUTTON

    @Override
    List<By> getComponentLocators() {
        [By.id('subjectDialog_c')]
    }

    @Override
    void performNavigationToUnderlyingPage() {
        exceptionSafeClick(cancelButtonLocator)
    }

    ImagingSessionEditPage assignToSubject(String subjectIdentifier) {
        exceptionSafeSelect(reassignToSubjectLocator, subjectIdentifier)
        uploadScreenshot()
        findElement(modifyButtonLocator).click()
        confirmReassign()
        underlyingPage
    }

    List<String> readSubjectOptions() {
        new Select(exceptionSafeFind(reassignToSubjectLocator)).options.drop(1)*.text // remove first element of "Select Subject"
    }

    protected void confirmReassign() {
        readXModal('Are you sure you want to make this change?').uploadScreenshot().clickOK()
    }

}
