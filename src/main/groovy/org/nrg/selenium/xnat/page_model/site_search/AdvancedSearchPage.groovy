package org.nrg.selenium.xnat.page_model.site_search

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.LoadablePage
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ui_element.GenericHtmlTable
import org.nrg.xnat.pogo.DataType
import org.openqa.selenium.By

class AdvancedSearchPage implements NavigablePageObject<AdvancedSearchPage>, LoadablePage {

    protected By advancedSearchButtonLocator = By.xpath("//*[@id='advanced-search-topnav']/a")
    protected By pivotDataTypeSectionScreenshotLocator = By.cssSelector('li.step1')
    protected By arbitraryAdditionalDataTypeLocator = By.xpath('.//td/b')
    protected XpathLocator additionalDataTypeTableLocator = new XpathLocator('//table[@class="addl-data-types"]')
    protected static final int DEFAULT_TABLE_SIZE = 10

    @Override
    void navigateTo() {
        exceptionSafeClick(advancedSearchButtonLocator)
    }

    AdvancedSearchPage selectPivotDataType(DataType dataType) {
        pseudoSelect(getPivotDataTypePseudoselectLocator(), dataType.pluralName)
        this
    }

    AdvancedSearchPage screenshotPivotDataTypeSection() {
        uploadTargetedScreenshot(pivotDataTypeSectionScreenshotLocator)
        this
    }

    AdvancedSearchPage capturePivotDataTypeSection() {
        captureStep(pivotDataTypeSectionScreenshotLocator)
        this
    }

    List<String> readAdditionalDataTypes() {
        findElements(arbitraryAdditionalDataTypeLocator)*.text
    }

    AdvancedSearchPage selectAdditionalDataType(DataType dataType, boolean isBrief) {
        exceptionSafeClick(additionalDataTypeRadioLocator(dataType, isBrief))
        this
    }

    AdvancedSearchPage screenshotAdditionalDataTypeTable(int maxRowsInCapture = DEFAULT_TABLE_SIZE) {
        loadPage(GenericHtmlTable).withLocator(additionalDataTypeTableLocator).uploadTable(DEFAULT_TABLE_SIZE)
        this
    }

    AdvancedSearchPage captureAdditionalDataTypeTable(int maxRowsInCapture = DEFAULT_TABLE_SIZE) {
        loadPage(GenericHtmlTable).withLocator(additionalDataTypeTableLocator).captureTable(maxRowsInCapture)
        this
    }

    AdvancedSearchPage2 nextSearchPage() {
        submitForm()
        loadPage(AdvancedSearchPage2)
    }

    protected By getPivotDataTypePseudoselectLocator() {
        locators.pseudoselect('exptType')
    }

    protected By additionalDataTypeRadioLocator(DataType dataType, boolean isBrief) {
        By.xpath("//input[@name='super_${dataType.xsiType}' and @value='${isBrief ? 'brief' : 'detailed'}']")
    }

}
