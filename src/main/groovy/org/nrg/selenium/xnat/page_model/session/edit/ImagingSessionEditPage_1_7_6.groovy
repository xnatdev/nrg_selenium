package org.nrg.selenium.xnat.page_model.session.edit

import org.nrg.xnat.versions.*

import static org.testng.AssertJUnit.assertTrue

class ImagingSessionEditPage_1_7_6 extends ImagingSessionEditPage_1_8_9_2 {

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev, Xnat_1_7_2, Xnat_1_7_3, Xnat_1_7_4, Xnat_1_7_5, Xnat_1_7_5_2, Xnat_1_7_6]
    }

    @Override
    protected void confirmSessionRelabel() {
        assertTrue(closeAlertAndGetItsText().matches("^Modifying the Session of an imaging session will result in the moving of files on the file server within the project's storage space\\. {2}Are you sure you want to make this change[\\s\\S]\$"))
    }

    @Override
    protected void confirmMoveSessionToProject() {
        String alertText = closeAlertAndGetItsText()
        if (alertText.contains('Is this OK?')) {
            alertText = closeAlertAndGetItsText()
        }
        assertTrue(alertText.contains('Are you sure you want to make this change?'))
    }

}
