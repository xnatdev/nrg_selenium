package org.nrg.selenium.processing.config_elements

import org.nrg.selenium.xnat.XnatDriver
import org.openqa.selenium.By

class SimpleClickElement implements PipelineConfigElement {

    private By by

    SimpleClickElement(By by) {
        this.by = by
    }

    @Override
    void performElementAction(XnatDriver xnatDriver) {
        xnatDriver.findElement(by).click()
    }

}
