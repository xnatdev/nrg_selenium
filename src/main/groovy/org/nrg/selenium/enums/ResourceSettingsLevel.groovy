package org.nrg.selenium.enums

enum ResourceSettingsLevel {

    DEFAULT ('DEFAULT (resources)', 'Level:default'),
    OUT ('outputs dir (out)', 'Level:out'),
    IN ('inputs dir (in)', 'Level:in')

    private final String selectionName
    private final String assertionName

    ResourceSettingsLevel(String selectionName, String assertionName) {
        this.selectionName = selectionName
        this.assertionName = assertionName
    }

    String getSelectionName() {
        selectionName
    }

    String getAssertionName() {
        assertionName
    }

}
