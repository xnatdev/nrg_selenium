package org.nrg.selenium.xnat.page_model.ui_element

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage
import org.nrg.selenium.xnat.page_model.subject.report.SubjectReportPage
import org.openqa.selenium.By

class BreadcrumbSection implements ScreenshotableComponent<BreadcrumbSection>, XnatPageComponent {

    protected XpathLocator breadcrumbsLocator = XnatLocators.byId('breadcrumbs')
    protected String projectIdentifier = 'PROJECT'
    protected String subjectIdentifier = 'SUBJECT'

    @Override
    List<By> getComponentLocators() {
        [breadcrumbsLocator]
    }

    ProjectReportPage clickProjectBreadcrumb() {
        exceptionSafeClick(breadcrumbFor(projectIdentifier))
        loadPage(ProjectReportPage)
    }

    SubjectReportPage clickSubjectBreadcrumb() {
        exceptionSafeClick(breadcrumbFor(subjectIdentifier))
        loadPage(SubjectReportPage)
    }

    protected By breadcrumbFor(String identifier) {
        breadcrumbsLocator.joinSublocator(XnatLocators.tagContainingText('a', "${identifier}: "))
    }

}
