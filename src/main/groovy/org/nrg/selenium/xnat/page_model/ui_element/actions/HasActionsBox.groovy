package org.nrg.selenium.xnat.page_model.ui_element.actions

import org.nrg.selenium.xnat.page_model.PageObjectSupport

trait HasActionsBox implements PageObjectSupport {

    ActionsBox readActionsBox() {
        loadPage(ActionsBox, false)
    }

    void actionsBoxNavigation(List<String> structure) {
        readActionsBox().actionsBoxNavigation(structure)
    }

}