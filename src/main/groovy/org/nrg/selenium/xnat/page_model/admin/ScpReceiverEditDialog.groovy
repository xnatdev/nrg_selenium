package org.nrg.selenium.xnat.page_model.admin

import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.ui_element.popup.XnatDialog
import org.nrg.xnat.pogo.dicom.DicomScpReceiver
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertEquals

class ScpReceiverEditDialog implements XnatPageComponent, OverlayingPage<ScpReceiverEditDialog, ScpReceiverManagementPage> {

    protected By aeTitleLocator = By.id('scp-title')
    protected By portLocator = By.id('scp-port')
    protected By dialogLocator = By.xpath("//div[contains(text(), 'DICOM SCP')]")
    protected By cancelButton = XnatLocators.CANCEL_BUTTON
    protected XnatDialog editDialog

    @Override
    void performInitialValidation() {
        editDialog = readXnatDialog(dialogLocator)
    }

    @Override
    void performNavigationToUnderlyingPage() {
        exceptionSafeClick(cancelButton)
    }

    ScpReceiverEditDialog fillAeTitle(String aeTitle) {
        fill(aeTitleLocator, aeTitle)
        this
    }

    ScpReceiverEditDialog fillPort(int port) {
        fill(portLocator, port)
        this
    }

    String readAeTitle() {
        getValue(aeTitleLocator)
    }

    ScpReceiverEditDialog assertAeTitle(String expectedAeTitle) {
        assertEquals(expectedAeTitle, readAeTitle())
        this
    }

    int readPort() {
        getValue(portLocator) as int
    }

    ScpReceiverEditDialog assertPort(int expectedPort) {
        assertEquals(expectedPort, readPort())
        this
    }

    ScpReceiverEditDialog uploadScreenshot() {
        editDialog.uploadScreenshot()
        this
    }

    ScpReceiverEditDialog capture() {
        editDialog.capture()
        this
    }

    ScpReceiverManagementPage submitDialog() {
        exceptionSafeClick(editDialog.getSubElementLocator(XnatLocators.SAVE_BUTTON))
        underlyingPage
    }

    ScpReceiverManagementPage createReceiver(DicomScpReceiver receiver) {
        fillAeTitle(receiver.aeTitle)
        fillPort(receiver.port)
        uploadScreenshot()
        submitDialog()
    }

}
