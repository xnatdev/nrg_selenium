package org.nrg.selenium.xnat.page_model.upload

import org.nrg.xnat.versions.XnatVersion
import org.nrg.xnat.versions.Xnat_1_6dev
import org.openqa.selenium.By

class CompressedUploader_1_6 extends CompressedUploader_1_7_6 {

    protected By appletLaunchFormLocator = By.id('launchUploadAppletForm')
    protected By compressedUploadLinkLocator = By.linkText('Click here.')

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev]
    }

    @Override
    void navigateTo(boolean takeAllScreenshots) {
        loadApplet()
        if (takeAllScreenshots) {
            captureStep(appletLaunchFormLocator)
        }
        findElement(compressedUploadLinkLocator).click()
    }

}
