package org.nrg.selenium.xnat.page_model.automation

import org.openqa.selenium.By

class SiteEventHandlersTab implements AutomationTab {

    EventHandlerListComponent getEventHandlerListComponent() {
        loadPage(EventHandlerListComponent)
    }

    @Override
    By tabLocator() {
        locators.tab('Site Event Handlers')
    }

}
