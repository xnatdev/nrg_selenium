package org.nrg.selenium.xnat.page_model.project.report

import org.nrg.selenium.xnat.page_model.IndexPage
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.users.CustomUserGroup
import org.nrg.xnat.pogo.users.UserGroup
import org.openqa.selenium.By

class RequestProjectAccessPage implements NavigablePageObject<RequestProjectAccessPage>, ScreenshotableComponent<RequestProjectAccessPage> {

    protected By pageTitleLocator = XnatLocators.H3_TAG
    protected By accessLevelDropdownLocator = By.name('access_level')
    protected By commentsLocator = By.name('comments')

    @Override
    List<By> getComponentLocators() {
        [By.name('form1')]
    }

    RequestProjectAccessPage assertDisplayedProject(Project project) {
        assertElementText("${project.title} Access Request", pageTitleLocator)
        this
    }

    RequestProjectAccessPage selectRequestedAccessLevel(UserGroup userGroup) {
        if (userGroup instanceof CustomUserGroup) {
            throw new UnsupportedOperationException('Users cannot request access to custom user groups from this page.')
        }
        exceptionSafeSelect(accessLevelDropdownLocator, userGroup.singularName())
        this
    }

    RequestProjectAccessPage fillComment(String comment) {
        fill(commentsLocator, comment)
        this
    }

    IndexPage requestAccess(UserGroup accessLevel, String comment) {
        selectRequestedAccessLevel(accessLevel)
        fillComment(comment)
        uploadScreenshot()
        submitForm()
        readNote().assertTextEquals('Access request sent.').capture()
        loadPage(IndexPage)
    }

}
