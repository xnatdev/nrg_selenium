package org.nrg.selenium.xnat.page_model.ui_element.manage_files

import groovy.util.logging.Log4j
import org.nrg.selenium.CustomConditions
import org.nrg.selenium.enums.ManageFilesResourceLevel
import org.nrg.selenium.exceptions.DuplicateUploadedFilesException
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.util.ResourceValidation
import org.nrg.selenium.xnat.manage_files.ManageFilesFileInformation
import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.report.XnatReportPage
import org.nrg.selenium.xnat.page_model.ui_element.popup.XModal
import org.nrg.selenium.xnat.page_model.ui_element.popup.PartialTextPopupLocator
import org.nrg.xnat.pogo.experiments.Scan
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertEquals
import static org.testng.AssertJUnit.assertTrue

@Log4j
class ManageFiles implements ScreenshotableComponent<ManageFiles>, XnatPageComponent, OverlayingPage<ManageFiles, XnatReportPage> {

    protected By titleLocator = By.xpath("//div[text()='File Manager']")
    protected By manageFilesLocator = By.id('fileListing')
    protected By closeButtonLocator = XnatLocators.buttonWithText('Close')
    protected By addFolderButtonLocator = XnatLocators.buttonWithText('Add Folder')
    protected By folderLevelLocator = By.id('folder_level')
    protected By folderNameLocator = By.id('folder_collection')
    protected By folderDialogLocator = By.id('fileUploadDialog_c')
    protected By createButtonLocator = XnatLocators.buttonWithText('Create')
    protected By uploadFilesButtonLocator = XnatLocators.buttonWithText('Upload Files')
    protected By overwriteCheckboxLocator = By.id('folder_overwrite')
    protected By uploadLevelLocator = By.id('upload_level')
    protected By uploadItemLocator = By.id('upload_item')
    protected By uploadFolderLocator = By.id('upload_collection')
    protected By renameLocator = By.id('file_name')
    protected By formatLocator = By.id('file_format')
    protected By contentLocator = By.id('file_content')
    protected By tagsLocator = By.id('file_tags')
    protected By fileUploadLocator = By.id('local_file')
    protected By fileUploadDialogLocator = folderDialogLocator
    protected By uploadButtonLocator = XnatLocators.buttonWithText('Upload')
    protected By catalogDialogLocator = By.xpath('//div[text()="Refreshing Catalog Information" or text()="Loading File Summaries"]')
    protected XpathLocator expanderClickSublocator = new XpathLocator('.//td/a')
    protected XpathLocator expanderStatusSublocator = expanderClickSublocator.parent()
    protected XpathLocator expanderClickRegisteredSublocator = new XpathLocator("./..[contains(@class, 'm ygtvfocus') or contains(@class, 'ygtvloading')]")
    protected XpathLocator expanderCompleteSublocator = new XpathLocator("./..[contains(@class, 'm ygtvfocus')]")
    protected XpathLocator arbitraryChildSublocator = new XpathLocator('./div/div/table/tbody/tr')
    protected XpathLocator itemClassifierSublocator = new XpathLocator('./td[contains(@id, "ygtvcontentel")]')
    protected XpathLocator fileNameSublocator = new XpathLocator('.//a[1]')
    protected XpathLocator subfolderNameSublocator = new XpathLocator('./td/span[contains(@id, "ygtvlabelel")]/a')
    protected List<String> alreadyExpandedIdentifiers = ['ygtvlm', 'ygtvtm']
    protected String fileClassifierIdentifier = 'ygtvhtml'
    protected String duplicateFileIdentifier = 'not uploaded'

    protected String[] archiveExtensions = ['.zip']
    protected static final int MAX_WORKAROUND_RETRIES = 10

    @Override
    List<By> getComponentLocators() {
        [manageFilesLocator]
    }

    @Override
    void performInitialValidation() {
        waitForElement(titleLocator)
    }

    @Override
    void performNavigationToUnderlyingPage() {
        findElement(closeButtonLocator).click()
    }

    ManageFilesResource getResource(String resourceName, ManageFilesResourceLevel level = ManageFilesResourceLevel.RESOURCES, String itemLevel = null) {
        final ManageFilesResource resource = loadPage(ManageFilesResource)
        resource.setFileManager(this)
        resource.setFolderName(resourceName)
        resource.setLevel(level)
        if (level == ManageFilesResourceLevel.ASSESSORS) {
            resource.setAssessorId(itemLevel)
        } else if (level == ManageFilesResourceLevel.SCANS) {
            resource.setScanId(itemLevel)
        }
        resource
    }

    ManageFilesResource getScanResource(String resourceName, Scan scan) {
        getResource(resourceName, ManageFilesResourceLevel.SCANS, scan.getId())
    }

    ManageFilesResource createFolder(ManageFilesResource resource) {
        resource.setFileManager(this)
        retryClickIfNotVisible(addFolderButtonLocator, folderLevelLocator, 2)
        exceptionSafeSelect(folderLevelLocator, resource.level.creationName)
        fill(folderNameLocator, resource.folderName)
        uploadTargetedScreenshot(folderDialogLocator)
        exceptionSafeClick(createButtonLocator)
        resource.assertFolderSizes(new ResourceValidation(numFiles: 0))
        resource
    }

    ManageFilesFile uploadFile(ManageFilesResource resource, File file, ManageFilesFileInformation fileInformation = null) throws DuplicateUploadedFilesException {
        resource.setFileManager(this)
        refreshingCatalogWorkaround {
            wait.until(CustomConditions.elementClickable(uploadFilesButtonLocator))
        }
        exceptionSafeClick(uploadFilesButtonLocator)
        performCheckBox(overwriteCheckboxLocator, fileInformation?.specifyOverwrite ?: false)
        exceptionSafeSelect(uploadLevelLocator, resource.level.creationName)
        exceptionSafeSelect(uploadItemLocator, resource.item)
        exceptionSafeSelect(uploadFolderLocator, resource.folderName)
        fill(renameLocator, fileInformation?.rename)
        fill(formatLocator, fileInformation?.format)
        fill(contentLocator, fileInformation?.content)
        fill(tagsLocator, fileInformation?.tags)
        findElement(fileUploadLocator).sendKeys(file.absolutePath)
        uploadTargetedScreenshot(fileUploadDialogLocator)
        findElement(uploadButtonLocator).click()
        if (file.name.endsWithAny(archiveExtensions)) {
            assertTrue(closeAlertAndGetItsText(fileInformation?.extract ?: false).matches("^Would you like the contents of this archive file to be extracted on the server[\\s\\S] Press 'OK' to extract or 'Cancel' to proceed with upload without extracting\\.\$"))
        }
        waitForElementNotVisible(fileUploadDialogLocator)
        if (isElementPresentQuick(new PartialTextPopupLocator(duplicateFileIdentifier).by())) {
            final XModal duplicateFilesModal = readXModal(duplicateFileIdentifier)
            final List<String> duplicateFilesLines = duplicateFilesModal.bodyText.split('\n')
            assertEquals('The following files were not uploaded because they already exist on the server:', duplicateFilesLines[0])
            duplicateFilesLines.remove(0)
            duplicateFilesModal.capture().clickOK()
            throw new DuplicateUploadedFilesException(duplicateFilesLines)
        }
        resource.getFile(fileInformation?.rename ?: file.name)
    }

    def <X extends ManageFilesDirectory<X>> X clickExpander(X directory) {
        directory.setFileManager(this)
        final XpathLocator clickLocator = directory.locator.joinSublocator(expanderClickSublocator)
        refreshingCatalogWorkaround {
            wait.until(CustomConditions.elementClickable(clickLocator))
        }
        clickUntilOtherElementVisible(clickLocator, clickLocator.joinSublocator(expanderClickRegisteredSublocator), 2) // attempt clicks until the expander is spinning or done
        waitForElementVisible(clickLocator.joinSublocator(expanderCompleteSublocator))
        directory
    }

    def <X extends ManageFilesDirectory<X>> X expand(X directory) {
        if (isDirectoryExpanded(directory)) {
            directory.setFileManager(this)
            directory
        } else {
            clickExpander(directory)
        }
    }

    def <X extends ManageFilesDirectory<X>> List<ManageFilesItem> readChildren(X directory) {
        expand(directory)
        findElements(directory.locator.joinSublocator(arbitraryChildSublocator)).collect { element ->
            if (element.findElement(itemClassifierSublocator).getAttribute('class').contains(fileClassifierIdentifier)) {
                directory.getFile(element.findElement(fileNameSublocator).text)
            } else {
                directory.getSubfolder(element.findElement(subfolderNameSublocator).text)
            }
        }
    }

    def <X extends ManageFilesDirectory<X>> boolean isDirectoryExpanded(X directory) {
        final String fullClassString = exceptionSafeGetAttribute(directory.locator.joinSublocator(expanderStatusSublocator), 'class')
        alreadyExpandedIdentifiers.any { identifier ->
            fullClassString.contains(identifier)
        }
    }

    By getTitleLocator() {
        titleLocator
    }

    private void refreshingCatalogWorkaround(Closure checkedPreaction) {
        int retries = 0
        while(retries++ <= MAX_WORKAROUND_RETRIES) {
            checkedPreaction()
            if (isElementPresentQuick(catalogDialogLocator)) {
                log.warn("Blocking dialog seems to be present. Attempting to reload the page and resume test (retry attempt ${retries}/${MAX_WORKAROUND_RETRIES})")
                navigate().refresh()
                underlyingPage.loadManageFiles()
            } else {
                log.info('Blocking dialog not present. Test resuming normal operations...')
                return
            }
        }
    }

}
