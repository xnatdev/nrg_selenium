package org.nrg.selenium.processing.jackson.deserializer

import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.selenium.processing.element_comparators.comparator.EqualityElementComparator

class EqualityElementComparatorDeserializer extends ElementComparatorDeserializer<EqualityElementComparator> {

    @Override
    EqualityElementComparator mainDeserialization(ObjectCodec objectCodec, JsonNode jsonNode) {
        new EqualityElementComparator()
    }

}
