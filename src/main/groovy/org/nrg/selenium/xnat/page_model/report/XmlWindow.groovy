package org.nrg.selenium.xnat.page_model.report

import org.nrg.selenium.extension.GenericPersistentAction
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.XnatPageComponent

import static org.testng.AssertJUnit.assertEquals

class XmlWindow<Y extends NavigablePageObject<Y>> implements OverlayingPage<XmlWindow, Y>, XnatPageComponent {

    @Override
    void performInitialValidation() {
        switchToNewWindow()
    }

    @Override
    void performNavigationToUnderlyingPage() {
        close()
        returnToMainWindow()
    }

    String readRootXmlElement() {
        readRootXmlElementFrom(pageSource)
    }

    String readXml() {
        // assumes that the root XML element won't have children of the same type
        genericPersistentAction(new GenericPersistentAction<>().failureString('Could not read XML within allotted time').action {
            final String source = getPageSource()
            final String rootXmlElement = readRootXmlElementFrom(source)
            source.trim().endsWith("</${rootXmlElement}>")
        })
        pageSource
    }

    XmlWindow<Y> assertRootXmlElement(String expectedValue) {
        assertEquals(expectedValue, readRootXmlElement())
        this
    }

    private String readRootXmlElementFrom(String source) {
        source.split('\n')[0].split(' ')[0].substring(1) // 1st row > content before the first space > trim off the leading bracket
    }

}
