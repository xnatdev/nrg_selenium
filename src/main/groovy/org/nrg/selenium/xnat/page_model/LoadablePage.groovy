package org.nrg.selenium.xnat.page_model

trait LoadablePage implements PageObjectSupport {

    abstract void navigateTo()

    void navigateTo(boolean takeAllScreenshots) {
        navigateTo()
    }

}