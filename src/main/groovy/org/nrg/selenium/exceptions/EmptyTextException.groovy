package org.nrg.selenium.exceptions

/**
 * Used when finding an element which should have text, but does not.
 */
class EmptyTextException extends Exception {}
