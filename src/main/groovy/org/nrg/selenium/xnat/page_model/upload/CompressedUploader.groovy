package org.nrg.selenium.xnat.page_model.upload

import org.nrg.selenium.xnat.page_model.LoadablePage
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.prearchive.XnatPrearchive
import org.nrg.selenium.xnat.page_model.session.report.ImagingSessionReportPage
import org.nrg.selenium.xnat.page_model.ui_element.activity.ActivityTab
import org.nrg.selenium.xnat.page_model.ui_element.activity.ActivityTabDetailsDialog
import org.nrg.selenium.xnat.page_model.ui_element.activity.ActivityTabItem
import org.nrg.testing.enums.TestData
import org.nrg.testing.xnat.conf.Settings
import org.nrg.xnat.pogo.Project
import org.openqa.selenium.By

class CompressedUploader implements NavigablePageObject<CompressedUploader>, LoadablePage, ScreenshotableComponent<CompressedUploader> {

    public static final int DEFAULT_MAX_UPLOAD_TIME = 120
    protected By headerLocator = By.xpath("//*[contains(text(), 'Compressed') and (self::h2 or self::h4)]")
    protected By projectLocator = By.id('project')
    protected By sessionInputLocator = By.id('image_archive')
    protected By uploadButtonLocator = By.id('directButton')
    protected By archiveRadioLocator = By.id('pc_2')
    protected By prearchiveRadioLocator = By.id('pc_0')

    @Override
    void navigateTo() {
        navigateTo(false)
    }

    @Override
    void navigateTo(boolean takeAllScreenshots) {
        topNav('Upload', 'Images', 'Compressed Uploader') // parameter only used in 1.6
    }

    @Override
    void performInitialValidation() {
        waitForElement(projectLocator)
    }

    @Override
    List<By> getComponentLocators() {
        [headerLocator, uploadButtonLocator]
    }

    ImagingSessionReportPage uploadToArchive(Project project, TestData session, int maximumUploadTime = DEFAULT_MAX_UPLOAD_TIME, boolean takeAllScreenshots = false) {
        uploadToArchive(project, session.toFile(), maximumUploadTime, takeAllScreenshots)
    }

    ImagingSessionReportPage uploadToArchive(Project project, File sessionZip, int maximumUploadTime = DEFAULT_MAX_UPLOAD_TIME, boolean takeAllScreenshots = false) {
        navigateToArchivedSession(performUpload(project, true, sessionZip, maximumUploadTime, takeAllScreenshots))
    }

    XnatPrearchive uploadToPrearchive(Project project, TestData session, int maximumUploadTime = 120, boolean takeAllScreenshots = false) {
        uploadToPrearchive(project, session.toFile(), maximumUploadTime, takeAllScreenshots)
    }

    XnatPrearchive uploadToPrearchive(Project project, File sessionZip, int maximumUploadTime = 120, boolean takeAllScreenshots = false) {
        navigateToPrearchivedSession(performUpload(project, false, sessionZip, maximumUploadTime, takeAllScreenshots))
    }

    protected ActivityTabItem startUpload(Project project, boolean archive, File session, boolean takeAllScreenshots) {
        capture()
        exceptionSafeSelect(projectLocator, project.runningTitle)
        if (takeAllScreenshots) {
            capture()
        }
        findElement(sessionInputLocator).sendKeys(session.absolutePath)
        if (takeAllScreenshots) {
            capture()
        }
        findElement(archive ? archiveRadioLocator : prearchiveRadioLocator).click()
        capture()
        upload()
    }

    protected ActivityTabItem upload() {
        final ActivityTab activityTab = readActivityTab(true)
        findElement(uploadButtonLocator).click()
        activityTab.waitForNewItem()
    }

    protected ActivityTabDetailsDialog captureSuccessfulUpload(ActivityTabItem activityTabItem, boolean archive, int maximumUploadTime) {
        activityTabItem.expandDetails().
                waitForSuccessEntry(maximumUploadTime).
                capture()
    }

    protected ActivityTabDetailsDialog performUpload(Project project, boolean archive, File session, int maximumUploadTime, boolean takeAllScreenshots) {
        captureSuccessfulUpload(
                startUpload(project, archive, session, takeAllScreenshots),
                archive,
                maximumUploadTime
        )
    }

    protected ImagingSessionReportPage navigateToArchivedSession(ActivityTabDetailsDialog detailsDialog) {
        detailsDialog.clickArchivedSessionLink()
    }

    protected XnatPrearchive navigateToPrearchivedSession(ActivityTabDetailsDialog detailsDialog) {
        detailsDialog.clickPrearchivedSessionLink()
    }

}
