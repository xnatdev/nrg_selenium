package org.nrg.selenium.browser

import org.nrg.testing.xnat.conf.Settings
import org.openqa.selenium.WebDriver
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions

import java.time.Duration

class FirefoxSupport extends BrowserSupport {

    @Override
    List<String> keys() {
        ['Firefox', 'Mozilla Firefox', 'FF', 'MozillaFirefox']
    }

    @Override
    WebDriver construct() {
        final FirefoxOptions options = new FirefoxOptions()
                .setPageLoadTimeout(Duration.ofSeconds(5))
                .addPreference('browser.download.folderList', 2)
                .addPreference('browser.download.manager.showWhenStarting', false)
                .addPreference('browser.download.dir', Settings.TEMP_SUBDIR)
                .addPreference('browser.helperApps.neverAsk.saveToDisk', 'text/csv,application/vnd.ms-excel,text/xml,application/json,application/zip')
                .addPreference('browser.download.viewableInternally.enabledTypes', '') // see https://bugzilla.mozilla.org/show_bug.cgi?id=1670651
                .addPreference('devtools.webconsole.persistlog', true)
        if (Settings.FIREFOX_PATH != null) {
            options.setBinary(Settings.FIREFOX_PATH)
        }
        if (Settings.HEADLESS) {
            options.setHeadless(true)
            options.addArguments(['--width=1200', '--height=6000'])
        }
        new FirefoxDriver(options)
    }

}
