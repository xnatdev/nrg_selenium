package org.nrg.selenium.xnat.page_model.ui_element

import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By
import org.openqa.selenium.support.ui.Select

import static org.testng.AssertJUnit.assertFalse
import static org.testng.AssertJUnit.assertTrue

class YuiTableEditColumnDialog implements XnatPageComponent, ScreenshotableComponent<YuiTableEditColumnDialog>, OverlayingPage<YuiTableEditColumnDialog, YuiTable> {

    int expectedNumberColumns
    protected By cancelButtonLocator = XnatLocators.buttonWithText('Cancel')
    protected By submitButtonLocator = XnatLocators.buttonWithText('Submit')
    protected By currentFieldsLocator = By.xpath("//div[@id='current_fields_select']//select")
    protected By availableFieldsLocator = By.xpath("//div[@id='potential_fields_select']//select")
    protected By addFieldButtonLocator = By.xpath("//img[contains(@src, '/images/left18.gif')]")
    protected By removeFieldButtonLocator = By.xpath("//img[contains(@src, '/images/right18.gif')]")

    @Override
    List<By> getComponentLocators() {
        [By.id('search_edit_popup')]
    }

    @Override
    void performNavigationToUnderlyingPage() {
        clickCancelButton()
    }

    YuiTable clickCancelButton() {
        exceptionSafeClick(cancelButtonLocator)
        underlyingPage
    }

    List<String> readCurrentFields() {
        new Select(exceptionSafeFind(currentFieldsLocator)).options*.text
    }

    List<String> readAvailableFields() {
        new Select(exceptionSafeFind(availableFieldsLocator)).options*.text
    }

    YuiTableEditColumnDialog assertFieldCurrent(String field) {
        assertTrue(readCurrentFields().contains(field))
        assertFalse(readAvailableFields().contains(field))
        this
    }

    YuiTableEditColumnDialog assertFieldAvailable(String field) {
        assertFalse(readCurrentFields().contains(field))
        assertTrue(readAvailableFields().contains(field))
        this
    }

    YuiTableEditColumnDialog addField(String field) {
        exceptionSafeSelect(availableFieldsLocator, field)
        exceptionSafeClick(addFieldButtonLocator)
        expectedNumberColumns++
        this
    }

    YuiTableEditColumnDialog removeField(String field) {
        exceptionSafeSelect(currentFieldsLocator, field)
        exceptionSafeClick(removeFieldButtonLocator)
        expectedNumberColumns--
        this
    }

    YuiTable submitModifications() {
        exceptionSafeClick(submitButtonLocator)
        loadPage(YuiTable).waitForExpectedNumberOfColumns(expectedNumberColumns)
    }

}
