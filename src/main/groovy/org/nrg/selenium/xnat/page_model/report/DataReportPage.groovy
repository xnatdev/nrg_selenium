package org.nrg.selenium.xnat.page_model.report

import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.PageObjectSupport
import org.nrg.selenium.xnat.page_model.custom_variable.CustomVariableReportSection
import org.nrg.selenium.xnat.page_model.ui_element.actions.ActionsBox
import org.nrg.selenium.xnat.page_model.ui_element.actions.HasActionsBox
import org.nrg.xnat.pogo.Extensible
import org.openqa.selenium.By

// TODO: can I make this extend XnatReportPage instead?
// TODO: harmonize "overlap" on HasActionsBox with XnatReportPage
trait DataReportPage<T extends Extensible<T>, X extends NavigablePageObject<X>> implements HasActionsBox {

    CustomVariableReportSection readCustomVariableSection() {
        loadPage(CustomVariableReportSection, false)
    }

    List<String> viewXmlActionsPath() {
        ['View', 'View XML']
    }

    XmlWindow<X> viewXml() {
        driverExtension.noteWindows()
        actionsBoxNavigation(viewXmlActionsPath())
        (loadPage(XmlWindow, false) as XmlWindow<X>).withUnderlyingPage(this as X)
    }

    String readStandardDetailField(String fieldIdentifier) {
        driverExtension.exceptionSafeGetText(detailFieldIdentifier(fieldIdentifier))
    }

    By detailFieldIdentifier(String fieldIdentifier) {
        By.xpath("//th[normalize-space(text())='${fieldIdentifier}:']/following-sibling::td") // normalize-space to handle cases with trailing spaces (e.g. Visit)
    }

}