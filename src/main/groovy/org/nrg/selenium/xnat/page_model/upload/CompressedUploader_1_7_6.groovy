package org.nrg.selenium.xnat.page_model.upload

import org.nrg.selenium.xnat.page_model.prearchive.XnatPrearchive
import org.nrg.selenium.xnat.page_model.session.report.ImagingSessionReportPage
import org.nrg.selenium.xnat.page_model.ui_element.activity.ActivityTabDetailsDialog
import org.nrg.selenium.xnat.page_model.ui_element.activity.ActivityTabItem
import org.openqa.selenium.By

class CompressedUploader_1_7_6 extends CompressedUploader{

    protected By archiveCompleteLocator = By.linkText('archiving operation complete...')
    protected By prearchiveCompleteLocator = By.partialLinkText('1 session')

    @Override
    protected ActivityTabDetailsDialog captureSuccessfulUpload(ActivityTabItem activityTabItem, boolean archive, int maximumUploadTime) {
        waitForElement(archive ? archiveCompleteLocator : prearchiveCompleteLocator, 'Upload did not complete in time, or it was not registered correctly', maximumUploadTime)
        captureStep(locators.otherIFrame)
        null
    }

    @Override
    protected ActivityTabItem upload() {
        findElement(uploadButtonLocator).click()
        null
    }

    @Override
    protected ImagingSessionReportPage navigateToArchivedSession(ActivityTabDetailsDialog detailsDialog) {
        switchToNewIframe()
        findElement(locators.HYPERLINK_TAG).click()
        leaveIframe()
        loadPage(ImagingSessionReportPage).capture()
    }

    @Override
    protected XnatPrearchive navigateToPrearchivedSession(ActivityTabDetailsDialog detailsDialog) {
        loadPrearchive()
    }

}
