package org.nrg.selenium.xnat.page_model.ui_element.popup

import org.openqa.selenium.By

class PartialTextPopupLocator implements PopupLocator {

    private String text

    PartialTextPopupLocator(String text) {
        this.text = text
    }

    @Override
    By by() {
        // https://stackoverflow.com/questions/38240763/xpath-difference-between-dot-and-text/38240971 : since contains(...) only looks at the first node, partial text must be in the first text node
        By.xpath(".//*[contains(text(), '${text}') and (self::div or self::p or self::th)]") // <th> added for DICOM dump dialog
    }

}
