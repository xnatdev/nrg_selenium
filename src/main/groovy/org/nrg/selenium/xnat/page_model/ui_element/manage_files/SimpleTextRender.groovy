package org.nrg.selenium.xnat.page_model.ui_element.manage_files

import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.TextAssertable
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By

class SimpleTextRender implements ResourceFileRender, TextAssertable<SimpleTextRender>, ScreenshotableComponent<SimpleTextRender> {

    protected By textLocator = XnatLocators.PRE_TAG

    @Override
    String getText() {
        exceptionSafeGetText(textLocator)
    }

    @Override
    List<By> getComponentLocators() {
        [textLocator]
    }

}
