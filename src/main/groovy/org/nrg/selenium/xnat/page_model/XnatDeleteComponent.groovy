package org.nrg.selenium.xnat.page_model

import org.openqa.selenium.By

trait XnatDeleteComponent {

    By deletionDialogLocator = By.id('rest_deleter_c')

    abstract <X extends NavigablePageObject<X>> X delete(int waitTolerance, boolean takeAllScreenshots)

}
