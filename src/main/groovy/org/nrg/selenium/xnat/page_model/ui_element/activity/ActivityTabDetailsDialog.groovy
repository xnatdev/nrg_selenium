package org.nrg.selenium.xnat.page_model.ui_element.activity

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.prearchive.XnatPrearchive
import org.nrg.selenium.xnat.page_model.session.report.ImagingSessionReportPage
import org.openqa.selenium.By

class ActivityTabDetailsDialog implements XnatPageComponent, ScreenshotableComponent<ActivityTabDetailsDialog> {

    protected XpathLocator successEntrySublocator = new XpathLocator('.//div[@class="prog success"]')
    protected String archiveCompleteMessage = '1 session(s) successfully uploaded to Archive:'
    protected String prearchiveCompleteMessage = '1 session(s) successfully uploaded to Prearchive: Visit the prearchive to review.'
    String baseId

    @Override
    List<By> getComponentLocators() {
        [dialogLocator()]
    }

    XpathLocator dialogLocator() {
        XnatLocators.byId("activity-details-${baseId}")
    }

    ActivityTabDetailsDialog withId(String apiId) {
        baseId = apiId
        this
    }

    ActivityTabDetailsDialog waitForSuccessEntry(int maxWait) {
        waitForElement(successEntryLocator(), "Couldn't find success entry in time", maxWait)
        this
    }

    ActivityTabDetailsDialog assertSuccessEntryContains(String text) {
        assertElementTextContains(text, successEntryLocator())
        this
    }

    ActivityTabDetailsDialog assertArchiveSuccess() {
        assertSuccessEntryContains(archiveCompleteMessage)
    }

    ActivityTabDetailsDialog assertPrearchiveSuccess() {
        assertSuccessEntryContains(prearchiveCompleteMessage)
    }

    ImagingSessionReportPage clickArchivedSessionLink() {
        noteWindows()
        exceptionSafeClick(successEntryLinkLocator())
        switchToNewWindow()
        loadPage(ImagingSessionReportPage)
    }

    XnatPrearchive clickPrearchivedSessionLink() {
        noteWindows()
        exceptionSafeClick(successEntryLinkLocator())
        switchToNewWindow()
        loadPage(XnatPrearchive, false, true)
    }

    protected XpathLocator successEntryLocator() {
        dialogLocator().joinSublocator(successEntrySublocator)
    }

    protected By successEntryLinkLocator() {
        successEntryLocator().joinSublocator('./a')
    }

}
