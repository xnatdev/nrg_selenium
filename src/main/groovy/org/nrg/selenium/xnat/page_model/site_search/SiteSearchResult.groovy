package org.nrg.selenium.xnat.page_model.site_search

import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.ui_element.YuiTable
import org.nrg.selenium.xnat.page_model.ui_element.YuiTableFilterDialog
import org.openqa.selenium.By

trait SiteSearchResult<X extends SiteSearchResult<X>> implements NavigablePageObject<X>, ScreenshotableComponent<X> {

    YuiTable dataTable

    @Override
    void performInitialValidation() {
        dataTable = loadPage(YuiTable)
    }

    @Override
    List<By> getComponentLocators() {
        [By.id('search_tabs')]
    }

    X sortDownOnColumn(String columnName) {
        dataTable.sortDownOnColumn(columnName)
        this as X
    }

    X sortUpOnColumn(String columnName) {
        dataTable.sortUpOnColumn(columnName)
        this as X
    }

    X hideColumn(String columnName) {
        dataTable.hideColumn(columnName)
        this as X
    }

    YuiTableFilterDialog launchFilteringOnColumn(String columnName) {
        dataTable.launchFilteringOnColumn(columnName)
    }

}