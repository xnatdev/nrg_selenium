package org.nrg.selenium.xnat.page_model.session.report

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.ui_element.HtmlTable
import org.nrg.xnat.pogo.experiments.Scan
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertEquals
import static org.testng.AssertJUnit.assertTrue

trait ScanListRecord<X extends ScanListRecord<X>> implements HtmlTable<X>, ScreenshotableComponent<X> { // TODO: why is declaring ScreenshotableComponent again necessary when it seems like it should be redundant

    abstract By scanAssertionReadyLocator()

    abstract List<String> readScanIds()

    abstract XpathLocator scanTableLocator()

    abstract List<Scan> readScans()

    @Override
    XpathLocator getTableLocator() {
        scanTableLocator()
    }

    X assertScansEqualTo(Collection<Scan> scans) {
        driverExtension.waitForElement(scanAssertionReadyLocator())
        final List<String> scanIds = readScanIds()

        // assert each element of scans is contained in scanIds (that is, each scan the list of scans is indeed on the page)
        scans.each { scan ->
            assertTrue(scan.id in scanIds)
        }

        // For *finite* sets A, B: A = B iff A ⊆ B and |A| = |B|
        assertEquals(scans.size(), scanIds.size())
        captureTable(10)
        this as X
    }

}