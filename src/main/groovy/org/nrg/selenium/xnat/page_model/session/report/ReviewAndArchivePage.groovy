package org.nrg.selenium.xnat.page_model.session.report

import org.nrg.selenium.util.ResourceValidation
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ui_element.activity.ActivityTab
import org.nrg.selenium.xnat.page_model.ui_element.activity.ActivityTabItem

class ReviewAndArchivePage implements NavigablePageObject<ReviewAndArchivePage> {

    ReviewAndArchiveScanTable scanTable

    @Override
    void performInitialValidation() {
        scanTable = loadPage(ReviewAndArchiveScanTable)
    }

    ImagingSessionReportPage archiveSession() {
        final ActivityTab activityTab = readActivityTab(true)
        submitForm()
        activityTab.waitForNewItem()
                .expandDetails()
                .clickArchivedSessionLink()
    }

    ReviewAndArchivePage assertScanSize(String scanId, ResourceValidation resourceValidation) {
        scanTable.assertScanSize(scanId, resourceValidation)
        this
    }

    ReviewAndArchivePage captureScans() {
        scanTable.capture()
        this
    }

}
