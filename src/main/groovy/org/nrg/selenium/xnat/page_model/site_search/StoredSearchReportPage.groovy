package org.nrg.selenium.xnat.page_model.site_search

import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.openqa.selenium.By

class StoredSearchReportPage implements NavigablePageObject<StoredSearchReportPage>, ScreenshotableComponent<StoredSearchReportPage> {

    protected By mainScreenshotLocator = By.id('layout_content')
    protected By idLocator = By.xpath('//td[preceding-sibling::td[text()="ID"]]')

    @Override
    List<By> getComponentLocators() {
        [mainScreenshotLocator]
    }

    String getStoredSearchId() {
        exceptionSafeGetText(idLocator)
    }

    StoredSearchReportPage assertStoredSearchId(String id) {
        assertElementText(id, idLocator)
        this
    }

}
