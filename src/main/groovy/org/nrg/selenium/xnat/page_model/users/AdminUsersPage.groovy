package org.nrg.selenium.xnat.page_model.users

import org.nrg.selenium.CustomConditions
import org.nrg.selenium.exceptions.UnknownInterpretableValueException
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.LoadablePage
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.ui_element.HtmlTable
import org.nrg.xnat.pogo.users.User
import org.openqa.selenium.By
import org.openqa.selenium.support.pagefactory.ByChained
import org.openqa.selenium.support.ui.ExpectedCondition

import static org.testng.AssertJUnit.assertEquals
import static org.testng.AssertJUnit.assertFalse
import static org.testng.AssertJUnit.assertTrue
import static org.testng.AssertJUnit.fail

class AdminUsersPage implements NavigablePageObject<AdminUsersPage>, LoadablePage, HtmlTable<AdminUsersPage> {

    protected By loadCompleteLocator = By.xpath('//div[@class="content-tabs xnat-tab-container"]')
    protected By addUserButtonLocator = By.id('create-new-user')
    protected By userPageEnabledSublocator = By.xpath(".//td[@class='enabled center']/a/i")

    @Override
    void navigateTo() {
        topNav('Administer', 'Users')
    }

    @Override
    void performInitialValidation() {
        waitForElementVisible(loadCompleteLocator)
    }

    @Override
    XpathLocator getTableLocator() {
        XnatLocators.byId('user-profiles')
    }

    AdminUsersPage createUser(User user) {
        launchUserCreation().createUser(user)
    }

    AdminUserEditDialog launchUserCreation() {
        exceptionSafeClick(addUserButtonLocator)
        viewEditDialog()
    }

    AdminUserEditDialog launchUserEdit(User user) {
        exceptionSafeClick(userLink(user))
        viewEditDialog()
    }

    boolean readUserEnableStatus(User user) {
        final String enabledIndicator = 'fa fa-check'
        final String disabledIndicator = 'fa'
        wait.until(CustomConditions.attributeIsOneOf(userEnabledIndicator(user), 'class', [enabledIndicator, disabledIndicator]))
        final String classValue = exceptionSafeGetAttribute(userEnabledIndicator(user), 'class')
        if (classValue == enabledIndicator) {
            true
        } else if (classValue == disabledIndicator) {
            false
        } else {
            throw new UnknownInterpretableValueException("Unknown class for user enable indicator: ${classValue}")
        }
    }

    List<String> readUsernames() {
        readEntriesForColumn('Username')
    }

    AdminUsersPage assertUserExists(User user) {
        assertTrue(user.username in readUsernames())
        this
    }

    AdminUsersPage assertUserDoesNotExist(String username) {
        assertFalse(username in readUsernames())
        this
    }

    AdminUsersPage assertUserEnabled(User user, boolean isEnabled) {
        wait.until({ webDriver ->
            readUserEnableStatus(user) == isEnabled
        } as ExpectedCondition<Boolean>)

        assertEquals(isEnabled, readUserEnableStatus(user))
        this
    }

    AdminUsersPage enableUser(User user) {
        final By userRow = userRow(user)
        if (!readUserEnableStatus(user)) {
            captureStep(userRow)
            launchUserEdit(user).setEnabled(true).save()
        }
        assertUserEnabled(user, true)
        user.enabled(true)
        captureStep(userRow)
        this
    }

    protected AdminUserEditDialog viewEditDialog() {
        loadPage(AdminUserEditDialog).withUnderlyingPage(this)
    }

    protected By userRow(User user) {
        By.xpath("//tr[contains(@id, '${user.username}-')]")
    }

    protected By userLink(User user) {
        new ByChained(userRow(user), By.linkText(user.username))
    }

    protected By userEnabledIndicator(User user) {
        new ByChained(userRow(user), userPageEnabledSublocator)
    }

}
