package org.nrg.selenium.xnat.page_model

import org.nrg.selenium.xnat.page_model.upload.CompressedUploader

class NavigableDelegate implements NavigablePageObject<NavigableDelegate> {

    void loadApplet() {
        throw new UnsupportedOperationException('Applets are dead.')
    }

}
