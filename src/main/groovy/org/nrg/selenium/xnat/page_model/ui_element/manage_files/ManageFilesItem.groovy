package org.nrg.selenium.xnat.page_model.ui_element.manage_files

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.openqa.selenium.By

trait ManageFilesItem implements XnatPageComponent, ScreenshotableComponent<ManageFilesItem> {

    ManageFiles fileManager
    ManageFilesDirectory parent
    XpathLocator relativeTextToItemSublocator = new XpathLocator("./ancestor::div[@class='ygtvitem'][1]")
    XpathLocator deleteSublocator = new XpathLocator(".//a[contains(@onclick, 'window.viewer.remove')]")

    @Override
    List<By> getComponentLocators() {
        fileManager.componentLocators
    }

    abstract XpathLocator getSelfSublocator()

    XpathLocator getLocator() {
        parent.locator.joinSublocator(selfSublocator)
    }

    XpathLocator fileManagerItem(String partialText) {
        simpleFileManagerText(partialText).joinSublocator(relativeTextToItemSublocator)
    }

    XpathLocator simpleFileManagerText(String itemName) {
        new XpathLocator(".//*[contains(translate(text(), '\u00ad', ''),'${itemName}')]")
    }

    XpathLocator deleteLocator() {
        locator.joinSublocator(deleteSublocator)
    }

    ManageFilesResource getBaseResource() {
        if (this instanceof ManageFilesResource) {
            this as ManageFilesResource
        } else {
            parent.baseResource
        }
    }

}