package org.nrg.selenium.xnat.page_model.ui_element.manage_files

import org.nrg.selenium.enums.DataUnit
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.util.BrowserDownloadManager
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.report.XnatReportPage
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertEquals

class ManageFilesFile implements ManageFilesItem, ScreenshotableComponent<ManageFilesFile> {

    String fileName

    @Override
    XpathLocator getSelfSublocator() {
        fileManagerItem(fileName)
    }

    @Override
    ManageFilesFile uploadScreenshot() {
        ScreenshotableComponent.super.uploadScreenshot() as ManageFilesFile
    }

    @Override
    ManageFilesFile capture() {
        ScreenshotableComponent.super.capture() as ManageFilesFile
    }

    String readSizeString() {
        readNormalizedText(locator)
    }

    ManageFilesFile assertFileSize(double fileSize, DataUnit unit = DataUnit.BYTES) {
        parent.expand()
        assertEquals("${fileName} ${unit.formatExpectedFileManagerValue(fileSize)} ${unit.manageFilesRepresentation}", readSizeString())
        this
    }

    ImageRender clickImageFile() {
        clickFile(ImageRender)
    }

    SimpleTextRender clickTextFile() {
        clickFile(SimpleTextRender)
    }

    BinaryFileDownload clickBinaryFile() {
        final BrowserDownloadManager downloadManager = new BrowserDownloadManager()
        clickFile(BinaryFileDownload).locateFile(downloadManager)
    }

    protected By getFileLinkLocator() {
        locator.joinSublocator(".//a[contains(translate(text(), '\u00ad', ''),'${fileName}')]")
    }

    protected <X extends ResourceFileRender> X clickFile(Class<X> renderClass) {
        parent.expand()
        exceptionSafeClick(fileLinkLocator)
        loadPage(renderClass)
    }

}
