package org.nrg.selenium.browser

import org.nrg.testing.xnat.conf.Settings
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.remote.DesiredCapabilities

class ChromeSupport extends BrowserSupport {

    @Override
    protected List<String> keys() {
        ['Chrome', 'Google Chrome', 'GoogleChrome']
    }

    @Override
    protected WebDriver construct() {
        final DesiredCapabilities caps = new DesiredCapabilities()
        final ChromeOptions options = new ChromeOptions()
        options.setExperimentalOption('prefs', ['download.default_directory' : Settings.TEMP_SUBDIR])
        caps.setCapability(ChromeOptions.CAPABILITY, options)
        new ChromeDriver(caps)
    }

}
