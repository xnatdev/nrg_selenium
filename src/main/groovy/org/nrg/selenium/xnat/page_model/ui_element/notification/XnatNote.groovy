package org.nrg.selenium.xnat.page_model.ui_element.notification

import org.openqa.selenium.By

class XnatNote implements XnatNotification<XnatNote> {

    @Override
    By getLocator() {
        By.cssSelector('div.note')
    }

}
