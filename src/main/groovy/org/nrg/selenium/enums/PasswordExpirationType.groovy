package org.nrg.selenium.enums

enum PasswordExpirationType {

    DISABLED, INTERVAL, DATE

}
