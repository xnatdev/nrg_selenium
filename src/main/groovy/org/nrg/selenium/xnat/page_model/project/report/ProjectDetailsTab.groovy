package org.nrg.selenium.xnat.page_model.project.report

import org.nrg.selenium.CustomConditions
import org.nrg.selenium.xnat.page_model.custom_variable.CustomVariableManagementPage
import org.nrg.selenium.xnat.page_model.project.delete.ProjectDeletePage
import org.nrg.selenium.xnat.page_model.project.edit.ProjectEditPage
import org.nrg.xnat.pogo.Project
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertEquals

class ProjectDetailsTab implements ProjectReportTab {

    protected By editButtonLocator = By.id('button1-button')
    protected By deleteButtonLocator = By.id('button3-button')
    protected By manageCustomVariablesLocator = By.id('button5-button')
    protected By idLocator = fieldLocator('ID')
    protected By descriptionLocator = fieldLocator('Description')
    protected By keywordsLocator = fieldLocator('Keywords')

    @Override
    By tabLocator() {
        locators.tab('Details')
    }

    @Override
    void performInitialValidation() {
        waitForElement(idLocator)
        assertElementPresent(idLocator)
    }

    void assertPageRepresents(Project project) {
        assertEquals(expectedProjectIdString(project), projectIdString)
        if (project.description != null) {
            assertEquals(escapeOnProjectPage(project.description), description)
        }
        if (!project.keywords.isEmpty()) {
            assertEquals(escapeOnProjectPage(project.keywords.join(' ')), keywords)
        }
    }

    ProjectDeletePage loadDeletePage() {
        exceptionSafeClick(deleteButtonLocator)
        loadPage(ProjectDeletePage)
    }

    String getProjectIdString() {
        wait.until(CustomConditions.textIsNonempty(idLocator))
        exceptionSafeGetText(idLocator).trim().replaceAll('\\s+', ' ')
    }

    String getDescription() {
        exceptionSafeGetText(descriptionLocator)
    }

    String getKeywords() {
        exceptionSafeGetText(keywordsLocator)
    }

    ProjectEditPage loadEditPage() {
        exceptionSafeClick(editButtonLocator)
        loadPage(ProjectEditPage, false, true)
    }

    CustomVariableManagementPage loadCustomVariableManagement() {
        exceptionSafeClick(manageCustomVariablesLocator)
        loadPage(CustomVariableManagementPage)
    }

    String escapeOnProjectPage(String input) {
        input
    }

    String expectedProjectIdString(Project project) {
        final List<String> aliases = project.aliases
        aliases.isEmpty() ? project.id : "${project.id} Aka: ${escapeOnProjectPage(aliases.join(' '))}"
    }

    protected By fieldLocator(String field) {
        By.xpath("//th[text()='${field}:']/following-sibling::td")
    }

}
