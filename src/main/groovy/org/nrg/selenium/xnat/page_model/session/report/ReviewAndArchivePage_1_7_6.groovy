package org.nrg.selenium.xnat.page_model.session.report

import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.xnat.versions.XnatVersion
import org.nrg.xnat.versions.XnatVersionList
import org.nrg.xnat.versions.Xnat_1_7_7

class ReviewAndArchivePage_1_7_6 implements NavigablePageObject<ReviewAndArchivePage_1_7_6> {

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        XnatVersionList.knownVersionsBefore(Xnat_1_7_7)
    }

    ImagingSessionReportPage archiveSession() {
        submitForm()
        loadPage(ImagingSessionReportPage)
    }

}
