package org.nrg.selenium.xnat.page_model.users

import org.nrg.jira.components.zephyr.TestStatus
import org.nrg.selenium.CustomConditions
import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.ui_element.popup.XnatPopup
import org.nrg.xnat.enums.SiteDataRole
import org.nrg.xnat.pogo.users.User
import org.openqa.selenium.By

class AdminUserEditDialog implements UserEditComponent<AdminUserEditDialog>, ScreenshotableComponent<AdminUserEditDialog>, OverlayingPage<AdminUserEditDialog, AdminUsersPage> {

    protected By dialogLocator = By.xpath("//div[text()='Create New User' or contains(text(), 'User Properties for')]")
    protected By verifiedLocator = By.id('verified')
    protected By enabledLocator = By.id('enabled')
    protected By saveButtonSublocator = XnatLocators.SAVE_BUTTON
    protected By advancedSettingsLocator = By.linkText('Edit Advanced Settings')
    protected By siteAdminLocator = By.id('role_Administrator')
    protected By allDataAccessLocator = By.id('data_access')
    protected By allDataAdminLocator = By.id('data_admin')
    protected By noDataAccessLocator = By.id('data_none')
    protected By securitySection = By.xpath("//h3[text()='Define security settings']/..")
    protected By userRoleEditSaveButtonLocator = By.id('update-user-roles')
    protected By disableUserLinkLocator = By.linkText('Disable Login')
    protected By cancelButtonLocator = XnatLocators.CANCEL_BUTTON
    protected XnatPopup editDialog
    protected boolean viewingAdvancedSettings = false

    @Override
    List<By> getComponentLocators() {
        editDialog.componentLocators
    }

    @Override
    By usernameLocator() {
        By.id('username')
    }

    @Override
    By passwordLocator() {
        By.id('password')
    }

    @Override
    By passwordCheckLocator() {
        By.id('confirm-password')
    }

    @Override
    By firstNameLocator() {
        By.id('first-name')
    }

    @Override
    By lastNameLocator() {
        By.id('last-name')
    }

    @Override
    By emailLocator() {
        By.id('email')
    }

    @Override
    void performInitialValidation() {
        editDialog = readXnatDialog(dialogLocator)
    }

    @Override
    void performNavigationToUnderlyingPage() {
        editDialog.clickOK()
    }

    AdminUserEditDialog setVerified(boolean verified) {
        performSwitchBox(verifiedLocator, verified)
        this
    }

    AdminUserEditDialog setEnabled(boolean enabled) {
        performSwitchBox(enabledLocator, enabled)
        this
    }

    AdminUserEditDialog save() {
        exceptionSafeClick(editDialog.getSubElementLocator(saveButtonSublocator))
        this
    }

    AdminUserEditDialog viewAdvancedSettings() {
        exceptionSafeClick(advancedSettingsLocator)
        switchToNewIframe()
        viewingAdvancedSettings = true
        this
    }

    AdminUserEditDialog closeAdvancedSettings() {
        leaveIframe()
        exceptionSafeClick(cancelButtonLocator)
        viewingAdvancedSettings = false
        this
    }

    AdminUserEditDialog setExtendedPermissions(boolean adminRole, SiteDataRole siteDataRole) {
        if (!viewingAdvancedSettings) {
            viewAdvancedSettings()
        }
        wait.until(CustomConditions.elementIsDisplayed(siteAdminLocator))
        final By accessLocator
        switch (siteDataRole) {
            case SiteDataRole.ALL_DATA_ACCESS :
                accessLocator = allDataAccessLocator
                break
            case SiteDataRole.ALL_DATA_ADMIN :
                accessLocator = allDataAdminLocator
                break
            default :
                accessLocator = noDataAccessLocator
        }
        exceptionSafeClick(accessLocator)
        performCheckBox(siteAdminLocator, adminRole)
        uploadTargetedScreenshot(securitySection)
        saveUserRoles()
        capture()
        closeAdvancedSettings()
    }

    AdminUsersPage createUser(User user) {
        capture()
        fillCommonFields(user)
        setVerified(true)
        setEnabled(true)
        uploadScreenshot()
        save()
        logUser(user)
        user.enabled(true).verified(true)
        if (user.isAdmin()) {
            underlyingPage.launchUserEdit(user) // save operation closes edit dialog
            setExtendedPermissions(true, SiteDataRole.ALL_DATA_ADMIN)
        } else if (user.dataRole in [SiteDataRole.ALL_DATA_ACCESS, SiteDataRole.ALL_DATA_ADMIN]) {
            underlyingPage.launchUserEdit(user)
            setExtendedPermissions(false, user.dataRole)
        }
        underlyingPage
    }

    protected void logUser(User user) {
        captureStep(stepCounter, TestStatus.PASS, "username = ${user.username}, password = ${user.password}, firstName = ${user.firstName}, lastName = ${user.lastName}, email = ${user.email}", true, userScreenshotElement(user))
    }

    protected By userScreenshotElement(User user) {
        underlyingPage.userRow(user)
    }

    protected void saveUserRoles() {
        exceptionSafeClick(userRoleEditSaveButtonLocator)
        waitForElementVisible(siteAdminLocator)
        waitForElementVisible(allDataAdminLocator)
        waitForAsyncCalls()
    }

}
