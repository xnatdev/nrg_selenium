package org.nrg.selenium.xnat.page_model.ui_element

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.XnatPageComponent

import static org.testng.AssertJUnit.assertFalse
import static org.testng.AssertJUnit.assertTrue

class TopNav implements XnatPageComponent {

    protected XpathLocator mainNavLocator = new XpathLocator('//ul[@class="nav"]')

    List<String> readNavHeaders() {
        findElements(new HeaderLevel().locatorForArbitraryChild())*.text
    }

    List<String> readSubnavHeaders(String headerName) {
        performNavigation(headerName)
        findElements(new HeaderLevel().traverse(headerName).screenshot().locatorForArbitraryChild())*.text
    }

    List<String> readSubnavSubtable(String headerName, String subnavOption) {
        performNavigation(headerName, subnavOption)
        findElements(
                new HeaderLevel().
                        traverse(headerName).
                        screenshot().
                        traverse(subnavOption).
                        screenshot().
                        locatorForArbitraryChild()
        )*.text
    }

    TopNav assertSubnavContains(String headerName, String expectedSubnavOption) {
        assertTrue(expectedSubnavOption in readSubnavHeaders(headerName))
        this
    }

    TopNav assertSubnavDoesNotContain(String headerName, String searchedOption) {
        assertFalse(searchedOption in readSubnavHeaders(headerName))
        this
    }

    TopNav assertSubnavSubtableContains(String headerName, String subnavOption, String expectedTableElement) {
        assertTrue(expectedTableElement in readSubnavSubtable(headerName, subnavOption))
        this
    }

    TopNav assertSubnavSubtableDoesNotContain(String headerName, String subnavOption, String searchedOption) {
        assertFalse(searchedOption in readSubnavSubtable(headerName, subnavOption))
        this
    }

    void performNavigation(String... options) {
        NavLevel cumulative = null
        options.collect { option ->
            cumulative = (cumulative ?: new HeaderLevel()).traverse(option)
        }
    }

    private abstract class NavLevel {
        XpathLocator cumulativeLocator

        NavLevel(XpathLocator locator) {
            setCumulativeLocator(locator)
            exceptionSafeClick(locator)
        }

        NavLevel() {}

        NavLevel screenshot() {
            uploadTargetedScreenshot(cumulativeLocator.joinSublocator('./following-sibling::ul'))
            this
        }

        abstract NavLevel traverse(String nextElement)
        abstract XpathLocator locatorForArbitraryChild()
    }

    private class HeaderLevel extends NavLevel {
        HeaderLevel() {
            super()
            setCumulativeLocator(mainNavLocator)
        }

        @Override
        NavLevel traverse(String nextElement) {
            new PrimaryItemLevel(cumulativeLocator.joinSublocator("./li/a[text()='${nextElement}']"))
        }

        @Override
        XpathLocator locatorForArbitraryChild() {
            cumulativeLocator.joinSublocator('./li[contains(@class, "more")]')
        }
    }

    private class PrimaryItemLevel extends NavLevel {
        PrimaryItemLevel(XpathLocator locator) {
            super(locator)
        }

        @Override
        NavLevel traverse(String nextElement) {
            new SecondaryItemLevel(cumulativeLocator.joinSublocator("./following-sibling::ul/li/a[text()='${nextElement}']"))
        }

        @Override
        XpathLocator locatorForArbitraryChild() {
            cumulativeLocator.joinSublocator('./following-sibling::ul/li/a')
        }
    }

    private class SecondaryItemLevel extends NavLevel {
        SecondaryItemLevel(XpathLocator locator) {
            super(locator)
        }

        @Override
        NavLevel traverse(String nextElement) {
            new TertiaryItemLevel(cumulativeLocator.joinSublocator("./following-sibling::ul//a[text()='${nextElement}']"))
        }

        @Override
        XpathLocator locatorForArbitraryChild() {
            cumulativeLocator.joinSublocator('./following-sibling::ul//a')
        }
    }

    private class TertiaryItemLevel extends NavLevel {
        TertiaryItemLevel(XpathLocator locator) {
            super(locator)
        }

        @Override
        NavLevel traverse(String nextElement) {
            throw new UnsupportedOperationException()
        }

        @Override
        XpathLocator locatorForArbitraryChild() {
            throw new UnsupportedOperationException()
        }
    }

}
