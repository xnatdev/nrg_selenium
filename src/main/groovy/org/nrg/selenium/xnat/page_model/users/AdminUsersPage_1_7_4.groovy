package org.nrg.selenium.xnat.page_model.users

import org.nrg.xnat.versions.*
import org.openqa.selenium.By

class AdminUsersPage_1_7_4 extends AdminUsersPage {

    AdminUsersPage_1_7_4() {
        userPageEnabledSublocator = By.xpath(".//td[@class='enabled center']//img")
    }

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_7_2, Xnat_1_7_3, Xnat_1_7_4]
    }

}
