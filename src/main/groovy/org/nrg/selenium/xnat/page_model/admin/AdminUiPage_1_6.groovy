package org.nrg.selenium.xnat.page_model.admin

import org.nrg.xnat.versions.*
import org.openqa.selenium.By

class AdminUiPage_1_6 extends AdminUiPage_1_8_3 {

    String siteInformationTab = 'Site Information'
    protected By showAppletLocator = By.id('showapplet')

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev]
    }

    @Override
    AdminUiPage setAppletLinkDisplayed(boolean state) {
        setConfigSetting(siteInformationTab, new AdminUiCheckbox(showAppletLocator, state))
    }

}
