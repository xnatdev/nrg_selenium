package org.nrg.selenium.xnat.page_model.automation

import org.nrg.selenium.xnat.page_model.LoadablePage
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.openqa.selenium.By

class AutomationPage implements NavigablePageObject<AutomationPage>, LoadablePage, AutomationBase {

    @Override
    void navigateTo() {
        topNav('Administer', 'Automation')
    }

    @Override
    void performInitialValidation() {
        final By scriptsLocator = getInstance(ScriptsTab).tabLocator()
        waitForElement(scriptsLocator)
        waitForElementVisible(scriptsLocator)
    }

}
