package org.nrg.selenium.xnat.page_model.ui_element.manage_files

import org.nrg.selenium.enums.DataUnit
import org.nrg.selenium.enums.ManageFilesResourceLevel
import org.nrg.selenium.exceptions.DuplicateUploadedFilesException
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.util.ResourceValidation
import org.nrg.selenium.xnat.manage_files.ManageFilesFileInformation
import org.nrg.testing.MathUtils

import static org.testng.AssertJUnit.assertEquals

class ManageFilesResource implements ManageFilesDirectory<ManageFilesResource> {

    protected XpathLocator sizeStringSublocator = new XpathLocator('./table')
    ManageFilesResourceLevel level = ManageFilesResourceLevel.RESOURCES
    String scanId
    String assessorId

    @Override
    XpathLocator getLocator() {
        final XpathLocator baseLocator = fileManagerItem(level.validationName)
        final XpathLocator intermediateLocator
        if (level == ManageFilesResourceLevel.SCANS) {
            intermediateLocator = baseLocator.joinSublocator(fileManagerItem(scanId.trim() + ' ')) // space enforces match of the whole ID
        } else if (level == ManageFilesResourceLevel.ASSESSORS) {
            intermediateLocator = baseLocator.joinSublocator(fileManagerItem(assessorId))
        } else {
            intermediateLocator = baseLocator
        }
        intermediateLocator.joinSublocator(selfSublocator)
    }

    String readSizeString() {
        readNormalizedText(locator.joinSublocator(sizeStringSublocator))
    }

    String getItem() {
        scanId ?: assessorId
    }

    ManageFilesResource assertFolderSizes(ResourceValidation resourceValidation) {
        assertEquals(resourceValidation.numFiles == 0 ?
                "${folderName} files, 0 ${DataUnit.BYTES.manageFilesRepresentation}" :
                "${folderName} ${resourceValidation.numFiles} files, ${resourceValidation.dataUnit.formatExpectedFileManagerValue(resourceValidation.fileSize)} ${resourceValidation.dataUnit.manageFilesRepresentation}",
                readSizeString()
        )
        this
    }

    ManageFilesFile uploadFile(File file, ManageFilesFileInformation fileInformation = null) throws DuplicateUploadedFilesException {
        checkFileManager()
        fileManager.uploadFile(this, file, fileInformation)
    }

}
