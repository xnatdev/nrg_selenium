package org.nrg.selenium.xnat.page_model.users

import org.nrg.selenium.xnat.page_model.IndexPage
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.ui_element.notification.XnatNote
import org.nrg.xnat.pogo.users.User
import org.openqa.selenium.By

class PasswordExpirationPage implements XnatPageComponent {

    protected By userInfoLocator = getInstance(IndexPage).userInfoLocator
    protected By loadCompleteLocator = By.xpath("//th[contains(text(), 'Change Password')] | //*[contains(text(), 'password has expired')]")
    protected By formAreaLocator = XnatLocators.FORM_TAG
    protected By currentPasswordLocator = By.id('current_password')
    protected By newPasswordLocator = By.id('new_password')
    protected By newPasswordConfirmLocator = By.id('confirm_password')
    protected By submitButtonLocator = By.xpath("//input[@type='submit']")

    @Override
    void performInitialValidation() {
        waitForElement(userInfoLocator)
        waitForElement(loadCompleteLocator)
    }

    PasswordExpirationPage fillCurrentPassword(String password) {
        fill(currentPasswordLocator, password)
        this
    }

    PasswordExpirationPage fillNewPassword(String password) {
        fill(newPasswordLocator, password)
        this
    }

    PasswordExpirationPage fillPasswordConfirmation(String password) {
        fill(newPasswordConfirmLocator, password)
        this
    }

    IndexPage updatePasswordFor(User user, String newPassword) {
        uploadTargetedScreenshot(formAreaLocator)
        captureStep(userInfoLocator)
        fillCurrentPassword(user.password)
        fillNewPassword(newPassword)
        fillPasswordConfirmation(newPassword)
        exceptionSafeClick(submitButtonLocator)
        final XnatNote note = readNote().assertTextEquals('Password changed.')
        user.setPassword(newPassword)
        uploadTargetedScreenshot(note.componentLocators + [userInfoLocator] as List<By>)
        loadPage(IndexPage).assertLogin(user)
    }

}
