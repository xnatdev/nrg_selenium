package org.nrg.selenium.xnat.page_model.ui_element.popup

import org.openqa.selenium.By

interface PopupLocator {

    By by()

}
