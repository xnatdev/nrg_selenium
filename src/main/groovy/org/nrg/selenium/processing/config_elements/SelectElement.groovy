package org.nrg.selenium.processing.config_elements

import org.nrg.selenium.xnat.XnatDriver
import org.openqa.selenium.By
import org.openqa.selenium.support.ui.Select

class SelectElement implements PipelineConfigElement {

    private By by
    private String text

    SelectElement(By by, String text) {
        this.by = by
        this.text = text
    }

    @Override
    void performElementAction(XnatDriver xnatDriver) {
        new Select(xnatDriver.findElement(by)).selectByVisibleText(text)
    }

}
