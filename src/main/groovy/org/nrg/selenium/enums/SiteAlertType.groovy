package org.nrg.selenium.enums

enum SiteAlertType {
    MESSAGE,
    ALERT,
    ERROR
}