package org.nrg.selenium.xnat.scripting

import org.apache.commons.lang3.StringUtils
import org.nrg.selenium.enums.ScriptingLanguageType
import org.nrg.selenium.util.WebDriverUtils
import org.openqa.selenium.Keys

class AutomationScript {

    boolean prespecifyLanguage = true
    boolean appendScriptOnDuplicate = false
    String scriptLabel
    String scriptDescription
    String scriptContents
    ScriptingLanguageType language
    int version = 1
    final String scriptID

    AutomationScript(String scriptID) {
        this.scriptID = scriptID
    }

    // Setters to chain multiple set operations together

    AutomationScript prespecify(boolean prespecify) {
        setPrespecifyLanguage(prespecify)
        this
    }

    AutomationScript appendSetting(boolean append) {
        setAppendScriptOnDuplicate(append)
        this
    }

    AutomationScript label(String label) {
        setScriptLabel(label)
        this
    }

    AutomationScript description(String description) {
        setScriptDescription(description)
        this
    }

    AutomationScript contents(String contents) {
        setScriptContents(contents)
        this
    }

    AutomationScript source(File script) {
        scriptContents = ""
        script.readLines().each { line ->
            scriptContents += line + '\n'
            if (WebDriverUtils.isChrome()) {
                scriptContents += Keys.BACK_SPACE * StringUtils.countMatches(line, '\t')
            }
        }
        this
    }

    AutomationScript language(ScriptingLanguageType scriptingLanguage) {
        setLanguage(scriptingLanguage)
        this
    }

    AutomationScript version(int scriptVersion) {
        setVersion(scriptVersion)
        this
    }

    String getEffectiveLabel() {
        scriptLabel ?: scriptID
    }

}
