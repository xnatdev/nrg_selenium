package org.nrg.selenium.xnat.page_model.custom_variable

import org.nrg.selenium.xnat.page_model.PageObjectSupport

trait CustomVariableEditPage implements PageObjectSupport {

    CustomVariableEditSection readCustomVariableSection() {
        loadPage(CustomVariableEditSection, false)
    }

}