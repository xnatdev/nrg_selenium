package org.nrg.selenium.xnat

import groovy.util.logging.Log4j
import io.restassured.response.Response
import org.apache.commons.lang3.mutable.MutableInt
import org.apache.commons.lang3.time.StopWatch
import org.nrg.jira.components.zephyr.TestStatus
import org.nrg.selenium.browser.BrowserSupport
import org.nrg.selenium.extension.WebDriverExtension
import org.nrg.selenium.xnat.page_model.IndexPage
import org.nrg.selenium.xnat.page_model.SyntheticPageObject
import org.nrg.selenium.xnat.page_model.XnatLoginPage
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.util.WebDriverUtils
import org.nrg.selenium.xnat.versions.XnatDriverList
import org.nrg.testing.FileIOUtils
import org.nrg.testing.TestController
import org.nrg.testing.TimeUtils
import org.nrg.testing.xnat.conf.Settings
import org.nrg.testing.xnat.conf.XnatConfig
import org.nrg.testing.xnat.processing.SessionRenewer
import org.nrg.testing.xnat.rest.XnatRestDriver
import org.nrg.xnat.interfaces.SyntheticXnatInterface
import org.nrg.xnat.interfaces.XnatInterface
import org.nrg.xnat.pogo.users.User
import org.nrg.xnat.versions.XnatVersion
import org.openqa.selenium.*
import org.openqa.selenium.support.pagefactory.ByChained

import java.nio.file.Paths

import static org.testng.AssertJUnit.*

@Log4j
@SuppressWarnings(['WeakerAccess', 'SameParameterValue'])
abstract class XnatDriver implements SessionRenewer {

    XnatRestDriver restDriver
    @Delegate WebDriverExtension driverExtension
    @Delegate(includes = 'isTestRunning') TestController testController
    WebDriver driver
    int horizIframeOffset = 0 // used for taking screenshots in iframes
    int vertIframeOffset = 0 // used for taking screenshots in iframes
    XnatConfig xnatConfig
    String testName
    String testClass
    MutableInt stepCounter
    private StopWatch sessionTimeoutChecker

    static XnatDriver getInstance(boolean suppressDriver, XnatConfig xnatConfig, XnatRestDriver restDriver) {
        XnatDriverList.readXnatVersions()

        final XnatConfig config = xnatConfig ?: Settings.DEFAULT_XNAT_CONFIG
        try {
            final XnatDriver xnatDriver = XnatDriverList.KNOWN_VERSION_CLASS_DRIVER_MAP[config.xnatVersion].newInstance()
            xnatDriver.setXnatConfig(config)
            xnatDriver.setRestDriver(restDriver)
            if (!suppressDriver) {
                xnatDriver.constructWebDriver()
            }
            return xnatDriver
        } catch (Exception e) {
            throw new RuntimeException('Could not read XNAT class due to:', e)
        }
    }

    static XnatDriver getInstance() {
        getInstance(false, null, null)
    }

    void setDriver(WebDriver webDriver) {
        this.driver = webDriver
        setDriverExtension(new WebDriverExtension(Settings.DEFAULT_TIMEOUT, webDriver))
    }

    void setTestController(TestController testController) {
        this.testController = testController
        if (testController != null) {
            setStepCounter(testController.stepCounter)
        }
    }

    int getStep() {
        testController?.step ?: -1
    }

    abstract void customize() // Will be called exactly once in @BeforeSuite.

    abstract boolean is16()

    abstract boolean is17()

    abstract List<Class<? extends XnatVersion>> getHandledVersions()

    @Override
    void startTimer() {
        sessionTimeoutChecker = TimeUtils.launchStopWatch()
    }

    @Override
    void checkAndRenewTimer() {
        /*if (TimeUtils.maxTimeReached(sessionTimeoutChecker, 10 * 60)) {
            exceptionSafeClick(locators.RENEW_SESSION_LINK)
            startTimer()
        }*/
        // TODO: re-implement when migrating pipeline tests
    }

    void constructWebDriver() {
        if (driver != null) {
            quit() // only 1 allowed at a time!
        }
        setDriver(BrowserSupport.setup(Settings.BROWSER))
        setTimeout(Settings.DEFAULT_TIMEOUT)
    }

    // setup methods

    void setupXnat() {
        initializeXnat()
        initializeAccounts()
    }

    void initializeXnat() {
        try {
            final Response initCheck = restDriver.adminCredentials.get(new SyntheticXnatInterface(xnatConfig.xnatUrl).formatXapiUrl('/siteConfig/initialized'))
            if (initCheck.statusCode == 200 && initCheck.body.asString() == 'true') {
                log.info('Site already seems to be initialized!')
                return
            }
        } catch (Exception ignored) {}

        // Initialize base XNAT dev sites
        log.debug('Site initialization required...')

        final User adminUser = xnatConfig.adminUser

        new SyntheticPageObject(this).
                loadPage(XnatLoginPage).
                loginToUnitializedXnat(adminUser).
                fillAdminEmail(Settings.EMAIL).
                submit().
                loadUserSelfServicePage(adminUser).
                changeEmail(Settings.permuteSeleniumEmail()).
                loadAdminUi().
                disableSiteAnonScript().
                logout()
    }

    void initializeAccounts() {
        final User adminUser = xnatConfig.adminUser
        [xnatConfig.mainUser, xnatConfig.mainAdminUser].each { user ->
            final String username = user.username
            final Response response = restDriver.getJson(restDriver.adminCredentials, new SyntheticXnatInterface(xnatConfig.xnatUrl).formatRestUrl("/user/${username}"))
            switch (response.statusCode) {
                case 200 :
                    if (response.path('enabled') == 'false') {
                        adminInterface().enableUser(user)
                    }
                    break
                case 400 :
                    adminInterface().createUser(user)
                    break
                default :
                    fail("Unexpected response code for checking status of '${username}' account: ${response.statusCode}")
            }
        }

        final String defaultAdmin = 'administrator@xnat.org'
        if (adminInterface().queryBase().get(new SyntheticXnatInterface(xnatConfig.xnatUrl).formatXapiUrl('/siteConfig/adminEmail')).asString() == defaultAdmin) {
            adminInterface().postToSiteConfig([adminEmail : Settings.EMAIL])
        }
        final User actualAdminAccount = adminInterface().readUser(adminUser.username)
        if (actualAdminAccount.email == defaultAdmin) {
            adminInterface().updateUser(actualAdminAccount.email(Settings.EMAIL))
        }
    }

    final IndexPage home() {
        navigate().to(xnatConfig.xnatUrl)
        new SyntheticPageObject(this).loadPage(IndexPage)
    }

    final int takeTargetedScreenshot(Integer step, By... elements) {
        if (!isTestRunning()) {
            return 0
        } // Don't try and take screenshots in early @BeforeSuite/@BeforeClass setup
        final String screenshotPath = Settings.getScreenshotPath(testClass)
        FileIOUtils.mkdirs(screenshotPath)
        final int screenShotNumber = 1 + FileIOUtils.listFilesRecursively(new File(screenshotPath)).count { screenshot ->
            screenshot.name.matches("${testName}_step_${step}_[0-9]+\\.png")
        }
        WebDriverUtils.takeTargetedScreenshot(screenshotPath, screenshotName(step, screenShotNumber), this, elements)
        screenShotNumber
    }

    private String screenshotName(int step, int screenshotNum) {
        "${testName}_step_${step}_${screenshotNum}.png"
    }

    private File screenshotFile(int step, int screenshotNum) {
        Paths.get(Settings.getScreenshotPath(testClass), screenshotName(step, screenshotNum)).toFile()
    }

    final void uploadTargetedScreenshot(Integer step, By... locators) {
        if (isTestRunning()) {
            testController.postStepAttachment(screenshotFile(step, takeTargetedScreenshot(step, locators)), step)
        }
    }

    final void uploadTargetedScreenshot(MutableInt step, By... locators) {
        if (isTestRunning()) {
            uploadTargetedScreenshot(step.value, locators)
        }
    }

    final void uploadTargetedScreenshot(By... locators) {
        uploadTargetedScreenshot(stepCounter, locators)
    }

    final void uploadTargetedScreenshot(List<By> locators) {
        uploadTargetedScreenshot(locators as By[])
    }

    final void commentStep(String comment) {
        if (testController != null) {
            testController.commentStep(comment)
        }
    }

    final void captureStep(int step, TestStatus status, String comment, boolean takeScreenshot, By... locators) {
        if (!isTestRunning()) { // even if we don't need to capture a step, we expect these to be on the page
            locators.each { by ->
                waitForElementVisible(by)
            }
            return
        }
        if (takeScreenshot) {
            testController.postStepAttachment(screenshotFile(step, takeTargetedScreenshot(step, locators)), step)
        }
        testController.currentTest.updateStepResult(step, status, comment)
        stepCounter.increment()
    }

    final void captureStep(int step, TestStatus status, String comment, boolean takeScreenshot, List<By> locators) {
        captureStep(step, status, comment, takeScreenshot, locators as By[])
    }

    final void captureStep(MutableInt step, TestStatus status, String comment, boolean takeScreenshot, By... locators) {
        try {
            captureStep(step.value, status, comment, takeScreenshot, locators)
        } catch (NullPointerException ignored) {} // tests not started yet
    }

    final void captureStep(MutableInt step, By... locators) {
        captureStep(step, TestStatus.PASS, null, true, locators) // lazy version
    }

    final void captureStep(By... locators) {
        captureStep(stepCounter, locators) // even lazier!
    }

    final void captureStep(List<By> locators) {
        captureStep(locators.toArray() as By[])
    }

    final void captureStep(By[][] locators) {
        locators.eachWithIndex{ locatorArray, index ->
            if (index < locators.length - 1) {
                uploadTargetedScreenshot(stepCounter, locatorArray)
            } else {
                captureStep(locatorArray)
            }
        }
    }

    final void captureScreenshotlessStep() {
        captureStep(stepCounter, TestStatus.PASS, null, false)
    }

    final void capturePreviousStep(By... locators) {
        // useful to overwrite previous step
        stepCounter.decrement()
        captureStep(locators)
    }

    final void skipStep() {
        stepCounter.increment()
    }

    final void switchToNewIframe() {
        final WebElement iFrame = findElement(By.xpath("//iframe[not(@id='_yuiResizeMonitor')]"))
        setHorizIframeOffset(iFrame.location.x)
        setVertIframeOffset(iFrame.location.y)
        switchTo().frame(iFrame)
    }

    final void leaveIframe() {
        switchTo().defaultContent()
        setHorizIframeOffset(0)
        setVertIframeOffset(0)
    }

    final void waitForDocumentReadyState() {
        try {
            wait.until {
                queryDocumentReadyState() == 'complete'
            }
        } catch (TimeoutException ignored) {
            log.warn("document.readyState never seemed to complete. Last known status: ${queryDocumentReadyState()}. Presuming intermittent issue in XNAT front-end and resuming test...")
        }
    }

    // returns true iff the state of the box was changed with a click
    boolean performSwitchBox(By checkboxBy, boolean enableSetting) {
        if (enableSetting != switchBoxIsChecked(checkboxBy)) {
            exceptionSafeClick(getSwitchBoxBy(checkboxBy))
            return true
        }
        false
    }

    boolean switchBoxIsChecked(By checkboxBy) {
        waitForElement(checkboxBy)
        exceptionSafeFind(checkboxBy).isSelected()
    }

    void toggleSwitchBox(By checkboxBy) {
        performSwitchBox(checkboxBy, !switchBoxIsChecked(checkboxBy))
    }

    // returns true iff the state of the box was changed with a click
    boolean performCheckBox(By checkboxBy, boolean enableSetting) {
        waitForElement(checkboxBy)
        waitForElementVisible(checkboxBy)
        if (enableSetting != findElement(checkboxBy).isSelected()) {
            exceptionSafeClick(checkboxBy)
            return true
        }
        false
    }

    void performTriStateCheckBox(By checkboxBy, boolean enableSetting) {
        waitForElement(checkboxBy)
        waitForElementVisible(checkboxBy)
        final CheckboxState initialState = readCheckboxState(checkboxBy)
        if (enableSetting) {
            if (initialState != CheckboxState.CHECKED) {
                exceptionSafeClick(checkboxBy)
            }
        } else {
            if (initialState == CheckboxState.CHECKED) {
                exceptionSafeClick(checkboxBy)
            } else if (initialState == CheckboxState.INDETERMINATE) {
                exceptionSafeClick(checkboxBy)
                exceptionSafeClick(checkboxBy)
            }
        }
    }

    void submitForm() {
        // Check the most common submit names and click the first one we find
        multipleLocatorClick(By.name('eventSubmit_doPerform'), By.name('eventSubmit_doInsert'), By.name('eventSubmit_doSetup'), By.xpath(".//button[@type='submit']"))
    }

    void clickOKButton() {
        exceptionSafeClick(By.xpath("//*[(self::button or self::div) and (text()='Ok' or text()='OK' or text()='Okay' or @class='ok close default button' or @class='ok default button') and ${XnatLocators.NOT_HIDDEN_CONDITION}]"))
    }

    void pseudoSelect(By selector, String selection) {
        if (selection != null) {
            exceptionSafeClick(selector)
            exceptionSafeClick(new ByChained(selector, By.xpath(".//li[normalize-space(text())='${selection}']")))
        }
    }

    By getSwitchBoxBy(By checkboxInput) {
        new ByChained(checkboxInput, By.xpath('./following-sibling::span'))
    }

    protected CheckboxState readCheckboxState(By checkboxLocator) {
        final WebElement checkboxElement = findElement(checkboxLocator)
        if (checkboxElement.isSelected()) {
            return CheckboxState.CHECKED
        } else if (checkboxElement.getAttribute('indeterminate') != null) {
            return CheckboxState.INDETERMINATE
        } else {
            return CheckboxState.UNCHECKED
        }
    }
    
    protected XnatInterface adminInterface() {
        restDriver.interfaceFor(xnatConfig.adminUser)
    }

    protected String queryDocumentReadyState() {
        (driver as JavascriptExecutor).executeScript('return document.readyState')
    }

    private enum CheckboxState { CHECKED, INDETERMINATE, UNCHECKED }

}