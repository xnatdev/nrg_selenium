package org.nrg.selenium.xnat.scripting

import org.nrg.selenium.enums.EventType

class EventHandler {

    boolean projectSpecific = true
    String project
    String eventID
    String description
    String status = 'Complete'
    EventType eventType = EventType.WORKFLOW_STATUS
    AutomationScript script

    EventHandler(String eventID) {
        this.eventID = eventID
    }

    // Setters this to chain multiple set operations together

    EventHandler scope(boolean projectSpecific) {
        setProjectSpecific(projectSpecific)
        this
    }

    EventHandler project(String project) {
        setProject(project)
        this
    }

    EventHandler eventID(String eventID) {
        setEventID(eventID)
        this
    }

    EventHandler description(String description) {
        setDescription(description)
        this
    }

    EventHandler status(String status) {
        setStatus(status)
        this
    }

    EventHandler eventType(EventType eventType) {
        setEventType(eventType)
        this
    }

    EventHandler script(AutomationScript script) {
        setScript(script)
        this
    }

}
