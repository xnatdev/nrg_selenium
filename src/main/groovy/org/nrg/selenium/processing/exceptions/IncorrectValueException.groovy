package org.nrg.selenium.processing.exceptions

import org.nrg.testing.xnat.processing.exceptions.ProcessingValidationException
import org.openqa.selenium.By

class IncorrectValueException extends ProcessingValidationException {

    private final By locator
    private final String explanation
    private final String expected
    private final String actual

    IncorrectValueException(By locator, String explanation, String expected, String actual) {
        this.locator = locator
        this.explanation = explanation
        this.expected = expected
        this.actual = actual
    }

    @Override
    String getMessage() {
        "Element comparison was not satisfied because ${explanation}.\nExpected value: ${expected}\n  Actual value: ${actual}\n       Locator: ${locator}"
    }

}
