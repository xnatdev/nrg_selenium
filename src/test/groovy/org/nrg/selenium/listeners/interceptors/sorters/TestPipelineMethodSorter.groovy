package org.nrg.selenium.listeners.interceptors.sorters

import org.nrg.testing.unit.UnitTestFilter
import org.testng.IMethodInstance
import org.testng.annotations.Listeners
import org.testng.annotations.Test

import static org.testng.AssertJUnit.*

@SuppressWarnings('UnnecessaryQualifiedReference') // qualified field required to get groovy to compile
@Test(dependsOnGroups = TestPipelineMethodSorter.DUMMY)
@Listeners(UnitTestFilter)
class TestPipelineMethodSorter {

    public static final String DUMMY = 'dummy'
    private static final PipelineMethodSorter INTERCEPTOR = new PipelineMethodSorter()

    void testPipelineSorting() {
        final int testId = 1
        final IMethodInstance launch100 = UnitTestFilter.getInstance('testLaunch100')
        final IMethodInstance launch55 = UnitTestFilter.getInstance('testLaunch55')
        final IMethodInstance launch50 = UnitTestFilter.getInstance('testLaunch50')
        final IMethodInstance launch40 = UnitTestFilter.getInstance('testLaunch40')
        final IMethodInstance launch30 = UnitTestFilter.getInstance('testLaunch30')
        final IMethodInstance launch20 = UnitTestFilter.getInstance('testLaunch20')
        final IMethodInstance launch5 = UnitTestFilter.getInstance('testLaunch5')
        final IMethodInstance check55 = UnitTestFilter.getInstance('testCheck55')
        final IMethodInstance check50 = UnitTestFilter.getInstance('testCheck50')
        final IMethodInstance check40 = UnitTestFilter.getInstance('testCheck40')
        final IMethodInstance check30 = UnitTestFilter.getInstance('testCheck30')
        final List<IMethodInstance> actualSort = INTERCEPTOR.orderByJobShopSolution(UnitTestFilter.getDummyTests(testId), 3)
        assertEquals(14, actualSort.size())
        assertTrue(actualSort[0 .. 6] in [[launch100, launch55, launch50, launch30, launch40, launch20, launch5], [launch100, launch50, launch55, launch30, launch40, launch20, launch5]])
        assertEquals([check50, check55, check30, check40], actualSort[7 .. 10]) // no point in checking the last 3 since the order is indeterminate
    }

    void testMissingCheckTest() {
        final int testId = 2
        try {
            INTERCEPTOR.orderByJobShopSolution(UnitTestFilter.getDummyTests(testId), 3)
            fail('Exception should have been thrown.')
        } catch (Exception e) {
            assertEquals('Could not find corresponding pipeline check test for launch test testLaunch5.', e.message)
        }

    }

}