package org.nrg.selenium.xnat.page_model.project.edit

import org.nrg.jira.components.zephyr.TestStatus
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatEditPage
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage
import org.nrg.selenium.xnat.page_model.report.XnatReportPage
import org.nrg.selenium.xnat.page_model.ui_element.notification.XnatAlert
import org.nrg.selenium.xnat.page_model.ui_element.popup.XModal
import org.nrg.xnat.enums.Accessibility
import org.nrg.xnat.pogo.Investigator
import org.nrg.xnat.pogo.Project
import org.openqa.selenium.By
import org.openqa.selenium.support.pagefactory.ByAll
import org.openqa.selenium.support.pagefactory.ByChained

class ProjectEditPage implements XnatEditPage<Project, ProjectEditPage, ProjectReportPage>, ScreenshotableComponent<ProjectEditPage>, NavigablePageObject<ProjectEditPage> {

    protected By investigatorsSection = By.xpath("//th[text()='Investigator(s)']/..")
    protected By piPseudoselect = By.xpath("//*[@id='primary-investigator-menu']/div")
    protected By createInvestigatorButton = By.id('create-investigator')
    protected By investigatorPanel = By.id('edit-investigator-panel')
    protected By investigatorPrimary = By.xpath("//input[@class='set-primary']")
    protected By investigatorTitle = By.id('title')
    protected By investigatorFirstname = By.id('firstname')
    protected By investigatorLastname = By.id('lastname')
    protected By investigatorInstitution = By.id('institution')
    protected By investigatorDepartment = By.id('department')
    protected By investigatorEmail = By.id('email')
    protected By investigatorPhone = By.id('phone')
    protected By titleLocator = projectData('name')
    protected By runningTitleLocator = projectData('secondary_ID')
    protected By idLocator = projectData('ID')
    protected By descriptionLocator = projectData('description')
    protected By keywordsLocator = projectData('keywords')
    protected By createButtonLocator = By.xpath('//input[@value="Create Project"]')
    protected By detailsTable = By.xpath("(//div[@class='container'])[1]")
    protected By accessibilityTable = By.xpath("(//div[@class='container'])[2]")
    protected String alertDeletionScript = 'Array.from(document.getElementsByClassName("alert")).forEach(x => x.remove())'

    @Override
    void populateFields(Project project, boolean takeAllScreenshots) {
        if (takeAllScreenshots) {
            captureDetailsTable()
        }
        setPI(project.pi) // TODO: other investigators
        fillProjectTitle(project.title)
        fillRunningTitle(project.runningTitle)
        fillProjectID(project.id)
        fillProjectDescription(project.description)
        fillKeywords(project.keywords)
        fillAliases(project.aliases)
        if (takeAllScreenshots) {
            captureStep(stepCounter, TestStatus.PASS, "Project Title = ${project.title}, Running Title = ${project.runningTitle}, ID = ${project.id}", true, detailsTable)
        }
        setProjectAccessibility(project.accessibility)
        if (takeAllScreenshots) {
            captureStep(accessibilityTable)
        }
    }

    @Override
    void navigateTo() {
        topNav('New', 'Project')
    }

    @Override
    Class<XnatReportPage> getAssociatedReportClass() {
        ProjectReportPage
    }

    @Override
    List<By> getComponentLocators() {
        [By.xpath('//*[contains(@id, "-project-form")]')]
    }

    ProjectEditPage captureDetailsTable() {
        captureStep(detailsTable)
        this
    }

    XnatAlert createProjectWithExpectedFailure(Project project) {
        populateFields(project, false)
        submitWithExpectedFailure()
    }

    XnatAlert submitWithExpectedFailure() {
        submit()
        readAlert()
    }

    ProjectEditPage setPI(Investigator pi) {
        if (pi != null) {
            if (!investigatorExists(pi)) {
                createInvestigator(pi, true)
            }
            selectPI(pi)
            captureStep(investigatorsSection)
        }
        this
    }

    boolean investigatorExists(Investigator investigator) {
        waitForElement(piPseudoselect)
        isElementPresentQuick(new ByChained(piPseudoselect, locators.byText(investigator.fullName)))
    }

    ProjectEditPage createInvestigator(Investigator investigator, boolean isPrimary) {
        exceptionSafeClick(createInvestigatorButton)

        final XModal investigatorModal = readXModal(investigatorPanel)
        fillInvestigatorFields(investigator)
        if (isPrimary) {
            exceptionSafeClick(investigatorPrimary)
        }
        investigatorModal.uploadScreenshot()
        investigatorModal.clickOK()
        this
    }

    ProjectEditPage fillInvestigatorFields(Investigator investigator) {
        fill(investigatorTitle, investigator.title)
        fill(investigatorFirstname, investigator.firstname)
        fill(investigatorLastname, investigator.lastname)
        fill(investigatorInstitution, investigator.institution)
        fill(investigatorDepartment, investigator.department)
        fill(investigatorEmail, investigator.email)
        fill(investigatorPhone, investigator.phone)
        this
    }

    ProjectEditPage selectPI(Investigator investigator) {
        pseudoSelect(piPseudoselect, investigator.fullName)
        this
    }

    ProjectEditPage fillProjectTitle(String projectTitle) {
        fill(titleLocator, projectTitle)
        this
    }

    ProjectEditPage fillRunningTitle(String runningTitle) {
        fill(runningTitleLocator, runningTitle)
        this
    }

    ProjectEditPage fillProjectID(String projectID) {
        fill(idLocator, projectID)
        this
    }

    ProjectEditPage fillProjectDescription(String projectDescription) {
        fill(descriptionLocator, projectDescription)
        this
    }

    ProjectEditPage fillKeywords(List<String> keywords) {
        if (keywords != null && !keywords.isEmpty()) {
            fill(keywordsLocator, keywords.join(' '))
        }
        this
    }

    ProjectEditPage fillAlias(int aliasNumber, String value) {
        // Starts with 0
        fill(alias(aliasNumber), value)
        this
    }

    ProjectEditPage fillAliases(List<String> aliases) {
        if (aliases != null) {
            if (aliases.size() > 4) {
                throw new UnsupportedOperationException('Too many aliases')
            }
            aliases.eachWithIndex{ alias, index ->
                fillAlias(index, alias)
            }
        }
        this
    }

    ProjectEditPage setProjectAccessibility(Accessibility accessibility) {
        if (accessibility != null) {
            exceptionSafeClick(projectAccessSetting(accessibility))
        }
        this
    }

    protected By projectData(String data) {
        By.id("xnat:projectData/${data}")
    }

    protected By alias(int aliasNumber) {
        projectData("aliases/alias[${aliasNumber}]/alias")
    }

    protected By projectAccessSetting(Accessibility accessibility) {
        new ByAll(By.id("${accessibility}_access"), By.xpath("//input[@name='accessibility' and @value='${accessibility}']"))
    }

}
