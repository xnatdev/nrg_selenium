package org.nrg.selenium.xnat.page_model

import org.openqa.selenium.By

class ReportIssuePage implements NavigablePageObject<ReportIssuePage>, LoadablePage, ScreenshotableComponent<ReportIssuePage> {

    protected By summaryLocator = By.xpath('//input[@name="summary"]')
    protected By descriptionLocator = By.xpath('//textarea[@name="description"]')
    protected By fileLocator = By.name('upload')
    protected By submitButton = By.xpath('//input[@type="submit"]')

    @Override
    List<By> getComponentLocators() {
        [By.xpath('//div[@class="issueReport"]')]
    }

    @Override
    void navigateTo() {
        topNav("Help", "Report a Problem")
    }

    ReportIssuePage fillSummary(String summary) {
        fill(summaryLocator, summary)
        this
    }

    ReportIssuePage fillDescription(String description) {
        fill(descriptionLocator, description)
        this
    }

    ReportIssuePage attachFile(File file) {
        exceptionSafeFind(fileLocator).sendKeys(file.absolutePath)
        this
    }

    IndexPage submitReport() {
        exceptionSafeClick(submitButton)
        loadPage(IndexPage)
    }

}
