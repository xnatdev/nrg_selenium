package org.nrg.selenium.xnat.page_model.ui_element

import org.nrg.selenium.CustomConditions
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.extension.XpathUnion
import org.nrg.selenium.util.BrowserDownloadManager
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.download.DataDownloadPage
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.session.report.ImagingSessionReportPage
import org.nrg.selenium.xnat.page_model.subject.report.SubjectReportPage
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.pagefactory.ByChained
import org.openqa.selenium.support.ui.ExpectedCondition

import static org.testng.AssertJUnit.assertEquals

class YuiTable implements XnatPageComponent, HtmlTable<YuiTable> {

    protected String sortDownIdentifier = 'Sort Down'
    protected String sortUpIdentifier = 'Sort Up'
    protected String hideColumnIdentifier = 'Hide Column'
    protected String filterIdentifier = 'Filter'
    protected By optionsLocator = By.linkText('Options')
    protected XpathLocator tabsLocator = XnatLocators.byId('search_tabs')
    protected XpathLocator arbitraryTabLocator = tabsLocator.joinSublocator('.//li[not(@class="disabled phantom")]//em')
    protected XpathLocator currentTabLocator = tabsLocator.joinSublocator('.//li[@title="active"]//em')
    protected By sortedColumnHeaderLocator = By.xpath('//th[@class="x_rs_th sorted"]')
    protected By tabDropdownLocator = By.id('search_selector')
    protected By loadSublocator = By.xpath(".//div[@class='yui-dt-liner']")
    protected XpathLocator mainTableLocator = new XpathLocator("//table[@class='x_rs_t' and not(./ancestor-or-self::*[@class='yui-hidden'])]")
    protected XpathLocator failedSearchLocator = tabsLocator.joinSublocator('./div[@class="yui-content"]')
    static final String SEARCH_FAILED_TEXT = 'Failed to create search results.'

    @Override
    List<By> getComponentLocators() {
        [tabsLocator]
    }

    @Override
    void performInitialValidation() {
        waitForElement(new ByChained(tabsLocator, loadSublocator))
        wait.until(CustomConditions.elementClickable(new XpathUnion([mainTableLocator, failedSearchLocator])))
    }

    @Override
    XpathLocator getTableLocator() {
        mainTableLocator
    }

    SubjectReportPage clickSubjectLink(String subjectIdentifier) {
        clickDataCellLink(subjectIdentifier)
        loadPage(SubjectReportPage)
    }

    ImagingSessionReportPage clickSessionLink(String sessionIdentifier) {
        clickDataCellLink(sessionIdentifier)
        loadPage(ImagingSessionReportPage)
    }

    boolean isTabAvailable(String tabName) {
        findElements(arbitraryTabLocator)*.text.contains(tabName)
    }

    YuiTable switchToTab(String tabName) {
        if (isTabAvailable(tabName)) {
            exceptionSafeClick(tabLocator(tabName))
        } else {
            exceptionSafeSelect(tabDropdownLocator, tabName)
            performInitialValidation()
        }
        this
    }

    String getCurrentTab() {
        exceptionSafeGetText(currentTabLocator).trim()
    }

    YuiTable assertCurrentTab(String expectedTab) {
        assertEquals(expectedTab, getCurrentTab())
        this
    }

    YuiTable assertSearchResultsFailed() {
        wait.until(CustomConditions.elementTextMatchesText(failedSearchLocator, SEARCH_FAILED_TEXT))
        assertElementText(SEARCH_FAILED_TEXT, failedSearchLocator)
        this
    }

    YuiTable waitForExpectedNumberOfColumns(int numColumns) {
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver webDriver) {
                countColumns() == numColumns
            }
        })
        this
    }

    YuiTable clickOptionsChoice(String clickedMenuOption) {
        waitForElement(optionsLocator)
        exceptionSafeClick(optionsLocator)
        exceptionSafeClick(optionLocator(clickedMenuOption))
        this
    }

    YuiTable clickHeaderOption(String headerName, String actionName) {
        exceptionSafeClick(headerLocator(headerName))
        exceptionSafeClick(headerActionLocator(actionName))
        this
    }

    YuiTable sortDownOnColumn(String columnName) {
        clickHeaderOption(columnName, sortDownIdentifier)
    }

    YuiTable sortUpOnColumn(String columnName) {
        clickHeaderOption(columnName, sortUpIdentifier)
    }

    YuiTable hideColumn(String columnName) {
        clickHeaderOption(columnName, hideColumnIdentifier)
    }

    List<String> readSortedColumn() {
        readEntriesForColumn(exceptionSafeGetText(sortedColumnHeaderLocator))
    }

    YuiTableFilterDialog launchFilteringOnColumn(String columnName) {
        clickHeaderOption(columnName, filterIdentifier)
        loadPage(YuiTableFilterDialog).withUnderlyingPage(this)
    }

    DataDownloadPage optionsDownload() {
        clickOptionsChoice('Download')
        loadPage(DataDownloadPage)
    }

    File optionsSpreadsheet() {
        final BrowserDownloadManager downloadManager = new BrowserDownloadManager()
        clickOptionsChoice('Spreadsheet')
        downloadManager.findDownload()
    }

    YuiTableEditColumnDialog optionsEditColumns() {
        final int numColumns = countColumns()
        clickOptionsChoice('Edit Columns')
        final YuiTableEditColumnDialog dialog = loadPage(YuiTableEditColumnDialog).withUnderlyingPage(this)
        dialog.setExpectedNumberColumns(numColumns)
        dialog
    }

    YuiSendEmailDialog optionsEmail() {
        clickOptionsChoice('Email')
        loadPage(YuiSendEmailDialog).withUnderlyingPage(this)
    }

    XmlDialog optionsShowXml() {
        clickOptionsChoice('Show XML')
        loadPage(XmlDialog).withUnderlyingPage(this)
    }

    SaveStoredSearchDialog optionsSaveSearch() {
        clickOptionsChoice('Save Search')
        loadPage(SaveStoredSearchDialog).withUnderlyingPage(this)
    }

    SaveStoredSearchDialog optionsSaveAsNewSearch() {
        clickOptionsChoice('Save as New Search')
        loadPage(SaveStoredSearchDialog).withUnderlyingPage(this)
    }

    protected By tabLocator(String tabName) {
        arbitraryTabLocator.joinSublocator("[normalize-space(text())='${tabName}']")
    }

    protected By optionLocator(String option) {
        By.xpath("//a[@class='yuimenuitemlabel' and text()='${option}']")
    }

    protected By headerLocator(String headerName) {
        mainTableLocator.joinSublocator(".//th/div[text()='${headerName}']")
    }

    protected By headerActionLocator(String actionName) {
        mainTableLocator.joinSublocator(".//a[text()='${actionName}']")
    }

}
