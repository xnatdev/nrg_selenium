package org.nrg.selenium.xnat.page_model.ui_element.notification

import org.openqa.selenium.By

class XnatMessage implements XnatNotification<XnatMessage> {

    @Override
    By getLocator() {
        By.cssSelector('div.message')
    }

}
