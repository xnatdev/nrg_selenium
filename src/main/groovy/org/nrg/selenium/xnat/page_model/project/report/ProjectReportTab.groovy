package org.nrg.selenium.xnat.page_model.project.report

import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.openqa.selenium.By

trait ProjectReportTab implements XnatPageComponent, ProjectReportBase {

    abstract By tabLocator()

}
