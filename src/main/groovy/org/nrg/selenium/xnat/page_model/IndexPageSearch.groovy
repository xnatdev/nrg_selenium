package org.nrg.selenium.xnat.page_model

import org.nrg.selenium.xnat.page_model.site_search.AdvancedSearchResult
import org.nrg.xnat.pogo.Investigator
import org.openqa.selenium.By

class IndexPageSearch implements NavigablePageObject<IndexPageSearch>, ScreenshotableComponent<IndexPageSearch>, SimpleTabbedPage<IndexPageSearch> {

    protected By mainLocator = By.id('front_search')
    protected By projectTabIdLocator = By.name('xnat:projectData.ID_equals')
    protected By projectTabTitleLocator = By.name('xnat:projectData.NAME_equals')
    protected By projectTabDescriptionLocator = By.name('xnat:projectData.DESCRIPTION_equals')
    protected By projectTabKeywordsLocator = By.name('xnat:projectData.KEYWORDS_equals')
    protected By projectTabInvestigatorLocator = By.id('project_investigator_chosen')
    protected static final String PROJECT_IDENTIFIER = 'Projects'

    IndexPageSearch switchToProjectTab() {
        switchToTab(PROJECT_IDENTIFIER)
    }

    @Override
    List<By> getComponentLocators() {
        [mainLocator]
    }

    IndexPageSearch fillProjectId(String projectId) {
        fill(projectTabIdLocator, projectId)
        this
    }

    IndexPageSearch fillProjectTitle(String title) {
        fill(projectTabTitleLocator, title)
        this
    }

    IndexPageSearch fillDescription(String description) {
        fill(projectTabDescriptionLocator, description)
        this
    }

    IndexPageSearch fillKeywords(String keywords) {
        fill(projectTabKeywordsLocator, keywords)
        this
    }

    IndexPageSearch selectInvestigator(Investigator investigator) {
        pseudoSelect(projectTabInvestigatorLocator, "${investigator.lastname}, ${investigator.firstname}")
        this
    }

    AdvancedSearchResult submit() {
        submitForm()
        loadPage(AdvancedSearchResult)
    }

    @Override
    By tabLocator(String tab) {
        By.xpath("//span[text()='${tab}']")
    }

}
