package org.nrg.selenium.extension

import org.openqa.selenium.By
import org.testng.annotations.Test

import static org.testng.AssertJUnit.assertEquals

class TestXpathLocator {

    @Test
    void testXpathLocatorJoin() {
        final XpathLocator baseLocator = new XpathLocator("//div[@id='archive']")
        assertEquals("//div[@id='archive']/div[3]", baseLocator.joinSublocator('./div[3]').expression)
        assertEquals("//div[@id='archive']/div[3]", baseLocator.joinSublocator('/div[3]').expression)
    }

    @Test
    void testXpathLocatorNthInstance() {
        assertEquals('(//div/div/div)[3]', new XpathLocator('//div/div/div').nthInstance(3).expression)
    }

    @Test
    void testSimpleAncestors() {
        assertEquals('//div/div/..', new XpathLocator('//div/div').parent().expression)
        assertEquals('//div/div/../..', new XpathLocator('//div/div').grandparent().expression)
    }

    @Test
    void testUnion() {
        assertEquals('(//div | //div/span | //span/div)', new XpathUnion([new XpathLocator('//div'), new XpathLocator('//div/span'), new XpathLocator('//span/div')]).expression)
    }

    @Test
    void testUnionSublocators() {
        assertEquals('(//div/div/span | //div/table/tbody)', new XpathLocator('//div').unionSublocators([new XpathLocator('./div/span'), new XpathLocator('./table/tbody')]).expression)
    }

}
