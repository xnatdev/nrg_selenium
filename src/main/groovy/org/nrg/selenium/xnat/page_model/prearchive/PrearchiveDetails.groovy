package org.nrg.selenium.xnat.page_model.prearchive

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.dicom.DicomDumpPage
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.session.report.ScanListRecord
import org.nrg.selenium.xnat.page_model.ui_element.actions.HasActionsBox
import org.nrg.xnat.pogo.experiments.Scan
import org.openqa.selenium.By
import org.openqa.selenium.support.pagefactory.ByChained

class PrearchiveDetails implements XnatPageComponent, ScreenshotableComponent<PrearchiveDetails>, ScanListRecord<PrearchiveDetails>, HasActionsBox {

    protected By scanDownloadAllLocator = By.id('dAscans')
    protected XpathLocator scansLocator = XnatLocators.byId('scans_div')
    protected XpathLocator scanIdentifierLocator = new XpathLocator("//tr[contains(@id, 'scanTR')]")
    protected XpathLocator scanIdSublocator = new XpathLocator('./td[1]')
    protected XpathLocator reviewDicomButtonSublocator = XnatLocators.buttonWithText('Review DICOM Tags')
    XnatPrearchive prearchive

    @Override
    List<By> getComponentLocators() {
        [scansLocator as By] + readActionsBox().componentLocators
    }

    @Override
    void performInitialValidation() {
        switchToNewWindow()
        waitForElement(scanDownloadAllLocator)
    }

    @Override
    By scanAssertionReadyLocator() {
        scansLocator
    }

    @Override
    List<String> readScanIds() {
        (1 .. findElements(scanIdentifierLocator).size()).collect { index ->
            exceptionSafeGetText(scanIdentifierLocator.nthInstance(index).joinSublocator(scanIdSublocator))
        }
    }

    @Override // TODO
    List<Scan> readScans() {
        throw new UnsupportedOperationException('Method not yet implemented for this page')
    }

    @Override
    XpathLocator scanTableLocator() {
        scansLocator.joinSublocator(XnatLocators.TABLE_TAG)
    }

    DicomDumpPage<PrearchiveDetails> readDicomDump(String scanId = null) {
        exceptionSafeClick(scanId ? scanRowLocator(scanId).joinSublocator(reviewDicomButtonSublocator) : reviewDicomButtonSublocator)
        loadPage(DicomDumpPage).withUnderlyingPage(this)
    }

    XnatPrearchive returnToPrearchive() {
        close()
        returnToMainWindow()
        prearchive
    }

    protected XpathLocator scanRowLocator(String scanId) {
        XnatLocators.byId("scanTR${scanId}")
    }

}
