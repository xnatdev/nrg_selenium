package org.nrg.selenium.xnat.page_model.project.report

import org.nrg.testing.CommonStringUtils
import org.nrg.xnat.versions.*

class ProjectDetailsTab_1_7_5 extends ProjectDetailsTab {

    @Override
    String escapeOnProjectPage(String input) {
        CommonStringUtils.replaceRecursively(input, ['&': '&amp', '<': '&lt', '>': '&gt', '\'': '&apos'])
    }

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_7_5]
    }

}
