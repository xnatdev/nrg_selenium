package org.nrg.selenium.xnat.page_model.users

import org.nrg.selenium.xnat.page_model.IndexPage
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatLoginPage
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.xnat.pogo.users.User
import org.openqa.selenium.By

class AcceptParPage implements XnatPageComponent, ScreenshotableComponent<AcceptParPage> {

    boolean pageStateIsRegister = true
    protected By registerButtonLocator = XnatLocators.buttonWithText('Register?')
    protected By alreadyHaveAccountButtonLocator = XnatLocators.buttonWithText('Already Have an Account?')

    @Override
    List<By> getComponentLocators() {
        [By.id('page_wrapper')]
    }

    AcceptParPage clickRegisterButton() {
        if (!pageStateIsRegister) {
            exceptionSafeClick(registerButtonLocator)
            pageStateIsRegister = true
        }
        this
    }

    AcceptParPage clickAlreadyHaveAccountButton() {
        if (pageStateIsRegister) {
            exceptionSafeClick(alreadyHaveAccountButtonLocator)
            pageStateIsRegister = false
        }
        this
    }

    // TODO: register

    IndexPage login(User user) {
        if (pageStateIsRegister) {
            clickAlreadyHaveAccountButton()
        }
        loginPageSubcomponent.login(user, true)
    }

    XnatLoginPage getLoginPageSubcomponent() {
        loadPage(XnatLoginPage, false, true)
    }

    RegistrationPage getRegistrationPageSubcomponent() {
        loadPage(RegistrationPage)
    }

}
