package org.nrg.selenium.xnat.page_model.admin

import org.openqa.selenium.By

class AdminUiSwitchBox implements AdminUiElement {

    boolean checked

    AdminUiSwitchBox(By by, boolean checked, By screenshot = null, boolean takeScreenshot = true) {
        setLocator(by)
        setScreenshotLocator(screenshot)
        setTakeScreenshot(takeScreenshot)
        setChecked(checked)
    }

    @Override
    void performElementAction() {
        performSwitchBox(locator, checked)
    }

}
