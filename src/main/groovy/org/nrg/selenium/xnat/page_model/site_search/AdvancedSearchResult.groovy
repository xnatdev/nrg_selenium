package org.nrg.selenium.xnat.page_model.site_search

class AdvancedSearchResult implements SiteSearchResult<AdvancedSearchResult> {

    public static final String PROJECT_SEARCH_ID = 'ID'
    public static final String PROJECT_SEARCH_DESCRIPTION = 'Description'

}
