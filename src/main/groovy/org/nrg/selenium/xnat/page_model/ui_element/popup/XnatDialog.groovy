package org.nrg.selenium.xnat.page_model.ui_element.popup

import org.openqa.selenium.By

class XnatDialog implements XnatPopup<XnatDialog> {

    @Override
    By getPopupLocatorHook() {
        By.xpath("./ancestor-or-self::div[contains(@class, 'xnat-dialog') and @data-uid]")
    }

    @Override
    By getTitleLocator() {
        By.xpath("./div[@class='xnat-dialog-header title']/div")
    }

    @Override
    By getBodyLocator() {
        By.xpath("./div[@class='body content xnat-dialog-body']")
    }

}