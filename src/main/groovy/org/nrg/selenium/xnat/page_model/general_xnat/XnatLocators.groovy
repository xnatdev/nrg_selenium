package org.nrg.selenium.xnat.page_model.general_xnat

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.openqa.selenium.By
import org.openqa.selenium.support.pagefactory.ByChained

@SuppressWarnings('unused')
class XnatLocators implements XnatPageComponent {

    // Generic
    public static final XpathLocator PARENT = new XpathLocator('./..')
    public static final XpathLocator GRANDPARENT = new XpathLocator('./../..')

    // HTML tags
    public static final XpathLocator HYPERLINK_TAG = byTagName('a')
    public static final XpathLocator TEXTAREA_TAG = byTagName('textarea')
    public static final XpathLocator TH_TAG = byTagName('th')
    public static final XpathLocator TR_TAG = byTagName('tr')
    public static final XpathLocator PRE_TAG = byTagName('pre')
    public static final XpathLocator IMG_TAG = byTagName('img')
    public static final XpathLocator H1_TAG = byTagName('h1')
    public static final XpathLocator H2_TAG = byTagName('h2')
    public static final XpathLocator H3_TAG = byTagName('h3')
    public static final XpathLocator FONT_TAG = byTagName('font')
    public static final XpathLocator LI_TAG = byTagName('li')
    public static final XpathLocator FORM_TAG = byTagName('form')
    public static final XpathLocator TABLE_TAG = byTagName('table')
    public static final XpathLocator TBODY_TAG = byTagName('tbody')
    public static final XpathLocator THEAD_TAG = byTagName('thead')
    public static final XpathLocator SPAN_TAG = byTagName('span')
    public static final XpathLocator SELECT_TAG = byTagName('select')
    public static final XpathLocator P_TAG = byTagName('p')
    public static final XpathLocator B_TAG = byTagName('b')
    public static final XpathLocator STRONG_TAG = byTagName('strong')

    // Common buttons by text
    public static final XpathLocator SELECT_BUTTON = buttonWithText('Select')
    public static final XpathLocator MODIFY_BUTTON = buttonWithText('Modify')
    public static final XpathLocator DONE_BUTTON = buttonWithText('Done')
    public static final XpathLocator EDIT_BUTTON = buttonWithText('Edit')
    public static final XpathLocator DELETE_BUTTON = buttonWithText('Delete')
    public static final XpathLocator SAVE_BUTTON = buttonWithText('Save')
    public static final XpathLocator CANCEL_BUTTON = buttonWithText('Cancel')

    public static final String NOT_HIDDEN_CONDITION = 'not(./ancestor-or-self::*[contains(@style, "display: none")])'

    public By cancelCloseSubbutton = By.xpath(".//button[@class='cancel close button']")
    public By okSubbuttons = By.xpath(".//button[@class='ok close default button' or @class='ok default button' or @class='okay close default button' or text()='OK']")
    public By otherIFrame = By.xpath("//iframe[not(@id='_yuiResizeMonitor')]")

    static final By parentOf(By child) {
        new ByChained(child, PARENT)
    }

    static final XpathLocator nearestAncestorByTag(String tag) {
        new XpathLocator("./ancestor::${tag}[1]")
    }

    static final XpathLocator tagContainingText(String tag, String text) {
        new XpathLocator(".//${tag}[contains(text(),'${text}')]") // Won't work for elements with multiple text nodes: https://stackoverflow.com/questions/38240763/xpath-difference-between-dot-and-text/38240971
    }

    static final XpathLocator tagWithText(String tag, String text) {
        new XpathLocator(".//${tag}[text()='${text}']")
    }

    static final XpathLocator tdWithText(String text) {
        tagWithText('td', text)
    }

    static final XpathLocator buttonWithText(String text) {
        tagWithText('button', text)
    }

    static final XpathLocator linkWithText(String text) {
        tagWithText('a', text)
    }

    static final XpathLocator byText(String text) {
        tagWithText('*', text)
    }

    static final XpathLocator byPartialText(String text) {
        tagContainingText('*', text)
    }

    static final XpathLocator byId(String id) {
        byPossibleIds([id])
    }

    static final XpathLocator byPossibleIds(List<String> ids) {
        final String idCriteria = ids.collect { id ->
            "@id='${id}'"
        }.join(' or ')
        new XpathLocator("//*[${idCriteria}]")
    }

    static final XpathLocator byClass(String className) {
        new XpathLocator(".//*[@class='${className}']")
    }

    static final XpathLocator byTagName(String tagName) {
        new XpathLocator(".//${tagName}")
    }

    static final By enclosingFormFor(By sublocator) {
        new ByChained(sublocator, nearestAncestorByTag('form'))
    }

    static final XpathLocator byPossibleTagName(List<String> possibleTags, boolean restrictToDirectChildren = false) {
        final String tagCriteria = possibleTags.collect { "self::${it}" }.join(' or ')
        final String addedDescentCharacters = restrictToDirectChildren ? '' : '/'
        new XpathLocator("./${addedDescentCharacters}*[${tagCriteria}]")
    }

    By pseudoselect(String identifier) {
        By.id("${identifier}_chosen")
    }

    By tab(String tabText) {
        By.xpath("//em[translate(text(), '\u00a0', '')='${tabText}']")
    }

}
