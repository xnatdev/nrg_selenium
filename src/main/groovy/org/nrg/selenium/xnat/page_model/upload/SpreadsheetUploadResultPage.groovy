package org.nrg.selenium.xnat.page_model.upload

import org.nrg.selenium.CustomConditions
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.ui_element.HtmlTable
import org.openqa.selenium.By

class SpreadsheetUploadResultPage implements NavigablePageObject<SpreadsheetUploadResultPage>, HtmlTable<SpreadsheetUploadResultPage> {

    protected By headerLocator = XnatLocators.H2_TAG

    @Override
    void performInitialValidation() {
        wait.until(CustomConditions.elementTextMatchesText(headerLocator, 'Your uploaded entries have been stored.'))
    }

    @Override
    List<By> getComponentLocators() {
        [By.xpath("//div[@id='breadcrumbs']/..")]
    }

    @Override
    XpathLocator getTableLocator() {
        new XpathLocator("//div[@id='layout_content']/table")
    }

}
