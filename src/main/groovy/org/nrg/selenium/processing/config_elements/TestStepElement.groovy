package org.nrg.selenium.processing.config_elements

import org.nrg.selenium.xnat.XnatDriver
import org.openqa.selenium.By

class TestStepElement implements PipelineConfigElement {

    private By[] locators

    TestStepElement(By... locators) {
        this.locators = locators
    }

    @Override
    void performElementAction(XnatDriver xnatDriver) {
        xnatDriver.captureStep(locators) // call the config steps "1", "2", ...
    }

}
