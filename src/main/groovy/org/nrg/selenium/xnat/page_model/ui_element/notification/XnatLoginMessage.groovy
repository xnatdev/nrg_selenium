package org.nrg.selenium.xnat.page_model.ui_element.notification

import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertTrue

class XnatLoginMessage implements XnatNotification<XnatLoginMessage> {

    protected String loginNoteString = 'Note: '

    @Override
    By getLocator() {
        By.xpath('//*[@id="login_area"]//div[@class="message"]') // as of 1.7.5, login page has an additional hidden message element, so we need to scope this one to get the right element
    }

    @Override
    String getText() {
        final String fullText = XnatNotification.super.getText()
        assertTrue(fullText.startsWith(loginNoteString))
        fullText.substring(loginNoteString.length())
    }

}
