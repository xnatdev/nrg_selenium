package org.nrg.selenium.enums

import com.fasterxml.jackson.annotation.JsonProperty
import org.openqa.selenium.By

enum LocatorType {

    @JsonProperty('id')
    ID {
        @Override
        By byFor(String value) {
            By.id(value)
        }
    },
    @JsonProperty('xpath')
    XPATH {
        @Override
        By byFor(String value) {
            By.xpath(value)
        }
    }

    abstract By byFor(String value)

}
