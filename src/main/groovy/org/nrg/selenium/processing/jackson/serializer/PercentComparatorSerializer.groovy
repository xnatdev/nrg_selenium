package org.nrg.selenium.processing.jackson.serializer

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.nrg.selenium.processing.element_comparators.comparator.MaxPercentToleranceElementComparator

class PercentComparatorSerializer extends ElementComparatorSerializer<MaxPercentToleranceElementComparator> {

    @Override
    void writeSpecificFields(MaxPercentToleranceElementComparator value, JsonGenerator gen, SerializerProvider provider) {
        gen.writeNumberField('percent', value.tolerance)
    }

}
