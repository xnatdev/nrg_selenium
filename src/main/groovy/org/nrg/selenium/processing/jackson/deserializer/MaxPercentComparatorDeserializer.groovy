package org.nrg.selenium.processing.jackson.deserializer

import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.selenium.processing.element_comparators.comparator.MaxPercentToleranceElementComparator

class MaxPercentComparatorDeserializer extends ElementComparatorDeserializer<MaxPercentToleranceElementComparator> {

    @Override
    MaxPercentToleranceElementComparator mainDeserialization(ObjectCodec objectCodec, JsonNode jsonNode) {
        new MaxPercentToleranceElementComparator(tolerance : jsonNode.get('percent').asDouble())
    }

}
