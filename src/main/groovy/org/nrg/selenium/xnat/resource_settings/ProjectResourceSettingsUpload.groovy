package org.nrg.selenium.xnat.resource_settings

class ProjectResourceSettingsUpload {

    String title
    String description
    File file
    Optional<Boolean> extractCompressedFiles = Optional.empty()
    Optional<Boolean> overwriteAllowed = Optional.empty()

    ProjectResourceSettingsUpload title(String title) {
        this.title = title
        this
    }

    ProjectResourceSettingsUpload description(String description) {
        this.description = description
        this
    }

    ProjectResourceSettingsUpload file(File file) {
        this.file = file
        this
    }

    ProjectResourceSettingsUpload extractCompressedFiles(boolean extract) {
        extractCompressedFiles = Optional.of(extract)
        this
    }

    ProjectResourceSettingsUpload overwriteAllowed(boolean allowed) {
        overwriteAllowed = Optional.of(allowed)
        this
    }

}
