package org.nrg.selenium.xnat.page_model

import org.openqa.selenium.By

trait SimpleTabbedPage<X extends SimpleTabbedPage<X>> implements PageObjectSupport {

    By tabLocator(String tab) {
        By.xpath("//a[text()='${tab}']")
    }

    X switchToTab(String tab) {
        driverExtension.exceptionSafeClick(tabLocator(tab))
        this as X
    }

}
