package org.nrg.selenium.xnat.page_model.project.report

import org.nrg.selenium.CustomConditions
import org.nrg.selenium.exceptions.UnknownInterpretableValueException
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.xnat.enums.Accessibility
import org.nrg.xnat.pogo.users.CustomUserGroup
import org.nrg.xnat.pogo.users.User
import org.nrg.xnat.pogo.users.UserGroup
import org.nrg.xnat.pogo.users.UserGroups
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import static org.testng.AssertJUnit.assertEquals

class ProjectAccessTab implements ProjectReportTab, ScreenshotableComponent<ProjectAccessTab> {

    protected By inviteUserButtonLocator = By.id('invite_user_button')
    protected By accessLevelLocator = By.id('invite_access_level')
    protected By inviteUserLocator = By.id('invite_user')
    protected By loadingIndicatorLocator = By.xpath("//*[contains(text(),'Loading users')]")
    protected By userListHeaderLocator = XnatLocators.tagContainingText('h4', 'Add Users from List')
    protected By userListButtonLocator = By.id('popup_all_users_button')
    protected By addUserDialogLocator = XnatLocators.tagWithText('div', 'Add Users From List')
    protected By addUserButtonLocator = XnatLocators.buttonWithText('Add Users')
    protected By addUserWithEmailButtonLocator = XnatLocators.buttonWithText('Add and Send Email')
    protected By addUserWithoutEmailButtonLocator = XnatLocators.buttonWithText('Add Only')
    protected By directInviteAccessLevelLocator = By.id('invite_access_level')
    protected By directInviteUserLocator = By.id('invite_user')
    protected By directInviteSectionLocator = By.id('user_invite_div')
    protected By directInviteButtonLocator = By.id('invite_user_button')
    protected XpathLocator userTableSectionLocator =  XnatLocators.byId('user_mgmt_div')
    protected By manageGroupsButton = XnatLocators.buttonWithText('Manage Groups')
    protected By manageFeaturesButton = XnatLocators.buttonWithText('Manage Features')
    protected By createCustomGroupButton = By.id('create_group')
    protected By userTableLoadingCompleteLocator = By.xpath("//a[contains(@title, 'Remove:')]")
    protected By selectedAccessibilityLocator = By.xpath('//input[@name="accessibility" and @checked]')
    protected By saveAccessibilityButtonLocator = By.id('accessibility_save')
    protected By saveAccessibilityLogoLocator = By.xpath("//img[@src='/xnat/scripts/yui/build/assets/skins/xnat/wait.gif']")

    @Override
    By tabLocator() {
        locators.tab('Access')
    }

    @Override
    void performInitialValidation() {
        wait.until(CustomConditions.elementClickable(inviteUserButtonLocator))
        waitForNotElement(loadingIndicatorLocator)
        wait.until(CustomConditions.elementClickable(accessLevelLocator))
        wait.until(CustomConditions.elementClickable(inviteUserLocator))
        waitForElement(userTableSectionLocator.joinSublocator(XnatLocators.byPartialText('Username')))
    }

    @Override
    List<By> getComponentLocators() {
        [userTableSectionLocator]
    }

    ProjectAccessTab waitForUserTable() {
        waitForElement(userTableLoadingCompleteLocator)
        this
    }

    ProjectAccessTab addUserToProject(User user, UserGroup accessLevel, boolean sendEmail) {
        exceptionSafeClick(userListButtonLocator)
        waitForElement(inviteUserAccessGroup(user))
        exceptionSafeSelect(inviteUserAccessGroup(user), accessLevel.pluralName())
        readXnatDialog(addUserDialogLocator).uploadScreenshot()
        exceptionSafeClick(addUserButtonLocator)
        userProjectInvitationModal(user.username, sendEmail)
        waitForElement(projectAccessGroup(user))
        assertUserBelongsTo(user, accessLevel)
        capture()
    }

    ProjectAccessTab addUserByEmail(User user, UserGroup accessLevel, boolean sendEmail) {
        performInitialValidation()
        new Select(findElement(directInviteAccessLevelLocator)).selectByVisibleText(accessLevel.pluralName())
        fill(directInviteUserLocator, user.email)
        uploadTargetedScreenshot(directInviteSectionLocator)
        waitForElementVisible(directInviteButtonLocator)
        exceptionSafeClick(directInviteButtonLocator)
        waitForElement(projectAccessGroup(user)) // assuming user with this email exists and is enabled
        assertUserBelongsTo(user, accessLevel)
        capture()
    }

    ProjectAccessTab sendPAR(String email, UserGroup accessLevel) {
        fill(directInviteUserLocator, email)
        exceptionSafeSelect(directInviteAccessLevelLocator, accessLevel.pluralName())
        uploadTargetedScreenshot(directInviteSectionLocator)
        exceptionSafeClick(directInviteButtonLocator)
        readXnatDialog('An email invitation').assertTextContains("An email invitation has been sent to ${email}").capture().clickOK()
        this
    }

    ProjectAccessTab changeProjectAccessRole(User user, UserGroup accessLevel, boolean sendEmail) {
        exceptionSafeSelect(projectAccessGroup(user), accessLevel.pluralName())
        changeProjectAccessDialog(user, sendEmail)
        waitForElement(projectAccessGroup(user))
        projectPage.waitForProjectLoad()
        assertUserBelongsTo(user, accessLevel)
        captureStep(userTableSectionLocator)
        this
    }

    ProjectAccessTab removeUserFromProject(User user) {
        final By removeUserLocator = removeUserFromProjectButton(user)
        waitForElement(removeUserLocator)
        waitForAsyncCalls()
        exceptionSafeClick(removeUserLocator)
        projectPage.waitForProjectLoad()
        waitForNotElement(removeUserLocator)
        assertElementNotPresent(removeUserLocator)
        captureStep(userTableSectionLocator)
        this
    }

    ProjectAccessTab createCustomUserGroup(CustomUserGroup group) {
        exceptionSafeClick(manageGroupsButton)
        exceptionSafeClick(createCustomGroupButton)
        loadPage(CustomUserGroupEditPage).createCustomUserGroup(group)
        this
    }

    ProjectManageFeaturesPage launchManageFeatures() {
        exceptionSafeClick(manageFeaturesButton)
        loadPage(ProjectManageFeaturesPage)
    }

    UserGroup readAccessLevelFor(User user, boolean allowNonUsers = false) {
        waitForUserTable()
        final By accessLocator = projectAccessGroup(user)
        if (allowNonUsers && !isElementPresentQuick(accessLocator)) {
            null
        } else {
            final String groupText = exceptionSafeGetSelectedOption(accessLocator)
            UserGroups.STANDARD_GROUPS.find { group ->
                group.pluralName() == groupText
            } ?: new CustomUserGroup(groupText)
        }
    }

    ProjectAccessTab assertUserBelongsTo(User user, UserGroup group) {
        assertEquals(group, readAccessLevelFor(user))
        this
    }

    Accessibility readAccessibility() {
        final List<WebElement> selectedAccessibilityElements = findElements(selectedAccessibilityLocator)
        if (selectedAccessibilityElements.size() != 1) {
            throw new UnknownInterpretableValueException("There must be exactly 1 option selected for project accessibility instead of ${selectedAccessibilityElements.size()}.")
        }
        Accessibility.get(selectedAccessibilityElements[0].getAttribute('value'))
    }

    ProjectAccessTab assertAccessibility(Accessibility accessibility) {
        assertEquals(accessibility, readAccessibility())
        this
    }

    ProjectAccessTab setAccessibility(Accessibility accessibility) {
        if (readAccessibility() != accessibility) {
            final By accessibilityLocator = accessibilityLocator(accessibility)
            exceptionSafeClick(accessibilityLocator)
            exceptionSafeClick(saveAccessibilityButtonLocator)
            waitForNotElement(saveAccessibilityLogoLocator)
            exceptionSafeClick(accessibilityLocator) // set it again to be safe. This guarantees the first save finishes.
        }
        this
    }

    boolean userIsRegisteredToProject(User user) {
        readAccessLevelFor(user, true) != null
    }

    ProjectAccessTab assertUserListIsAvailable() {
        assertElementPresent(userListHeaderLocator)
        assertElementPresent(userListButtonLocator)
        this
    }

    ProjectAccessTab assertUserListUnavailable() {
        assertElementNotPresent(userListHeaderLocator)
        assertElementNotPresent(userListButtonLocator)
        this
    }

    protected ProjectAccessTab changeProjectAccessDialog(User user, boolean sendEmail) {
        readXnatDialog('updated the user role').assertTextContains("You have updated the user role for ${user.username}")
        exceptionSafeClick(locators.tagWithText('button', sendEmail ? 'Send Email' : 'No'))
        this
    }

    protected void userProjectInvitationModal(String identifier, boolean sendEmail) {
        readXnatDialog('confirmation emails').uploadScreenshot().assertTextEquals(getUserInviteMessage(identifier))
        exceptionSafeClick(sendEmail ? addUserWithEmailButtonLocator : addUserWithoutEmailButtonLocator)
    }

    protected String getUserInviteMessage(String identifier) {
        'You have added 1 users to this project. Do you want to send confirmation emails to each user?'
    }

    protected By inviteUserAccessGroup(User user) {
        By.xpath(".//select[@title='${user.username}']")
    }

    protected By projectAccessGroup(User user) {
        By.xpath("//select[@title='${user.username}']")
    }

    protected By removeUserFromProjectButton(User user) {
        By.xpath(".//a[contains(@title, 'Remove: ${user.username}|')]")
    }

    protected By accessibilityLocator(Accessibility accessibility) {
        By.xpath("//input[@name='accessibility' and @value='${accessibility}']")
    }

}
