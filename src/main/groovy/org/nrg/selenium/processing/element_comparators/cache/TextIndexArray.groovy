package org.nrg.selenium.processing.element_comparators.cache

import groovy.transform.EqualsAndHashCode
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver

@EqualsAndHashCode(callSuper = true)
class TextIndexArray extends LocatorCache {

    int offset = 0

    @Override
    void addToCache(WebDriver driver, Map<String, String> currentCache) {
        driver.findElements(By.xpath(locator)).eachWithIndex { element, index ->
            currentCache.put("\$${id}(${element.text})".toString(), String.valueOf(index + offset))
        }
    }

}
