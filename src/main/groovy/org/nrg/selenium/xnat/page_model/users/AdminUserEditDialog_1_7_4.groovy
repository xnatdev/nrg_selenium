package org.nrg.selenium.xnat.page_model.users

import groovy.util.logging.Log4j
import org.nrg.xnat.versions.*
import org.openqa.selenium.By

@Log4j
class AdminUserEditDialog_1_7_4 extends AdminUserEditDialog {

    AdminUserEditDialog_1_7_4() {
        dialogLocator = By.xpath("//div[@title='Create New User']")
    }

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_7_2, Xnat_1_7_3, Xnat_1_7_4]
    }

    @Override
    AdminUserEditDialog fillPasswordCheck(String s) {
        log.info('Prior to XNAT 1.7.5, creating a new user as an admin did not offer the password check field. Skipping this input...')
        this
    }

    @Override
    By passwordCheckLocator() {
        null
    }

    @Override
    void performInitialValidation() {
        editDialog = readXModal(dialogLocator)
    }

}
