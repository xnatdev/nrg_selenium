package org.nrg.selenium.xnat.page_model.ui_element.popup

import org.nrg.selenium.extension.XpathLocator

class FormValidationModal extends XModal {

    protected String errorIndicator = 'Errors found:'
    protected XpathLocator modalLocator = new XpathLocator("//div[@title='Form Validation Error']")

    FormValidationModal locate() {
        locatedBy(modalLocator) as FormValidationModal
    }

    FormValidationModal assertErrorsEqual(List<String> expectedErrors) {
        assertTextEquals(([errorIndicator] + expectedErrors).join('\n')) as FormValidationModal
    }

    XpathLocator getModalLocator() {
        modalLocator
    }

}
