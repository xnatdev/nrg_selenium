package org.nrg.selenium.xnat.page_model.prearchive

import org.nrg.selenium.enums.PrearchiveStatus
import org.nrg.xnat.pogo.Project

class PrearchiveSearchCriteria {

    Integer expectedNumber
    PrearchiveStatus status
    List<Project> projects
    int maxWaitTime = 120
    int samplingTime = 10

}
