package org.nrg.selenium.xnat.page_model.ui_element

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.XnatPageComponent

class GenericHtmlTable implements HtmlTable<GenericHtmlTable>, XnatPageComponent {

    XpathLocator locator

    @Override
    XpathLocator getTableLocator() {
        locator
    }

    GenericHtmlTable withLocator(XpathLocator xpathLocator) {
        locator = xpathLocator
        this
    }

}
