package org.nrg.selenium.xnat.page_model.admin

import org.openqa.selenium.By

class AdminUiScreenshot implements AdminUiElement {

    AdminUiScreenshot(By by) {
        setLocator(by)
    }

    @Override
    void performElementAction() {}

}
