package org.nrg.selenium.xnat.page_model.automation

import org.nrg.selenium.xnat.page_model.PageObjectSupport
import org.nrg.selenium.xnat.page_model.XnatPageComponent

trait AutomationBase implements PageObjectSupport {

    AutomationPage automationPage

    def <X extends AutomationTab> X loadTab(Class<X> tab) {
        driverExtension.exceptionSafeClick(getInstance(tab).tabLocator())
        final X tabObject = loadPage(tab, false)
        if (automationPage != null) {
            tabObject.setAutomationPage(automationPage)
        } else {
            tabObject.setAutomationPage(this as AutomationPage)
        }
        tabObject
    }

    AutomationPage getAutomationPage() {
        if (automationPage == null) {
            automationPage = (this as XnatPageComponent).loadPage(AutomationPage, false, true)
        }
        automationPage
    }

    ScriptsTab loadScriptsTab() {
        loadTab(ScriptsTab)
    }

    SiteEventHandlersTab loadSiteEventHandlersTab() {
        loadTab(SiteEventHandlersTab)
    }

    SiteEventsTab loadSiteEventsTab() {
        loadTab(SiteEventsTab)
    }

}