package org.nrg.selenium.enums

enum DataUnit {

    BYTES ('B', 'bytes') {
        @Override
        String formatExpectedFileManagerValue(double value) {
            Math.round(value) as String
        }
    },
    KILOBYTES ('KB', 'KB') {
        @Override
        String formatExpectedFileManagerValue(double value) {
            Math.round(value) as String
        }
    },
    MEGABYTES ('MB', 'MB'),
    GIGABYTES ('GB', 'GB')

    String sessionPageRepresentation
    String manageFilesRepresentation

    DataUnit(String sessionPageRepresentation, String manageFilesRepresentation) {
        setSessionPageRepresentation(sessionPageRepresentation)
        setManageFilesRepresentation(manageFilesRepresentation)
    }

    String formatExpectedFileManagerValue(double value) {
        value as String
    }

}