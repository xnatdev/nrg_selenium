package org.nrg.selenium.processing.element_comparators

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode
class Element {

    Locator locator
    String value = ''

}
