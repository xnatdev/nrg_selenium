package org.nrg.selenium.xnat.page_model.defaults

import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.testing.email.MessageTemplate

class XnatDefaults implements XnatPageComponent {

    int defaultMaxFailedLogins = 20
    String standardExpirationInterval = '2 years'
    String standardExpirationDate = '2010-10-20'
    String defaultPasswordComplexity = '^.*$'
    String defaultPasswordComplexityWarning = 'Password is not sufficiently complex.'
    String defaultSessionTimeout = '15 minutes'
    String defaultAliasTokenInterval = '2 days'
    String defaultAliasTokenSchedule = '0 0 * * * *'
    int defaultMaxSessions = 1000
    String defaultInactivityLockout = '1 year'
    String defaultInactivitySchedule = '0 0 1 * * ?'

    String getDefaultLoginFailureMessage() {
        new MessageTemplate('login_failure_message.txt').replace('%MAX_FAILED_LOGINS%', defaultMaxFailedLogins).read()
    }

}
