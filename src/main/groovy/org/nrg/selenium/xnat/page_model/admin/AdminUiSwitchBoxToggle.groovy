package org.nrg.selenium.xnat.page_model.admin

import org.openqa.selenium.By

class AdminUiSwitchBoxToggle extends AdminUiSwitchBox {

    AdminUiSwitchBoxToggle(By by, By screenshot = null, boolean takeScreenshot = true) {
        super(by, true, screenshot, takeScreenshot)
    }

    @Override
    void performElementAction() {
        toggleSwitchBox(locator)
    }

}
