package org.nrg.selenium;

import org.nrg.selenium.xnat.page_model.IndexPage;
import org.nrg.selenium.xnat.page_model.custom_variable.CustomVariableReportSection;
import org.nrg.selenium.xnat.page_model.project.edit.ProjectEditPage;
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportBase;
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage;
import org.nrg.selenium.xnat.page_model.upload.*;
import org.testng.annotations.Test;

/**
 * For reasons I don't quite understand at this time, I've run into issues so far where both IntelliJ and even the compiler when building the jar from groovy sources
 * report no issues, but the produced jar is invalid from the generics not being quite right. It shows up when attempting to reference a class which has this type
 * of generics issue somewhere in its inheritance structure, but *only in Java*. This class simply references several of the page classes from Java to detect this
 * at build time if it comes back.
 */
public class TestJavaCompilation {

    @Test
    public void testReferences() {
        new IndexPage();
        new ProjectReportPage();
        new ProjectEditPage();
        new SpreadsheetUploadLandingPage();
        new SpreadsheetUploadStep2Page();
        new SpreadsheetUploadStep3Page();
        new SpreadsheetUploadPreview();
        new SpreadsheetUploadReviewPage();
        new SpreadsheetUploadResultPage();
        new CustomVariableReportSection();
    }

    /*@Test
    public void testTempCompilation() {
        final ProjectReportPage reportPage = new ProjectEditPage().create(null);
    }*/

}
