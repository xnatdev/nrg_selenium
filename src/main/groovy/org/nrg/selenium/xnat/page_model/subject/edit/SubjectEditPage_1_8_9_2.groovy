package org.nrg.selenium.xnat.page_model.subject.edit

import org.nrg.xnat.versions.XnatVersion
import org.nrg.xnat.versions.XnatVersionList
import org.nrg.xnat.versions.Xnat_1_8_10

class SubjectEditPage_1_8_9_2 extends SubjectEditPage {

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        XnatVersionList.knownVersionsBefore(Xnat_1_8_10)
    }

    @Override
    String changeLabelMessage() {
        "Modifying the label of a Subject will result in the moving of files on the file server within the project's storage space.  Are you sure you want to make this change?"
    }

}
