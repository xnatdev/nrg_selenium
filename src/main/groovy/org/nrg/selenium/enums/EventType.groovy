package org.nrg.selenium.enums

enum EventType {
    WORKFLOW_STATUS ('WorkflowStatusEvent'),
    SCRIPT_LAUNCH_REQUEST ('ScriptLaunchRequestEvent')

    private final String displayName

    EventType(String displayName) {
        this.displayName = displayName
    }

    String getDisplayName() {
        displayName
    }
}
