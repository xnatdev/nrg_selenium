package org.nrg.selenium.xnat.page_model

import org.nrg.xnat.versions.*

class NavigableDelegate_1_6 extends NavigableDelegate {

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev]
    }

    @Override
    void loadApplet() {
        topNav('Upload', 'Images')
    }

}
