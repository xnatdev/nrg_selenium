package org.nrg.selenium.xnat.page_model

import org.apache.log4j.Logger
import org.nrg.selenium.CustomConditions
import org.nrg.selenium.enums.AuditTrailScope
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.admin.AdminUiPage
import org.nrg.selenium.xnat.page_model.admin.LegacyAdminOptionsPage
import org.nrg.selenium.xnat.page_model.admin.ScpReceiverManagementPage
import org.nrg.selenium.xnat.page_model.automation.AutomationPage
import org.nrg.selenium.xnat.page_model.prearchive.XnatPrearchive
import org.nrg.selenium.xnat.page_model.project.edit.ProjectEditPage
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage
import org.nrg.selenium.xnat.page_model.session.edit.ImagingSessionEditPage
import org.nrg.selenium.xnat.page_model.session.report.ImagingSessionReportPage
import org.nrg.selenium.xnat.page_model.site_search.AdvancedSearchPage
import org.nrg.selenium.xnat.page_model.site_search.DataTypeListing
import org.nrg.selenium.xnat.page_model.site_search.StoredSearchResult
import org.nrg.selenium.xnat.page_model.subject.edit.SubjectEditPage
import org.nrg.selenium.xnat.page_model.subject.report.SubjectReportPage
import org.nrg.selenium.xnat.page_model.ui_element.activity.ActivityTab
import org.nrg.selenium.xnat.page_model.ui_element.TopNav
import org.nrg.selenium.xnat.page_model.upload.CompressedUploader
import org.nrg.selenium.xnat.page_model.upload.SpreadsheetUploadLandingPage
import org.nrg.selenium.xnat.page_model.upload.XmlUploadPage
import org.nrg.selenium.xnat.page_model.users.AdminUsersPage
import org.nrg.selenium.xnat.page_model.users.UserSelfServicePage
import org.nrg.testing.TimeUtils
import org.nrg.xnat.pogo.DataType
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.users.User
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertEquals
import static org.testng.AssertJUnit.assertTrue

trait NavigablePageObject<X extends NavigablePageObject<X>> implements XnatPageComponent, SiteAlertContainer {

    By searchBoxLocator = By.id('searchValue')
    By searchButtonLocator = By.id('search_btn')
    By logoutLinkLocator = By.id('logout_user')
    By systemLogoLocator = By.cssSelector('img.logo_img')
    XpathLocator userInfoLocator = new XpathLocator("//*[@id='user_info']")
    By timeLeftLocator = By.id('timeLeft')
    XpathLocator loggedInUserSublocator = new XpathLocator("./*[@style='color:red;' or contains(@href, 'MyXNAT') or contains(@href, 'UpdateUser')]")
    static final Logger logger = Logger.getLogger(NavigablePageObject)

    X waitForSessionRebuilder(Integer waitInMinutes = 5) {
        logger.info("Time to wait ${waitInMinutes} minutes for XNAT's session rebuilder...")
        waitInMinutes.times {
            TimeUtils.sleep(60000)
            logger.info("Waited ${it + 1} minute${it > 0 ? 's' : ''} so far...")
        }
        captureScreenshotlessStep()
        this as X
    }

    X assertLoginSuccessful() {
        captureStep(systemLogoLocator, userInfoLocator) // wait for login to finish
        this as X
    }

    X assertLogin(User user, boolean takeScreenshot = false) {
        final String username = (user == null || user == User.GUEST) ? 'Guest' : user.username
        assertEquals("Logged in as: ${username}", normalizeText(exceptionSafeGetText(userInfoLocator).split('\\|')[0]))
        if (takeScreenshot) {
            captureStep([systemLogoLocator, userInfoLocator])
        } else {
            captureScreenshotlessStep()
        }
        this as X
    }

    String readCurrentLoggedInUsername() {
        exceptionSafeGetText(loggedInUserLocator())
    }

    String readSessionTimer() {
        wait.until(CustomConditions.elementTextMatchesText(timeLeftLocator, '\\d+:\\d+:\\d+'))
        exceptionSafeGetText(timeLeftLocator)
    }

    X assertSessionTimer(int currentTotalSeconds, int comparisonTolerance) {
        final String[] timeComponents = readSessionTimer().split(':')
        final int currentTimeLeft = 3600 * Integer.parseInt(timeComponents[0]) + 60 * Integer.parseInt(timeComponents[1]) + Integer.parseInt(timeComponents[2])
        assertTrue(Math.abs(currentTimeLeft - currentTotalSeconds) < comparisonTolerance)
        captureStep(userInfoLocator)
        this as X
    }

    void topNav(String... options) {
        readTopNav().performNavigation(options)
    }

    def <X extends NavigablePageObject> X searchFor(String entity, Class<X> resultClass) {
        waitForDocumentReadyState()
        fill(searchBoxLocator, entity)
        if (entity != null && entity != exceptionSafeGetText(searchBoxLocator)) {
            fill(searchBoxLocator, entity)
        }
        uploadTargetedScreenshot(searchBoxLocator, searchButtonLocator)
        exceptionSafeClick(searchButtonLocator)
        loadPage(resultClass)
    }

    ActivityTab readActivityTab(boolean aggressiveTimeoutBehavior = false) {
        if (aggressiveTimeoutBehavior) {
            setTimeout(0)
        }
        final ActivityTab activityTab = loadPage(ActivityTab)
        if (aggressiveTimeoutBehavior) {
            restoreTimeout()
        }
        activityTab
    }

    TopNav readTopNav() {
        loadPage(TopNav)
    }

    ProjectReportPage createProject(Project project) {
        loadPage(ProjectEditPage).create(project)
    }

    SubjectReportPage createSubject(Subject subject) {
        loadPage(SubjectEditPage).create(subject)
    }

    ImagingSessionReportPage createSession(ImagingSession session) {
        loadPage(ImagingSessionEditPage).create(session)
    }

    ProjectReportPage searchForProject(String projectId) {
        searchFor(projectId, ProjectReportPage)
    }

    ProjectReportPage searchForProject(Project project) {
        searchForProject(project.id)
    }

    SubjectReportPage searchForSubject(String subject) {
        searchFor(subject, SubjectReportPage)
    }

    ImagingSessionReportPage searchForSession(String sessionIdentifier) {
        searchFor(sessionIdentifier, ImagingSessionReportPage)
    }

    IndexPage verifyNoSearchResultsFound(String searchString) {
        searchFor(searchString, IndexPage).assertNoMatchingItems()
    }

    XmlUploadPage loadXmlUploadPage() {
        loadPage(XmlUploadPage)
    }

    XnatPrearchive loadPrearchive() {
        topNav('Upload', 'Go to prearchive')
        loadPage(XnatPrearchive)
    }

    void loadApplet() {
        loadPage(NavigableDelegate).loadApplet()
    }

    ProjectEditPage loadProjectCreationPage() {
        loadPage(ProjectEditPage)
    }

    SubjectEditPage loadSubjectCreationPage() {
        loadPage(SubjectEditPage)
    }

    ImagingSessionEditPage loadSessionCreationPage() {
        loadPage(ImagingSessionEditPage)
    }

    CompressedUploader loadCompressedUploader(boolean takeAllScreenshots = false) {
        loadPage(CompressedUploader, takeAllScreenshots)
    }

    SpreadsheetUploadLandingPage loadSpreadsheetUploader() {
        loadPage(SpreadsheetUploadLandingPage)
    }

    AdminUsersPage loadAdminUsersPage() {
        loadPage(AdminUsersPage)
    }

    AdminUiPage loadAdminUi() {
        loadPage(AdminUiPage)
    }

    AuditTrailPage loadSiteAuditTrail(AuditTrailScope scope) {
        topNav('Administer', 'Audit Trail', scope.uiText)
        loadPage(AuditTrailPage)
    }

    AutomationPage loadAutomationPage() {
        loadPage(AutomationPage)
    }

    LegacyAdminOptionsPage loadLegacyAdminPage() {
        loadPage(LegacyAdminOptionsPage)
    }

    ScpReceiverManagementPage loadScpReceiverManagementPage() {
        loadPage(ScpReceiverManagementPage)
    }

    UserSelfServicePage loadUserSelfServicePage(User user) {
        exceptionSafeClick(loggedInUserLocator())
        final UserSelfServicePage userPage = loadPage(UserSelfServicePage)
        userPage.setCurrentUser(user)
        userPage
    }

    DataTypeListing loadDataTypeListing(DataType dataType) {
        topNav('Browse', 'Data', dataType.pluralName)
        loadPage(DataTypeListing)
    }

    StoredSearchResult loadStoredSearch(String search) {
        topNav('Browse', 'Stored Searches', search)
        loadPage(StoredSearchResult)
    }

    AdvancedSearchPage loadAdvancedSearchPage() {
        loadPage(AdvancedSearchPage)
    }

    XnatLogoutPage logout() {
        exceptionSafeClick(logoutLinkLocator)
        loadPage(XnatLogoutPage)
    }

    XnatLogoutPage logLogout() {
        final XnatLogoutPage logout = logout()
        captureScreenshotlessStep() // Don't need a screenshot of the login page
        logout
    }

    // TODO: handle case where guest access is allowed
    XnatLoginPage assertSessionTimeout() {
        readXnatDialog('User session ended').uploadScreenshot()
        loadPage(XnatLogoutPage).asLoginPage().assertTimeoutMessage() // TODO: the dialog goes away on its own, but this is dicey for low timeout values, or when/if I strip out implicit timeouts
    }

    By loggedInUserLocator() {
        userInfoLocator.joinSublocator(loggedInUserSublocator)
    }

}
