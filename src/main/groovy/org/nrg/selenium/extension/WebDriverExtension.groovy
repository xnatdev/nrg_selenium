package org.nrg.selenium.extension

import groovy.util.logging.Log4j
import org.apache.commons.lang3.time.StopWatch
import org.nrg.selenium.CustomConditions
import org.nrg.selenium.exceptions.EmptyTextException
import org.nrg.testing.TimeUtils
import org.openqa.selenium.Alert
import org.openqa.selenium.By
import org.openqa.selenium.ElementNotInteractableException
import org.openqa.selenium.InvalidElementStateException
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.NoSuchWindowException
import org.openqa.selenium.UnhandledAlertException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebDriverException
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.Select
import org.openqa.selenium.support.ui.WebDriverWait
import org.scijava.util.PlatformUtils

import java.time.Duration

import static org.testng.AssertJUnit.*

@Log4j
@SuppressWarnings('unused')
class WebDriverExtension {

    @Delegate WebDriver driver
    WebDriverWait wait
    WebDriverWait oneSec
    String mainWindow
    Set<String> windows
    private final int defaultTimeout

    WebDriverExtension(int timeout, WebDriver webDriver) {
        defaultTimeout = timeout
        setDriver(webDriver)
    }

    void setDriver(WebDriver webDriver) {
        driver = webDriver
        wait = new WebDriverWait(webDriver, Duration.ofSeconds(defaultTimeout), Duration.ofMillis(100))
        oneSec = new WebDriverWait(driver, Duration.ofSeconds(1))
    }

    final void setTimeout(int timeout) {
        if (driver != null) {
            try {
                manage().timeouts().implicitlyWait(Duration.ofSeconds(timeout))
            } catch (UnhandledAlertException | NoSuchWindowException ignored) {}
        }
    }

    final void restoreTimeout() {
        setTimeout(defaultTimeout)
    }

    final String normalizeText(String rawText) {
        rawText.replaceAll('\\s+', ' ').replaceAll('\\u00AD', '').trim() // trim out extra spaces and weird soft hyphens
    }

    final String readNormalizedText(By locator) {
        normalizeText(exceptionSafeGetText(locator))
    }

    final boolean isElementVisible(By by) {
        try {
            findElement(by).isDisplayed()
        } catch (NoSuchElementException ignored) {
            false
        }
    }

    final boolean isElementPresent(By by) {
        findElements(by).size() > 0
    }

    final boolean isElementPresentQuick(By by) {
        setTimeout(0)
        final boolean isPresent = isElementPresent(by)
        restoreTimeout()
        isPresent
    }

    final boolean isSubelementPresent(By by, WebElement parent) {
        try {
            parent.findElement(by)
            true
        } catch (NoSuchElementException ignored) {
            false
        }
    }

    final void waitForElement(By by, String failureMessage, int timeout) {
        final WebDriverWait waiter = new WebDriverWait(driver, Duration.ofSeconds(timeout))
        waiter.withMessage(failureMessage ?: "Could not find element located by ${by}")
        waiter.until(ExpectedConditions.presenceOfElementLocated(by))
    }

    final void waitForElement(By by, String failureMessage) {
        waitForElement(by, failureMessage, defaultTimeout)
    }

    final void waitForElement(By by) {
        waitForElement(by, null)
    }

    final void waitForElements(By by, int numElements, int timeout) {
        final WebDriverWait waiter = new WebDriverWait(driver, Duration.ofSeconds(timeout))
        waiter.withMessage("Could not find elements located by the following in time: ${by}")
        waiter.until(CustomConditions.presenceOfElementsLocated(by, numElements, defaultTimeout))
    }

    final void waitForElements(By by, int numElements) {
        waitForElements(by, numElements, defaultTimeout)
    }

    final void waitForElementWithRefresh(By by, int timeout) {
        waitForElements(by, 1, timeout)
    }

    final void waitForElementWithRefresh(By by) {
        waitForElementWithRefresh(by, defaultTimeout)
    }

    final void waitForNotElement(By by) {
        final WebDriverWait waiter = new WebDriverWait(driver, Duration.ofSeconds(defaultTimeout))
        setTimeout(0) // If we don't do this, the condition checks for the full timeout length before agreeing that the element isn't there
        waiter.until(CustomConditions.elementNotPresent(by))
        restoreTimeout()
    }

    final void waitForElementVisible(By by, int timeout) {
        final WebDriverWait waiter = new WebDriverWait(driver, Duration.ofSeconds(timeout))
        waiter.withMessage("Could not locate a visible element located by ${by}")
        waiter.until(CustomConditions.elementIsDisplayed(by))
    }

    final void waitForElementVisible(By by) {
        waitForElementVisible(by, defaultTimeout)
    }

    final void waitForElementNotVisible(By by) {
        wait.until(ExpectedConditions.not(CustomConditions.elementIsDisplayed(by)))
    }

    final void waitForAsyncCalls() {
        wait.until(CustomConditions.asyncDone())
    }

    final void clickUntilOtherElementVisible(By clicked, By visibilityCheck, int checkTime) {
        wait.until(CustomConditions.clickUntilOtherElementVisible(clicked, visibilityCheck, checkTime))
    }

    final void clickUntilAttributeEquals(By locator, String attribute, String attributeValue) {
        wait.until(CustomConditions.clickUntilAttributeEquals(locator, attribute, attributeValue))
    }

    final void assertElementNotPresent(By by) {
        setTimeout(0)
        assertFalse(isElementPresent(by))
        restoreTimeout()
    }

    final void assertElementPresent(By by) {
        assertTrue("Could not find element: ${by}", isElementPresent(by))
    }

    final void assertElementText(String expectedText, By elementLocator) {
        assertEquals(expectedText ?: '', exceptionSafeGetText(elementLocator) ?: '')
    }

    final void assertNormalizedText(String expectedText, By elementLocator) {
        assertEquals(expectedText, readNormalizedText(elementLocator))
    }

    final void assertElementTextContains(String expectedContainedText, By elementLocator) {
        assertTrue(exceptionSafeGetText(elementLocator).contains(expectedContainedText))
    }

    final void specifyFile(By by, File file) {
        if (file != null) {
            exceptionSafeFind(by).sendKeys(file.absolutePath)
        }
    }

    final void fill(By by, String textToFill, boolean disableClickCheck) {
        if (textToFill != null) {
            if (!disableClickCheck) {
                wait.until(CustomConditions.elementClickable(by))
            }
            exceptionSafeClear(by)
            exceptionSafeFind(by).sendKeys(textToFill)
        }
    }

    final void fill(By by, String textToFill) {
        fill(by, textToFill, false)
    }

    final void fill(By by, Integer textToFill) {
        fill(by, textToFill?.toString())
    }

    final void fill(WebElement element, String textToFill) {
        if (textToFill != null) {
            element.clear()
            element.sendKeys(textToFill)
        }
    }

    final void fillNonzero(By by, int textToFill) {
        if (textToFill != 0) {
            fill(by, textToFill)
        }
    }

    final boolean selectContainsOption(By locator, String option) {
        option in new Select(findElement(locator)).options.text
    }

    final <X> X genericPersistentAction(GenericPersistentAction<X> actionDefinition) {
        final StopWatch stopWatch = TimeUtils.launchStopWatch()
        if (!actionDefinition.skipTimeoutSetting) {
            setTimeout(0)
        }

        while (true) {
            if (TimeUtils.maxTimeReached(stopWatch, defaultTimeout)) {
                if (!actionDefinition.skipTimeoutSetting) {
                    setTimeout(defaultTimeout)
                }
                final String exceptionString = actionDefinition.exceptions.collect { exception, count ->
                    "${count} ${exception}${count == 1 ? '' : 's'}"
                }.join('\n')
                fail("${actionDefinition.failureString}. The following exceptions were encountered after ${stopWatch.time / 1000} seconds of search:\n${exceptionString}")
            }
            try {
                final X actionResult = actionDefinition.actionClosure()
                if (actionResult || actionDefinition.allowFalseyReturn) {
                    if (!actionDefinition.skipTimeoutSetting) {
                        restoreTimeout()
                    }
                    return actionResult
                }
            } catch (Exception e) {
                if (formatException(actionDefinition.exceptions, e) && actionDefinition.voidResult) {
                    return null
                }
            }
        }
    }

    // Fills until webElement.getAttribute('value') == textToFill
    final void persistentFill(By by, String textToFill) {
        if (textToFill != null) {
            genericPersistentAction(new GenericPersistentAction<>().failureString("Searching for an element given by ${by}, filling it, and checking its value failed even with exception-safe searching").action({
                fill(by, textToFill)
                if (textToFill == findElement(by).getAttribute('value')) {
                    return true
                } else {
                    throw new InvalidElementStateException('Wrong text')
                }
            }))
        }
    }

    final WebElement exceptionSafeFind(By by) {
        genericPersistentAction(new GenericPersistentAction<>().failureString("Searching for an element given by ${by} failed even with exception-safe searching").action({ findElement(by) })) as WebElement
    }

    final String getNonemptyText(By by) {
        genericPersistentAction(new GenericPersistentAction<>().failureString("Searching for an element given by ${by} failed even with exception-safe searching").action({
            final String elementText = findElement(by).text
            if ('' == elementText) {
                throw new EmptyTextException()
            } else {
                return elementText
            }
        }))
    }

    final String exceptionSafeGetText(By by, boolean allowEmpty = true) {
        genericPersistentAction(
                new GenericPersistentAction<>().
                        failureString("Searching for an element given by ${by} failed even with exception-safe searching").
                        allowFalseyReturn(allowEmpty).
                        action({ findElement(by).text })
        )
    }

    final List<String> exceptionSafeGetTextList(By by) {
        genericPersistentAction(
                new GenericPersistentAction<>().
                        failureString("Searching for elements given by ${by} and extracting text failed even with exception-safe searching").
                        allowFalseyReturn(true).
                        action({ findElements(by)*.text })
        ) as List<String>
    }

    final void exceptionSafeClick(By by) {
        genericPersistentAction(new GenericPersistentAction<>().failureString("Searching for an element given by ${by} and clicking it failed even with exception-safe searching").voidAction({
            if (isElementPresent(by)) {
                oneSec.until(CustomConditions.elementClickable(by))
                setTimeout(0) // previous line resets timeout
                findElement(by).click() // If we didn't hit an exception by now, we're golden
                return true
            } else {
                throw new NoSuchElementException("No element: ${by}")
            }
        }))
    }

    final void exceptionSafeClick(WebElement element) {
        genericPersistentAction(new GenericPersistentAction<>().failureString("Searching for an element ${element} and clicking it failed even with exception-safe searching").voidAction({
            if (element.isDisplayed()) {
                element.click()
                return true
            } else {
                throw new ElementNotInteractableException('Element not displayed or interactable')
            }
        }))
    }

    final void exceptionSafeSelect(By select, String selection) {
        if (selection == null) {
            return
        }
        setTimeout(0)
        final int enabledTimeout = defaultTimeout.intdiv(3).intValue()
        final WebDriverWait waiter = new WebDriverWait(driver, Duration.ofSeconds(enabledTimeout))
        waiter.withMessage("Could not find an enabled <select> given by ${select} within ${enabledTimeout} seconds.")
        waiter.until(CustomConditions.elementIsEnabled(select))
        genericPersistentAction(new GenericPersistentAction<>().failureString("Searching for a <select> given by ${select} and selecting an option in it failed even with exception-safe searching").action({
            new Select(findElement(select)).selectByVisibleText(selection) // If we didn't hit an exception by now, we're golden
            true
        }))
    }

    final String exceptionSafeGetSelectedOption(By select) {
        genericPersistentAction(new GenericPersistentAction<>().failureString("Finding the selected option for a <select> given by ${select} failed even with exception-safe searching").action({
            final String option = new Select(findElement(select)).firstSelectedOption.text
            if (option == '') {
                throw new WebDriverException('Unexpected value')
            } else {
                return option
            }
        }))
    }

    final String exceptionSafeGetAttribute(By locator, String attribute) {
        genericPersistentAction(new GenericPersistentAction<>().failureString("Searching for an element given by ${locator} and locating its '${attribute}' attribute failed even with exception-safe searching").
                allowFalseyReturn(true).action({ findElement(locator).getAttribute(attribute) }))
    }

    final void exceptionSafeClear(By element) {
        genericPersistentAction(new GenericPersistentAction<>().failureString("Finding the element given by ${element} and clearing it failed even with exception-safe searching").action({
            findElement(element).clear()
            true
        }))
    }

    final boolean exceptionSafeQueryCheckbox(By box) {
        waitForElement(box)
        waitForElementVisible(box)
        genericPersistentAction(new GenericPersistentAction<>().failureString("Finding the checkbox ${box} and seeing if it's checked failed even with exception-safe searching").allowFalseyReturn(true).action({ findElement(box).isSelected() }))
    }

    final String closeAlertAndGetItsText(boolean acceptAlert) {
        genericPersistentAction(new GenericPersistentAction<>().failureString('Could not find alert in time.').allowFalseyReturn(true).skipTimeoutSetting(true).action({
            final Alert alert = switchTo().alert()
            final String alertText = alert.text
            if (acceptAlert) {
                alert.accept()
            } else {
                alert.dismiss()
            }
            alertText
        }))
    }

    final String closeAlertAndGetItsText() {
        closeAlertAndGetItsText(true)
    }

    final void multipleLocatorClick(By... bys) {
        // should not be rolled into multipleLocatorFind due to elements being returned who aren't visible, etc.
        // mainWindow may be switched to at the end
        final Map<String, Integer> exceptions = [:]
        genericPersistentAction(new GenericPersistentAction<>().failureString("Searching for and clicking an element identified by the following various locators failed:\n${bys.join('\n')}").exceptions(exceptions).action({
            bys.any { by ->
                try {
                    findElement(by).click()
                    return true
                } catch (NoSuchWindowException ignored) {
                    switchTo().window(mainWindow)
                    return true
                } catch (Exception e) {
                    return formatException(exceptions, e)
                }
            }
        }))
    }

    final void switchToNewWindow() {
        genericPersistentAction(new GenericPersistentAction<>().failureString('Could not find a new window in time.').action({
            final String handle = getWindowHandles().find { handle ->
                !(handle in windows)
            }
            if (handle != null) {
                switchTo().window(handle)
            }
            handle
        }))
    }

    // Clicks the clicked element and waits for the waited element to be visible. If not visible within waitPeriod, clicks clicked again and waits
    // Somewhat nuclear option to try clicking, but needed since XNAT/Selenium are being flaky with each other
    final void retryClickIfNotVisible(By clicked, By waited, int waitPeriod) {
        exceptionSafeClick(clicked)
        try {
            waitForElementVisible(waited, waitPeriod)
        } catch (Exception ignored) {
            exceptionSafeClick(clicked)
            waitForElementVisible(waited)
        }
    }

    private boolean formatException(Map<String, Integer> exceptions, Exception e) {
        final String exceptionName = e.class.canonicalName
        final boolean isTimeoutException = exceptionName.contains('TimeoutException')
        if (isTimeoutException) {
            log.warn('Encountered a TimeoutException...', e)
            log.warn('We encountered a TimeoutException. This seems to happen even after the specified action has been taken successfully. We will move on and assume that happened.')
        }
        exceptions.put(exceptionName, 1 + (exceptions[exceptionName] ?: 0))
        isTimeoutException
    }

    final void clickAll(List<By> bys) {
        bys.each { by ->
            exceptionSafeClick(by)
        }
    }

    final void clickAllResults(By by) {
        findElements(by).each { element ->
            exceptionSafeClick(element)
        }
    }

    final String getValue(By locator) {
        exceptionSafeGetAttribute(locator, 'value')
    }

    final void forceClear(By by) {
        // Useful for clearing text from tricky <textarea> elements that don't store the text in the element
        final WebElement areaToClear = findElement(by)
        areaToClear.sendKeys((PlatformUtils.isMac() ? Keys.COMMAND : Keys.CONTROL) + 'a')
        areaToClear.sendKeys(Keys.DELETE)
    }

    final void mouseover(By element) {
        scrollToElement(element)
        final Actions mouseoverAction = new Actions(driver)
        mouseoverAction.moveToElement(findElement(element)).build().perform()
        TimeUtils.sleep(100) // I'm fine with this use of sleep(). It's 1/10th of a second, and allows the page to pop up content
    }

    final void mouseoverCombo(List<By> locators) {
        // Mouses over each locator in locators successively, finally clicking on the last one
        locators.eachWithIndex{ by, index ->
            if (index < locators.size() - 1) {
                mouseover(by)
                waitForElementVisible(locators[index + 1])
            } else {
                exceptionSafeClick(by)
            }
        }
    }

    final void scrollToElement(By locator) {
        (driver as JavascriptExecutor).executeScript('arguments[0].scrollIntoView(true);', exceptionSafeFind(locator))
    }

    final void noteWindows() {
        mainWindow = getWindowHandle()
        windows = getWindowHandles()
    }

    final void returnToMainWindow() {
        switchTo().window(mainWindow)
    }

}
