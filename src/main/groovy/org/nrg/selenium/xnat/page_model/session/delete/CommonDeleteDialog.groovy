package org.nrg.selenium.xnat.page_model.session.delete

import org.nrg.selenium.xnat.page_model.XnatDeleteComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage
import org.openqa.selenium.By

class CommonDeleteDialog implements XnatPageComponent, XnatDeleteComponent {

    protected By deleteButtonLocator = XnatLocators.DELETE_BUTTON

    @Override
    ProjectReportPage delete(int waitTolerance, boolean takeAllScreenshots) {
        if (takeAllScreenshots) {
            captureStep(deletionDialogLocator)
        } else {
            waitForElement(deletionDialogLocator)
        }
        exceptionSafeClick(deleteButtonLocator)
        setTimeout(waitTolerance)
        handleSuccessDialog()
        restoreTimeout()
        loadPage(ProjectReportPage)
    }

    protected void handleSuccessDialog() {
        readXnatDialog('successfully deleted').assertTextEquals('All items were successfully deleted.').capture().clickOK()
    }

}
