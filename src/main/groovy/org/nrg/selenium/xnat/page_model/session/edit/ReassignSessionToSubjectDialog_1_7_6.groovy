package org.nrg.selenium.xnat.page_model.session.edit

import org.nrg.xnat.versions.*

import static org.testng.AssertJUnit.assertTrue

class ReassignSessionToSubjectDialog_1_7_6 extends ReassignSessionToSubjectDialog {

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev, Xnat_1_7_2, Xnat_1_7_3, Xnat_1_7_4, Xnat_1_7_5, Xnat_1_7_5_2, Xnat_1_7_6]
    }

    @Override
    protected void confirmReassign() {
        assertTrue(closeAlertAndGetItsText().contains('Are you sure you want to make this change?'))
    }

}
