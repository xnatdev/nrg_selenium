package org.nrg.selenium.enums

enum SeriesImportFilterType {

    BLACKLIST ('Blacklist'),
    WHITELIST ('Whitelist'),
    MODALITY_MAP ('Modality Map')

    String uiDisplayText

    SeriesImportFilterType(String text) {
        setUiDisplayText(text)
    }

}