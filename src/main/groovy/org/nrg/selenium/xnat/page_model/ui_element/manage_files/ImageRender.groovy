package org.nrg.selenium.xnat.page_model.ui_element.manage_files

import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By

class ImageRender implements ResourceFileRender, ScreenshotableComponent<ImageRender> {

    protected By imageLocator =  XnatLocators.IMG_TAG

    @Override
    void performInitialValidation() {
        waitForElement(imageLocator)
    }

    @Override
    List<By> getComponentLocators() {
        [imageLocator]
    }

}
