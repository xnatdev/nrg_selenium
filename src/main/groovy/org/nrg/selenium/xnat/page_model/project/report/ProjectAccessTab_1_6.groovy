package org.nrg.selenium.xnat.page_model.project.report

import org.nrg.xnat.versions.*
import org.openqa.selenium.By

class ProjectAccessTab_1_6 extends ProjectAccessTab {

    ProjectAccessTab_1_6() {
        userTableLoadingCompleteLocator = By.xpath("//input[contains(@onclick, 'window.userManager.removeUser')]")
    }

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev]
    }

}
