package org.nrg.selenium.util

import org.apache.commons.lang3.time.StopWatch
import org.nrg.testing.TimeUtils
import org.nrg.testing.xnat.conf.Settings

import static org.testng.AssertJUnit.*

class BrowserDownloadManager {

    private String downloadDir
    private List<File> currentFiles
    private int timeout = 300

    BrowserDownloadManager(String downloadDirectory, int timeout) {
        downloadDir = downloadDirectory
        if (timeout > 0) {
            this.timeout = timeout
        }
        currentFiles = listFiles() as List<File>
    }

    BrowserDownloadManager() {
        this(Settings.TEMP_SUBDIR, 0)
    }

    File findDownload(String expectedFilename = null) {
        final StopWatch stopWatch = TimeUtils.launchStopWatch()
        final int previousNumFilesInDir = currentFiles.size()

        while (true) {
            if (listFiles().length > previousNumFilesInDir) {
                break
            }
            TimeUtils.checkStopWatch(stopWatch, Settings.DEFAULT_TIMEOUT, 'Downloaded file did not show up in download folder in time')
        }

        final File file = (expectedFilename == null) ? findNewFile() : new File(downloadDir, expectedFilename)
        assertTrue(file.exists())
        waitForDownloadComplete(file)
        file
    }

    private File findNewFile() {
        final File file = listFiles().find { file ->
            file.isFile() && !currentFiles.contains(file)
        }
        if (file == null) {
            fail('Downloaded file did not show up in download folder in time')
        }
        file
    }

    private void waitForDownloadComplete(File partiallyDownloadedFile) {
        final StopWatch stopWatch = TimeUtils.launchStopWatch()
        while (true) {
            if (partiallyDownloadedFile.length() > 0) break
            TimeUtils.checkStopWatch(stopWatch, timeout, 'No file contents were downloaded in time.')
        }

        long partialFileSize = partiallyDownloadedFile.length()
        while (true) {
            // Check every second until the file size stops increasing (wait for download to finish)
            TimeUtils.sleep(1000)
            if (partiallyDownloadedFile.length() == partialFileSize) {
                return
            } else {
                partialFileSize = partiallyDownloadedFile.length()
            }
        }
    }

    private File[] listFiles() {
        new File(downloadDir).listFiles()
    }

}
