package org.nrg.selenium.xnat.page_model.admin

import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By
import org.openqa.selenium.support.pagefactory.ByChained

// TODO: make abstract class instead to fix subclass constructors
trait AdminUiElement implements ScreenshotableComponent<AdminUiElement>, XnatPageComponent {

    By locator
    By screenshotLocator
    boolean takeScreenshot = true

    @Override
    List<By> getComponentLocators() {
        [screenshotLocator ?: new ByChained(locator, XnatLocators.GRANDPARENT)]
    }

    void setElement() {
        performElementAction()
        if (takeScreenshot) {
            uploadScreenshot()
        }
    }

    abstract void performElementAction()

}
