package org.nrg.selenium.xnat.page_model.users

import org.nrg.xnat.versions.*

class PasswordExpirationPage_1_6 extends PasswordExpirationPage {

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev]
    }

    @Override
    PasswordExpirationPage fillCurrentPassword(String password) {
        this
    }

}
