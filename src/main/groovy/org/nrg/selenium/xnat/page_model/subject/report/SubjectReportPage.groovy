package org.nrg.selenium.xnat.page_model.subject.report

import org.nrg.jira.components.zephyr.TestStatus
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.report.DataReportPage
import org.nrg.selenium.xnat.page_model.report.XnatReportPage
import org.nrg.selenium.xnat.page_model.session.delete.CommonDeleteDialog
import org.nrg.selenium.xnat.page_model.session.report.ImagingSessionReportPage
import org.nrg.selenium.xnat.page_model.subject.edit.SubjectEditPage
import org.nrg.xnat.pogo.DataType
import org.nrg.xnat.pogo.Subject
import org.openqa.selenium.By
import static org.testng.AssertJUnit.assertEquals

class SubjectReportPage implements DataReportPage<Subject, SubjectReportPage>, XnatReportPage<Subject, SubjectReportPage>, NavigablePageObject<SubjectReportPage> {

    protected By subjectDetailsLocator = By.xpath("//div[contains(text(), 'Subject Details')]")
    protected By birthYearLocator = subjectFieldLocator('Birth year')
    protected By genderLocator = subjectFieldLocator('Gender')
    protected By handednessLocator = subjectFieldLocator('Handedness')
    protected By educationLocator = subjectFieldLocator('Education')
    protected By raceLocator = subjectFieldLocator('Race')
    protected By ethnicityLocator = subjectFieldLocator('Ethnicity')
    protected By heightLocator = subjectFieldLocator('Height (inches)')
    protected By weightLocator = subjectFieldLocator('Weight (lbs)')
    protected By groupLocator = subjectFieldLocator('Group')
    protected By sourceLocator = subjectFieldLocator('Recruitment Source')
    protected By arbitraryMrSessionLocator = By.xpath("//div[@id='all_expts']//a[text()='${DataType.MR_SESSION.singularName}']")
    protected By arbitraryCTSessionLocator = By.xpath("//div[@id='all_expts']//a[text()='${DataType.CT_SESSION.singularName}']")
    protected By arbitraryPetSessionLocator = By.xpath("//div[@id='all_expts']//a[text()='${DataType.PET_SESSION.singularName}']")
    protected By projectTabLocator = By.xpath("//em[contains(text(), 'Projects')]")

    @Override
    void performInitialValidation() {
        waitForElement(subjectDetailsLocator)
    }

    @Override
    SubjectEditPage loadEditPage() {
        findElement(editLinkLocator).click()
        loadPage(SubjectEditPage, false, true)
    }

    @Override
    void validateObject(Subject subject) {
        assertSubjectDetailsRepresentSubject(subject.label)
        captureStep(stepCounter.intValue(), TestStatus.PASS, "Project = ${subject.project}, Subject = ${subject}", true, componentLocators)
    }

    @Override
    List<By> getComponentLocators() {
        [subjectDetailsLocator] + readActionsBox().componentLocators + readExperimentTable().componentLocators + readBreadcrumbs().componentLocators
    }

    @Override
    CommonDeleteDialog loadDeleteComponent() {
        actionsBoxNavigation(['Delete'])
        loadPage(CommonDeleteDialog)
    }

    @Override
    List<String> viewXmlActionsPath() {
        ['View XML']
    }

    SubjectReportPage assertSubjectDetailsRepresentSubject(String subjectLabel) {
        assertEquals("Subject Details: ${subjectLabel}", readSubjectDetails())
        this
    }

    SubjectReportPage assertProjectTabNotPresent() {
        assertElementNotPresent(projectTabLocator)
        this
    }

    ImagingSessionReportPage clickSessionLink(String sessionIdentifier) {
        exceptionSafeClick(sessionLinkLocator(sessionIdentifier))
        loadPage(ImagingSessionReportPage)
    }

    ImagingSessionReportPage clickMrSessionLink() {
        exceptionSafeClick(arbitraryMrSessionLocator)
        loadPage(ImagingSessionReportPage)
    }

    ImagingSessionReportPage clickCTSessionLink() {
        exceptionSafeClick(arbitraryCTSessionLocator)
        loadPage(ImagingSessionReportPage)
    }

    ImagingSessionReportPage clickPetSessionLink() {
        exceptionSafeClick(arbitraryPetSessionLocator)
        loadPage(ImagingSessionReportPage)
    }

    ExperimentTable readExperimentTable() {
        loadPage(ExperimentTable).withUnderlyingPage(this)
    }

    String readSubjectDetails() {
        exceptionSafeGetText(subjectDetailsLocator)
    }

    int readBirthYear() {
        Integer.parseInt(exceptionSafeGetText(birthYearLocator))
    }

    String readGenderDisplay() {
        exceptionSafeGetText(genderLocator)
    }

    String readHandednessDisplay() {
        exceptionSafeGetText(handednessLocator)
    }

    int readEducation() {
        Integer.parseInt(exceptionSafeGetText(educationLocator))
    }

    String readRace() {
        exceptionSafeGetText(raceLocator)
    }

    String readEthnicity() {
        exceptionSafeGetText(ethnicityLocator)
    }

    String readHeight() {
        exceptionSafeGetText(heightLocator)
    }

    String readWeight() {
        exceptionSafeGetText(weightLocator)
    }

    String readGroup() {
        exceptionSafeGetText(groupLocator)
    }

    String readSource() {
        exceptionSafeGetText(sourceLocator)
    }

    protected By subjectFieldLocator(String fieldName) {
        By.xpath("//th[text()='${fieldName}']/following-sibling::td")
    }

    protected By sessionLinkLocator(String sessionIdentifier) {
        By.xpath("//div[@id='all_expts']//a[./../following-sibling::td[text()='${sessionIdentifier}']]")
    }

}
