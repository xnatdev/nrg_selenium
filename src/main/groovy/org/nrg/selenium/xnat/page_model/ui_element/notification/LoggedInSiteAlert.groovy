package org.nrg.selenium.xnat.page_model.ui_element.notification

import org.nrg.selenium.enums.SiteAlertType
import org.openqa.selenium.By
import org.openqa.selenium.WebElement

class LoggedInSiteAlert implements XnatSiteAlert {

    @Override
    SiteAlertType readType(WebElement element) {
        final String elementClass = element.getAttribute('class')
        SiteAlertType.values().find { type ->
            elementClass == "${type.name().toLowerCase()} headNotification"
        }
    }

    @Override
    By getLocator() {
        By.xpath("//div[contains(@class, 'headNotification')]")
    }

}
