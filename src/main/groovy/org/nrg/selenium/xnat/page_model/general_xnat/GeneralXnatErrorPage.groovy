package org.nrg.selenium.xnat.page_model.general_xnat

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.openqa.selenium.By

class GeneralXnatErrorPage implements XnatPageComponent, ScreenshotableComponent<GeneralXnatErrorPage> {

    XpathLocator mainSectionLocator = new XpathLocator('//div[@class="errorDisplay"]')
    XpathLocator pageVerificationSublocator = new XpathLocator('./p[1]')

    @Override
    void performInitialValidation() {
        assertElementText('XNAT has encountered an error with your request:', mainSectionLocator.joinSublocator(pageVerificationSublocator))
    }

    @Override
    List<By> getComponentLocators() {
        [mainSectionLocator]
    }

}
