package org.nrg.selenium.processing.jackson.serializer

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.nrg.selenium.processing.element_comparators.comparator.EqualityElementComparator

class EqualsComparatorSerializer extends ElementComparatorSerializer<EqualityElementComparator> {

    @Override
    void writeSpecificFields(EqualityElementComparator value, JsonGenerator gen, SerializerProvider provider) {}

}
