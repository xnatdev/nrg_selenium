package org.nrg.selenium.xnat.page_model.admin

import org.nrg.selenium.xnat.page_model.LoadablePage
import org.nrg.selenium.xnat.page_model.NavigablePageObject

class DataTypesManagementPage implements NavigablePageObject<DataTypesManagementPage>, LoadablePage {

    @Override
    void navigateTo() {
        topNav('Administer', 'Data Types')
    }

}
