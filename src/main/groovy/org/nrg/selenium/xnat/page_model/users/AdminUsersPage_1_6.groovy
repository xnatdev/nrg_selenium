package org.nrg.selenium.xnat.page_model.users

import org.nrg.xnat.versions.*
import org.openqa.selenium.By

class AdminUsersPage_1_6 extends AdminUsersPage_1_7_4 {

    AdminUsersPage_1_6() {
        addUserButtonLocator = By.xpath("//div[@id='user-list']//button")
    }

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev]
    }


}
