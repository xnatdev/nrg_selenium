package org.nrg.selenium.xnat.page_model.admin

import org.apache.commons.lang3.StringUtils
import org.nrg.selenium.enums.SiteAlertEnablingOption
import org.nrg.selenium.enums.SiteAlertType
import org.nrg.xnat.versions.*

class AdminUiPage_1_8_3 extends AdminUiPage {

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_7_0, Xnat_1_7_1, Xnat_1_7_2, Xnat_1_7_3, Xnat_1_7_4, Xnat_1_7_5, Xnat_1_7_5_2, Xnat_1_7_6, Xnat_1_7_7, Xnat_1_8_0, Xnat_1_8_1, Xnat_1_8_2, Xnat_1_8_2_2, Xnat_1_8_3]
    }

    @Override
    AdminUiPage setZipExtensions(String zipExtensions) {
        setConfigSetting(fileSystemTab, new AdminUiInput(zipExtensionsLocator, zipExtensions))
    }

    @Override
    AdminUiPage setSiteAlert(SiteAlertEnablingOption enablingOption, String alertMessage, SiteAlertType alertType) {
        setConfigSettings(siteSetupTab, [
                new AdminUiSelect(siteAlertStatusLocator, enablingOption.settingName, null, false),
                new AdminUiInput(siteAlertContentLocator, alertMessage, null, false),
                new AdminUiSelect(siteAlertTypeLocator, StringUtils.capitalize(alertType.name().toLowerCase()))
        ])
    }

}
