package org.nrg.selenium.util

import org.nrg.selenium.enums.DataUnit

import static org.testng.AssertJUnit.assertEquals

class ResourceValidation {

    double fileSize
    DataUnit dataUnit
    int numFiles
    double tolerance = 0.3
    boolean compareExactly = false

    ResourceValidation() {}

    ResourceValidation(double fileSize, DataUnit dataUnit, int numFiles) {
        setFileSize(fileSize)
        setDataUnit(dataUnit)
        setNumFiles(numFiles)
    }

    void assertSizeMatches(String actualSize) {
        if (compareExactly) {
            assertEquals(String.valueOf(fileSize), actualSize)
        } else {
            assertEquals(fileSize, Double.parseDouble(actualSize), tolerance)
        }
    }

}
