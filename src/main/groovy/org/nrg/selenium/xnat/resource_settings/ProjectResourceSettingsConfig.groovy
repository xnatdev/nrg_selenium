package org.nrg.selenium.xnat.resource_settings

import org.nrg.selenium.enums.ResourceSettingsCategory
import org.nrg.selenium.enums.ResourceSettingsLevel

class ProjectResourceSettingsConfig {

    ResourceSettingsCategory category
    String title
    String description
    String script
    String dataType
    ResourceSettingsLevel level
    String scanTypes
    String resourceFolder
    String subfolder
    boolean overwrite

    ProjectResourceSettingsConfig category(ResourceSettingsCategory category) {
        setCategory(category)
        this
    }

    ProjectResourceSettingsConfig title(String title) {
        setTitle(title)
        this
    }

    ProjectResourceSettingsConfig description(String description) {
        setDescription(description)
        this
    }

    ProjectResourceSettingsConfig script(String script) {
        setScript(script)
        this
    }

    ProjectResourceSettingsConfig dataType(String dataType) {
        setDataType(dataType)
        this
    }

    ProjectResourceSettingsConfig level(ResourceSettingsLevel level) {
        setLevel(level)
        this
    }

    ProjectResourceSettingsConfig scanTypes(List<String> scanTypes) {
        setScanTypes(scanTypes.join(','))
        this
    }

    ProjectResourceSettingsConfig resourceFolder(String resourceFolder) {
        setResourceFolder(resourceFolder)
        this
    }

    ProjectResourceSettingsConfig subfolder(String subfolder) {
        setSubfolder(subfolder)
        this
    }

    ProjectResourceSettingsConfig overwrite(boolean overwrite) {
        setOverwrite(overwrite)
        this
    }

}
