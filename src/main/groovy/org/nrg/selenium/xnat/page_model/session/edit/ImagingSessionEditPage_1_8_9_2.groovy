package org.nrg.selenium.xnat.page_model.session.edit

import org.nrg.xnat.versions.XnatVersion
import org.nrg.xnat.versions.Xnat_1_8_0
import org.nrg.xnat.versions.Xnat_1_8_1
import org.nrg.xnat.versions.Xnat_1_8_2
import org.nrg.xnat.versions.Xnat_1_8_2_2
import org.nrg.xnat.versions.Xnat_1_8_3
import org.nrg.xnat.versions.Xnat_1_8_4
import org.nrg.xnat.versions.Xnat_1_8_5
import org.nrg.xnat.versions.Xnat_1_8_6
import org.nrg.xnat.versions.Xnat_1_8_6_1
import org.nrg.xnat.versions.Xnat_1_8_7
import org.nrg.xnat.versions.Xnat_1_8_8
import org.nrg.xnat.versions.Xnat_1_8_9
import org.nrg.xnat.versions.Xnat_1_8_9_1
import org.nrg.xnat.versions.Xnat_1_8_9_2

class ImagingSessionEditPage_1_8_9_2 extends ImagingSessionEditPage {

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_8_0, Xnat_1_8_1, Xnat_1_8_2, Xnat_1_8_2_2, Xnat_1_8_3, Xnat_1_8_4, Xnat_1_8_5, Xnat_1_8_6, Xnat_1_8_6_1, Xnat_1_8_7, Xnat_1_8_8, Xnat_1_8_9, Xnat_1_8_9_1, Xnat_1_8_9_2]
    }

    @Override
    String relabelSessionMessage() {
        "Modifying the Session of an imaging session will result in the moving of files on the file server within the project's storage space. Are you sure you want to make this change?"
    }

    @Override
    String moveSessionMessage() {
        "Modifying the primary project of an imaging session will result in the moving of files on the file server into the new project's storage space. Are you sure you want to make this change?"
    }

}
