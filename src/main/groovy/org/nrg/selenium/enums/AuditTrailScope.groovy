package org.nrg.selenium.enums

enum AuditTrailScope {

    ALL_EVENTS_7_DAYS ('All Events - 7 days'),
    ALL_EVENTS_30_DAYS ('All Events - 30 days'),
    ALL_EVENTS_90_DAYS ('All Events - 90 days'),
    ADMIN_EVENTS_ONLY_7_DAYS ('Admin Events Only - 7 days'),
    ADMIN_EVENTS_ONLY_30_DAYS ('Admin Events Only - 30 days'),
    ADMIN_EVENTS_ONLY_90_DAYS ('Admin Events Only - 90 days')

    final String uiText

    AuditTrailScope(String text) {
        uiText = text
    }

}