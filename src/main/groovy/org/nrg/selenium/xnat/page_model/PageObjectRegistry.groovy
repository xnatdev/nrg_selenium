package org.nrg.selenium.xnat.page_model

import org.nrg.xnat.versions.XnatVersion
import org.nrg.xnat.versions.XnatVersionList
import org.reflections.Reflections

class PageObjectRegistry {

    public static final Map<Class<? extends XnatPageComponent>, Map<Class<? extends XnatVersion>, Class<? extends XnatPageComponent>>> REGISTRY = [:]

    static <X extends XnatPageComponent> Class<X> getPageObject(Class<X> pageClass, Class<XnatVersion> versionClass) {
        if (!REGISTRY.containsKey(pageClass)) {
            final Set<Class<? extends X>> subClasses = new Reflections('org.nrg.selenium.xnat.page_model').getSubTypesOf(pageClass)
            REGISTRY.put(pageClass, XnatVersionList.XNAT_VERSION_GRAPH.nodes().collectEntries { xnatVersion ->
                [(xnatVersion) : subClasses.find { xnatVersion in it.newInstance().handledVersions } ?: pageClass]
            })
        }
        REGISTRY[pageClass][versionClass] as Class<X>
    }

}
