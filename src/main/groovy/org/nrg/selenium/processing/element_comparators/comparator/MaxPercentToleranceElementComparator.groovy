package org.nrg.selenium.processing.element_comparators.comparator

import groovy.transform.EqualsAndHashCode
import org.nrg.selenium.processing.exceptions.IncorrectValueException
import org.nrg.testing.MathUtils
import org.nrg.testing.xnat.processing.exceptions.ProcessingValidationException
import org.nrg.selenium.processing.exceptions.WebElementNotFound
import org.nrg.selenium.xnat.XnatDriver
import org.openqa.selenium.By
import org.openqa.selenium.WebDriverException

@EqualsAndHashCode
class MaxPercentToleranceElementComparator extends ElementComparator {

    double tolerance // Tolerance in percent

    @Override
    void checkPointsMatch(XnatDriver xnatDriver, By locator, String expectedValue) throws ProcessingValidationException {
        try {
            final String actualValueString = xnatDriver.findElement(locator).text
            final double expected = Double.parseDouble(expectedValue)
            if (MathUtils.percentError(expected, Double.parseDouble(actualValueString)) > tolerance) {
                throw new IncorrectValueException(locator, "elements were expected to be within ${tolerance} percent error.", expectedValue, actualValueString)
            }
        } catch (WebDriverException ignored) {
            throw new WebElementNotFound(locator)
        }
    }

}

