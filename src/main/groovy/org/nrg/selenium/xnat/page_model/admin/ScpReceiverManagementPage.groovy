package org.nrg.selenium.xnat.page_model.admin

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.LoadablePage
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.ui_element.HtmlTable
import org.nrg.selenium.xnat.page_model.ui_element.popup.XnatPopup
import org.nrg.xnat.pogo.dicom.DicomScpReceiver
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertEquals

class ScpReceiverManagementPage implements NavigablePageObject<ScpReceiverManagementPage>, HtmlTable<ScpReceiverManagementPage>, LoadablePage {

    String tabName = 'DICOM SCP Receivers'
    String aeTitleIdentifier = 'AE Title'
    String portIdentifier = 'Port'
    String enabledIdentifier = 'Enabled'
    String identifierIdentifier = 'Identifier'
    String actionsIdentifier = 'Actions'
    protected By addReceiverButton = XnatLocators.buttonWithText('New DICOM SCP Receiver')
    protected XpathLocator enabledSublocator = new XpathLocator('.//input[@type="checkbox"]')
    protected XpathLocator editSublocator = XnatLocators.EDIT_BUTTON
    protected XpathLocator deleteSublocator = XnatLocators.DELETE_BUTTON
    protected String deleteDialogIdentifier = 'Delete receiver?'
    protected XpathLocator confirmDeleteSublocator = XnatLocators.DELETE_BUTTON

    @Override
    void navigateTo() {
        loadPage(AdminUiPage).switchToTab(tabName)
    }

    @Override
    XpathLocator getTableLocator() {
        XnatLocators.byClass('dicom-scp-receivers xnat-table')
    }

    List<DicomScpReceiver> readReceivers() {
        final List<String> headers = readHeaders()
        final Map<String, Integer> headerIndices = readHeaderIndices()
        final int aeIndex = headers.indexOf(aeTitleIdentifier)
        final int portIndex = headers.indexOf(portIdentifier)
        final int enabledIndex = headers.indexOf(enabledIdentifier)
        final int identifierIndex = headers.indexOf(identifierIdentifier)
        readAllDataRows().indexed().collect { index, row ->
            new DicomScpReceiver(
                    row[headerIndices[aeTitleIdentifier]],
                    Integer.parseInt(row[headerIndices[portIdentifier]]),
                    switchBoxIsChecked(enabledSwitchboxLocator(index)),
                    xnatConfig.hostName,
                    null // TODO: DICOM Object Identifier - information displayed doesn't match up 1-1 with my object for it
            )
        }
    }

    List<DicomScpReceiver> findReceiversOnPort(int port) {
        readReceivers().findAll { receiver ->
            receiver.port == port
        }
    }

    DicomScpReceiver findReceiverOnPort(int port) {
        readReceivers().find { receiver ->
            receiver.port == port
        }
    }

    DicomScpReceiver findReceiverByAeTitle(String aeTitle) {
        readReceivers().find { receiver ->
            receiver.aeTitle == aeTitle
        }
    }

    ScpReceiverManagementPage createScpReceiver(DicomScpReceiver receiver) {
        exceptionSafeClick(addReceiverButton)
        readEditDialog().createReceiver(receiver)
        validateAndCaptureReceiver(receiver)
    }

    ScpReceiverEditDialog launchEditDialog(DicomScpReceiver receiver) {
        exceptionSafeClick(receiverRowLocator(receiver.aeTitle).joinSublocator(editSublocator))
        readEditDialog()
    }

    ScpReceiverEditDialog readEditDialog() {
        loadPage(ScpReceiverEditDialog).withUnderlyingPage(this)
    }

    ScpReceiverManagementPage assertReceiverFields(DicomScpReceiver receiver) {
        final DicomScpReceiver parsedReceiver = findReceiverByAeTitle(receiver.aeTitle)
        assertEquals(receiver.port, parsedReceiver.port)
        assertEquals(receiver.enabled, parsedReceiver.enabled)
        this
    }

    ScpReceiverManagementPage validateAndCaptureReceiver(DicomScpReceiver receiver) {
        assertReceiverFields(receiver)
        commentReceiver(receiver)
        capture()
    }

    ScpReceiverManagementPage disableReceiver(DicomScpReceiver receiver) {
        setReceiverState(receiver, false)
    }

    ScpReceiverManagementPage enableReceiver(DicomScpReceiver receiver) {
        setReceiverState(receiver, true)
    }

    ScpReceiverManagementPage setReceiverState(DicomScpReceiver receiver, boolean enabled) {
        performSwitchBox(receiverEnablingCheckbox(receiver.aeTitle), enabled)
        receiver.setEnabled(enabled)
        this
    }

    ScpReceiverManagementPage assertAeTitleNotPresent(String aeTitle) {
        assertElementNotPresent(receiverRowLocator(aeTitle))
        this
    }

    ScpReceiverManagementPage deleteReceivers(List<DicomScpReceiver> receivers) {
        receivers.each { receiver ->
            exceptionSafeClick(deleteReceiverButton(receiver.aeTitle))
            readDeleteDialog().
                    assertTextEquals("Are you sure you'd like to delete the '${receiver.aeTitle}' DICOM Receiver?\nThis action cannot be undone.").
                    uploadScreenshot().
                    clickSubelement(confirmDeleteSublocator)
            waitForNotElement(receiverRowLocator(receiver.aeTitle))
            assertAeTitleNotPresent(receiver.aeTitle)
            receiver.setEnabled(false)
        }
        this
    }

    ScpReceiverManagementPage deleteReceiver(DicomScpReceiver receiver) {
        deleteReceivers([receiver])
    }

    ScpReceiverManagementPage deleteAllReceiversOnPort(int port) {
        deleteReceivers(findReceiversOnPort(port))
    }

    protected XpathLocator receiverRowLocator(String aeTitle) {
        tableLocator.joinSublocator("./tr[@title='${aeTitle}']")
    }

    protected XpathLocator receiverEnablingCheckbox(String aeTitle) {
        receiverRowLocator(aeTitle).joinSublocator(enabledSublocator)
    }

    protected XpathLocator deleteReceiverButton(String aeTitle) {
        receiverRowLocator(aeTitle).joinSublocator(deleteSublocator)
    }

    protected By enabledSwitchboxLocator(int rowIndex) {
        dataRowLocator(rowIndex).joinSublocator(enabledSublocator)
    }

    protected void commentReceiver(DicomScpReceiver receiver) {
        commentStep("aeTitle = ${receiver.aeTitle}, port = ${receiver.port}, enabled = ${receiver.enabled}")
    }

    protected XnatPopup readDeleteDialog() {
        readXnatDialog(deleteDialogIdentifier)
    }

}
