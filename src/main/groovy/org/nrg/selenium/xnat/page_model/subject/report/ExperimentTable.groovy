package org.nrg.selenium.xnat.page_model.subject.report

import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.ui_element.HtmlTable
import org.nrg.xnat.pogo.DataType
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.experiments.Experiment
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.NonimagingAssessor
import org.nrg.xnat.pogo.experiments.SessionAssessor
import org.nrg.xnat.pogo.experiments.SubjectAssessor
import org.openqa.selenium.By
import org.openqa.selenium.WebElement

import java.time.LocalDate

import static org.testng.AssertJUnit.assertTrue

class ExperimentTable implements OverlayingPage<ExperimentTable, SubjectReportPage>, HtmlTable<ExperimentTable>, XnatPageComponent {

    protected By experimentSectionLocator = By.id('expts_container')
    protected XpathLocator tableClassifierSublocator = XnatLocators.PARENT
    protected String projectSpecificIdentifier = 'proj_expts'
    protected String emptyTableIdentifier = 'No data to show'
    protected int dateIndex = 0
    protected int experimentIndex = 1
    protected int projectIndex = 2
    protected int labelIndex = 3

    @Override
    XpathLocator getTableLocator() {
        new XpathLocator('//div[contains(@id, "_expts") and @style="display: block;"]/table')
    }

    @Override
    List<By> getComponentLocators() {
        [experimentSectionLocator]
    }

    @Override
    void performNavigationToUnderlyingPage() {}

    boolean currentTableIsProjectSpecific() {
        exceptionSafeGetAttribute(tableLocator.joinSublocator(tableClassifierSublocator), 'id') == projectSpecificIdentifier
    }

    boolean hasNoExperiments() {
        readDataCellEntry(0, 0) == emptyTableIdentifier
    }

    List<SubjectAssessor> readExperiments() {
        if (hasNoExperiments()) {
            []
        } else {
            final int numRows = countDataRows()
            final List<SubjectAssessor> subjectAssessors = []
            (1 .. numRows).each { rowIndex ->
                final WebElement webElement = findElement(specificDataCellLocator(experimentIndex + 1, rowIndex).joinSublocator('./a'))
                final String dataTypeName = webElement.text
                final List<String> experimentLinkSplit = webElement.getAttribute('href').split('/')
                final DataType dataType = DataType.lookup(experimentLinkSplit[experimentLinkSplit.indexOf('search_element') + 1].replace('%3A', ':'))
                dataType.setSingularName(dataTypeName)
                final Class<? extends Experiment> associatedClass = dataType.associatedExperimentClass
                final List<String> row = readDataRow(rowIndex)
                if (row[experimentIndex].startsWith('  ')) {
                    final SessionAssessor sessionAssessor = (associatedClass ?: SessionAssessor).newInstance() as SessionAssessor
                    setFields(sessionAssessor, dataType, row)
                    (subjectAssessors.last() as ImagingSession).addAssessor(sessionAssessor)
                } else {
                    final SubjectAssessor subjectAssessor = selectSubjectAssessorClass(associatedClass, dataType).newInstance()
                    setFields(subjectAssessor, dataType, row)
                    subjectAssessors << subjectAssessor
                }
            }
            subjectAssessors
        }
    }

    ExperimentTable assertNoExperiments() {
        assertTrue(hasNoExperiments())
        this
    }

    private void setFields(Experiment experiment, DataType dataType, List<String> elementRow) {
        experiment.setDataType(dataType)
        if (elementRow[dateIndex] != '') {
            experiment.setDate(LocalDate.parse(elementRow[dateIndex]))
        }
        experiment.setPrimaryProject(new Project(elementRow[projectIndex]))
        experiment.setLabel(elementRow[labelIndex])
    }

    private Class<? extends SubjectAssessor> selectSubjectAssessorClass(Class<? extends Experiment> associatedClass, DataType dataType) {
        if (associatedClass != null) {
            associatedClass as Class<? extends SubjectAssessor>
        } else if (dataType.xsiType.endsWith('SessionData')) {
            ImagingSession
        } else {
            NonimagingAssessor
        }
    }

}
