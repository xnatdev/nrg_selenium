package org.nrg.selenium.xnat.page_model.project.report

import org.openqa.selenium.By

class ProjectPipelineTab implements ProjectReportTab {

    @Override
    By tabLocator() {
        locators.tab('Pipelines')
    }

}
