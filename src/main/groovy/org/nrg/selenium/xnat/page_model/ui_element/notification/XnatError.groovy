package org.nrg.selenium.xnat.page_model.ui_element.notification

import org.openqa.selenium.By

class XnatError implements XnatNotification<XnatError> {

    @Override
    By getLocator() {
        By.cssSelector('div.error')
    }

}
