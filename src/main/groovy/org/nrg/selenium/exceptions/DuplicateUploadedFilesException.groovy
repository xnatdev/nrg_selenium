package org.nrg.selenium.exceptions

class DuplicateUploadedFilesException extends Exception {

    List<String> reportedFileNames

    DuplicateUploadedFilesException(List<String> reportedFileNames) {
        setReportedFileNames(reportedFileNames)
    }

}
