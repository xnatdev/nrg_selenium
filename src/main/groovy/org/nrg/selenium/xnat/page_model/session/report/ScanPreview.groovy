package org.nrg.selenium.xnat.page_model.session.report

import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.dicom.DicomDumpPage
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.ui_element.popup.XnatDialog
import org.openqa.selenium.By
import org.openqa.selenium.Dimension
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebDriverException
import org.openqa.selenium.support.ui.ExpectedCondition

import static org.testng.AssertJUnit.assertTrue

class ScanPreview implements ScreenshotableComponent<ScanPreview>, OverlayingPage<ScanPreview, ImagingSessionReportPage>, XnatPageComponent {

    protected By previewLocator = By.xpath("//div[@class='xnat-dialog-title inner' and contains(text(), 'Scan ')]")
    protected By previewCloseLocator = By.xpath(".//button[text()='Close' and not(@disabled)]")
    XnatDialog previewDialog

    @Override
    List<By> getComponentLocators() {
        [previewLocator]
    }

    @Override
    void performInitialValidation() {
        previewDialog = readXnatDialog(previewLocator)
    }

    @Override
    void performNavigationToUnderlyingPage() {
        exceptionSafeClick(previewDialog.getSubElementLocator(previewCloseLocator))
    }

    DicomDumpPage<ScanPreview> readDicomDump() {
        previewDialog.clickSubelement(XnatLocators.linkWithText('View DICOM Headers'))
        loadPage(DicomDumpPage).withUnderlyingPage(this)
    }

    ScanPreview assertSnapshotPresent() {
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver webDriver) {
                try {
                    final Dimension dimension = findElement(previewDialog.getSubElementLocator(locators.IMG_TAG)).size
                    return (dimension.height > 0 && dimension.width > 0)
                } catch (WebDriverException ignored) {}
                return false
            }
        })
        this
    }

    ImagingSessionReportPage closePreview() {
        returnToUnderlyingPage()
    }

}
