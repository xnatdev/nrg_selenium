package org.nrg.selenium.xnat.page_model.dicom

import org.dcm4che3.data.Tag
import org.dcm4che3.data.VR
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.testing.TestNgUtils
import org.nrg.testing.dicom.DicomObject
import org.nrg.testing.dicom.DicomTag
import org.nrg.testing.dicom.DicomValidator
import org.nrg.testing.dicom.values.DicomSequence
import org.openqa.selenium.support.ui.ExpectedConditions

import static org.testng.AssertJUnit.assertEquals

class DicomDumpPage<X extends XnatPageComponent> extends DicomValidator implements XnatPageComponent, OverlayingPage<DicomDumpPage, X> {

    protected XpathLocator tagToValueSublocator = new XpathLocator('./following-sibling::td[2]')
    protected XpathLocator closeDialogSublocator = XnatLocators.buttonWithText('Close')

    DicomDumpPage<X> validate(DicomObject expectedDicomObject) {
        expectedDicomObject.markChildren()
        validate([(new File('dummyfile')) : expectedDicomObject])
        this
    }

    @Override
    void performInitialValidation() {
        wait.until(ExpectedConditions.textToBePresentInElementLocated(dicomValueByTag(new DicomTag(Tag.StudyInstanceUID)), "."))
    }

    @Override
    void validate(File actualFile, DicomObject expectedDicomObject) {
        expectedDicomObject.dicomMap.values().each { dicomValue ->
            dicomValue.assertValuesSatisfied(this)
        }
    }

    @Override
    void checkTagPresent(DicomTag tag) {
        assertDicomTagPresent(tag)
    }

    @Override
    void checkTagNotPresent(DicomTag tag) {
        assertDicomTagNotPresent(tag)
    }

    @Override
    void checkTagStartsWith(DicomTag tag, String value) {
        throw new UnsupportedOperationException('Method not supported in selenium DICOM validator.')
    }

    @Override
    void checkTagHasValue(DicomTag tag, String value) {
        assertDicomValueEqual(tag, value)
    }

    @Override
    void checkTagHasValue(DicomTag dicomTag, DicomTag dicomTag1) {
        throw new UnsupportedOperationException('Method not supported in selenium DICOM validator.')
    }

    @Override
    void checkTagHasValue(DicomTag dicomTag, String s, VR vr) {
        throw new UnsupportedOperationException('Method not supported in selenium DICOM validator.')
    }

    @Override
    void checkTagDoesntHaveValue(DicomTag tag, String value) {
        assertDicomValueNotEqual(tag, value)
    }

    @Override
    void validateSequence(DicomSequence sequence) {
        throw new UnsupportedOperationException('Sequences must be validated using a direct file validator.')
    }

    @Override
    void performNavigationToUnderlyingPage() {
        readXnatDialog('The DICOM tags').clickSubelement(closeDialogSublocator)
    }

    String getTagValue(DicomTag tag) {
        exceptionSafeGetText(dicomValueByTag(tag))
    }

    DicomDumpPage<X> screenshot(DicomTag dicomTag) {
        uploadTargetedScreenshot(screenshotLocator(dicomTag))
        this
    }

    DicomDumpPage<X> assertDicomTagPresent(DicomTag tag) {
        assertElementPresent(dicomTag(tag))
        screenshot(tag)
        this
    }

    DicomDumpPage<X> assertDicomTagNotPresent(DicomTag tag) {
        assertElementNotPresent(dicomTag(tag))
        this
    }

    DicomDumpPage<X> assertDicomValueEqual(DicomTag tag, String expectedValue) {
        assertEquals(expectedValue, getTagValue(tag))
        this
    }

    DicomDumpPage<X> assertDicomValueNotEqual(DicomTag tag, String expectedValue) {
        TestNgUtils.assertNotEquals(expectedValue, getTagValue(tag))
        this
    }

    protected XpathLocator dicomTag(DicomTag tag) {
        XnatLocators.tdWithText(tag.fullHexString().toUpperCase()) // not acceptable once we need sequences
    }

    protected XpathLocator dicomValueByTag(DicomTag tag) {
        dicomTag(tag).joinSublocator(tagToValueSublocator)
    }

    protected XpathLocator screenshotLocator(DicomTag tag) {
        dicomTag(tag).parent()
    }

}
