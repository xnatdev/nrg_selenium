package org.nrg.selenium.browser

import groovy.transform.PackageScope
import org.openqa.selenium.WebDriver
import org.reflections.Reflections

abstract class BrowserSupport {

    public static BrowserSupport cachedBrowserSupport

    protected abstract List<String> keys()

    protected abstract WebDriver construct()

    static WebDriver setup(String id) {
        if (cachedBrowserSupport == null) {
            cacheBrowser(id)
        }
        cachedBrowserSupport.construct()
    }

    // separate method so we can unit test it without actually constructing WebDriver objects (we don't want unit tests to make annoying popups or to only work on Windows)
    @PackageScope
    static void cacheBrowser(String id) {
        final Set<Class<? extends BrowserSupport>> browserClasses = new Reflections('org.nrg.selenium.browser').getSubTypesOf(BrowserSupport)
        final Class<? extends BrowserSupport> browserClass = browserClasses.find { candidate ->
            candidate.newInstance().keys().any { key ->
                id.equalsIgnoreCase(key)
            }
        }
        if (browserClass == null) {
            throw new RuntimeException("Unknown browser: ${id}.\nKnown browsers: ${browserClasses.collect { it.newInstance().keys() }.flatten()}")
        }
        cachedBrowserSupport = browserClass.newInstance()
    }

}
