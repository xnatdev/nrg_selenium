package org.nrg.selenium.xnat.page_model.defaults

import org.nrg.xnat.versions.*

class XnatDefaults_1_6 extends XnatDefaults {

    XnatDefaults_1_6() {
        standardExpirationInterval = '730'
        standardExpirationDate = '10/20/2010'
    }

    @Override
    List<Class<? extends XnatVersion>> getHandledVersions() {
        [Xnat_1_6dev]
    }

}
