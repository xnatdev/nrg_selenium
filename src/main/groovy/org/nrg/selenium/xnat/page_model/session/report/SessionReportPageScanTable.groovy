package org.nrg.selenium.xnat.page_model.session.report

import org.nrg.selenium.enums.DataUnit
import org.nrg.selenium.extension.XpathLocator
import org.nrg.selenium.util.ResourceValidation
import org.nrg.selenium.xnat.page_model.OverlayingPage
import org.nrg.selenium.xnat.page_model.XnatPageComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.selenium.xnat.page_model.report.CustomResourceUploadDialog
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.Scan
import org.openqa.selenium.By
import org.openqa.selenium.support.pagefactory.ByChained

import static org.testng.AssertJUnit.assertEquals

class SessionReportPageScanTable implements ScanListRecord<SessionReportPageScanTable>, OverlayingPage<SessionReportPageScanTable, ImagingSessionReportPage>, XnatPageComponent {

    protected XpathLocator scansListLocator = XnatLocators.byId('scan-data-table')
    protected By arbitraryScanTableScanId = new ByChained(arbitraryScanTableRow(), By.xpath('./td[2]/a'))
    protected XpathLocator sessionFileStringLocator = new XpathLocator("//*[@id='total_dicom_files']/span[1]")
    protected XpathLocator scanCustomResourceUploadSublocator = new XpathLocator('.//i[@title="Upload Resource" and not(@style="display: none;")]')
    protected String scanIdIdentifier = 'Scan'
    protected String scanTypeIdentifier = 'Type'
    protected String seriesDescriptionIdentifier = 'Series Desc'
    protected String usabilityIdentifier = 'Usability'
    protected String scanNoteIdentifier = 'Note'

    @Override
    By scanAssertionReadyLocator() {
        arbitraryScanTableScanId
    }

    @Override
    List<String> readScanIds() {
        findElements(arbitraryScanTableScanId).text
    }

    @Override
    XpathLocator scanTableLocator() {
        scansListLocator
    }

    @Override
    List<Scan> readScans() {
        final Map<String, Integer> headerIndices = readHeaderIndices()
        readAllDataRows().collect { scanRow ->
            final Scan scan = new Scan().
                    id(scanRow[headerIndices[scanIdIdentifier]]).
                    type(scanRow[headerIndices[scanTypeIdentifier]]).
                    quality(scanRow[headerIndices[usabilityIdentifier]]).
                    note(scanRow[headerIndices[scanNoteIdentifier]])
            if (seriesDescriptionIdentifier in headerIndices.keySet()) {
                scan.seriesDescription(scanRow[headerIndices[seriesDescriptionIdentifier]])
            }
            scan
        }
    }

    @Override
    void performNavigationToUnderlyingPage() {}

    boolean isCustomResourceUploadOptionPresent(Scan scan) {
        isElementPresentQuick(customResourceUploadLocator(scan))
    }

    CustomResourceUploadDialog<ImagingSession, ImagingSessionReportPage> loadCustomResourceUploaderForScan(Scan scan) {
        mouseover(scanTableRow(scan))
        exceptionSafeClick(customResourceUploadLocator(scan))
        loadPage(CustomResourceUploadDialog).withUnderlyingPage(underlyingPage)
    }

    ScanPreview loadScanPreview() {
        loadPage(ScanPreview).withUnderlyingPage(underlyingPage)
    }

    ScanPreview viewFirstScan() {
        exceptionSafeClick(arbitraryScanTableScanId)
        loadScanPreview()
    }

    ScanPreview viewScan(Scan scan) {
        exceptionSafeClick(scanTableIdLink(scan))
        loadScanPreview()
    }

    String readScanFileString(Object scanId) {
        exceptionSafeGetText(scanFileStringLocator(scanId))
    }

    String readSessionFileString() {
        exceptionSafeGetText(sessionFileStringLocator)
    }

    SessionReportPageScanTable assertScanResourcesRepresent(Object scanId, ResourceValidation resourceValidation) {
        assertSessionFiles(scanFileStringLocator(scanId), resourceValidation)
    }

    SessionReportPageScanTable assertSummedScanResourcesRepresent(ResourceValidation resourceValidation) {
        assertSessionFiles(sessionFileStringLocator, resourceValidation)
    }

    SessionReportPageScanTable assertFilesRepresentSample1(double tolerance = 0.3) {
        [
                4 : 32.2,
                5 : 32.1,
                6 : 32.4
        ].each { scanId, scanSize ->
            assertScanResourcesRepresent(scanId, new ResourceValidation(fileSize: scanSize, dataUnit: DataUnit.MEGABYTES, numFiles: 176, tolerance: tolerance))
        }
        assertSummedScanResourcesRepresent(new ResourceValidation(fileSize: 96.8, dataUnit: DataUnit.MEGABYTES, numFiles: 528, tolerance: tolerance))
    }

    protected XpathLocator arbitraryScanTableRow() {
        scansListLocator.joinSublocator("./tbody/tr[@valign='top']")
    }

    protected XpathLocator scanTableRow(Scan scan) {
        arbitraryScanTableRow().joinSublocator("[${scanTableIdLink(scan).expression}]")
    }

    protected XpathLocator scanTableIdLink(Scan scan) {
        new XpathLocator(".//td[@class='scan-${scan.id}-id']")
    }

    protected By scanFileStringLocator(Object scanId) {
        By.xpath("//td[@class='scan-${scanId}-files']/span")
    }

    protected XpathLocator customResourceUploadLocator(Scan scan) {
        scanTableRow(scan).joinSublocator(scanCustomResourceUploadSublocator)
    }

    private SessionReportPageScanTable assertSessionFiles(By fileStringLocator, ResourceValidation resourceValidation) {
        final String[] splitStringRepresentation = exceptionSafeGetText(fileStringLocator).split(' ', 2)
        resourceValidation.assertSizeMatches(splitStringRepresentation[0])
        assertEquals("${resourceValidation.dataUnit.sessionPageRepresentation} in ${resourceValidation.numFiles} files", splitStringRepresentation[1])
        uploadTargetedScreenshot(fileStringLocator)
        this
    }

}
