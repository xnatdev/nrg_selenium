package org.nrg.selenium.xnat.page_model

import org.nrg.selenium.xnat.page_model.ui_element.notification.LoginPageSiteAlert
import org.nrg.selenium.xnat.page_model.ui_element.notification.XnatLoginMessage
import org.nrg.selenium.xnat.page_model.ui_element.notification.XnatSiteAlert
import org.nrg.selenium.xnat.page_model.users.ForgotCredentialsPage
import org.nrg.selenium.xnat.page_model.users.PasswordExpirationPage
import org.nrg.selenium.xnat.page_model.users.RegistrationPage
import org.nrg.xnat.pogo.users.User
import org.openqa.selenium.By

import static org.testng.AssertJUnit.assertTrue

class XnatLoginPage implements LoadablePage, XnatPageComponent, SiteAlertContainer, ScreenshotableComponent<XnatLoginPage> {

    protected By usernameLocator = By.id('username')
    protected By passwordLocator = By.name('password')
    protected By loginAreaLocator = By.name('form1')
    protected By loginButtonLocator = By.name('login')
    protected By registerLinkLocator = By.linkText('Register')
    protected By forgotCredentialsLinkLocator = By.linkText('Forgot login or password?')
    protected String loginUrlFragment = '/app/template/Login.vm'

    @Override
    void navigateTo() {
        get(restDriver.mainInterface().formatXnatUrl(loginUrlFragment))
    }

    @Override
    void performInitialValidation() {
        waitForElement(loginButtonLocator)
    }

    @Override
    Class<? extends XnatSiteAlert> alertClass() {
        LoginPageSiteAlert
    }

    @Override
    List<By> getComponentLocators() {
        [By.id('page_wrapper')]
    }

    XnatLoginPage assertRedirectionToLoginPage() {
        performInitialValidation()
        assertTrue(currentUrl.startsWith(restDriver.mainInterface().formatXnatUrl(loginUrlFragment)))
        this
    }

    XnatLoginMessage getLoginMessage() {
        loadPage(XnatLoginMessage)
    }

    XnatLoginPage assertLoginMessage(String messageText) {
        loginMessage.assertTextEquals(messageText).capture()
        this
    }

    XnatLoginPage assertVerificationMessageFor(User user) {
        assertLoginMessage("${user.email} has been verified for the following users: ${user.username}")
    }

    IndexPage login(User user, boolean blockNavigateStep = false) {
        performLoginSubmission(user, blockNavigateStep)
        final IndexPage indexPage = loadPage(IndexPage)
        indexPage.assertLoginSuccessful()
        indexPage
    }

    IndexPage seleniumLogin() {
        if (restDriver.mainUsername == null || restDriver.mainPassword == null) {
            throw new RuntimeException('Missing Selenium non-admin account credentials')
        }
        login(restDriver.mainUser)
    }

    IndexPage seleniumAdminLogin() {
        if (restDriver.mainAdminUsername == null || restDriver.mainAdminPassword == null) {
            throw new RuntimeException('Missing Selenium admin account credentials')
        }
        login(restDriver.mainAdminUser)
    }

    IndexPage adminLogin() {
        if (restDriver.adminUsername == null || restDriver.adminPassword == null) {
            throw new RuntimeException('Missing admin account credentials')
        }
        login(restDriver.adminUser)
    }

    XnatSetupPage loginToUnitializedXnat(User user) {
        performLoginSubmission(user)
        loadPage(XnatSetupPage)
    }

    XnatLoginPage assertFailedLogin(User user, String loginFailureMessage = defaults.defaultLoginFailureMessage) {
        performLoginSubmission(user)
        assertLoginMessage(loginFailureMessage)
    }

    void performLoginSubmission(User user, boolean blockNavigateStep = false) {
        if (!blockNavigateStep) {
            navigateTo()
        }
        fill(usernameLocator, user.username)
        fill(passwordLocator, user.password)
        if (findElement(usernameLocator).getAttribute('value') != user.username) {
            // QA-64: randomly this clears or doesn't fill in, so just check and fill it in if needed.
            fill(usernameLocator, user.username)
        }
        uploadTargetedScreenshot(loginAreaLocator)
        findElement(loginButtonLocator).click()
    }

    XnatLoginPage assertTimeoutMessage() {
        if (isElementPresentQuick(getInstance(XnatLoginMessage).locator)) {
            loginMessage.assertTextContains('Session timed out at').capture()
        } else {
            commentStep('Begrudgingly passing step. Timeout message should be present and is not (see XNAT-4863 for details), but user does at least appear to be on the login page.')
            capture()
        }
        this
    }

    PasswordExpirationPage loginToExpiredPasswordAccount(User expiredUser) {
        performLoginSubmission(expiredUser)
        loadPage(PasswordExpirationPage)
    }

    RegistrationPage clickRegisterLink() {
        exceptionSafeClick(registerLinkLocator)
        loadPage(RegistrationPage)
    }

    ForgotCredentialsPage clickForgotCredentialsLink() {
        exceptionSafeClick(forgotCredentialsLinkLocator)
        loadPage(ForgotCredentialsPage)
    }

}
