package org.nrg.selenium.xnat.page_model.admin

import org.openqa.selenium.By

class AdminUiSelect implements AdminUiElement {

    String selection

    AdminUiSelect(By by, String selection, By screenshot = null, boolean takeScreenshot = true) {
        setLocator(by)
        setSelection(selection)
        setScreenshotLocator(screenshot)
        setTakeScreenshot(takeScreenshot)
    }

    @Override
    void performElementAction() {
        if (locator != null && selection != null) {
            exceptionSafeSelect(locator, selection)
        } else {
            setTakeScreenshot(false)
        }
    }

}
