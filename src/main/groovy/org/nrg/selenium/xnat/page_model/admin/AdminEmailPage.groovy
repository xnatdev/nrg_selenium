package org.nrg.selenium.xnat.page_model.admin

import org.nrg.selenium.xnat.page_model.LoadablePage
import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.xnat.pogo.users.User
import org.openqa.selenium.By

class AdminEmailPage implements NavigablePageObject<AdminEmailPage>, LoadablePage, ScreenshotableComponent<AdminEmailPage> {

    protected By sendEmailButtonLocator = By.id('send-email')
    protected By subjectLocator = By.name('subject')
    protected By messageBodyLocator = By.name('message')

    @Override
    void navigateTo() {
        topNav('Administer', 'Send Email')
    }

    @Override
    List<By> getComponentLocators() {
        [XnatLocators.H3_TAG, sendEmailButtonLocator]
    }

    AdminEmailPage addRecipient(User user) {
        exceptionSafeClick(recipientCheckboxLocator(user))
        this
    }

    AdminEmailPage addCcRecipient(User user) {
        exceptionSafeClick(ccCheckboxLocator(user))
        this
    }

    AdminEmailPage addBccRecipient(User user) {
        exceptionSafeClick(bccCheckboxLocator(user))
        this
    }

    AdminEmailPage fillSubject(String subject) {
        fill(subjectLocator, subject)
        this
    }

    AdminEmailPage fillMessageBody(String body) {
        fill(messageBodyLocator, body)
        this
    }

    AdminEmailPage clickSendEmailButton() {
        exceptionSafeClick(sendEmailButtonLocator)
        this
    }

    AdminEmailPage confirmSendSuccess() {
        readSuccess().assertTextEquals("Message sent.").capture()
        this
    }

    protected By recipientCheckboxLocator(User user) {
        checkboxForLocator(user, 'to')
    }

    protected By ccCheckboxLocator(User user) {
        checkboxForLocator(user, 'cc')
    }

    protected By bccCheckboxLocator(User user) {
        checkboxForLocator(user, 'bcc')
    }

    protected By checkboxForLocator(User user, String emailType) {
        By.xpath("//td[normalize-space(text())='${user.firstName} ${user.lastName}']/..//input[@class='${emailType}' or @class='${emailType} dirty']")
    }

}
