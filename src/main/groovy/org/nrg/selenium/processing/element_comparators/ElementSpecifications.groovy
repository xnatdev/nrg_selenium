package org.nrg.selenium.processing.element_comparators

import groovy.transform.EqualsAndHashCode
import org.nrg.selenium.processing.element_comparators.cache.LocatorCache
import org.nrg.selenium.processing.element_comparators.comparator.ElementComparator

@EqualsAndHashCode
class ElementSpecifications {

    List<ElementComparator> comparators = []
    List<LocatorCache> locatorCaches = []
    Map<String, String> locatorReplacements = [:]

}
