package org.nrg.selenium.xnat.page_model.error

import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.openqa.selenium.By

class GenericTomcatError implements GenericError<GenericTomcatError> {

    @Override
    By getMainErrorLocator() {
        XnatLocators.H1_TAG
    }

}
