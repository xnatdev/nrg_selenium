package org.nrg.selenium.exceptions

class UnknownInterpretableValueException extends RuntimeException {

    UnknownInterpretableValueException(String message) {
        super(message)
    }

}
