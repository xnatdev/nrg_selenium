package org.nrg.selenium.xnat.page_model.project.report

import org.nrg.selenium.xnat.page_model.NavigablePageObject
import org.nrg.selenium.xnat.page_model.ScreenshotableComponent
import org.nrg.selenium.xnat.page_model.general_xnat.XnatLocators
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.users.User
import org.nrg.xnat.pogo.users.UserGroup
import org.openqa.selenium.By
import org.openqa.selenium.support.pagefactory.ByChained

import static org.testng.AssertJUnit.*

class ManageProjectAccessRequestPage implements NavigablePageObject<ManageProjectAccessRequestPage>, ScreenshotableComponent<ManageProjectAccessRequestPage> {

    protected By formLocator = By.name('form1')
    protected By titleLocator = new ByChained(formLocator, XnatLocators.H2_TAG)
    protected By explanationLocator = new ByChained(formLocator, By.xpath('./p[1]'))
    protected By userDetailsLocator = new ByChained(formLocator, By.xpath('./div'))
    protected By approveLocator = By.xpath("//input[@name='eventSubmit_doApprove']")
    protected By denyLocator = By.xpath("//input[@name='eventSubmit_doDenial']")

    @Override
    void performInitialValidation() {
        waitForElement(formLocator)
    }

    @Override
    List<By> getComponentLocators() {
        [formLocator]
    }

    String getTitle() {
        exceptionSafeGetText(titleLocator)
    }

    String getExplanation() {
        exceptionSafeGetText(explanationLocator)
    }

    String getUserDetails() {
        exceptionSafeGetText(userDetailsLocator)
    }

    ProjectAccessTab processRequestedAccess(boolean acceptRequest, User requestingUser, Project parProject, UserGroup requestedAccessLevel) {
        assertEquals("${parProject.title} Access Request", title)
        assertTrue(explanation.contains("The following user has requested ${requestedAccessLevel.singularName().toLowerCase()} access"))
        assertEquals(
                "Username: ${requestingUser.username}\nFirstname: ${requestingUser.firstName}\nLastname: ${requestingUser.lastName}\nEmail: ${requestingUser.email}\nRequested Access Level: ${requestedAccessLevel.singularName().toLowerCase()}",
                userDetails
        )
        capture()
        exceptionSafeClick(acceptRequest ? approveLocator : denyLocator)
        final ProjectAccessTab accessTab = loadPage(ProjectReportPage).loadAccessTab()
        if (acceptRequest) {
            accessTab.assertUserBelongsTo(requestingUser, requestedAccessLevel)
        } else {
            assertFalse(accessTab.userIsRegisteredToProject(requestingUser))
        }
        accessTab
    }


}
