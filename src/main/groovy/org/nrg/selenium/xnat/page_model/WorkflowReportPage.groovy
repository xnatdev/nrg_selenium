package org.nrg.selenium.xnat.page_model

import org.nrg.xnat.pogo.DataType
import org.nrg.xnat.pogo.Workflow
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.ExpectedCondition

import java.time.LocalDateTime

import static org.testng.AssertJUnit.assertEquals

class WorkflowReportPage implements NavigablePageObject<WorkflowReportPage>, ScreenshotableComponent<WorkflowReportPage> {

    protected String idIdentifier = 'ID'
    protected List<String> externalIdIdentifiers = ['ExternalID', 'External ID']
    protected String dataTypeIdentifier = 'Datatype'
    protected String pipelineIdentifier = 'Pipeline'
    protected String statusIdentifier = 'Status'
    protected String launchTimeIdentifier = 'Launch time'
    protected String percentageCompleteIdentifier = 'Percentage complete'

    @Override
    List<By> getComponentLocators() {
        [By.id('layout_content')]
    }

    WorkflowReportPage assertId(String expectedId) {
        assertEquals(expectedId, readId())
        this
    }

    WorkflowReportPage assertExternalId(String expectedId) {
        assertEquals(expectedId, readExternalId())
        this
    }

    WorkflowReportPage assertDatatype(DataType dataType) {
        assertEquals(dataType.xsiType, readDataType().xsiType)
        this
    }

    WorkflowReportPage assertPipeline(String expectedPipeline) {
        assertEquals(expectedPipeline, readPipeline())
        this
    }

    WorkflowReportPage assertStatus(String expectedStatus) {
        assertEquals(expectedStatus, readStatus())
        this
    }

    WorkflowReportPage assertPercentageComplete(double expectedPercent) {
        assertEquals(expectedPercent, readPercentageComplete(), 0.00001)
        this
    }

    String readId() {
        exceptionSafeGetText(locatorFor(idIdentifier))
    }

    String readExternalId() {
        exceptionSafeGetText(locatorFor(externalIdIdentifiers))
    }

    DataType readDataType() {
        DataType.lookup(exceptionSafeGetText(locatorFor(dataTypeIdentifier)))
    }

    String readPipeline() {
        exceptionSafeGetText(locatorFor(pipelineIdentifier))
    }

    String readStatus() {
        exceptionSafeGetText(locatorFor(statusIdentifier))
    }

    LocalDateTime readLaunchTime() {
        LocalDateTime.parse(exceptionSafeGetText(locatorFor(launchTimeIdentifier)))
    }

    WorkflowReportPage waitForWorkflowComplete() {
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver input) {
                if (readStatus() == Workflow.WORKFLOW_COMPLETE) {
                    return true
                } else {
                    navigate().refresh()
                    loadPage(WorkflowReportPage)
                    return false
                }
            }
        })
        this
    }

    double readPercentageComplete() {
        exceptionSafeGetText(locatorFor(percentageCompleteIdentifier)).toDouble()
    }

    // handle some trivial labeling changes across XNAT versions without subclassing
    protected By locatorFor(List<String> possibleIdentifiers) {
        final String query = possibleIdentifiers.collect { identifier ->
            "text()='${identifier}'"
        }.join(' or ')
        By.xpath("//td[${query}]/following-sibling::td")
    }

    protected By locatorFor(String identifier) {
        locatorFor([identifier])
    }

}
